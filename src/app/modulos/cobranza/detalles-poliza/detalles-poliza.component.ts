import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {RegistroService} from '../../../@core/data/services/venta-nueva/registro.service';



@Component({
  selector: 'app-detalles-poliza',
  templateUrl: './detalles-poliza.component.html',
  styleUrls: ['./detalles-poliza.component.scss']
})
export class DetallesPolizaComponent implements OnInit {
  idPoliza: number = this.rutaActiva.snapshot.params.idPoliza;
  detallesPoliza;
  datoCliente: any;
  permisos = JSON.parse(window.localStorage.getItem('User'));


  constructor(
    private detallesPolizaService: RegistroService,
    private rutaActiva: ActivatedRoute,
    private _location: Location
  ) { }

  getById() {
    this.detallesPolizaService.detallesPoliza(this.idPoliza).subscribe(result => {
      this.detallesPoliza = result;
      this.datoCliente = this.detallesPoliza.datos;
    });
  }
  ngOnInit(){
    this.getById();
  }

  cerrar() {
    this._location.back();
  }
}
