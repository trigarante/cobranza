import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Router} from '@angular/router';
import {LinkPagoService} from '../../../@core/data/services/link-pago/link-pago.service';
import {SolicitudesPagoService} from '../../../@core/data/services/link-pago/solicitudes-pago';
import {MatDialog} from '@angular/material/dialog';
import {SolicitudesPago} from '../../../@core/data/interfaces/venta-nueva/solicitudesPago';
import {InfoTarjetaComponent} from '../modals/info-tarjeta/info-tarjeta.component';

@Component({
  selector: 'app-solicitudes-pago',
  templateUrl: './solicitudes-pago.component.html',
  styleUrls: ['./solicitudes-pago.component.scss']
})
export class SolicitudesPagoComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource;
  errorSpin = false;
  displayedColumns: string[] = ['id', 'empleado', 'cliente', 'poliza', 'socio', 'fecha', 'descripcion', 'tarjetaDigital', 'detalle'];
  empleado: string;

  constructor(
    private solPagoService: SolicitudesPagoService,
    private dialog: MatDialog
  ) {
    this.empleado = sessionStorage.getItem('Empleado');
  }

  ngOnInit(): void {
    this.getDatos();
  }

  getDatos() {
    this.dataSource = new MatTableDataSource([]);
    this.solPagoService.getByIdEmpleado(this.empleado).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  infoTarjeta(element: SolicitudesPago){
    console.log(element);
    this.dialog.open(InfoTarjetaComponent, {
      data: {
        tarjeta: element.numeroTarjeta,
        titular: element.titular,
        banco: element.banco,
        fechaPago: element.fechaPago,
        formaPago: element.metodoPago,
        plazoPago: element.tipoPago,
        csv: element.csv,
        mesVencimiento: element.mesVencimiento,
        anioVencimiento: element.anioVencimiento,
        tipoSeguro: element.descripcion,
        tarjetaDigital: element.tarjetaDigital,
      }
    });
  }

}
