import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MarcasOnline} from '../../../../../@core/data/interfaces/cotizador/marcas-online';
import {ModelosOnline} from '../../../../../@core/data/interfaces/cotizador/modelos-online';
import {DescripcionesOnline} from '../../../../../@core/data/interfaces/cotizador/descripciones-online';
import {SubmarcasOnline} from '../../../../../@core/data/interfaces/cotizador/submarcas-online';
import {CatalogoService} from '../../../../../@core/data/services/cotizador/catalogo.service';
import {CotizarRequest} from '../../../../../@core/data/interfaces/cotizador/cotizar';
import {Router} from '@angular/router';
import {ProspectoService} from '../../../../../@core/data/services/venta-nueva/prospecto.service';
import {SolicitudesvnService} from '../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {CotizacionesAliService} from '../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {CotizacionesAli} from '../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';

@Component({
  selector: 'app-modal-cotizacion',
  templateUrl: './modal-cotizacion.component.html',
  styleUrls: ['./modal-cotizacion.component.scss']
})
export class ModalCotizacionComponent implements OnInit {
  formCotizacion: FormGroup;

  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  descuentos = Array.from(Array(51).keys(), n => n);

  marca: string;
  modelo: string;
  submarca: string;
  descripcion: string;
  marcas: MarcasOnline[];
  modelos: ModelosOnline[];
  descripciones: DescripcionesOnline[];
  submarcas: SubmarcasOnline[];

  descuento: string;
  edad: number;
  genero: string;

  clave: number;
  fecha: any;
  anoActual: any;
  anoNacimiento: any;
  cotizacion: CotizarRequest ;
  jsoncotizacion: any;
  cotizacionAli: CotizacionesAli;
  cotizacionRequest: any;

  constructor(public  dialogref: MatDialogRef<ModalCotizacionComponent>, private fb: FormBuilder,
              private catalogoService: CatalogoService, @Inject(MAT_DIALOG_DATA) public data ,
              private router: Router, private prospectoService: ProspectoService, private solictudesVnService: SolicitudesvnService,
              private cotizacionesAliService: CotizacionesAliService) { }

  ngOnInit(): void {
    this.formCotizacion = this.fb.group({
      cotizadores: new FormControl(''),
      aseguradoras: new FormControl(''),
      edad: new FormControl('', Validators.required),
      cp: new FormControl('', Validators.compose([Validators.required])),
      genero: new FormControl('', Validators.required),
      marca: new FormControl('', Validators.required),
      modelos: new FormControl('', Validators.required),
      submarca: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      descuento: new FormControl(''),
    });
    this.getMarcas();
    this.getDatos();
  }

  getMarcas() {
    if (this.data.nombreAseguradora === 'GNP') {
      this.formCotizacion.controls.descuento.setValue(0);
      this.formCotizacion.controls.descuento.disable();
    }
    if (this.data.nombreAseguradora === 'QUALITAS') {
      this.formCotizacion.controls.descuento.setValue(23);
      this.formCotizacion.controls.descuento.disable();
    }

    this.modelo = '';
    this.submarca = '';
    this.descripcion = '';
    this.formCotizacion.controls.modelos.setValue('');
    this.catalogoService.getMarcas(this.data.urlServidor, this.data.urlCatalogo).subscribe(m => {
      this.marcas = m;
    });
  }


    getModelos() {
    this.modelos = [];
    this.submarcas = [];
    this.descripciones = [];


    this.formCotizacion.controls.modelos.setValue('');
    this.formCotizacion.controls.submarca.setValue('');
    this.formCotizacion.controls.descripcion.setValue('');
    this.catalogoService.getModelos(this.data.urlServidor, this.data.urlCatalogo, this.marca).subscribe(m => {
      this.modelos = m;
    });
  }

  getSubmarca() {
    this.submarcas = [];
    this.descripciones = [];
    this.descripcion = '';
    this.formCotizacion.controls.submarca.setValue('');
    this.formCotizacion.controls.descripcion.setValue('');
    this.catalogoService.getSubMarca(this.data.urlServidor, this.data.urlCatalogo, this.marca, this.modelo)
      .subscribe(m => {
        this.submarcas = m;
      });

  }

  getDescripcion() {
    this.descripciones = [];
    this.formCotizacion.controls.descripcion.setValue('');
    this.catalogoService.getDescripcion(this.data.urlServidor, this.data.urlCatalogo,
      this.marca, this.modelo,
      this.submarca).subscribe(m => {
      this.descripciones = m;
    });
  }

  validarCp(value) {
    // const cpParse = parseInt(value);
    // this.sepomexService.getColoniaByCp(cpParse).subscribe(res => {
    //   if (res.length !== 0) {
    //     this.formCotizacion.controls['cp'].setErrors(null);
    //   } else {
    //     this.formCotizacion.controls['cp'].setErrors({'incorrect': true});
    //   }
    // }, error => error);
  }
  cotizar(){
    // se muestra una ventana para seleccionar los datos de cotizacion y se enviara a un modulo donde se mostrara la cotizacion real
    this.clave = this.descripciones.filter(descripcion => descripcion.text ===
      this.formCotizacion.controls.descripcion.value)[0].id,
      this.fecha = new Date();
    this.anoActual = this.fecha.getFullYear();
    this.anoNacimiento = this.anoActual - this.formCotizacion.controls.edad.value;

    this.cotizacion = {
      aseguradora: this.data.nombreAseguradora,
      clave: this.clave.toString(),
      cp: this.formCotizacion.controls.cp.value,
      descripcion: this.formCotizacion.controls.descripcion.value,
      descuento: this.formCotizacion.controls.descuento.value,
      edad: this.formCotizacion.controls.edad.value,
      fechaNacimiento: '01/01/' + this.anoNacimiento,
      genero: this.formCotizacion.controls.genero.value,
      marca: this.formCotizacion.controls.marca.value,
      modelo: this.modelo,
      movimiento: 'cotizacion',
      paquete: 'All',
      servicio: 'PARTICULAR',

    };

    this.jsoncotizacion = JSON.stringify(this.cotizacion);

    if (this.data.tipo === undefined) {
      this.data.tipo = 'interno';
    }
    if (this.data.idTipoModificar === undefined) {
      this.data.idTipoModificar = 1;
    }

    if (this.data.idSolicitudDetalles === undefined) {
      this.data.idSolicitudDetalles = 1;
    }

    this.router.navigate(['/modulos/cobranza/mostrarCotizacion', this.jsoncotizacion, this.data.tipo,
      this.data.idProspecto, this.data.urlServidor, this.data.urlCotizacion, this.data.nombreAseguradora,
      this.formCotizacion.controls.submarca.value, this.data.idTipoModificar, this.data.idSolicitudDetalles] );

    this.dialogref.close();
  }

  getDatos() {
    // poner datos de prospecto a datos cotizacion edad copostal, genero
    // si es recotizacion volver a poner datos de marca auto

    this.prospectoService.getById(this.data.idProspecto).subscribe(
      dataPros => {
        this.formCotizacion.controls.edad.setValue(dataPros.edad);
        if (dataPros.sexo === 'M') {

          this.formCotizacion.controls.genero.setValue('MASCULINO');
        }
        if (dataPros.sexo === 'F') {

          this.formCotizacion.controls.genero.setValue('FEMENINO');
        }

      });
    // si entra a recotizar busca los datos ateriores de cotizado y los vuelve a poner en su control, marca
    // modelo, submarca, descripcion
    if (this.data.idTipoModificar === '2') {
      // console.log('entro :)')
      this.solictudesVnService.getSolicitudesByIdOnline(this.data.idSolicitudDetalles).subscribe(data => {

        this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
          this.cotizacionAli = dataAli;
          this.cotizacionRequest = this.cotizacionAli.peticion;
          this.formCotizacion.controls.marca.setValue(this.cotizacionRequest['marca']);
          this.marca = this.cotizacionRequest['marca'];
          this.catalogoService.getModelos(this.data.urlServidor, this.data.urlCatalogo, this.cotizacionRequest['marca']).subscribe(m => {
            this.modelos = m;
          });
          this.formCotizacion.controls.modelos.setValue(this.cotizacionRequest['modelo']);
          this.modelo = this.cotizacionRequest['modelo'];
          this.catalogoService.getSubMarca(this.data.urlServidor, this.data.urlCatalogo, this.cotizacionRequest['marca'],
            this.cotizacionRequest['modelo'])
            .subscribe(m => {
              this.submarcas = m;
            });
          this.formCotizacion.controls.submarca.setValue(this.cotizacionRequest['subMarca']);
          this.submarca = this.cotizacionRequest['subMarca'];
          this.catalogoService.getDescripcion(this.data.urlServidor, this.data.urlCatalogo,
            this.cotizacionRequest['marca'],
            this.cotizacionRequest['modelo'],
            this.cotizacionRequest['subMarca']).subscribe(m => {
            this.descripciones = m;
          });
          this.formCotizacion.controls.descripcion.setValue(this.cotizacionRequest['descripcion']);
          this.formCotizacion.controls.cp.setValue(this.cotizacionRequest['cp']);
        });
      });
    }
  }

  refrescar() {
    this.formCotizacion.reset();
  }
  dismiss() {
    this.dialogref.close();
  }

}
