import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecotizadorTabsComponent } from './recotizador-tabs.component';

describe('RecotizadorTabsComponent', () => {
  let component: RecotizadorTabsComponent;
  let fixture: ComponentFixture<RecotizadorTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecotizadorTabsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecotizadorTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
