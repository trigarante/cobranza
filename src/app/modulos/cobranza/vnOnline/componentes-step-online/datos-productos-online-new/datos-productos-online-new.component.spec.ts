import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProductosOnlineNewComponent } from './datos-productos-online-new.component';

describe('DatosProductosOnlineNewComponent', () => {
  let component: DatosProductosOnlineNewComponent;
  let fixture: ComponentFixture<DatosProductosOnlineNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosProductosOnlineNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProductosOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
