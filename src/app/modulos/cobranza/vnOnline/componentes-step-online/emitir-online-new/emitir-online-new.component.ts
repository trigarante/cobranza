import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SolicitudesvnService} from '../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {ActivatedRoute} from '@angular/router';
import {CotizacionesAliService} from '../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {CotizacionesAli} from '../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {urlAseguradoras} from '../../../../../@core/data/interfaces/cotizador/urlAseguradoras';
import Swal from 'sweetalert2';
import {CatalogoService} from '../../../../../@core/data/services/cotizador/catalogo.service';
import {LogCotizadorService} from '../../../../../@core/data/services/cotizador/log-cotizador.service';
import {ModalViewPdfPolizaFunnel} from '../../../../../shared/componentes/modal-view-pdf-poliza-funnel/modal-view-pdf-poliza-funnel.component';
import {MatDialog} from '@angular/material/dialog';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {SubRamoService} from '../../../../../@core/data/services/comerciales/sub-ramo.service';
import {SociosService} from '../../../../../@core/data/services/comerciales/socios.service';
import {
  SociosComercial,
  SociosComercialOnline
} from '../../../../../@core/data/interfaces/comerciales/socios-comercial';
import {Ramo} from '../../../../../@core/data/interfaces/comerciales/ramo';
import {SubRamo} from '../../../../../@core/data/interfaces/comerciales/sub-ramo';
import {ProductoNameCorto, ProductoSocios} from '../../../../../@core/data/interfaces/comerciales/producto-socios';
import {RamoService} from '../../../../../@core/data/services/comerciales/ramo.service';
import {ProductoSociosService} from '../../../../../@core/data/services/comerciales/producto-socios.service';
import moment from 'moment';
import {RegistroService} from '../../../../../@core/data/services/venta-nueva/registro.service';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {Periodicidad} from '../../../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad';
import {TipoPagoVn} from '../../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn';
import {PeriodicidadService} from '../../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service';
import {ProductoClienteService} from '../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {ProductoCliente} from '../../../../../@core/data/interfaces/venta-nueva/producto-cliente';

@Component({
  selector: 'app-emitir-online-new',
  templateUrl: './emitir-online-new.component.html',
  styleUrls: ['./emitir-online-new.component.scss']
})
export class EmitirOnlineNewComponent implements OnInit {
  @Input() idProductoCliente: number;
  @Output() idProductoClientesaliente = new EventEmitter<{ idproductoc: number, idRegistro: number}>();
  @Input() idRegistroPoliza: number;

  registroForm: FormGroup = new FormGroup({
    idTipoPago: new FormControl('', Validators.required),
    idSocio: new FormControl('', Validators.required),
    idRamo: new FormControl('', Validators.required),
    idSubRamo: new FormControl('', Validators.required),
    idProductoSocio: new FormControl('', Validators.required),
    poliza: new FormControl('', [Validators.required]),
    oficina: new FormControl(''),
    fechaInicio: new FormControl(new Date()),
    fechaFin: new FormControl(''),
    primaNeta: new FormControl('', [Validators.required, Validators.pattern('[0-9.]+')]),
    primerPago: new FormControl(''),
    periodo: new FormControl('', Validators.required),
  });
  idSolicitud: number;
  cotizacionAli: CotizacionesAli;
  responseEmision: any;
  responseEmisionPoliza: any;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urldocumentos: string;
  urlEmision: string;
  dataArrayEmision: any;
  banderaEmitirMal = false;
  responsedocumentosPDF: any;
  socio: SociosComercial;
  socioOnline: SociosComercialOnline;
  socios: SociosComercial[];
  sociosOnline: SociosComercialOnline[];
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  productoSocioName: ProductoNameCorto;
  x: string;
  filesToUpload: FileList;
  archivoValido = false;
  gruposCargados = false;
  cantidad: number;
  periodicidad: Periodicidad[];
  numeroPagos: number;
  tipoPagos: TipoPagoVn[];
  ocultarboton = true;
  ocultarFormulario = false;
  urlPreventa: any;
  filesToUploadCliente: FileList;
  archivoValidoCliente = false;
  productoCliente: ProductoCliente;
  ocultarBotonCliente = false;
  extensionesDeArchivoAceptadas = ['application/pdf'];

  jsonEmitirQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
  };

  constructor(private solicitudesService: SolicitudesvnService,
              private route: ActivatedRoute,
              private cotizacionesAliService: CotizacionesAliService,
              private router: ActivatedRoute,
              private catalogoService: CatalogoService,
              private logCotizadorService: LogCotizadorService,
              private matDialog: MatDialog,
              private subRamoService: SubRamoService,
              private sociosService: SociosService,
              private ramoService: RamoService,
              private productoSocioService: ProductoSociosService,
              private registroService: RegistroService,
              private notificaciones: NotificacionesService,
              private periodicidadService: PeriodicidadService,
              private notificacionesService: NotificacionesService,
              private productoClienteService: ProductoClienteService, ) {
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urlEmision = this.service.urlEmision;
    this.urldocumentos = this.service.urldocumentos;
  }

  ngOnInit(): void {
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));
    this.emitir(this.idSolicitud);
  }
  getRamoNuevo(idSocio: number, idRamo?: number) {
    this.sociosService.getByIdPaisStep(1).subscribe(data => {
      this.sociosOnline = data;
      this.socioOnline = this.sociosOnline.filter(sociosOnline => sociosOnline.id === idSocio)[0];
      if (!this.socioOnline.expresionRegular) {
        this.registroForm.controls.poliza.setValidators(Validators.required);
      } else {
        if (this.socioOnline.id !== 2 && this.socioOnline.id !== 8) {
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(`${this.socioOnline.expresionRegular}`)]));
        } else {
          const arrExp = this.socioOnline.expresionRegular.split(',');
          const expresionesRegulares =
            this.socioOnline.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(expresionesRegulares)]));
        }
      }

      this.registroForm.controls.poliza.updateValueAndValidity();
      if (idRamo === undefined) {
        this.registroForm.controls.idRamo.setValue('');
        this.registroForm.controls.idSubRamo.setValue('');
        this.registroForm.controls.idProductoSocio.setValue('');
        this.ramos = [];
        this.subRamos = [];
        this.productosSocio = [];
      }

      this.ramoService.getByIdSocio(idSocio).subscribe({
        next: dataR => {
          this.ramos = dataR;
          if (this.ramos.length === 0) {
            this.mensajeErrorCatalogos('ramos');
          }
        }, complete: () => {
          if (idRamo !== undefined) {
            this.registroForm.controls.idRamo.setValue(idRamo);
          }
        }
      });
    });
  }
  getRamo(idSocio: number, idRamo?: number) {
    this.socio = this.socios.filter( socios => socios.id === this.registroForm.controls.idSocio.value)[0];

    if (!this.socio.expresionRegular) {
      this.registroForm.controls.poliza.setValidators(Validators.required);
    } else {
      if (this.socio.id !== 2 && this.socio.id !== 8) {
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
      } else {
        const arrExp = this.socio.expresionRegular.split(',');
        const expresionesRegulares =
          this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(expresionesRegulares)]));
      }
    }

    this.registroForm.controls.poliza.updateValueAndValidity();
    if (idRamo === undefined) {
      this.registroForm.controls.idRamo.setValue('');
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getByIdSocio(idSocio).subscribe( {next: data => {
        this.ramos = data;
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
        if (idRamo !== undefined) {
          this.registroForm.controls.idRamo.setValue(idRamo);
        }
      }});
  }
  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getProductoSocioByIdSubRamo(subRamo).subscribe({
      next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        if (idProductoSocio !== undefined) {
          this.registroForm.controls.idProductoSocio.setValue(idProductoSocio);
        }
      }
    });
  }
  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (idSubRamo === undefined) {
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getSubramosByIdRamo(idRamo).subscribe({
      next: data => {
        this.subRamos = data;
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      }, complete: () => {
        if (idSubRamo !== undefined) {
          this.registroForm.controls.idSubRamo.setValue(idSubRamo);
        }
      }
    });
  }
  getProductoSocioNombre(subRamo: number, nombre: string, idProductoSocio?: number) {

    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }
    this.productoSocioService.getProductoSocioByIdSubRamoByNombre(subRamo, nombre).subscribe({
      next: result => {
        this.productoSocioName = result;
        console.log(this.productoSocioName);
        if (this.productoSocioName === null) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        this.registroForm.controls.idProductoSocio.setValue(this.productoSocioName.id);
      }
    });
  }

  getNumeroPagos(): number {
    if (this.tipoPagos !== undefined && this.registroForm.controls.idTipoPago.valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.registroForm.controls.idTipoPago.value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.registroForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9.]+')], this.setErrorMensual.bind(this)));
      } else {
        if (this.registroForm.contains('pagoMensual')) {
          this.registroForm.removeControl('pagoMensual');
        }
      }
      return this.numeroPagos;
    } else {
      return 0;
    }
  }
  getPeriodicidad() {
    this.periodicidadService.get().subscribe(res => {
      this.periodicidad = res;
    });
  }

  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.registroForm.controls.periodo.setValue('');
  }
  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (Number(this.registroForm.controls.primerPago.value) >= Number(this.registroForm.controls.pagoMensual.value)) {
        this.registroForm.controls.pagoMensual.setErrors(null);
      } else {
        this.registroForm.controls.pagoMensual.setErrors({ 'incorrect': true });
      }
    });
  }

  emitir(idSol) {

    let dataEmision: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;

        switch (this.nombreAseguradora) {
          case 'QUALITAS':

            if (this.responseEmision['codigoError'] === 'Poliza ' + this.responseEmision['emision']['poliza'] + ' '
              + 'ha sido emitida no cobrada.' || this.responseEmision['emision']['poliza'] !== ''){
              console.log('poliza ya emitida no cobrada')
              // por si y emitio pero no descargo la poliza, y sale puede volve a entrar y quedarse en emision y descargar poliza
              // checar futuro con emitir previa
              if (this.idRegistroPoliza === undefined){
                this.obtenerPoliza(idSol);
              }
            }else {
              this.swalCargando2();
              this.responseEmision['aseguradora'];
              // console.log('entra a emision')
              // delete this.responseEmision['emision'];
              // // ejemplo de pago que devuelve
              // this.responseEmision['emision'] = {
              //   "poliza": "",
              //   "derechos": "",
              //   "impuesto": "",
              //   "recargos": "",
              //   "terminal": "",
              //   "documento": "",
              //   "primaNeta": "",
              //   "resultado": false,
              //   "primaTotal": "",
              //   "primerPago": "",
              //   "idCotizacion": "",
              //   "pagosSubsecuentes": ""
              // }
              console.log(this.responseEmision);
              // #region comments
              // servicio real
              this.catalogoService.emitir(this.urlService, this.urlEmision, this.responseEmision).subscribe(
                (data) => {
                  dataEmision = data;

                },
                (error) => {
                  console.error('Error => ', error);
                  // para gnp si sale error
                  this.dataArrayEmision = error.error;
                  console.log(this.dataArrayEmision);

                  this.jsonEmitirQualitas.request = this.responseEmision;

                  this.jsonEmitirQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonEmitirQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonEmitirQualitas.response = this.dataArrayEmision;
                  this.jsonEmitirQualitas.error = JSON.parse(this.dataArrayEmision['codigoError']);
                  this.jsonEmitirQualitas.poliza = this.dataArrayEmision['emision']['poliza'];
                  this.logCotizadorService.qualitasemitir(this.jsonEmitirQualitas).subscribe(log => {
                  });
                  delete this.responseEmision['emision'];

                  this.responseEmision['emision'] = this.dataArrayEmision['emision'];
                  this.responseEmision['codigoError'] = this.dataArrayEmision['codigoError'];
                  this.responseEmision['urlRedireccion'] = this.dataArrayEmision['urlRedireccion'];
                  this.responseEmision['periodicidadDePago'] = this.dataArrayEmision['periodicidadDePago'];
                  console.log(this.responseEmision['emision']['poliza'])
                  this.cotizacionAli['respuesta'] = this.responseEmision;
                  this.cotizacionesAliService.put(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                    },
                    errorb => {
                    },
                    () => {
                    });
                },
                () => {
                  this.dataArrayEmision = dataEmision;
                  console.log(this.dataArrayEmision);

                  this.jsonEmitirQualitas.request = this.responseEmision;

                  this.jsonEmitirQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonEmitirQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonEmitirQualitas.response = this.dataArrayEmision;
                  this.jsonEmitirQualitas.error = this.dataArrayEmision['codigoError'];
                  this.jsonEmitirQualitas.poliza = this.dataArrayEmision['emision']['poliza'];
                  this.logCotizadorService.qualitasemitir(this.jsonEmitirQualitas).subscribe(log => {
                  });

                  delete this.responseEmision['emision'];

                  this.responseEmision['emision'] = this.dataArrayEmision['emision'];
                  this.responseEmision['codigoError'] = this.dataArrayEmision['codigoError'];
                  this.responseEmision['urlRedireccion'] = this.dataArrayEmision['urlRedireccion'];
                  this.responseEmision['periodicidadDePago'] = this.dataArrayEmision['periodicidadDePago'];
                  console.log(this.responseEmision['emision']['poliza'])
                  this.cotizacionAli['respuesta'] = this.responseEmision;
                  this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                    },
                    error => {
                    },
                    () => {
                      if (this.responseEmision['codigoError'] === 'Poliza ' + this.responseEmision['emision']['poliza'] + ' '
                        + 'ha sido emitida no cobrada.') {
                        this.banderaEmitirMal = false;
                        // despues de obtener bien la poliza en qualitas se prodece a visualizar la poliza y descargarla
                        this.obtenerPoliza(this.idSolicitud);
                        // this.swalCargando3();
                        console.log('entro emitio bien')
                      } else {
                        this.swalCargando4();
                        // this.emisionMal.emit();
                        this.banderaEmitirMal =  true;
                        // this.emisionMal.emit();
                        console.log('no emitida')
                      }
                    });
                }); // fin de servicio

              // #endregion comments

            }
            break;
          case 'GNP0' :
            // this.swalCargando2();
            // this.responseEmision['aseguradora'];
            // // console.log('entra a emision')
            // // delete this.responseEmision['emision'];
            // // // ejemplo de pago que devuelve
            // // this.responseEmision['emision'] = {
            // //   "poliza": "",
            // //   "derechos": "",
            // //   "impuesto": "",
            // //   "recargos": "",
            // //   "terminal": "",
            // //   "documento": "",
            // //   "primaNeta": "",
            // //   "resultado": false,
            // //   "primaTotal": "",
            // //   "primerPago": "",
            // //   "idCotizacion": "",
            // //   "pagosSubsecuentes": ""
            // // }
            // console.log(this.responseEmision);
            //
            // // servicio real
            // this.catalogoService.emitir(this.urlService, this.urlEmision, this.responseEmision).subscribe(
            //   (data) => {
            //     dataEmision = data;
            //
            //   },
            //   (error) => {
            //     console.error('Error => ', error);
            //     // para gnp si sale error
            //     this.dataArrayEmision = error.error;
            //     console.log(this.dataArrayEmision);
            //     this.jsonEmitirGnp.request = this.responseEmision;
            //     delete this.responseEmision;
            //
            //     this.responseEmision = this.dataArrayEmision;
            //     // json emitir
            //
            //     this.jsonEmitirGnp.idEmpleado = sessionStorage.getItem('Empleado');
            //     this.jsonEmitirGnp.idSolicitudVn =  this.idSolicitud;
            //     this.jsonEmitirGnp.response = this.responseEmision;
            //     this.jsonEmitirGnp.error = JSON.parse(this.responseEmision['codigoError']);
            //
            //     this.logCotizadorService.gnpemitir(this.jsonEmitirGnp).subscribe(log => {
            //     });
            //     this.cotizacionAli['respuesta'] = this.responseEmision;
            //     this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
            //       },
            //       errorb => {
            //       },
            //       () => {
            //       });
            //   },
            //   () => {
            //     this.dataArrayEmision = dataEmision;
            //     console.log(this.dataArrayEmision);
            //     // envia la peticion de emision antes
            //     this.jsonEmitirGnp.request = this.responseEmision;
            //
            //
            //     delete this.responseEmision;
            //
            //     this.responseEmision = this.dataArrayEmision;
            //     this.cotizacionAli['respuesta'] = this.responseEmision;
            //     // pone los datos de response al log emitir
            //     this.jsonEmitirGnp.idEmpleado =  sessionStorage.getItem('Empleado');
            //     this.jsonEmitirGnp.idSolicitudVn =  this.idSolicitud;
            //     this.jsonEmitirGnp.response = this.responseEmision;
            //     this.jsonEmitirGnp.error = JSON.parse(this.responseEmision['codigoError']);
            //
            //     this.logCotizadorService.gnpemitir(this.jsonEmitirGnp).subscribe(log => {
            //     });
            //     this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
            //       },
            //       error => {
            //       },
            //       () => {
            //         if (this.responseEmision['codigoError'] === '') {
            //           this.banderaEmitirMal = false;
            //           // this.swalCargando3();
            //           console.log('entro')
            //         } else if (this.responseEmision['codigoError'] !== '' && this.responseEmision['emision']['resultado'] === false) {
            //           this.swalCargando4();
            //           this.banderaEmitirMal =  false;
            //           // this.emisionMal.emit();
            //           console.log('no emitida')
            //         }
            //       });
            //   }); // fin de servicio
            // break;
          default:

            break;
        }
      });
    });

  }

  swalCargando2() {
    Swal.fire({
      title: 'Se está emitiendo, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
  }
  swalCargando4() {
    Swal.fire({
      title: 'Se obtuvo un erro al amitir',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 6000);


  }
// obtener la poliza para poder visualizarla y descargarla
  obtenerPoliza(idSolicitud) {
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(data => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
          this.cotizacionAli = dataAli;
          console.log(this.cotizacionAli);
          this.responseEmisionPoliza = this.cotizacionAli.respuesta;

          this.registroForm.controls.poliza.setValue(this.responseEmisionPoliza['emision']['poliza']);
          this.registroForm.controls.poliza.disable();
          this.registroForm.controls.idTipoPago.setValue(1);
          this.registroForm.controls.idTipoPago.disable();
          this.registroForm.controls.primaNeta.setValue(this.responseEmision['emision']['primaNeta']);
          this.registroForm.controls.primaNeta.disable();

          this.subRamoService.getSubramoById(this.cotizacionAli.idSubRamo).subscribe(datasubramo => {
            // console.log(datasubramo);
            this.registroForm.controls.idSocio.setValue(datasubramo.idSocio);
            this.getRamoNuevo(datasubramo.idSocio, datasubramo.idRamo);
            this.getSubRamo(datasubramo.idRamo, datasubramo.id);
            this.x = this.responseEmision['paquete'];
            this.x = this.x.toUpperCase();
            this.getProductoSocioNombre(datasubramo.id, this.x, datasubramo.idSocio);
            this.registroForm.controls.idSocio.disable();
            this.registroForm.controls.idRamo.disable();
            this.registroForm.controls.idSubRamo.disable();
            this.registroForm.controls.idProductoSocio.disable();

          });
          this.registroForm.controls.periodo.setValue(2);
          this.registroForm.controls.periodo.disable();

          this.registroForm.controls.fechaInicio.disable();
        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {



          switch (this.nombreAseguradora) {
            case 'QUALITAS':

              let datadoc: any;
              this.catalogoService.pdf(this.urlService, this.urldocumentos, this.responseEmisionPoliza['emision']['poliza']).subscribe(
                (datad) => {
                  datadoc = datad;
                  console.log(datadoc)

                },
                (error) => {
                  console.error('Error => ', error);
                },
                () => {
                  this.responsedocumentosPDF = datadoc;
                  // console.log(this.responsedocumentosPDF);
                  this.openDialog();
                });

              break;
            case 'GNP0':

              break;
            default:

              break;
          }

        });
    });
  }
  // abre el dialog para poder visualizar la poliza
  private openDialog() {
    console.log('ID Solicitud Dialog => ', this.idSolicitud);

    const arrayInfoDownloadedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
    const downloadedPDF = arrayInfoDownloadedPDF.find(e => e.solicitudID === this.idSolicitud).downloaded;

    console.log(`¿El ID Póliza ${this.idSolicitud} ya descargó el PDF? => ${downloadedPDF}`);
    this.ocultarBotonCliente = true;
    if (downloadedPDF === false) {
      this.matDialog.open(ModalViewPdfPolizaFunnel, {
        width: '900px',
        height: '670px',
        disableClose: false,
        data: {
          idsolicitud: this.idSolicitud,
          documentos: this.getDocuments()
        }
      }).afterClosed().subscribe(() => {
        // this.idProductoClientesaliente.emit(this.idProductoCliente);
        return;
      });
    }
  }
  getDocuments() {
    // simulacion
    // const arrayDocuments = this.response['Emision'].Documento.split('|');
    // real

    const arrayDocuments = this.responsedocumentosPDF.split('|');

    console.log('Array Documentos => ', arrayDocuments);
    return arrayDocuments;

  }

  mensajeErrorCatalogos(catalogo: string) {
    Swal.fire({
      title: 'Sin elementos disponibles',
      text: '¡Por favor agregue ' + catalogo + ' en el catálogo correspondiente para poder continuar!',
    });
  }
  // Método para obtener el pdf desde la url y convertirlo a base64 y guardar ese pdf el registro
  async dataUrlToBlob() {
    Swal.fire({
      title: 'Comenzando proceso de registro, por favor espere',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    // simulacion
    // const documentsArray = this.getDocuments();
    //  const policie = documentsArray[documentsArray.length - 1];
    // real
    console.log(this.responseEmision['aseguradora'].toLowerCase());
    console.log(this.registroForm.controls.poliza.value);
    const policie = this.findPolicieUrl( this.responseEmision['aseguradora'].toLowerCase() , this.registroForm.controls.poliza.value);
    // Se hace un request para indicar que se va a obtener un blob de la url
    const request = new XMLHttpRequest();
    console.log(request);
    request.open('GET', policie, true);
    console.log(request);
    request.responseType = 'blob';
    console.log(request);
    let blob: Blob;
    // se implementa una funcion que cuando carga se creará un nuevo objeto de un FileReader
    // tslint:disable-next-line:only-arrow-functions
    request.onload = function() {
      const reader = new FileReader();
      console.log(request.response);
      reader.readAsDataURL(request.response);
      reader.onload = function (e: any) {
        // Se asigna el string de base64 a una constante para usarlo mas adelante
        const urlInBase64 = e.target.result;
        console.log(e.target.result);
        // Se obtiene el base64 sin especificar el tipo de aplicacion o mimeType
        const byteString = atob(urlInBase64.split(",")[1]);
        // Se obtiene el tipo de aplicacion o el mimeType
        const mimeString = urlInBase64.split(',')[0].split(':')[1].split(';')[0];
        // Se crear un arraybuffer del tamaño del base64
        const ab = new ArrayBuffer(byteString.length);
        // Se convierte a un int
        const ia = new Uint8Array(ab);
        // El base64 ese convierte a un char y se pasa en cada posicion del arreglo
        for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
        // a partir del arreglo al que se asigno el base64 se crea un blob nuevo para obtener el archivo pdf
        blob = new Blob([ia], {
          type: mimeString
        });
        console.log(blob)
      }
    };
    request.send();
    setTimeout(() => {
      // Se llama a la funcion para que se asigne al FileList el arreglo simulado como FileList
      // Se envian como parametros el blob y el nombre del archivo (el cual no tendra nignu efecto)
      console.log(blob)
      this.filesToUpload = this.blobToFileList(blob, 'Ejemplo');
      // this.subirADrive('poliza', this.idRegistroPoliza);
      // Se llama a la funcion de almacenar alrchivo para subirlo automaticamente sin seleccionar el pdf desde el equipo
      this.almacenarArchivo();
    }, 5000)
  }

  // Metodo para agregar el blob al array simular ser un FileList
  private blobToFileList(blobResult: Blob, fileName: string) {
    // Se crea un arreglo de tipo file el cual se asignara el archivo blob
    console.log(blobResult)
    const fileArray: File[] = [];
    // Se crea un nuevo blob y se especifica el mimeType
    const blobObject = new Blob([blobResult], {
      type: 'application/pdf'
    });
    // Se crea un File a partir del blob generado, se envia el nombre del archivo y el mimeType
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El file generado, se agrega al array
    fileArray.push(file);
    // Se retorna el array simulando que es un filelist
    return fileArray as unknown as FileList;
  }

  // Método para buscar la url que contiene el pdf
  findPolicieUrl(aseguradora: string, policieNumber: string) {
    // Arreglo el cual traera consigo los documentos del servicio y el que se usará para buscar la URL
    // nuevo servicio
    const documentsArray = this.responsedocumentosPDF.split('|');
    // const documentsArray = this.responseEmision['emision']['documento'].split('|');
    // Arreglo que almacenará todas las url
    // const documentsArray = "http://qbcenter.qualitas.com.mx/poliza/c7160186330.pdf|" +
    //   "http://qbcenter.qualitas.com.mx/poliza/r7160186330.pdf|" +
    //   "http://qbcenter.qualitas.com.mx/poliza/p7160186330.pdf".split('|');
    // En caso de que las aseguradoras tengan prefijos diferentes con los cuales identificar el pdf de la poliza
    // se añadirá el prefijo y la aseguradora en este array

    switch (this.nombreAseguradora) {
      case 'QUALITAS':

        const prefixArray = [
          { aseguradora: 'qualitas', prefix: 'p' }
        ];
        // Se encuentra el pregfijo y se almacena en la variable
        const prefixFound = prefixArray.find(e => e.aseguradora === aseguradora).prefix;
        // Para qualitas se está usando esta varible donde se completa el prefijo concatenando el prefijo con el número de la poliza
        const completePrefix = `${prefixFound}${policieNumber}`;
        // Variable donde se almacenará la url que tenga el prefijo espcificado
        let urlPolicie: string;
        // Ciclo con el cual se hace la comparación para verificar cual url del arreglo es la que contiene el prefijo
        for (let i = 0; i < documentsArray.length; i++) {
          // Se compara si el array en la posición de i incluye el prefijo y asignarlo a la variable
          if (documentsArray[i].includes(completePrefix)) {
            urlPolicie = documentsArray[i];
          }
        }
        console.log('URL Policie Contain => ', urlPolicie);
        return urlPolicie;
        break;
      case 'GNP0' :

        // // Variable donde se almacenará la url que tenga el prefijo espcificado
        // let urlPolicie1: string;
        // urlPolicie1 = documentsArray[0];
        // console.log('URL Policie Contain => ', urlPolicie1);
        // return urlPolicie1;
        break;
      default:

        break;
    }

  }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    let extensionValida;
    let tamanioArchivo;
    this.archivoValido = false;
    // this.filesToUpload = (<HTMLInputElement>event.target).files;
    console.log('Almacenar Archivo => ', this.filesToUpload);
    if (this.filesToUpload.length !== 0) {
      if (!this.filesToUpload.item) {
        extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload[0].type);
        tamanioArchivo = this.filesToUpload[0].size * .000001;
      } else {
        extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
        tamanioArchivo = this.filesToUpload.item(0).size * .000001;
      }
      if (this.filesToUpload.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          Swal.fire({
            title: 'Archivos listos',
            text: '¡El archivo está listo para ser guardado!',
          });
          this.archivoValido = true;
          this.crearRegistroEnUno();
        } else {
          let titulo: string;
          let texto: string;
          if (!extensionValida) {
            titulo = 'Extensión no soportada';
            texto = 'Solo puedes subir archivos pdf';
          } else {
            titulo = 'Archivo demasiado grande';
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          Swal.fire({
            title: titulo,
            text: texto,
          });
        }
      }
    }
  }
   // se guarda registro y recibo en la base de datos
  crearRegistroEnUno() {
    console.log(this.idProductoCliente);
    const poliza = this.registroForm.controls.poliza.value;


    this.registroService.getPolizaExistente(poliza, this.registroForm.controls.idSocio.value,
      this.registroForm.controls.fechaInicio.value.toISOString().split('T')[0]).subscribe({
      next: value => {
        const advertencia = `
          <p>La póliza <strong>${poliza}</strong> ya ha sido registrada. </p>
        `;
        if (value.length > 0) {
          Swal.fire({
            title: '¡Advertencia!',
            html: advertencia,
          });
        } else {
          const fecha = moment(this.registroForm.controls.fechaInicio.value).format('YYYY-MM-DD');
          let idRegistroPoliza: number;
          this.gruposCargados = false;
          if (this.registroForm.controls.idTipoPago.value === 1) {
            this.cantidad = 1;
          } else {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls['periodo'].value).id === 1) {
              this.cantidad = this.numeroPagos * .5;
            } else {
              this.cantidad = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls['periodo'].value).cantidadPagos
              );
            }
          }
          const datosRegistro: any = {
            idProducto: this.idProductoCliente,
            idEmpleado: sessionStorage.getItem('Empleado'),
            archivo: '',
            idEstadoPoliza: 1,
            idTipoPago: this.registroForm.controls.idTipoPago.value,
            idProductoSocio: this.registroForm.controls.idProductoSocio.value,
            idFlujoPoliza: 1,
            idSocio: this.registroForm.controls.idSocio.value,
            poliza: this.registroForm.controls.poliza.value,
            oficina: this.registroForm.controls.oficina.value,
            fechaInicio: this.registroForm.controls.fechaInicio.value,
            primaNeta: this.registroForm.controls.primaNeta.value,
            idDepartamento: +sessionStorage.getItem('idDepartamento'),
            idPeriodicidad: this.registroForm.controls.periodo.value,
            online: 0,
          };
          let textoPrimerAviso = ''
          // let textoPrimerAviso =
          //   '<style>#swal2-content {' +
          //   '  text-align: left;' +
          //   '  margin-left: 40px;' +
          //   '}</style>' +
          //   '<strong>Tipo de pago: </strong>' +
          //   this.tipoPagos.find(val => val.id === datosRegistro.idTipoPago).tipoPago +
          //   '</br>' +
          //   // '<strong>Número de mensualidades: </strong> <a style="color: red; font-weight: bold">' +
          //   // this.cantidad +
          //   // '</a></br>' +
          //   '<strong>Socio: </strong>' +
          //   this.socios.find(r => r.id === this.registroForm.controls.idSocio.value).nombreComercial +
          //   '</br>' +
          //   '<strong>Ramo: </strong>' +
          //   this.ramos.find(r => r.id === this.registroForm.controls.idRamo.value).tipoRamo +
          //   '</br>' +
          //   '<strong>Subramo: </strong>' +
          //   this.subRamos.find(r => r.id === this.registroForm.controls.idSubRamo.value).tipoSubRamo +
          //   '</br>' +
          //   '<strong>Producto socio: </strong>' +
          //   this.productosSocio.find(r => r.id === this.registroForm.controls.idProductoSocio.value).tipoProducto +
          //   '</br>' +
          //   '<strong>Inicio de vigencia: </strong>' +
          //   fecha +
          //   '</br>' +
          //   // '<strong>Oficina: </strong>' +
          //   // this.registroForm.controls['oficina'].value +
          //   // '</br>' +
          //   '<strong>Poliza: </strong>' +
          //   this.registroForm.controls.poliza.value +
          //   '</br>' +
          //   '<strong>Prima neta: </strong> $' +
          //   this.registroForm.controls.primaNeta.value +
          //   '</br>';
          // // '<strong>Prima neta del primer pago: </strong> $' +
          // // this.registroForm.controls['primerPago'].value +
          // // '</br>'
          if (this.getNumeroPagos() > 1) {
            textoPrimerAviso +=
              '<strong>Prima neta de las mensualidades: </strong> $' +
              this.registroForm.controls.pagoMensual.value +
              '</br>';

          }
          let cantidadAPagar: number = this.registroForm.controls.primerPago.value;
          let cantidadAPagarprima: number = this.registroForm.controls.primaNeta.value;
          const recibos: any[] = [];
          let fechaVigencia = moment(this.registroForm.controls.fechaInicio.value);
          // let fechaVigencia = moment(this.registroForm.controls['fechaInicio'].value).format('YYYY-MM-DD');
          // Swal.fire({
          //   title: 'Generando registro',
          //   allowOutsideClick: false,
          //   onBeforeOpen: () => {
          //     Swal.showLoading();
          //   },
          // });
          if (this.registroForm.controls.idTipoPago.value !== 1) {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).id === 1) {
              this.numeroPagos = this.numeroPagos * .5;
              this.cantidad = this.numeroPagos;
            } else {
              this.numeroPagos = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).cantidadPagos);
              this.cantidad = this.numeroPagos;
            }
          } else {
            this.numeroPagos = 1;
          }
          for (let i = 0; i < this.numeroPagos; i++) {
            if (i > 0) {
              switch (this.registroForm.controls.idTipoPago.value) {
                case 2:
                  fechaVigencia = moment(fechaVigencia).add(1, 'months');
                  break;
                case 3:
                  fechaVigencia = moment(fechaVigencia).add(3, 'months');
                  break;
                case 4:
                  fechaVigencia = moment(fechaVigencia).add(4, 'months');
                  break;
                case 6:
                  fechaVigencia = moment(fechaVigencia).add(2, 'months');
                  break;
                case 5:
                  fechaVigencia = moment(fechaVigencia).add(6, 'months');
                  break;
              }
              cantidadAPagar = this.registroForm.controls.pagoMensual.value;
              cantidadAPagarprima = this.registroForm.controls.primaNeta.value;
              // fechaVigencia = moment(fechaVigencia).add(1, 'months');
            }
            if (this.registroForm.controls.idTipoPago.value === 1) {
              recibos.push({
                idEstadoRecibos: 2,
                idEmpleado: sessionStorage.getItem('Empleado'),
                numero: i + 1,
                cantidad: cantidadAPagarprima,
                fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
              });
            } else {
              if (this.registroForm.controls.periodo.value === 1 && this.registroForm.controls.idTipoPago.value === 5) {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00'),
                });
              } else {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
                });
              }
            }
          }
          this.gruposCargados = true;
          // this.registroEnUno = {
          //   registro: datosRegistro,
          //   recibos: recibos,
          // };
          this.registroService.postInOneOnline({...datosRegistro, recibos},
            this.filesToUpload, this.route.snapshot.paramMap.get('id_solicitud')).subscribe(
            result => {
              this.notificaciones.exito('Se guardo la póliza');
              this.idRegistroPoliza = result.idRegistro;
              // this.idProductoClientesaliente.emit(this.idProductoCliente);
              this.idProductoClientesaliente.emit({idproductoc: this.idProductoCliente, idRegistro: this.idRegistroPoliza});
              console.log('entro bien a pago :)');
            },
            () => {
              this.notificaciones.error('Hubo un error al registrar los datos');
            },
          );
        }
      },
      error: () => {
        Swal.fire({
          title: '¡Algo salió mal!',

        });
      },
    });
  }
  /** Guardar archivo **/
  almacenarArchivoCliente() {
    this.filesToUploadCliente = ( <HTMLInputElement>event.target).files;
    const extensionValida = this.extensionesDeArchivoAceptadas.includes(this.filesToUploadCliente.item(0).type);
    const tamanioArchivo = this.filesToUploadCliente.item(0).size * .000001;

    if (this.filesToUploadCliente.length === 1) {
      // El servicio en Spring solo acepta como máximo 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        Swal.fire({
          title: 'Archivos listos',
          text: '¡Tu archivo está listo para ser guardado!',
        });
        this.archivoValidoCliente = true;
        if (this.archivoValidoCliente) {
          this.subirADriveCliente('cliente');
          this.ocultarboton = false;
        }
      } else {
        let titulo: string;
        let texto: string;

        if (!extensionValida) {
          titulo = 'Extensión no soportada';
          texto = 'Solo puedes subir archivos pdf';
        } else {
          titulo = 'Archivo demasiado grande';
          texto = 'Los archivos que subas deben pesar menos de 7 MB';
        }

        Swal.fire({
          title: titulo,
          text: texto,
        });
      }
    }
  }
  subirADriveCliente(tipoArchivo: string) {
    let idFolder: string;
    Swal.fire({
      title: 'Se están subiendo los archivos',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    console.log(this.idProductoCliente)
    console.log(this.filesToUploadCliente)

    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe({
      next: dataproducto => {
        this.productoCliente = dataproducto;
        this.productoClienteService.putInOneOnline(this.idProductoCliente, this.productoCliente, this.filesToUploadCliente).subscribe({
          next: dataC => {
            this.notificacionesService.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
        });
      },
      error: () => {
        this.notificacionesService.error();
      },
    });

  }
}
