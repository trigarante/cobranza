import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocitudesActivacionComponent } from './socitudes-activacion.component';

describe('SocitudesActivacionComponent', () => {
  let component: SocitudesActivacionComponent;
  let fixture: ComponentFixture<SocitudesActivacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocitudesActivacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocitudesActivacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
