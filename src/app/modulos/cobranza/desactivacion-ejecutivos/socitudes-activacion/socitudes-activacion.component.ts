import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {
  SolicitudesCarruselService
} from '../../../../@core/data/services/activaciones-empleado/solicitudes-carrusel.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-socitudes-activacion',
  templateUrl: './socitudes-activacion.component.html',
  styleUrls: ['./socitudes-activacion.component.scss']
})
export class SocitudesActivacionComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource();
  cols = ['ejecutivo', 'supervisor', 'fecha', 'ingreso', 'sede', 'departamento', 'puesto', 'estado', 'contactos', 'motivoSolicitud', 'motivo', 'acciones'];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  constructor(private empleadosActivosService: EmpleadosService,
              private notificacionesService: NotificacionesService,
              private solicitudesCarrusel: SolicitudesCarruselService
  ) {
  }

  ngOnInit() {
    this.getSolicitudes();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Con esta función se obtienen todos los empleados existentes en el servidor. */
  getSolicitudes() {
    this.dataSource.data = [];
    this.solicitudesCarrusel.getByIdEstado(1).subscribe({
      next: data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  updateSolicitud(cancelarSolicitud: boolean, infoEmpleado) {
    Swal.fire({
      icon: 'question',
      title: '¿Estás seguro que deseas ' + (cancelarSolicitud ? 'CANCELAR' : 'PROCEDER con') + ' la solicitud del empleado: ' +
        infoEmpleado.ejecutivo + '?',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Continuar',
      reverseButtons: true,
    }).then(value => {
      if (value.value) {
        this.notificacionesService.carga('Actualizando la solicitud');
        this.solicitudesCarrusel.update({
          id: infoEmpleado.id,
          idEstadoSolicitudCarrusel: cancelarSolicitud ? 4 : 2,
          idEmpleadoCoordinador: +sessionStorage.getItem('Empleado'),
        }).subscribe({
          complete: () => this.notificacionesService.exito('Solicitud actualizada').then(() => {
            this.dataSource.data = [];
            this.getSolicitudes();
          }),
        });
      }
    });
  }
}
