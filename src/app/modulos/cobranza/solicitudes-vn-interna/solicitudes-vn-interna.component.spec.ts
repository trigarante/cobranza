import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesVnInternaComponent } from './solicitudes-vn-interna.component';

describe('SolicitudesVnInternaComponent', () => {
  let component: SolicitudesVnInternaComponent;
  let fixture: ComponentFixture<SolicitudesVnInternaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudesVnInternaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesVnInternaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
