import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {CotizacionesAli} from '../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {AgendaLlamadasService} from '../../../@core/data/services/telefonia/agendaLlamadas/agenda-llamadas.service';
import {ReasignarSolicitudComponent} from '../modals/reasignar-solicitud/reasignar-solicitud.component';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {interval, Subscription} from 'rxjs';
import {StepsCotizador} from '../../../@core/data/interfaces/venta-nueva/steps-cotizador';
import {TooltipPosition} from '@angular/material/tooltip';
import {DatosSolicitudComponent} from '../modals/datos-solicitud/datos-solicitud.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {FormControl} from '@angular/forms';
import {Solicitudes} from '../../../@core/data/interfaces/venta-nueva/solicitudes';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SesionUsuarioService} from '../../../@core/data/services/sesiones/sesion-usuario.service';
import {StepsCotizadorService} from '../../../@core/data/services/venta-nueva/steps-cotizador.service';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';
import {CotizacionesAliService} from '../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {EmpleadosService} from '../../../@core/data/services/rrhh/empleados.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {LlamadaSalidaComponent} from '../../telefonia/modals/llamada-salida/llamada-salida.component';
import {AgregarNumeroComponent} from '../../telefonia/modals/agregar-numero/agregar-numero.component';
import {ChatWhatsappComponent} from '../modals/chat-whatsapp/chat-whatsapp.component';
import {WhatsMsgService} from '../../../@core/data/services/whatspp/whats-msg.service';

@Component({
  selector: 'app-solicitudes-vn',
  templateUrl: './solicitudes-vn.component.html',
  styleUrls: ['./solicitudes-vn.component.scss']
})
export class SolicitudesVnComponent implements OnInit, OnDestroy {
  // -----variables tabla---
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  dataSource;
  displayedColumns: string[] = ['detalle', 'id', 'nombre', 'nombreProspecto', 'numeroProspecto', 'estado', 'departamento',
    'fechaSolicitud', 'acciones'];
  filterSelectObj = [];
  permisosS = JSON.parse(window.localStorage.getItem('User'));
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  max: any = new Date();
  errorSpin: boolean = false;
  // -----fin variables tabla---

  @ViewChild('tablaInboundPaginator', {read: MatPaginator}) tablaInboundPaginator: MatPaginator; //
  @ViewChild('btnOptions') btnOptions: ElementRef;
  @ViewChild('addStyle') addStyle: ElementRef;
  @ViewChild('addStyleMM') addStyleMM: ElementRef;
  @ViewChild('mMenu') mMenu: ElementRef;
  @ViewChild('tablaInboundSort', {read: MatSort}) tablaInboundSort: MatSort; //
  extension = localStorage.getItem('extension');
  /**Tooltip**/
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);
  mensajes = new FormControl('');
  // sockets
  private stompClient = null;
  // Esta variable define el estado de las solicitudes de outbound
  tipo: string;
  tipopermiso: string;
  tituloComponent: string;
  permisos: any;
  activo: boolean;
  crearSolicitud: boolean;
  exportarExcel: boolean;
  // VALIDACIÓN ASINCRONA
  btnSendingDates: boolean = false;
  sendingDatesCounter: number = 0;
  // Para la optimización de vn
  // Puesto Empleado
  puestoEmpleado: number;
  mandarPlantilla = false;
  datosCanalMensajes: any;
  solicitudesPorTelefono: any;
  identificador: number;
  notificacion: number;
  auxIdSolicitud;
  socketsWhatsappService: any;
  solicitudesConMensajes: Array<number> = [];
  solicitudConMensajes;
  index;
  inde;
  aux = [];
  i;
  dataMensajesCanal: any[];
  cantidadMensajesPorCanal: any;
  cantidadNotificaciones: any;
  cantidadNotificacion: number;
  efect: number;
  private estadoCambiadoSubs: Subscription;
  estadoCambiado: Subscription;
  cambioDisponible: Subscription;
  Events: Subscription;
  ocultar: number;
  idSesion = Number(localStorage.getItem('sesion'));
  isinterno = false;
  routeListener: Subscription;
  plantillaEnviada: boolean;
  private mensajesWhats = interval(3000);
  observableWhats: Subscription;
  idPuesto = +sessionStorage.getItem('idPuesto');
  socketServiceSubscription: Subscription;
  cargando = true;
  numeros: any[];
  puedeLlamar: boolean;
  pjsipEvents: Subscription;
  cotizacionAli: CotizacionesAli;
  peticiones: any;
  idEstado;


  constructor(
    private actualRoute: ActivatedRoute,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router,
    private render: Renderer2,
    private notificaciones: NotificacionesService,
    private empleadoService: EmpleadosService,
    private pjsip: WebrtcService,
    private sesionUsuarioService: SesionUsuarioService,
    private agendaService: AgendaLlamadasService,
    private stepsService: StepsCotizadorService,
    private solictudesVnService: SolicitudesvnService,
    private cotizacionesAliService: CotizacionesAliService,
    private whatsMsgService: WhatsMsgService,
    private notificacionesService: NotificacionesService,
  ) {

    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }

  // ====================================================
  // =========== metodos tabla===========
  // ====================================================

  ngOnDestroy() {

    if (this.estadoCambiado) {
      this.estadoCambiado.unsubscribe();
    }
    if (this.cambioDisponible) {
      this.cambioDisponible.unsubscribe();
    }
    if (this.Events) {
      this.Events.unsubscribe();
    }
    if (this.pjsipEvents) {
      this.pjsipEvents.unsubscribe();
    }
  }

  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.dataSource.filter = JSON.stringify(this.filterValues);
  }

  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.dataSource.filter = '';
  }
  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  getEmpleado() {
    this.empleadoService.getEmpleadoById(sessionStorage.getItem('Empleado')).subscribe(data => {
      this.puestoEmpleado = data.idPuesto;
    });
  }

  disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
  }

  getFechas(recarga: boolean) {
    if (recarga) { this.dataSource = null; }
    this.errorSpin = false;
    this.sendingDatesCounter = 0;
    this.btnSendingDates = true;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);

    this.solictudesVnService.getFechas(fechaInicio, fechaFin, 1, this.idEstado).subscribe({
      next: valor => {
        this.solicitudesPorTelefono = valor;
        if (valor.length === 0 && this.router.url.includes('interno')) {
          this.notificaciones.informacion('Oops, no tienes solicitudes por mostrar');
        }

        this.dataSource = new MatTableDataSource(valor);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        // Lineas para los filtros
      }, error: () => {
        this.btnSendingDates = false;
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }, complete: () => {
        this.sendingDatesCounter ++;
        if (this.sendingDatesCounter >= 2) {
          this.btnSendingDates = false;
        }
        this.datosActualizados();
        this.btnSendingDates = false;
      }});
  }
  ngOnInit() {
    this.listenPjsipEvents();
    this.getEmpleado();
    this.actualRoute.url.subscribe(() => {
      this.tipo = this.actualRoute.snapshot.paramMap.get('tipo');
      this.dataSource = new MatTableDataSource([]);
      // this.connect();
      this.estadoCambiadoSubs = this.sesionUsuarioService.getestadoSesionListener()
        .subscribe(resp => {
          this.ocultar = resp;
        });
      this.sesionUsuarioService.getEstadoSesionById(localStorage.getItem('sesion')).subscribe((resp: any) => {
        this.ocultar = resp.sesion[0].idEstadoSesion;
      });
      switch (this.tipo) {
        case 'no-contactado':
          this.tituloComponent = 'NO CONTACTADOS';
          this.idEstado = 1;
          break;
        case 'contactado':
          this.tituloComponent = 'CONTACTADOS';
          this.idEstado = 2;
          break;

      }
      this.getFechas(false);
    });
    /**********************Notificaciones de mensajes de whatsapp*****************************/
    const tipo = this.actualRoute.snapshot.paramMap.get('tipo');
    const strTipo = 'no-contactado, contactado, inbound, interno';
    if (this.idPuesto === 8) {
      const idEmpleado = sessionStorage.getItem('Empleado');
    }
  }

  listenPjsipEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.pjsipEvents = this.pjsip.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 5000);
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  connect() {
    // const socket = new SockJS(environment.GLOBAL_SOCKET + 'vnsolicitudes');
    // this.stompClient = Stomp.over(socket);
    // const _this = this;
    // this.stompClient.connect({}, function (frame) {
    //   _this.stompClient.subscribe('/task/panelVnSolicitudes/' + _this.tipo, (content) => {
    //     const response = JSON.parse(content.body);
    //     switch (response.idTipoPeticion) {
    //       // Post
    //       case 1:
    //         const tiempo = moment(response.fechaSolicitud).valueOf();
    //         _this.fechaInicio = moment(_this.rangeDates[0]).valueOf();
    //         _this.fechaFin = moment(_this.rangeDates[1]).valueOf();
    //         if (tiempo - _this.fechaInicio >= 0 && _this.fechaFin - tiempo >= 0)
    //           _this.showMessage(JSON.parse(content.body));
    //         break;
    //       // Update Etiqueta Solicitud
    //       case 2:
    //         _this.actualizarEtiquetaSolicitud(response.id, response.estado, response.etiquetaSolicitud);
    //         break;
    //       // Reasignar Solicitud
    //       case 3:
    //         _this.updateEmpleadoSolicitud(response.id, response.nombre, response.apellidoPaterno, response.apellidoMaterno);
    //         break;
    //       // Quitar la solicitud cuando llego a inspecciones
    //       case 4:
    //         _this.removeSolicitud(response.id);
    //         break;
    //       // Actualizar prospecto
    //       case 5:
    //         _this.actualizarProspecto(response.idProspecto, response.nombreProspecto, response.numeroProspecto);
    //     }
    //   });
    // });
  }

  actualizarProspecto(idProspecto, nombreProspecto, numeroProspecto) {
    const jsonAux = [{
      idProspecto,
      nombreProspecto,
      numeroProspecto,
    }];
    this.dataSource.data = this.dataSource.data.map(solicitud => {
      const found = jsonAux.find(e => e['idProspecto'] === solicitud['idProspecto']);
      if (found) {
        solicitud = Object.assign(solicitud, found);
      }
      return solicitud;
    });

    this.datosActualizados();
  }

  removeSolicitud(idSolicitud) {
    const index = this.dataSource.data.findIndex(dato => dato.id === idSolicitud);
    if (index >= 0) {
      const aux = this.dataSource.data;
      aux.splice(index, 1);
      this.dataSource.data = aux;
      this.datosActualizados();
    }
  }

  updateEmpleadoSolicitud(idSolicitud, nombre, apellidoPaterno, apellidoMaterno) {
    this.dataSource.data.map(function (dato) {
      if (dato.id === idSolicitud) {
        dato.nombre = nombre;
        dato.apellidoPaterno = apellidoPaterno;
        dato.apellidoMaterno = apellidoMaterno;
      }
    });
    this.datosActualizados();
  }

  actualizarEtiquetaSolicitud(idSolicitud, estado, etiquetaSolicitud) {
    this.dataSource.data.map(function (dato) {
      if (dato.id === idSolicitud) {
        dato.estado = estado;
        dato.etiquetaSolicitud = etiquetaSolicitud;
      }
    });
    this.datosActualizados();
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
    this.datosActualizados();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }

  getSolicitudVN(mensajeAct) {
    // this.dataSource = new MatTableDataSource([]);
    // this.solictudesVnService.getFechasTipo(this.tipo, fechaInicio, this.fechaFin).subscribe({
    //   next: valor => {
    //     this.solicitudesPorTelefono = valor;
    //     if (valor.length === 0 && this.router.url.includes('interno')) {
    //       this.notificaciones.informacion('Oops, no tienes solicitudes por mostrar')
    //     this.dataSource = new MatTableDataSource(valor);
    //     this.dataSource.sort = this.sort;
    //     this.dataSource.paginator = this.paginator;
    //     this.setFilterEmision();
    //     if (this.dataSource.data.length === 0 && this.router.url.includes('solicitudes')) {
    //       this.notificaciones.informacion('Oops, no tienes solicitudes por mostrar')
    //   }, complete: () => {
    //     if (mensajeAct === 1) {
    //       this.datosActualizados();
    //     }
    //   },
    // });
  }

  datosActualizados() {
    // this.snackBar.open('Datos actualizados con éxito', '',
    //   {
    //     duration: 2000,
    //     horizontalPosition: 'right',
    //     verticalPosition: 'top',
    //     panelClass: ['mat-toolbar', 'mat-primary'],
    //   });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  filtrarPorMensajeNoLeido(correlationId) {
    if (this.dataSource.data.length !== 0) {
      const coId = String(correlationId);
      this.dataSource.filter = coId.trim().toLowerCase();
    } else {
      this.notificaciones.informacion('¡Atencion! Tabla sin datos.');
    }
  }

  etiquetaSolicitud(idSolicitud, idCotizacionAli, idEmpleado, idLlamada) {
    // localStorage.setItem('tipificando', '1');
    // const dialog = this.dialog.open(EtiquetaSolicitudComponent, {
    //   context: {
    //     idSolicitud: idSolicitud,
    //     idCotizacionAli: idCotizacionAli,
    //     idEmpleado: idEmpleado,
    //     tipo: this.tipo,
    //     idLlamada: idLlamada,
    //   },
    //   closeOnBackdropClick: false,
    //   closeOnEsc: false,
    // }).onClose.subscribe(reload => {
    //   // se ejcutara cuando haya un error
    //   localStorage.setItem('tipificando', '0');
    //   this.flujoLlamadaService.available();
    //   this.getSolicitudVN(1);
    // });
  }

  verDatos(idProspecto, idProducto) {
    this.dialog.open(DatosSolicitudComponent, {
      width: 'auto',
      data: {idProspecto, idProducto},
    });
  }

  historicoLlamadas(idSolicitud) {
    this.router.navigate(['/modulos/telefonia/historial-llamadas/' + idSolicitud]);
  }


  reasignarSolicitud(solicitud: Solicitudes) {
    this.dialog.open(ReasignarSolicitudComponent, {
      width: '400px',
      data: {
        solicitud,
        tipo: this.tipo,
      },
    }).afterClosed().subscribe(data => {
      // Se ejecuta la peticion completa cuando hay un error para evitar perder información
      if (data) {
        this.getSolicitudVN(1);
      }
    });
  }

  generarEmision(idSolicitud) {
    this.router.navigate([`modulos/venta-nueva/step-cotizador/${idSolicitud}`]);
  }

  generarEmisionOnline(idSolicitud) {
    this.router.navigate(['modulos/cobranza/step-cotizador-online/' + idSolicitud]);
  }


  generarEmisionOnlineFunnel(idSolicitud) {

    this.solictudesVnService.getSolicitudesById(idSolicitud).subscribe(data => {
      this.cotizacionesAliService.getCotizacionAliById(data.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.peticiones = JSON.parse(this.cotizacionAli.peticion);

        switch (this.peticiones.aseguradora) {
          case 'QUALITAS':
            this.router.navigate(['modulos/cobranza/step-online/' + idSolicitud + '/'
            + this.peticiones.aseguradora ]);
            break;
          // case 'GNP' :
          //   this.router.navigate(['modulos/cobranza/step-cotizador-online-funnel2/' + idSolicitud + '/'
          //   + this.peticiones.aseguradora ]);
          //   break;
          default:

            break;
        }

      });
    }); // fin de solicitud

  }


  llamar(idSolicitud, numero) {
    this.snackBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
        idSolicitud,
        numero,
      }
    });
  }




// función para mandar mensajes una vez hayan respondido la plantilla
  mensajeNormal(data) {
    const mensaje = {
      'destinations': [
        {
          'correlationId': `${data.id}`,
          'destination': `521${data.numeroProspecto}`,
        },
      ],
      'message': {
        'messageText': 'Mensaje de prueba para MO',
      },
    };
  }

  enviarSMS() {
    const json = {
      'destination': 5217711908401,
      'correlationId': '152364',
      'messageText': 'HOLA TU!!',
    };
  }

  enviarWha(data) {
    if (!data.notificacion || data.notificacion === 1) {
      this.dataSource.data.map((dataMap, index) => {
        if (String(dataMap.id) === String(data.id)) {
          this.dataSource.data[index].notificacion = 0;
        }
      });
    }
    const indice = this.solicitudesConMensajes.indexOf(data.id);
    this.solicitudesConMensajes.splice(indice, 1);
  }

  mostrarChat(data) {
    //   this.socket.emit('chatBtnHeader', 'chat');
    //   if (!data.notificacion || data.notificacion === 1) {
    //     this.dataSource.data.map((dataMap, index) => {
    //       if (String(dataMap.id) === String(data.id)) {
    //         this.dataSource.data[index].notificacion = 0;
    //       }
    //     });
    //   }
    //   const indice = this.solicitudesConMensajes.indexOf(data.id);
    //   this.solicitudesConMensajes.splice(indice, 1);
    // }
    this.notificacionesService.carga('Abriendo chat');
    this.whatsMsgService.getMensajesByIdCanal(data.id, data.numeroProspecto)
      .subscribe({
        next: (value: any) => {
          if (value.cuerpo === null) {
            this.plantillaEnviada = false;
            this.notificacionesService.informacion('No se puede mostrar el chat debido a que no se ha enviado una plantilla a este lead');
          } else {
            if (value.cuerpo !== null) {
              this.notificacionesService.exitoWhats();
              this.datosCanalMensajes = value;
              const credentials = value.catalogoData;
              this.plantillaEnviada = true;
              this.modalChat(data, credentials, value);
            }
          }
        },
        error: err => {
          this.notificacionesService.error('Ocurrió un error');
        }
      });
  }

  modalChat(data, credentials, dataCanal) {
    sessionStorage.setItem('idSolicitud', String(data.id));
    this.dialog.open(ChatWhatsappComponent, {
      width: '550px',
      height: '450px',
      data: {
        data,
        credentials,
        dataCanal
      }
      // panelClass: ['estilosModal', 'col-lg-8'],
      // data: {data: data, credentials: credentials},
    }).afterClosed().subscribe((value: any) => {
      // this.socket.emit(`solicitudesRoom`, 2);
      // // this.socket.emit(`tablaSolicitudesHeaderRoom`, 3);
      // if (data.notificacion !== false && data.notificacion === 1) {
      //   this.dataSource.data.map((dataMap, index) => {
      //     if (String(dataMap.id) === String(data.id)) {
      //       this.dataSource.data[index].notificacion = 0;
      //     }
      //   });
      // } else {
      //   if (!data.notificacion) {
      //     this.dataSource.data.map((dataMap, index) => {
      //       if (String(dataMap.id) === String(data.id)) {
      //         this.dataSource.data[index].notificacion = 0;
      //       }
      //     });
      //   }
      // }
    });
  }

  clickBtn() {
    this.getDataCanal();
    if (this.dataSource.length !== 0) {
      if (this.cantidadNotificacion !== 0) {
        const style = this.addStyle.nativeElement;
        this.render.setAttribute(this.addStyle.nativeElement, 'style', 'width: 280px !important;');
        this.render.setAttribute(this.mMenu.nativeElement, 'style', 'max-height: 235px !important; overflow-x: hidden;');
        document.getElementsByClassName('mat-menu-content')[0]
          .setAttribute('style', 'width: 280px; overflow: hidden;');
      } else {
        this.notificaciones.advertencia('No tienes mensajes por responder');
      }
    } else {
      this.notificaciones.advertencia('No hay informacion en la tabla de solicitudes.');
    }
  }

  getDataCanal() {
    // this.whatsMsgService.getMensajesCanalGeneral().subscribe({
    //   next: (value: any) => {
    //     this.cantidadMensajesPorCanal = value.cantidadMensajesIndividuales;
    //     this.dataMensajesCanal = value.mensajes;
    //     this.cantidadNotificaciones = {cantidadNotificaciones: +value.cantidadNotificaciones, efect: 0};
    //     this.cantidadNotificacion = this.cantidadNotificaciones.cantidadNotificaciones;
    //     this.efect = this.cantidadNotificaciones.efect;
    //   },
    // });
  }

  iconoMatMenu(id) {
    const data = this.solicitudesPorTelefono.filter(val => {
      return val.id === id;
    });
  }

  agregarNumero(idSolicitud) {
    this.dialog.open(AgregarNumeroComponent, {
      data: {
        idSolicitud
      }
    });
  }

  getNumeros(idSolicitud, numeroOriginal) {
    this.cargando = true;
    this.numeros = [];
    this.agendaService.getNumerosByIdSolicitud(idSolicitud).subscribe((resp) => {
      resp.forEach((telefono) => {
        this.numeros.push(telefono.numero);
      });
      this.numeros.unshift(numeroOriginal);
    }, () => {}, () => {
      this.cargando = false;
    });
  }

  recotizar(idProspecto, idTipoc, idSolicitudDetalles) {

    let infoCliente: StepsCotizador;
    this.stepsService.getByIdOnline(idSolicitudDetalles).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {

      },
      () => {
        if (infoCliente.idRegistro !== null) {
          this.swalError();
        }else {
          window.open('/modulos/cobranza/recotizador-vn/' + this.tipo + '/' + idProspecto + '/' + idTipoc
            + '/' + idSolicitudDetalles, '_blank');
        }
      });
  }

  swalError() {
    this.notificaciones.advertencia('No se puede Recotizar, Ya se creo la emision').then((result) => {
      if (result.value) {
        window.close();
      }
    });
  }
}
