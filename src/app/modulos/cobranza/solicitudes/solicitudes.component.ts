import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {DatosSolicitudComponent} from '../modals/datos-solicitud/datos-solicitud.component';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {AgregarNumeroComponent} from '../../telefonia/modals/agregar-numero/agregar-numero.component';
import {NuevoNumeroService} from '../../../@core/data/services/telefonia/nuevo-numero.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LlamadaSalidaComponent} from '../../telefonia/modals/llamada-salida/llamada-salida.component';
import {Router} from '@angular/router';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {Subscription} from 'rxjs';
import {WhatsMsgService} from '../../../@core/data/services/whatspp/whats-msg.service';
import {ChatWhatsappComponent} from '../modals/chat-whatsapp/chat-whatsapp.component';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cols: any[] = ['detalle', 'idSolicitud', 'cliente', 'empleado', 'campania', 'fechaSolicitud',
    'numero', 'acciones'];
  dataSource = new MatTableDataSource([]);
  max = new Date();
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  extension = localStorage.getItem('extension');
  puedeLlamar: boolean;
  idSesion = Number(localStorage.getItem('sesion'));
  tipo: string;
  ocultar: number;
  cargando = true;
  numeros: any[];
  webRtcEvents: Subscription;
  mandarPlantilla = false;
  datosCanalMensajes: any;
  plantillaEnviada: boolean;

  constructor(private dialog: MatDialog,
              private solicitudesService: SolicitudesvnService,
              private numerosService: NuevoNumeroService,
              private snackBar: MatSnackBar,
              private whatsMsgService: WhatsMsgService,
              private notificacionesService: NotificacionesService,
              private webRTCService: WebrtcService,
              private router: Router) {
    this.isConnected();
  }

  ngOnInit() {
    this.getSolicitudes(this.rangeDates[0].toISOString().split('T')[0], this.rangeDates[1].toISOString().split('T')[0]);
    this.listenWebrtcEvents();
  }

  ngOnDestroy() {
    if (this.webRtcEvents) this.webRtcEvents.unsubscribe();
  }

  isConnected() {
    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }


  getSolicitudes(fechaInicio, fechaFin) {
    this.solicitudesService
      .getFechas(fechaInicio, fechaFin, 20, 2).subscribe((resp: any[]) => {
        if (!resp.length) {
          this.notificacionesService.advertencia('No tienes solicitudes en el rango de fechas seleccionado', 'Sin solicitudes');
        }
        this.dataSource = new MatTableDataSource(resp);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.setFilterEmision();
    });
  }

  getFechas() {
    this.dataSource = new MatTableDataSource([]);
    const fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);
    this.getSolicitudes(fechaCompleta1, fechaCompleta2);
  }
  setFilterEmision() {
    this.dataSource.filterPredicate = (data, filter) => {
      const dataStr = (
        data.nombre                            // Nombre del representante
        + ' ' + data.apellidoPaterno           // Apellido paterno del representante
        + ' ' + data.apellidoMaterno           // Apellido Materno del representante
        + ' ' + data.nombreProspecto           // Nombre del Prospecto
        + ' ' + data.id                        // Identificador
        + ' ' + data.etiquetaSolicitud         // Etiqueta de la solicitud
        + ' ' + data.numeroProspecto           // Número del prospecto
        + ' ' + data.subArea                   // Subárea
        + ' ' + data.fechaInicio
        + ' ' + data.estadoLlamada
        + ' ' + data.segundosLlamada
        + ' ' + data.numero
        + ' ' + data.idEmpleado
        + ' ' + data.nombreEmpleado

      ).toLocaleLowerCase();
      return dataStr.indexOf(filter.toLocaleLowerCase()) !== -1;
    };
  }

  verDatos(idProducto: number){
    this.dialog.open(DatosSolicitudComponent, {
      width: '500px',
      data: idProducto
    });
  }

  agregarNumero(idSolicitud) {
    this.dialog.open(AgregarNumeroComponent, {
      data: {
        idSolicitud
      }
    });
  }

  getNumeros(idSolicitud, numeroOriginal) {
    this.cargando = true;
    this.numeros = [];
    this.numerosService.getNumerosSolicitud(idSolicitud).subscribe((resp: any[]) => {
      resp.forEach((telefono) => {
        this.numeros.push(telefono.numero);
      });
      this.numeros.unshift(numeroOriginal);
      this.cargando = false;
    });
  }

  llamar(idSolicitud, numero) {
    this.snackBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
       idSolicitud,
       numero,
        predictivo: 1
      }
    });
  }

  historicoLlamadas(idSolicitud) {
    this.router.navigate(['/modulos/telefonia/historial-llamadas/' + idSolicitud]);
  }

  listenWebrtcEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.webRtcEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 5000);
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  generarEmision(idSolicitud) {
    this.router.navigate([`modulos/venta-nueva/step-cotizador/${idSolicitud}`]);
  }

  mostrarChat(data) {
    this.notificacionesService.carga('Abriendo chat');
    this.whatsMsgService.getMensajesByIdCanal(data.id, data.CotizacionesAli.Cotizacione.ProductoSolicitud.Prospecto.numero)
      .subscribe({
        next: (value: any) => {
          if (value.cuerpo === null) {
            this.plantillaEnviada = false;
            this.notificacionesService.informacion('No se puede mostrar el chat debido a que no se ha enviado una plantilla a este lead');
          } else {
            if (value.cuerpo !== null) {
              this.notificacionesService.exitoWhats();
              this.datosCanalMensajes = value;
              const credentials = value.catalogoData;
              this.plantillaEnviada = true;
              this.modalChat(data, credentials, value);
            }
          }
        },
        error: err => {
          this.notificacionesService.error('Ocurrió un error');
        }
      });
  }

  modalChat(data, credentials, dataCanal) {
    sessionStorage.setItem('idSolicitud', String(data.id));
    this.dialog.open(ChatWhatsappComponent, {
      width: '550px',
      height: '450px',
      data: {
        data,
        credentials,
        dataCanal
      }
    }).afterClosed().subscribe((value: any) => {
      // this.socket.emit(`solicitudesRoom`, 2);
      // // this.socket.emit(`tablaSolicitudesHeaderRoom`, 3);
      // if (data.notificacion !== false && data.notificacion === 1) {
      //   this.dataSource.data.map((dataMap, index) => {
      //     if (String(dataMap.id) === String(data.id)) {
      //       this.dataSource.data[index].notificacion = 0;
      //     }
      //   });
      // } else {
      //   if (!data.notificacion) {
      //     this.dataSource.data.map((dataMap, index) => {
      //       if (String(dataMap.id) === String(data.id)) {
      //         this.dataSource.data[index].notificacion = 0;
      //       }
      //     });
      //   }
      // }
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
