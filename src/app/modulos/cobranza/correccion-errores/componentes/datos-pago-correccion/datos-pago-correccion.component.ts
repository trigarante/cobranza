import {Component, DoCheck, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PagosService} from '../../../../../@core/data/services/venta-nueva/pagos.service';
import {FormaPagoService} from '../../../../../@core/data/services/venta-nueva/catalogos/forma-pago.service';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-datos-pago-correccion',
  templateUrl: './datos-pago-correccion.component.html',
  styleUrls: ['./datos-pago-correccion.component.scss']
})
export class DatosPagoCorreccionComponent implements OnInit, DoCheck {
  @Input() variablesPorMostrar: string[];
  @Input() variablesComentarios: string[];
  @Input() idPago: number;
  @Output() isAutorizacion = new EventEmitter<any>();
  @Output() formularioCorrect = new EventEmitter<any>();

  formasPago;
  datosPago = new FormBuilder().group( {
    cantidad: new FormControl({value: ''}, [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]),
    idFormaPago: new FormControl('', Validators.required),
    fechaPago: new FormControl({value: new Date()}, Validators.required),
    numeroTarjeta: new FormControl(''),
    titular: new FormControl(''),
    datos: new FormGroup({
        codigo: new FormControl(''),
        codigoToken: new FormControl(''),
        numeroTarjeta: new FormControl(''),
        titular: new FormControl(''),
        fechaVencimiento: new FormControl('')
    }),
  });
  constructor(private pagoService: PagosService,
              private formaPago: FormaPagoService,
              private notificaciones: NotificacionesService) { }

  ngOnInit() {
    this.getInformacionPago();
    this.getFormaPago();
  }

  getInformacionPago() {
    this.pagoService.getPagoById(this.idPago).subscribe({
      next: value => {
        this.datosPago.patchValue(value);
      },
    });
  }

  seleccionarForma() {
    if (this.datosPago.controls.idFormaPago.value === 1 || this.datosPago.controls.idFormaPago.value === 2) {
      this.datosPago.controls.numeroTarjeta.setValidators(Validators.required);
      this.datosPago.controls.titular.setValidators(Validators.required);
    } else {
      this.datosPago.controls.numeroTarjeta.clearValidators();
      this.datosPago.controls.titular.clearValidators();
    }
    this.datosPago.controls.numeroTarjeta.updateValueAndValidity();
    this.datosPago.controls.titular.updateValueAndValidity();
  }

  getFormaPago() {
    this.formaPago.get().subscribe(data => this.formasPago = data );
  }

  putPago() {
    this.datosPago.controls.datos.patchValue(this.datosPago.getRawValue());
    this.notificaciones.carga('Subiendo la corrección ingresada');
    this.pagoService.put(this.idPago, this.datosPago.getRawValue()).subscribe({
      complete: () => {
        this.isAutorizacion.emit({titulo: 'Pago', tipoDocumento: 'pago'});
      },
    });
  }

  ngDoCheck(): void {
    this.formularioCorrect.emit(this.datosPago.valid ? {isCorrecto: true} : {isCorrecto: false});
  }
}
