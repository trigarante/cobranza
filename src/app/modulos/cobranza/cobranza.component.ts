import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cobranza',
  template:  `
    <router-outlet></router-outlet>
  `,
})
export class CobranzaComponent {}
