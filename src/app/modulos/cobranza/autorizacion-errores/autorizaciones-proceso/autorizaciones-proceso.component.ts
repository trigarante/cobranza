import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort, Sort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FiltrosTablasService} from '../../../../@core/filtros-tablas.service';
import Swal from 'sweetalert2';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {VerificacionRegistro} from '../../../../@core/data/interfaces/verificacion/verificacion-registro';
import {ActivatedRoute, Router} from '@angular/router';
import {VerificacionRegistroService} from '../../../../@core/data/services/verificacion/verificacion-registro.service';
import {AdminVnViewService} from '../../../../@core/data/services/venta-nueva/adminVnView.service';
import {AutorizacionRegistroService} from '../../../../@core/data/services/venta-nueva/autorizacion/autorizacion-registro.service';
import { LiveAnnouncer } from '@angular/cdk/a11y';

@Component({
  selector: 'app-autorizaciones-proceso',
  templateUrl: './autorizaciones-proceso.component.html',
  styleUrls: ['./autorizaciones-proceso.component.scss']
})
export class AutorizacionesProcesoComponent implements OnInit, OnDestroy, AfterViewInit {
  // -----variables tabla---
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  autorizacionesEnProceso;
  displayedColumns: string[] = ['poliza', 'numero', 'nombre', 'nombreCliente', 'fechaCreacion', 'fechaPago',
    'nombreComercial', 'descripcion', 'horasTranscurridas', 'Acciones'];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  max: any = new Date();
  // -----fin variables tabla---

  // -----variables comoponente---
  leyendasToolTip = ['Autorizar datos',
                      'En proceso de corrección',
                      'Verificar corrección',
                      'Autorización concluida'];
  estadoVerificacion;
  titulo: string;
  errorSpin = false;
  // -----fin variables comoponente---






  constructor(private filtrosTablasService: FiltrosTablasService,
              private notificaciones: NotificacionesService,
              private router: Router,
              private route: ActivatedRoute,
              private autorizacionRegistrosService: AutorizacionRegistroService,
              private _liveAnnouncer: LiveAnnouncer,
              private administracionPolizaService: AdminVnViewService,
              private registrosEnProcesoService: VerificacionRegistroService) {


    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'poliza', nombre: 'POLIZA'},
                                                                                  {id: 'numero', nombre: 'INTENTOS'},
                                                                                  {id: 'nombre', nombre: 'NOMBRE'},
                                                                                  {id: 'nombreCliente', nombre: 'CLIENTE'},
                                                                                  {id: 'nombreComercial', nombre: 'COMERCIAL'},
                                                                                  {id: 'descripcion', nombre: 'DEPARTAMENTO'}, ]);


  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.estadoVerificacion = +params.estadoAutorizacion;

      switch (this.estadoVerificacion){
        case 1:
          this.titulo = 'PENDIENTES';
          break;
        case 2:
          this.titulo = 'EN PROCESO';
          break;
        case 3:
          this.titulo = 'CORREGIDAS';
          break;

      }
      this.getRegistrosEnProceso(false);
    });
  }

  // ====================================================
  // ===========metodos tabla===========
  // ====================================================


  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }


  getRegistrosEnProceso(recarga: boolean) {
    if (recarga) { this.autorizacionesEnProceso = null; }
    this.errorSpin = false;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);
    this.autorizacionRegistrosService.getTabla(this.estadoVerificacion, fechaInicio, fechaFin).subscribe({
      next: data => {
        if (data.length === 0) {
          this.notificaciones.informacion('No hay solicitudes');
        }

        this.autorizacionesEnProceso = new MatTableDataSource(data);
        this.autorizacionesEnProceso.sort = this.sort;
        this.autorizacionesEnProceso.paginator = this.paginator;

       },
      error: () => {
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }
    });
  }

  // Lineas para los filtros
  filterChange(filter, event) {

    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.autorizacionesEnProceso.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.autorizacionesEnProceso.filter = '';
  }
  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  enviarVerificacion(registro) {
    if (+this.diferenciaDeHoras(registro.fechaCreacion) < 0) {
      this.notificaciones.informacion('Has tardado más de 24 horas en realizar la autorización, serás acredor a una sanción.')
      .then(() => this.insertarVerificacion(registro));
    } else {
      this.insertarVerificacion(registro);
    }
  }
  mostrarDocumentos(verificacionRegistro: VerificacionRegistro, input: number) {
    this.router.navigate(['/modulos/cobranza/validar-documentos/' + input + '/' + verificacionRegistro.id]);
  }

  diferenciaDeHoras(fechaCreacion: any): number {
    const horaEnFormatoCorrecto: any = new Date(new Date(fechaCreacion).toLocaleString('en-US', {timeZone: 'America/Mexico_City'}));
    const diferencia = horaEnFormatoCorrecto - this.max;
    const diff = 24 - Math.abs(diferencia) / 36e5;
    return +diff.toString().split('.')[0];
    /*const test = await this.contador.toPromise().then(() => id++);
+    return test.valueOf();*/
  }

  async insertarVerificacion(registro) {
    const respuesta = await this.notificaciones.pregunta('¿Deseas comenzar el proceso de verificación de esta póliza?');
    if (respuesta.value) {
      this.notificaciones.carga();
      this.registrosEnProcesoService.post(registro.idRegistro).subscribe({
        error: () => this.notificaciones.error('Hubo un error al comenzar el proceso de verificacion, inténtelo más tarde o ' +
            'contacte al departamento de TI.'),
        complete: () => {
          this.notificaciones.exito('La póliza seleccionada ha comenzado su proceso de verificación.').then(() => {
              this.autorizacionesEnProceso = new MatTableDataSource([]);
              this.getRegistrosEnProceso(true);
            });
        },
      });
    }
  }

  openHelpModal() {
    Swal.fire({
      title: 'NOMENCLATURA DE COLORES',
      text: 'Seguido de la casilla de color se encuentra una descripción de lo que significa cada color en los iconos de los documentos',
      html: '<section style="text-align: left; font-family: Helvetica; margin-left: 50px;">' +
        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #1b5900; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Autorización del documento concluida</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #850100; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Documento en proceso de corrección</div>' +

        '<div style="display: inline-flex;">' +
        '<div style="background-color: #155b94; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 17px;"></div> ' +
        'Documento corregido por el ejecutivo</div>' +

        '</section>',
      icon: 'info',
    });
  }
  documentosPendientes(autorizacionRegistro: VerificacionRegistro) {
    if (autorizacionRegistro.carpetaCliente) {
      if (!autorizacionRegistro.idEmpleadoAutorizacion) {
        this.notificaciones.carga('Comenzando la autorización, por favor espere');

        this.autorizacionRegistrosService.put(autorizacionRegistro.id, {
          idEmpleado: +sessionStorage.getItem('Empleado'),
          fechaInicio: new Date(),
          idEstadoVerificacion: 2,
        }).subscribe({
          complete: () => {
            this.notificaciones.cerrar();
            this.router.navigate(['/modulos/cobranza/validar-documentos/' + autorizacionRegistro.id]);
          },
        });
      } else {
        this.router.navigate(['/modulos/cobranza/validar-documentos/' + autorizacionRegistro.id]);
      }
    } else {
      this.notificaciones
        .advertencia(
          '¡Por favor sube el archivo con los datos del cliente desde tu administrador de pólizas antes de continuar!',
           '¡Archivo de cliente inexistente!');

    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.autorizacionesEnProceso.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
  }

  announceSortChange(sortState: Sort) {
    this.autorizacionesEnProceso.sort = this.sort;
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

}
