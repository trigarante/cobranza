import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import Swal from 'sweetalert2';
import {Subscription} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import moment from 'moment';
import {Router} from '@angular/router';
import {WhatsappMsgService} from '../../../../@core/data/services/whatsappMsg/whatsapp-msg.service';

@Component({
  selector: 'app-chat-whatsapp',
  templateUrl: './chat-whatsapp.component.html',
  styleUrls: ['./chat-whatsapp.component.scss']
})
export class ChatWhatsappComponent implements OnInit {
  texto = '';
  textoAux = '';
  elemento: HTMLElement;
  mensajes: any[] = [];
  mensajesAux: any[] = [];
  websocketsWhatsapp: any;
  localizacion: any;
  dataChat: any;
  dataMensajesCanal: any;
  socketServiceSubscription: Subscription;
  idSolicitud = sessionStorage.getItem('idSolicitud');

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private whatsappMsgService: WhatsappMsgService,
    private dom: DomSanitizer,
    private router: Router,
    public ref: MatDialogRef<ChatWhatsappComponent>) { }

  ngOnInit(): void {
    this.getMensajesCanal();
  }

  getMensajesCanal() {
    this.dataMensajesCanal = this.data.dataCanal;
    if (this.data.dataCanal.mensajes.length === 1) {
      this.dataChat = this.data.dataCanal.mensajes.filter(moMessages => {
        return moMessages.idTipoEnvio === 2;
      });
      for (let i = 0; i < this.data.dataCanal.mensajes.length; i++) {
        this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`

            https://maps.google.com/maps?q=${this.data.dataCanal.mensajes[i].mensaje.split(',')[0]},${this.data.dataCanal.mensajes[i].mensaje.split(',')[1]}&z=15&output=embed
                      `);

        this.mensajes.push(
          {
            idTipoEnvio: this.data.dataCanal.mensajes[i].idTipoEnvio,
            mensaje: this.data.dataCanal.mensajes[i].mensaje,
            fecha: moment(this.data.dataCanal.mensajes[i].fecha).format('LT'),
            idTipoMensajeWhatsapp: this.data.dataCanal.mensajes[i].idTipoMensajeWhatsapp,
            localizacion: this.localizacion,
          },
        );
        //   }
      }
    } else {
      if (this.identificadorDeTiempo(this.data.dataCanal.mensajes[1].fecha) || this.dataMensajesCanal.canalData.activacion === 1) {
        this.dataChat = this.data.dataCanal.mensajes.filter(moMessages => {
          return moMessages.idTipoEnvio === 2;
        });
        /*.....separa los mensajes cuendo son mas de uno...............................................*/
        for (let i = 0; i < this.data.dataCanal.mensajes.length; i++) {
          //   for (let j = 0; j <= value[i].mensaje.length - 1; j++) {
          // this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`
          //           https://maps.google.com/maps?
          //           q=${value[i].mensaje.split(',')[0]},${value[i].mensaje.split(',')[1]}&hl=es;z=14&amp;output=embed`);
          this.localizacion = this.dom.bypassSecurityTrustResourceUrl(`

            https://maps.google.com/maps?q=${this.data.dataCanal.mensajes[i].mensaje.split(',')[0]},${this.data.dataCanal.mensajes[i].mensaje.split(',')[1]}&z=15&output=embed
                      `);

          this.mensajes.push(
            {
              idTipoEnvio: this.data.dataCanal.mensajes[i].idTipoEnvio,
              mensaje: this.data.dataCanal.mensajes[i].mensaje,
              fecha: moment(this.data.dataCanal.mensajes[i].fecha).format('LT'),
              idTipoMensajeWhatsapp: this.data.dataCanal.mensajes[i].idTipoMensajeWhatsapp,
              localizacion: this.localizacion,
            },
          );
          //   }
        }
      } else {
        this.ref.close();
        if (!this.router.url.includes('mensajes-pendientes')) {
          Swal.fire({
            icon: 'warning',
            title: '¡Atención!',
            text: 'El usuario ya no puede recibir mensajes debido a que ya pasó un lapso de 24 horas desde que ' +
              'respondió la primer plantilla.' +
              'Si es necesario continuar con la conversaación, por favor envía otra plantilla.',
            showConfirmButton: true,
            confirmButtonText: 'Enviar nueva plantilla',
            showCancelButton: true,
            cancelButtonText: 'Cerrar',
            reverseButtons: true,
          }).then( val => {
            if (val.value) {
              const jsonData = {
                ...this.data.data,
                numeroOrigen: this.dataMensajesCanal.canalData.origen,
              };
            }
          });
        }
      }
    }
  }

  cerrar() {
    this.ref.close();
  }

  press(e) {
    if (e.key === 'Enter') {
      this.enviar();
    }
  }

  enviar() {

    if ( this.texto.trim().length === 0 ) {
      return;
    }
    let numero;
    if (this.data.data.CotizacionesAli) {numero = this.data.data.CotizacionesAli.Cotizacione.ProductoSolicitud.Prospecto.numero; }
    if (this.data.data.numeroProspecto) {numero = this.data.data.numeroProspecto; }
    if (this.data.data.numero) {numero = this.data.data.numero; }
    /****************************************************************************************************************/
    const mensaje = {
      'destinations': [
        {
          'correlationId': `${this.data.data.id}`,
          'destination': `521${numero}`,
        },
      ],
      'message': {
        // 'messageText': 'Mensaje de prueba para MO',
        'messageText': `${this.texto}`,
      },
    };
    /******mostrar mensaje inmediato*****************/
    this.elemento = document.getElementById('container');
    this.mensajes.push( {idTipoEnvio: 1, mensaje: this.texto} );
    this.textoAux = this.texto;
    this.texto = '';
    setTimeout(() => {
      this.elemento.scrollTop = this.elemento.scrollHeight;
    }, 50);
    /**************************************************/
    this.whatsappMsgService.sendMessage(mensaje, this.data.credentials).subscribe({
      next: valu => {
        const respuestaDelEjecutivo: any = valu;
        const resp = {
          dataSend: [
            {
              'correlationId': `${this.data.data.id}`,
              'origin': respuestaDelEjecutivo.destinations[0].destination,
              'datos': respuestaDelEjecutivo,
              'message': {
                // 'messageText': 'Mensaje de prueba para MO, desde un ejecutivo',
                // 'messageText': `${this.texto}`,
                'messageText': `${this.textoAux}`,
                'type': 'TEXT',
              },
            },
          ],
          'idEmpleadoEnvio': sessionStorage.getItem('Empleado'),
        };
        this.whatsappMsgService.postCanalWhatsapp(resp).subscribe({
          error: err => {
            Swal.fire({
              icon: 'error',
              title: 'Mensaje no enviado!',
              text: 'Ha ocurrido un error al momento de enviar el mensaje',
            });
          },
          complete: () => {
            const json = {
              idSolicitud: this.data.data.id,
              mensaje: this.textoAux,
            };
          },
        });
      },
    });
    this.whatsappMsgService.mensajeRespondido(this.dataChat).subscribe({
      next: msgRespondido => {
        console.log(msgRespondido);
      },
      error: err => {},
      complete: () => {},
    });
    /****************************************************************************************************************/
  }
  // Para saber cuando un canal ya pasó sus 24 hrs y volver a enviar alguna plantilla para reactivar el chat
  identificadorDeTiempo(fecha) {
    const date = new Date(fecha);
    const dateConHoras = new Date(fecha).setHours(24);
    const dateActual = new Date();
    const dateMsecActual = dateActual.getTime();

    const fechasData = this.conversion(date, dateConHoras, dateMsecActual);
    return fechasData.milisecActual < fechasData.milisecFechaRegistroMasHoras;
  }

  conversion(fechaC, fechaMasHora, dateMsec) {
    const msecPorMinuto = 1000 * 60;
    const msecPorHora = msecPorMinuto * 60;
    const msecPorDia = msecPorHora * 24;

    const milisecFechaRegistro = fechaC / msecPorDia;
    const milisecFechaRegistroMasHoras = fechaMasHora / msecPorDia;
    const milisecActual = dateMsec / msecPorDia;

    return {
      milisecFechaRegistro: milisecFechaRegistro,
      milisecFechaRegistroMasHoras: milisecFechaRegistroMasHoras,
      milisecActual: milisecActual,
    };
  }
}
