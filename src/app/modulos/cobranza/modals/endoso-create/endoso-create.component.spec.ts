import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndosoCreateComponent } from './endoso-create.component';

describe('EndosoCreateComponent', () => {
  let component: EndosoCreateComponent;
  let fixture: ComponentFixture<EndosoCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndosoCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndosoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
