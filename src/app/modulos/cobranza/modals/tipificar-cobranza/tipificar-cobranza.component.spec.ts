import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipificarCobranzaComponent } from './tipificar-cobranza.component';

describe('TipificarCobranzaComponent', () => {
  let component: TipificarCobranzaComponent;
  let fixture: ComponentFixture<TipificarCobranzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipificarCobranzaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipificarCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
