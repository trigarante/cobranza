import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SolicitudEndosoService} from '../../../../@core/data/services/endososCancelaciones/solicitud-endoso.service';
import {TipoEndosoService} from '../../../../@core/data/services/endososCancelaciones/tipo-endoso.service';
import {
  MotivoEndosoService
} from '../../../../@core/data/services/endososCancelaciones/motivo-endoso.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';



@Component({
  selector: 'app-solicitud-endoso-create',
  templateUrl: './solicitud-endoso-create.component.html',
  styleUrls: ['./solicitud-endoso-create.component.scss'],
})
export class SolicitudEndosoCreateComponent implements OnInit {
  endosoCreateform: FormGroup;
  tipoEndosos;
  motivoEndosos;
  permisos = JSON.parse(window.localStorage.getItem('User'));

  constructor(public dialogRef: MatDialogRef<SolicitudEndosoCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private solicitudEndosoService: SolicitudEndosoService,
              private fb: FormBuilder,
              private notificaciones: NotificacionesService,
              private tipoEndosoService: TipoEndosoService,
              private motivoEndosoService: MotivoEndosoService
  ) { }


  dismiss() {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.endosoCreateform = this.fb.group({
      idRegistro: this.data,
      idEmpleadoSolicitante: sessionStorage.getItem('Empleado'),
      idPenalizacionEndoso: 3,
      comentarios: new FormControl('', Validators.compose([Validators.required])),
      idTipoEndoso: new FormControl('', Validators.compose([Validators.required])),
      idMotivoEndoso: new FormControl('', Validators.compose([Validators.required])),
    });
    this.getTipoEndosos();
  }

  createEndoso() {
    this.notificaciones.carga();
    this.solicitudEndosoService.post(this.endosoCreateform.value).subscribe({ error: () => {
        this.notificaciones.error('Tu solicitud de endoso no ha sido ingresada');
      },
      complete: () => {
        this.notificaciones.exito('Tu solicitud ha sido ingresada').then(() => this.dialogRef.close(true));
      }});
  }

  getTipoEndosos() {
    this.tipoEndosoService.get().subscribe(data => this.tipoEndosos = data);
  }

  getMotivoEndosos() {
    this.motivoEndosoService.getByIdTipoEndoso(this.endosoCreateform.controls.idTipoEndoso.value).subscribe(data => {
      this.motivoEndosos = data;
    });
  }
}
