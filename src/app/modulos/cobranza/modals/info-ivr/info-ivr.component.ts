import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InfoTarjeta } from '../../../../@core/data/interfaces/venta-nueva/solicitudesPago';

@Component({
  selector: 'app-info-tarjeta',
  templateUrl: './info-ivr.component.html',
  styleUrls: ['./info-ivr.component.scss']
})
export class InfoIvrComponent implements OnInit {
  data: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) data: any) {
    this.data = data;
    }

  ngOnInit(): void {
    console.log('this.data ', this.data);
  }

}
