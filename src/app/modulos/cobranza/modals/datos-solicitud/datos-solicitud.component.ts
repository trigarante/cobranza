import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProductoSolicitudvnService} from '../../../../@core/data/services/venta-nueva/producto-solicitudvn.service';

@Component({
  selector: 'app-datos-solicitud',
  templateUrl: './datos-solicitud.component.html',
  styleUrls: ['./datos-solicitud.component.scss']
})
export class DatosSolicitudComponent implements OnInit {
  prospecto;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  solicitud: any;
  constructor(public dialogRef: MatDialogRef<DatosSolicitudComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private productoSolicitudService: ProductoSolicitudvnService) { }


  /** Esta función cierra el modal. */
  dismiss() {
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log(this.data);
    this.getProductoSolicitud();
  }

  getProductoSolicitud() {
    this.productoSolicitudService.getProductoSolicitud(this.data.idProducto).subscribe(data => {
      if (data !== null) {
        this.solicitud = data.datos;
      }
      this.prospecto = data;
    });
  }

}
