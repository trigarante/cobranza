import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LinkPagoService} from '../../../../@core/data/services/link-pago/link-pago.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-pagar-solicitudes-pago',
  templateUrl: './pagar-link-pago.component.html',
  styleUrls: ['./pagar-link-pago.component.scss']
})
export class PagarLinkPagoComponent implements OnInit {
  form = new FormGroup({
    primaTotal: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+([.]+[0-9]{2})?$')]),
    url: new FormControl({value: null, disabled: true}),
    idRegistro: new FormControl(null),
    nombre: new FormControl(null),
    poliza: new FormControl(null),
    idRecibo: new FormControl(null),
  });

  constructor(
    public dialogRef: MatDialogRef<PagarLinkPagoComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private pagarLinkService: LinkPagoService,
    private notificaciones: NotificacionesService
  ) { }

  ngOnInit(): void {
    this.pagarLinkService.getInfoCliente(this.data.idRecibo, this.data.idRegistro).subscribe(data => {
      this.form.patchValue({...data, ...this.data});
    });
  }

  generarLink() {
    this.notificaciones.carga();
    this.pagarLinkService.post(this.form.getRawValue()).subscribe(data => {
      this.form.controls.url.setValue(data.url);
      this.notificaciones.exito();
    });
  }
  copiarLink() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.form.controls.url.value;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.notificaciones.exito('Link copiado al portapeles');
  }

  cerrar(reload) {
    this.dialogRef.close(reload);
  }
}
