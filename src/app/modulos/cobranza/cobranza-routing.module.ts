import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CobranzaComponent } from './cobranza.component';
import {CommonModule} from '@angular/common';
import {StepCotizadorComponent} from './step-cotizador/step-cotizador.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import {CotizadorComponent} from './cotizador/cotizador.component';
import {SolicitudesVnInternaComponent} from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import {AdministracionPolizasComponent} from './administracion-polizas/administracion-polizas.component';
import {TablaCascaronComponent} from './tabla-cascaron/tabla-cascaron.component';
import {AutorizacionesProcesoComponent} from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import {MostrarDocumentosAutorizacionComponent} from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';
import {CorreccionErroresDocumentosComponent} from './correccion-errores/correccion-errores-documentos/correccion-errores-documentos.component';
import {CorreccionErroresDatosComponent} from './correccion-errores/correccion-errores-datos/correccion-errores-datos.component';
import {DocumentosCorreccionComponent} from './correccion-errores/documentos-correccion/documentos-correccion.component';
import {DetallesPolizaComponent} from './detalles-poliza/detalles-poliza.component';
import {ViewerComponent} from './viewer/viewer.component';
import {RecibosComponent} from './componentes-steps/recibos/recibos.component';
import {AdministradorRecibosComponent} from './componentes-steps/administrador-recibos/administrador-recibos.component';
import {PagosComponent} from './componentes-steps/pagos/pagos.component';
import {MostrarCotizacionComponent} from './vnOnline/mostrar-cotizacion/mostrar-cotizacion.component';
import {NewStepOnlineComponent} from './vnOnline/new-step-online/new-step-online.component';
import {RecotizadorTabsComponent} from './vnOnline/recotizador-tabs/recotizador-tabs.component';
import {InspeccionesComponent} from './componentes-steps/inspecciones/inspecciones.component';
import {SolicitudesVnComponent} from './solicitudes-vn/solicitudes-vn.component';
import {WhatsPendientesComponent} from './whats-pendientes/whats-pendientes.component';
import {
  SocitudesActivacionComponent
} from './desactivacion-ejecutivos/socitudes-activacion/socitudes-activacion.component';
import {SolicitudesPagoComponent} from './solicitudes-pago/solicitudes-pago.component';
import {SolicitudesIvrComponent} from "./solicitudes-ivr/solicitudes-ivr.component";


const routes: Routes = [{
  path: '',
  component: CobranzaComponent,
  children: [
    {
      path: 'step-cotizador/:id',
      component: StepCotizadorComponent,
    }, {
      path: 'solicitudes-vn/:tipo',
      component: SolicitudesVnComponent,
    },
    {
      path: 'solicitudes-interna',
      component: SolicitudesVnInternaComponent,
    },
    {
      path: 'cotizador-vn/:id',
      component: CotizadorComponent
    },
    {
      path: 'cotizador-vn/:id/:idSolicitud',
      component: CotizadorComponent
    },
    {path: 'solicitudes', component: SolicitudesComponent},
    {
      path: 'administracion-polizas', component: AdministracionPolizasComponent},
    {
      path: 'tabla-cascaron',
      component: TablaCascaronComponent
    },
    {
      path: 'administracion-poliza',
      component: AdministracionPolizasComponent
    },
    {
      path: 'administracion-poliza/:idTipo',
      component: AdministracionPolizasComponent
    },
    {
      path: 'autorizacion-errores/:estadoAutorizacion',
      component: AutorizacionesProcesoComponent
    },
    {
      path: 'validar-documentos/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent
    },
    {
      path: 'correccion-errores/documentos/:tipo_correccion',
      component: CorreccionErroresDocumentosComponent
    },
    {
      path: 'correccion-errores/datos/:tipo_correccion',
      component: CorreccionErroresDatosComponent
    },

    {
      path: 'validar-documentos/:id_tipo_documento/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent
    },
    {
      path: 'documentos-correccion/:tipoError/:etapaCorreccion/:idTipoDocumento/:idverificacionAutorizacionRegistro',
      component: DocumentosCorreccionComponent,
    },
    {path: 'solicitudes', component: SolicitudesComponent},
    {
      path: 'detalles-poliza/:idPoliza',
      component: DetallesPolizaComponent,
    },
    {
      path: 'viewer/:id_tipo_documento/:id_autorizacion_registro',
      component: ViewerComponent,
    },
    {
      path: 'recibos-recibos/:idRegistro/:idRecibo',
      component: RecibosComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo',
      component: AdministradorRecibosComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo/:idSolicitud',
      component: AdministradorRecibosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro/:id',
      component: PagosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro',
      component: PagosComponent,
    },
    {
      path: 'inspecciones/:idRegistro/:idCliente',
      component: InspeccionesComponent,
    },
    {path: 'solicitudes', component: SolicitudesComponent
    },
      {path: 'whats-pendientes', component: WhatsPendientesComponent},
      {
        path: 'mostrarCotizacion/:cotizacion/' +
          ':tipo/:idProspecto/:urlServidor/:urlCotizacion/:nombreAseguradora/:submarca/:idTipoModificar/:idSolicitudDetalles',
        component: MostrarCotizacionComponent,
      },
      {
        path: 'step-online/:id_solicitud/:nombreAseguradora',
        component: NewStepOnlineComponent,
      },
      // para recotizar
      {
        path: 'recotizador-vn/:tipo_contacto/:idProspectoExis/:idTipoModificar/:idSolicitudDetalles',
        component: RecotizadorTabsComponent,
      },
    {path: 'solicitudes', component: SolicitudesComponent},
    {path: 'whats-pendientes', component: WhatsPendientesComponent},
    {path: 'solicitudes-desactivacion', component: SocitudesActivacionComponent},
    {path: 'solicitudes-pago', component: SolicitudesPagoComponent},
    {path: 'solicitudes-ivr', component: SolicitudesIvrComponent},
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CobranzaRoutingModule { }
