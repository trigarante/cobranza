import { createAction, props } from '@ngrx/store';

export const crearCliente = createAction(
  '[Cliente] Nuevo Cliente',
  props<{cliente: any}>()
);
