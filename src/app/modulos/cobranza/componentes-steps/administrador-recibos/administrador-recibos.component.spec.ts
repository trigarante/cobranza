import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorRecibosComponent } from './administrador-recibos.component';

describe('AdministradorRecibosComponent', () => {
  let component: AdministradorRecibosComponent;
  let fixture: ComponentFixture<AdministradorRecibosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministradorRecibosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministradorRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
