import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import Swal from 'sweetalert2';
import {MatDialog} from '@angular/material/dialog';
import {Recibos} from '../../../../@core/data/interfaces/venta-nueva/recibos';
import {RecibosService} from '../../../../@core/data/services/venta-nueva/recibos.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {PagarLinkPagoComponent} from '../../modals/pagar-link-pago/pagar-link-pago.component';
@Component({
  selector: 'app-recibos',
  templateUrl: './recibos.component.html',
  styleUrls: ['./recibos.component.scss'],
})
export class RecibosComponent implements OnInit {
  recibo;
  @Input()idRegistro;
  @Input()esStep;
  conjuntoRecibos: Recibos[];
  count: number;
  @Output() salida = new EventEmitter();


  constructor(
    private recibosService: RecibosService,
    public dialog: MatDialog,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private _location: Location

  ) {
  }

  ngOnInit() {
    this.count = 0;
    this.getRegistro();
  }

  siguiente() {
    if (this.count < this.conjuntoRecibos.length - 1) {
      this.count ++;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  regresar() {
    if (this.count > 0) {
      this.count--;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  getRegistro() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistro).subscribe(data => {
      this.conjuntoRecibos = data;
      this.recibo = this.conjuntoRecibos[0];
    });
  }

  cerrar() {
    this._location.back();
  }
  pagarAhoraDespues() {
    Swal.fire({
      title: '¿Deseas pagar ahora?.',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Pagar después',
      confirmButtonText: 'Si, deseo pagar ahora',
      reverseButtons: true,
      allowOutsideClick: true,
      allowEscapeKey: true,
      showCloseButton: true,
      backdrop: true,
    }).then(resp => {
      if (String(resp.dismiss) === 'cancel') {
        this.salida.emit({numero: 3, id: 'idRecibo', idRecibo: this.idRegistro, idAux: 'idInspeccion', numAux: 5, avanzar: 2});
      } else {
        this.salida.emit({numero: 3, id: 'idRecibo', idRecibo: this.idRegistro, idAux: 'idPago', numAux: 4});
      }
    });
  }

  pagarLinkPago() {
    this.dialog.open(PagarLinkPagoComponent, {
      width: '500px',
      data: {idRegistro: this.idRegistro, idRecibo: this.recibo.id}
    }).afterClosed().subscribe(reload => {
      if (reload) {
        this.getRegistro();
        this.salida.emit({numero: 3, id: 'idRecibo', idRecibo: this.idRegistro, idAux: 'idInspeccion', numAux: 5, avanzar: 2});
      }
    });
  }
}
