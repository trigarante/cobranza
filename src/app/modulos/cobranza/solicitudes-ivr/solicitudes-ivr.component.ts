import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Router} from '@angular/router';
import {LinkPagoService} from '../../../@core/data/services/link-pago/link-pago.service';
import {SolicitudesPagoService} from '../../../@core/data/services/link-pago/solicitudes-pago';
import {MatDialog} from '@angular/material/dialog';
import {SolicitudesPago} from '../../../@core/data/interfaces/venta-nueva/solicitudesPago';
import {InfoTarjetaComponent} from '../modals/info-tarjeta/info-tarjeta.component';
import {SolicitudesPagoIvrService} from '../../../@core/data/services/link-pago/solicitudes-pago-ivr';
import {InfoIvrComponent} from "../modals/info-ivr/info-ivr.component";

@Component({
  selector: 'app-solicitudes-pago',
  templateUrl: './solicitudes-ivr.component.html',
  styleUrls: ['./solicitudes-ivr.component.scss']
})
export class SolicitudesIvrComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource;
  errorSpin = false;
  displayedColumns: string[] = ['poliza', 'fecha', 'detalle'];
  empleado: string;

  constructor(
    private solPagoService: SolicitudesPagoService,
    private dialog: MatDialog,
    private solicitudesPagoIvrService: SolicitudesPagoIvrService
  ) {
    this.empleado = sessionStorage.getItem('Empleado');
  }

  ngOnInit(): void {
    this.getDatos();
  }

  getDatos() {
    this.dataSource = new MatTableDataSource([]);
    this.solicitudesPagoIvrService.getAll().subscribe(data => {
      console.log('trajo ', data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  infoPoliza(element: any){
    this.dialog.open(InfoIvrComponent, {
      data: element
    });
  }

}
