import {NgModule } from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import {CobranzaRoutingModule } from './cobranza-routing.module';
import {CobranzaComponent } from './cobranza.component';
import {DatosProductoComponent } from './componentes-steps/datos-producto/datos-producto.component';
import {RegistroPolizaComponent } from './componentes-steps/registro-poliza/registro-poliza.component';
import {StepCotizadorComponent } from './step-cotizador/step-cotizador.component';
import {InspeccionesComponent } from './componentes-steps/inspecciones/inspecciones.component';
import {PagosComponent } from './componentes-steps/pagos/pagos.component';
import {RecibosComponent } from './componentes-steps/recibos/recibos.component';
import {CreateEmisionComponent} from './componentes-steps/create-emision/create-emision.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SolicitudesVnInternaComponent } from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import {MaterialModule} from '../material.module';
import { DatosSolicitudComponent } from './modals/datos-solicitud/datos-solicitud.component';
import { ReasignarSolicitudComponent } from './modals/reasignar-solicitud/reasignar-solicitud.component';
import { ProspectoCreateComponent } from './modals/prospecto-create/prospecto-create.component';
import { CotizadorComponent } from './cotizador/cotizador.component';
import {TelefoniaModule} from '../telefonia/telefonia.module';
import {SolicitudesComponent} from './solicitudes/solicitudes.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { TablaCascaronComponent } from './tabla-cascaron/tabla-cascaron.component';
import { AutorizacionesProcesoComponent } from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import { MostrarDocumentosAutorizacionComponent } from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';

import { ViewerComponent } from './viewer/viewer.component';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import { CorreccionErroresDatosComponent } from './correccion-errores/correccion-errores-datos/correccion-errores-datos.component';
import { CorreccionErroresDocumentosComponent } from './correccion-errores/correccion-errores-documentos/correccion-errores-documentos.component';
import { DocumentosCorreccionComponent } from './correccion-errores/documentos-correccion/documentos-correccion.component';
import { DatosClienteCorreccionComponent } from './correccion-errores/componentes/datos-cliente-correccion/datos-cliente-correccion.component';
import { DatosPagoCorreccionComponent } from './correccion-errores/componentes/datos-pago-correccion/datos-pago-correccion.component';
import { DatosRegistroCorreccionComponent } from './correccion-errores/componentes/datos-registro-correccion/datos-registro-correccion.component';
import { ProductoClienteCorreccionComponent } from './correccion-errores/componentes/producto-cliente-correccion/producto-cliente-correccion.component';
import { ViewerCorreccionComponent } from './correccion-errores/componentes/viewer-correccion/viewer-correccion.component';
import { DetallesPolizaComponent } from './detalles-poliza/detalles-poliza.component';
import {SolicitudEndosoCreateComponent} from './modals/solicitud-endoso-create/solicitud-endoso-create.component';
import {SubirArchivoClienteComponent} from './modals/subir-archivo-cliente/subir-archivo-cliente.component';
import { AdministradorRecibosComponent } from './componentes-steps/administrador-recibos/administrador-recibos.component';
import { SolicitudesVnComponent } from './solicitudes-vn/solicitudes-vn.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatBadgeModule} from '@angular/material/badge';
import { ChatWhatsappComponent } from './modals/chat-whatsapp/chat-whatsapp.component';
import { WhatsPendientesComponent } from './whats-pendientes/whats-pendientes.component';
import { ViewerImgWhatsComponent } from './modals/viewer-img-whats/viewer-img-whats.component';
import { LogAseguradorasComponent } from './vnOnline/log-aseguradoras/log-aseguradoras.component';
import { ModalCotizacionComponent } from './vnOnline/modal/modal-cotizacion/modal-cotizacion.component';
import {MostrarCotizacionComponent} from './vnOnline/mostrar-cotizacion/mostrar-cotizacion.component';
import {SharedModule} from '../../shared/shared.module';
import { NewStepOnlineComponent } from './vnOnline/new-step-online/new-step-online.component';
import { CreateEmisionOnlineNewComponent } from './vnOnline/componentes-step-online/create-emision-online-new/create-emision-online-new.component';
import {MatStepperModule} from '@angular/material/stepper';
import { DatosProductosOnlineNewComponent } from './vnOnline/componentes-step-online/datos-productos-online-new/datos-productos-online-new.component';
import { PagosOnlineNewComponent } from './vnOnline/componentes-step-online/pagos-online-new/pagos-online-new.component';
import {MatDialogModule} from '@angular/material/dialog';
import { InspeccionesOnlineNewComponent } from './vnOnline/componentes-step-online/inspecciones-online-new/inspecciones-online-new.component';
import {getSpanishPaginatorIntl} from '../../traduccion-paginator';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { RecotizadorTabsComponent } from './vnOnline/recotizador-tabs/recotizador-tabs.component';
import {RegistroPolizaOnlineNewComponent} from './vnOnline/componentes-step-online/registro-poliza-online-new/registro-poliza-online-new.component';
import { EmitirOnlineNewComponent } from './vnOnline/componentes-step-online/emitir-online-new/emitir-online-new.component';
import { SocitudesActivacionComponent } from './desactivacion-ejecutivos/socitudes-activacion/socitudes-activacion.component';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { BajaRecibosComponent } from './modals/baja-recibos/baja-recibos.component';
import { EndosoCreateComponent } from './modals/endoso-create/endoso-create.component';
import { TipificarCobranzaComponent } from './modals/tipificar-cobranza/tipificar-cobranza.component';
import { TipificacionPolizaComponent } from './modals/tipificacion-poliza/tipificacion-poliza.component';
import {PagarLinkPagoComponent} from './modals/pagar-link-pago/pagar-link-pago.component';
import {SolicitudesPagoComponent} from './solicitudes-pago/solicitudes-pago.component';
import {InfoTarjetaComponent} from './modals/info-tarjeta/info-tarjeta.component';
import {SolicitudesIvrComponent} from "./solicitudes-ivr/solicitudes-ivr.component";
import {InfoIvrComponent} from "./modals/info-ivr/info-ivr.component";

@NgModule({
  declarations: [
    CobranzaComponent,
    CreateEmisionComponent,
    DatosProductoComponent,
    RegistroPolizaComponent,
    StepCotizadorComponent,
    InspeccionesComponent,
    PagosComponent,
    RecibosComponent,
    SolicitudesVnInternaComponent,
    DatosSolicitudComponent,
    ReasignarSolicitudComponent,
    ProspectoCreateComponent,
    CotizadorComponent,
    SolicitudesComponent,
    AdministracionPolizasComponent,
    TablaCascaronComponent,
    AutorizacionesProcesoComponent,
    MostrarDocumentosAutorizacionComponent,
    ViewerComponent,
    CorreccionErroresDatosComponent,
    CorreccionErroresDocumentosComponent,
    DocumentosCorreccionComponent,
    DatosClienteCorreccionComponent,
    DatosPagoCorreccionComponent,
    DatosRegistroCorreccionComponent,
    ProductoClienteCorreccionComponent,
    ViewerCorreccionComponent,
    DetallesPolizaComponent,
    SolicitudEndosoCreateComponent,
    SubirArchivoClienteComponent,
    AdministradorRecibosComponent,
    SolicitudesVnComponent,
    ChatWhatsappComponent,
    WhatsPendientesComponent,
    ViewerImgWhatsComponent,
    LogAseguradorasComponent,
    ModalCotizacionComponent,
    MostrarCotizacionComponent,
    NewStepOnlineComponent,
    CreateEmisionOnlineNewComponent,
    DatosProductosOnlineNewComponent,
    PagosOnlineNewComponent,
    RegistroPolizaOnlineNewComponent,
    InspeccionesOnlineNewComponent,
    RecotizadorTabsComponent,
    EmitirOnlineNewComponent,
    SocitudesActivacionComponent,
    BajaRecibosComponent,
    EndosoCreateComponent,
    TipificarCobranzaComponent,
    TipificacionPolizaComponent,
    PagarLinkPagoComponent,
    SolicitudesPagoComponent,
    InfoTarjetaComponent,
    SolicitudesIvrComponent,
    InfoIvrComponent
  ],
  imports: [
    CommonModule,
    CobranzaRoutingModule,
    TelefoniaModule,
    FlexLayoutModule,
    MaterialModule,
    NgxExtendedPdfViewerModule,
    MatProgressBarModule,
    MatBadgeModule,
    SharedModule,
    // CurrencyPipe,
    MatStepperModule,
    MatDialogModule,

    // NgxExtendedPdfViewerModule,

  ],
  providers: [
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],

    exports: [
        ChatWhatsappComponent,
        LogAseguradorasComponent
    ]
})
export class CobranzaModule { }
