import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {GetTestDataService} from '../../../@core/data/services/get-test-data.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-tabla-cascaron',
  templateUrl: './tabla-cascaron.component.html',
  styleUrls: ['./tabla-cascaron.component.scss']
})
export class TablaCascaronComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'numero', 'fechaInicio', 'nombre', 'extension' , 'campania', 'acciones'];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate())),
    new Date(new Date().setDate(new Date().getDate())),
  ];
  max = new Date();

  constructor(private filtrosTablasService: FiltrosTablasService,
              private testDataService: GetTestDataService) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect(['id', 'numero', 'nombre', 'extension' , 'subarea']);
  }

  ngOnInit(): void {
    this.getRemoteData();
  }

  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }

  getRemoteData() {
    this.testDataService.getTestData().subscribe((resp: any[]) => {
      this.dataSource.data = resp;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;


      // Lineas para los filtros
      this.filterSelectObj.filter((o) => {
        o.options = this.filtrosTablasService.getFilterObject(resp, o.columnProp);
      });

      // Lineas para los filtros
      this.dataSource.filterPredicate = this.filtrosTablasService.createFilter();

    });
  }

  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();

    this.dataSource.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.dataSource.filter = '';
  }
}
