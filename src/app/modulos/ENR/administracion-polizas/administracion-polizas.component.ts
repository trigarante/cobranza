import {FiltrosTablasService} from '../../../@core/filtros-tablas.service';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import Swal from 'sweetalert2';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {LlamadaSalidaComponent} from '../../telefonia/modals/llamada-salida/llamada-salida.component';
import {Subscription} from 'rxjs';
import {DescargasService} from '../../../@core/data/services/reportes/descargas.service';
import {XlsxService} from '../../../@core/data/services/reportes/xlsx.service';
import {NuevoNumeroService} from '../../../@core/data/services/telefonia/nuevo-numero.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LogueoAppService} from '../../../@core/data/services/app/logueo-app.service';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {EndosoCreateComponent} from '../../cobranza/modals/endoso-create/endoso-create.component';
import {SubirArchivoClienteComponent} from '../../cobranza/modals/subir-archivo-cliente/subir-archivo-cliente.component';
import {AplicacionesViewENRService} from '../../../@core/data/services/ENR/aplicaciones-view-enr.service';
import {InspeccionesComponent} from '../inspecciones/inspecciones.component';
import {ModificarProductoClienteComponent} from '../modificar-producto-cliente/modificar-producto-cliente.component';
import {ModificarClienteComponent} from '../modificar-cliente/modificar-cliente.component';
import {RegistroService} from '../../../@core/data/services/venta-nueva/registro.service';
import {CancelarRegistroComponent} from '../modals/cancelar-registro/cancelar-registro.component';
import {BajaRecibosComponent} from '../baja-recibos/baja-recibos.component';
import {RecibosService} from '../../../@core/data/services/venta-nueva/recibos.service';
@Component({
  selector: 'app-administracion-polizas',
  templateUrl: './administracion-polizas.component.html',
  styleUrls: ['./administracion-polizas.component.scss']
})
export class AdministracionPolizasComponent implements OnInit, OnDestroy {
  // ==================variables tabla==================
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  errorSpin = false;
  dataSource;

  displayedColumns: string[] = ['detalle',
    'poliza',
    'nombreCliente',
    'telefonoMovil',
    'nombre',
    'nombreComercial',
    'estadoPoliza',
    'estadoRecibo',
    'fechaInicio',
    'fechaRegistro',
    'numeroSerie',
    'estadoPago',
    'descripcion',
    'Acciones'];
  filterSelectObj = [];
  rangeDates = [
    new Date(new Date().setDate(new Date().getDate() - 15 )),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  max: any = new Date();

  // ==================fin variables tabla==================

  idTipo: string;
  fechaCompleta1: string;
  fechaCompleta2: string;
  extension = localStorage.getItem('extension');
  pjsipEvents: Subscription;
  puedeLlamar: boolean;
  puesto: string = sessionStorage.getItem('idPuesto');
  cargando = true;
  numeros: any[];
  permisos = JSON.parse(window.localStorage.getItem('User'));

  constructor(private notificaciones: NotificacionesService,
              private _liveAnnouncer: LiveAnnouncer,
              private filtrosTablasService: FiltrosTablasService,
              private rutaActiva: ActivatedRoute,
              private aplicacionesViewENRService: AplicacionesViewENRService,
              private pjsip: WebrtcService,
              private descargasService: DescargasService,
              private xlsxService: XlsxService,
              private snackBar: MatSnackBar,
              private router: Router,
              private agendaService: NuevoNumeroService,
              private registroService: RegistroService,
              private recibosService: RecibosService,
              public dialog: MatDialog,
              private appServices: LogueoAppService, ) {
    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }

  ngOnInit(): void {
    this.listenPjsipEvents();
    this.getPolizas(true);
  }

  announceSortChange(sortState: Sort) {
    this.dataSource.sort = this.sort;
  }
  listenPjsipEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.pjsipEvents = this.pjsip.listenEvents()
        .subscribe((resp) => {
          switch (resp.evento) {
            case 'connected':
              this.puedeLlamar = true;
              break;
            case 'disconnected':
              this.puedeLlamar = false;
              break;
          }
        });
    }
  }
  openHelpModal() {
    Swal.fire({
      title: 'NOMENCLATURA DE COLORES',
      text: 'Seguido de la casilla de color se encuentra una descripción de lo que significa cada color en los iconos de los documentos',
      html: '<section style="text-align: left; font-family: Helvetica; margin-left: 50px;">' +
        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #d7ffcf; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 13px;"></div> ' +
        'Recibo pagado</div>' +

        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #32cbff; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 13px;"></div> ' +
        'Recibo pagado sin aplicar</div>' +

        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #b388eb; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 13px;"></div> ' +
        'Recibo no pagado aplicado</div>' +

        '<p></p>' +
        '<div style="display: inline-flex;">' +
        '<div style="background-color: #ffdbe2; width: 15px; height: 15px; margin-right: 10px; margin-bottom: 13px;"></div> ' +
        'Recibo no pagado</div>' +

        '</section>',
      icon: 'info',
    });
  }

  convertirFechas() {
    this.fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    this.fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);
  }
  detallePoliza(id, idTipo) {
    this.router.navigateByUrl(`/modulos/cobranza/detalles-poliza/${id}`);
  }

  enviarEmail(idRegistro) {
    Swal.fire({
      title: '¿Esta seguro que desea enviar email al cliente?',
      text: 'Se enviara un email al cliente con la información de acceso al portal y aplicación',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, enviar email',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        this.appServices.enviarEmail(idRegistro).subscribe({
          next: data => {
            this.notificaciones.exito('Correo enviado');
            this.getPolizas(true);
          },
          error: data => {
            if (data.error.message === 'Error idRegistro no ligado'){
              this.notificaciones.error('aun no se encuentra ligado el usuario con el registro');
            }
          }
        });
      }
    });
  }

  getNumeros(idRegistro, numeroOriginal) {
    this.cargando = true;
    this.numeros = [];
    this.agendaService.getNumerosByIdRegistro(idRegistro).subscribe((resp: any) => {
        resp.forEach(telefono => this.numeros.push(telefono.numero));
        this.numeros.unshift(numeroOriginal);
      },
      () => {},
      () => this.cargando = false);
  }
  llamar(idRegistro, numero) {
    this.snackBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
        idRegistro,
        numero,
        predictivo: 1
      }
    });
  }

  crearEndoso(idRegistro) {
    this.dialog.open(EndosoCreateComponent, {
      data: {idRegistro}
    }).afterClosed().subscribe(x => {
      if (x) {
        this.getPolizas(true);
      }
    });
  }
  mostrarPoliza(id) {
    this.router.navigate(['modulos/cobranza/viewer/2', id]);
  }

  recibos(idRegistro, idRecibo, idTipo, idSolicitud) {
    if (idSolicitud) {
      this.router.navigate(['/modulos/ENR/recibos-administrador-recibos', idRegistro, idRecibo, idSolicitud],
        {queryParams: { idTipo }});
    } else {
      this.router.navigate(['/modulos/ENR/recibos-administrador-recibos', idRegistro, idRecibo],
        {queryParams: { idTipo }});
    }
  }

  subirArchivo(idCliente) {
    this.dialog.open(SubirArchivoClienteComponent, {
      width: '500px',
      data: idCliente,
    }).afterClosed().subscribe( reload => {
      if (reload) {
        this.getPolizas(true);
      }
    } );
  }

  historico(idRegistro) {
    this.router.navigate(['/modulos/telefonia/historial-llamadas/registro/' + idRegistro]);
  }
  // ====================================================
  // ===========metodos tabla===========
  // ====================================================


  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];

    if (this.pjsipEvents) {
      this.pjsipEvents.unsubscribe();
    }

  }

  tablaYfiltro(data: any){
    if (data.length === 0) {
      this.notificaciones.informacion('No hay solicitudes');
    }
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getPolizas(recarga: boolean) {
    if (recarga) { this.dataSource = null; }
    this.errorSpin = false;
    this.convertirFechas();
    const puesto = sessionStorage.getItem('idPuesto');
    this.aplicacionesViewENRService.getPolizasENR(this.fechaCompleta1, this.fechaCompleta2, puesto).subscribe({
      next: data => {
        this.tablaYfiltro(data);
      },
      error: () => {
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }
    });
  }


  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();
    this.dataSource.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.dataSource.filter = '';
  }

  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getByPoliza(poliza) {
    this.aplicacionesViewENRService.getByPolizaENR(poliza).subscribe(data => {
      this.tablaYfiltro(data);
    });
  }

  getByNumero(numero) {
    this.aplicacionesViewENRService.getPolizaENRByNumero(numero).subscribe(data => {
      this.tablaYfiltro(data);
    });
  }

  generarInspeccion(id, idCliente) {
    this.dialog.open(InspeccionesComponent, {
      data: {id, idCliente},
      width: '700px',
    }).afterClosed().subscribe(data => {
      this.getPolizas(true);
    });
  }

  modificarProductoCliente(idProducto, idEmision, idCliente) {
    this.dialog.open(ModificarProductoClienteComponent, {
      data: {
        idProducto: idProducto,
        idEmision: idEmision,
        idCliente: idCliente,
      },
    });
  }

  modificarCliente(datos) {
    this.dialog.open(ModificarClienteComponent, {
      data: datos,
    }).afterClosed().subscribe(data => {
      this.getPolizas(true);
    });;
  }
  cancelarRegistro(info: any) {
    this.dialog.open(CancelarRegistroComponent, {
      width: '400px',
      data: info
    }
    ).afterClosed().subscribe(data => {
      this.getPolizas(true);
    });
  }
  bajaRecibos(idRegistro) {
    this.notificaciones.pregunta('',
      '¿Estás seguro que quieres dar de baja todos los recibos?',
      'Si, dar de baja',
      'Cancelar').then((result) => {
      if (result.value) {
        this.dialog.open(BajaRecibosComponent, {
            width: '1000px',
            data: idRegistro
          }
        ).afterClosed().subscribe(data => {
          this.getPolizas(true);
        });
      }
    });
  }
}
