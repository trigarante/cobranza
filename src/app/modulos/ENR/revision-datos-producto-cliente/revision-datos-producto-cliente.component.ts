import {Component, Inject, OnInit} from '@angular/core';
import {Registro} from '../../../@core/data/interfaces/venta-nueva/registro';
import {Recibos} from '../../../@core/data/interfaces/venta-nueva/recibos';
import {MarcaInterno} from '../../../@core/data/interfaces/cotizador/marca-interno';
import {ModeloInterno} from '../../../@core/data/interfaces/cotizador/modelo-interno';
import {DetalleInterno} from '../../../@core/data/interfaces/cotizador/detalle-interno';
import {TipoSubramo} from '../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClienteVn} from '../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {SubRamo} from '../../../@core/data/interfaces/comerciales/sub-ramo';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CatalogoInternoService} from '../../../@core/data/services/cotizador/catalogo-interno.service';
import {TipoSubramoService} from '../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';
import {ProductoClienteService} from '../../../@core/data/services/venta-nueva/producto-cliente.service';
import {DriveService} from '../../../@core/data/services/ti/drive.service';
import {ClienteVnService} from '../../../@core/data/services/venta-nueva/cliente-vn.service';
import {map} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-revision-datos-producto-cliente',
  templateUrl: './revision-datos-producto-cliente.component.html',
  styleUrls: ['./revision-datos-producto-cliente.component.scss']
})
export class RevisionDatosProductoClienteComponent implements OnInit {

  registro: Registro;
  recibos: Recibos[];
  marcas: MarcaInterno[];
  modelos: ModeloInterno[];
  descripciones: DetalleInterno[];
  subdescripciones: DetalleInterno[];
  detalles: DetalleInterno[];
  marcaValue: MarcaInterno;
  modeloValue: ModeloInterno;
  descripcioneValue: DetalleInterno;
  subdescripcioneValue: DetalleInterno;
  detalleVlaue: DetalleInterno;
  tipoSubRamos: TipoSubramo[];
  tipoSubRamo: TipoSubramo;
  crearProd = false;
  aseguradoras = [];
  formCotizacion: FormGroup;
  formDatos: FormGroup = new FormGroup({
    'numeroMotor': new FormControl('', Validators.compose([
      Validators.pattern('^[A-Z0-9 ]+$'), Validators.maxLength(30)])),
    'numeroPlacas': new FormControl(/**/'S/N'),
    'numeroSerie': new FormControl('', Validators.compose([ Validators.required,
      Validators.pattern('^[A-Z0-9]+$'), Validators.minLength(8), Validators.maxLength(17)])),
    // this.validarNoSerieNoExista.bind(this)),
    'catalogos': new FormControl(''),
    'marca': new FormControl('', Validators.required),
    'modelos': new FormControl('', Validators.required),
    'descripcion': new FormControl('', Validators.required),
    'subdescripcion': new FormControl('', Validators.required),
    'detalles': new FormControl('', Validators.required),
  });
  cliente: ClienteVn;
  idEmision: number;
  subRamo: SubRamo[];
  socios: any = {
    value: '',
    aseguradora: '',
    idSubRamo: '',
  };
  /**Variable que almacena los datos del JSON*/
  datos: any;
  datosRespuestas: any;
  respuestaVacia = false;


  constructor(public dialogRef: MatDialogRef<RevisionDatosProductoClienteComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private catalogoService: CatalogoInternoService,
              private tipoSubRamoService: TipoSubramoService,
              private productoClienteService: ProductoClienteService,
              private driveService: DriveService,
              private clienteService: ClienteVnService,
              private dialog: MatDialog,
  ) {
    this.tipoSubRamoService.get().pipe(
      map( result => {
        return result.filter(dataa => dataa.id === 1 || dataa.id === 2 ||
          dataa.id === 9);
      })).subscribe(dat => {
      this.tipoSubRamos = dat;
    });
  }

  ngOnInit() {
    this.getCliente();
    this.getProductoCliente();
    this.formCotizacion = new FormGroup({
      'catalogos': new FormControl(''),
      'marca': new FormControl('', Validators.required),
      'modelos': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'subdescripcion': new FormControl('', Validators.required),
      'detalles': new FormControl('', Validators.required),
    });
  }

  getCliente() {
    if (JSON.stringify(this.datosRespuestas) === '{}' || !this.datosRespuestas) {
      this.respuestaVacia = true;
    } else {
      this.respuestaVacia = false;
      this.clienteService.getClienteById(this.data.idCliente).subscribe( result => {
        this.cliente = result;
      });
    }
  }

  getMarcas() {
    this.catalogoService.getMarcas(this.tipoSubRamo.id).subscribe(m => {
      this.marcas = m;
    });
  }

  getModelos() {
    this.catalogoService.getModelos().subscribe(m => {
      this.modelos = m;
    });
  }

  getDescripcion() {
    this.catalogoService.getDescripcionENR(this.marcaValue.id, this.modeloValue.id)
      .subscribe(m => {
        this.descripciones = m;
      });
  }

  getSubdescripcion() {
    this.catalogoService.getSubdescripcionENR(this.marcaValue.id, this.modeloValue.id, this.descripcioneValue.id).subscribe(m => {
      this.subdescripciones = m;
    });
  }

  getDetalles() {
    this.catalogoService.getDetalles(this.marcaValue.id, this.modeloValue.id, this.descripcioneValue.id,
      this.subdescripcioneValue.id).subscribe({
      next: d => {
        this.detalles = d;
      }});

  }

  actualizarProducto(tipo?) {
    const datos = {};
    const productoCliente: any = {
      'idSolicitud': null, // dude
      'idEmision': +this.data.idEmision,
      'idCliente': +this.data.idCliente,
      'datos': '',
      'idSubRamo': 51,
    };
    datos['numeroPlacas'] = this.formDatos.controls['numeroPlacas'].value;
    datos['numeroMotor'] = this.formDatos.controls['numeroMotor'].value;
    datos['numeroSerie'] = this.formDatos.controls['numeroSerie'].value;
    if (tipo === 'recotizado') {
      datos['marca'] = this.marcaValue.nombre;
      datos['modelo'] = this.modeloValue.nombre;
      datos['descripcion'] = this.descripcioneValue.descripcion;
      datos['subDescripcion'] = this.subdescripcioneValue.subDescripcion;
      datos['detalle'] = this.detalleVlaue.detalle;
    }
    productoCliente.datos = JSON.stringify(datos);
    this.productoClienteService.post(productoCliente).subscribe({complete: () => {
        // this.registroDatos();
      }});
  }

  getProductoCliente () {
    this.productoClienteService.getProductoClienteById(this.data.idProducto).subscribe( { next: data => {
        // const datos = JSON.parse(data.datos);
        this.datos = JSON.parse(data.datos);
        if (this.datos.length === 0) {
          this.crearProd = true;
        } else {
          this.crearProd = false;
        }
        this.formDatos.controls['numeroPlacas'].setValue(this.datos.numeroPlacas);
        this.formDatos.controls['numeroMotor'].setValue(this.datos.numeroMotor);
        this.formDatos.controls['numeroSerie'].setValue(this.datos.numeroSerie);
      }, complete: () => {
        Swal.close();
        // this.formDatos.controls['numeroSerie'].setAsyncValidators(this.validarNoSerieNoExista.bind(this));
      }});
  }

  verificarNumeroPlaca() {
    if (this.formDatos.controls['numeroPlacas'].value === '') {
      this.formDatos.controls['numeroPlacas'].setValue('S/N');
    }
  }
  // registroDatos() {
  //   Swal.close();
  //   this.dialogRef.close();
  //   this.dialog.open(DatosPolizaComponent, {
  //     data: this.data,
  //     disableClose: true,
  //   });
  // }

}
