import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionDatosProductoClienteComponent } from './revision-datos-producto-cliente.component';

describe('RevisionDatosProductoClienteComponent', () => {
  let component: RevisionDatosProductoClienteComponent;
  let fixture: ComponentFixture<RevisionDatosProductoClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RevisionDatosProductoClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionDatosProductoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
