import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkPagoComponent } from './link-pago.component';

describe('LinkPagoComponent', () => {
  let component: LinkPagoComponent;
  let fixture: ComponentFixture<LinkPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
