import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Router} from '@angular/router';
import {LinkPagoService} from '../../../@core/data/services/link-pago/link-pago.service';

@Component({
  selector: 'app-solicitudes-pago',
  templateUrl: './link-pago.component.html',
  styleUrls: ['./link-pago.component.scss']
})
export class LinkPagoComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource([]);
  cols: any[] = ['titular', 'poliza', 'fecha', 'nombre', 'acciones'];

  constructor(
    private linkPagoService: LinkPagoService,
    private router: Router) { }

  ngOnInit(): void {
    this.getFechas();
  }

  getFechas() {
    this.dataSource = new MatTableDataSource([]);
    this.linkPagoService.getENR().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  recibos(idRegistro, idRecibo) {
    this.router.navigate(['modulos/ENR/recibos-administrador-recibos', idRegistro, idRecibo]);
  }
}
