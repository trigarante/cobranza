import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CommonModule} from '@angular/common';
import {ENRComponent} from './ENR.component';
import {AdministracionPolizasComponent} from './administracion-polizas/administracion-polizas.component';
import {InspeccionesComponent} from './inspecciones/inspecciones.component';
import {
  AutorizacionesProcesoComponent
} from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import {
  CorreccionErroresDatosComponent
} from './correccion-errores/correccion-errores-datos/correccion-errores-datos.component';
import {
  CorreccionErroresDocumentosComponent
} from './correccion-errores/correccion-errores-documentos/correccion-errores-documentos.component';
import {SolicitudesVnInternaComponent} from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import {CotizadorComponent} from './cotizador/cotizador.component';
import {StepCotizadorComponent} from './step-cotizador/step-cotizador.component';
import {MostrarCotizacionComponent} from './vnOnline/mostrar-cotizacion/mostrar-cotizacion.component';
import {NewStepOnlineComponent} from './vnOnline/new-step-online/new-step-online.component';
import {
  MostrarDocumentosAutorizacionComponent
} from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';
import {DocumentosCorreccionComponent} from './correccion-errores/documentos-correccion/documentos-correccion.component';
import {CancelacionEnrComponent} from './cancelacion-enr/cancelacion-enr.component';
import {AdministradorRecibosComponent} from './componentes-steps/administrador-recibos/administrador-recibos.component';
import {PagosComponent} from './componentes-steps/pagos/pagos.component';
import {PagosBajasComponent} from './componentes-steps/pagos-baja-recibo/pagos-bajas.component';
import {LinkPagoComponent} from './link-pago/link-pago.component';


const routes: Routes = [{
  path: '',
  component: ENRComponent,
  children: [
    {
      path: 'administracion-poliza',
      component: AdministracionPolizasComponent
    },
    {
      path: 'inspecciones/:idRegistro/:idCliente',
      component: InspeccionesComponent,
    },
    {
      path: 'autorizacion-errores/:estadoAutorizacion',
      component: AutorizacionesProcesoComponent
    },
    {
      path: 'correccion-errores/datos/:tipo_correccion',
      component: CorreccionErroresDatosComponent
    },
    {
      path: 'correccion-errores/documentos/:tipo_correccion',
      component: CorreccionErroresDocumentosComponent
    },
    {
      path: 'solicitudes-interna',
      component: SolicitudesVnInternaComponent
    },
    {
      path: 'cotizador-vn/:id',
      component: CotizadorComponent
    },
    {
      path: 'step-cotizador/:id',
      component: StepCotizadorComponent,
    },
    {
      path: 'mostrarCotizacion/:cotizacion/' +
        ':tipo/:idProspecto/:urlServidor/:urlCotizacion/:nombreAseguradora/:submarca/:idTipoModificar/:idSolicitudDetalles',
      component: MostrarCotizacionComponent,
    },
    {
      path: 'step-online/:id_solicitud/:nombreAseguradora',
      component: NewStepOnlineComponent,
    },
    {
      path: 'validar-documentos/:id_tipo_documento/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent,
    },
    {
      path: 'validar-documentos/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent
    },
    {
      path: 'documentos-correccion/:tipoError/:etapaCorreccion/:idTipoDocumento/:idverificacionAutorizacionRegistro',
      component: DocumentosCorreccionComponent,
    },
    {
      path: 'historial-cancelaciones',
      component: CancelacionEnrComponent
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo',
      component: AdministradorRecibosComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo/:idSolicitud',
      component: AdministradorRecibosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro/:id',
      component: PagosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro',
      component: PagosComponent,
    },
    {
      path: 'pagos-recibos/:idRegistro',
      component: PagosBajasComponent,
    },
    {
      path: 'solicitudes-pago',
      component: LinkPagoComponent
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ENRRoutingModule {}
