import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {RecibosService} from '../../../@core/data/services/venta-nueva/recibos.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TipoPagoVn} from '../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn';
import {TipoPagoVnService} from '../../../@core/data/services/venta-nueva/catalogos/tipo-pago-vn.service';
import {RegistroService} from '../../../@core/data/services/venta-nueva/registro.service';
import * as moment from 'moment';
import {Periodicidad} from '../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad';
import {PeriodicidadService} from '../../../@core/data/services/venta-nueva/catalogos/periodicidad.service';

@Component({
  selector: 'app-baja-recibos',
  templateUrl: './baja-recibos.component.html',
  styleUrls: ['./baja-recibos.component.scss']
})
export class BajaRecibosComponent implements OnInit {
  idRegistroPoliza = this.data;
  tipoPagos: TipoPagoVn[];
  recibosForm: FormGroup = new FormGroup({
    idTipoPago: new FormControl('', Validators.required),
    idPeriodicidad: new FormControl('', Validators.required),
    fechaInicio: new FormControl('', Validators.required),
    primerPago: new FormControl('', [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]),
  });
  numeros: RegExp = /[0-9.]/;
  numeroPagos: number;
  cantidad: any;
  periodicidad: Periodicidad[];
  constructor(public dialogRef: MatDialogRef<BajaRecibosComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private fb: FormBuilder,
              private notificaciones: NotificacionesService,
              private tipoPagoService: TipoPagoVnService,
              private registroService: RegistroService,
              private periodicidadService: PeriodicidadService,
              private recibosService: RecibosService) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.getTipoPago();
    this.getPeriodicidad();
  }
  getTipoPago() {
    this.tipoPagoService.get().subscribe(result => {
      this.tipoPagos = result;
    });
  }
  getPeriodicidad() {
    this.periodicidadService.get().subscribe({
      next: res => this.periodicidad = res,
      complete: () => this.getNumeroPagos(),
    });
  }
  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.recibosForm.controls.idPeriodicidad.setValue('');
    this.getNumeroPagos();
  }
  getNumeroPagos() {
    if (this.recibosForm.controls.idTipoPago.valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.recibosForm.controls.idTipoPago.value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.recibosForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')], this.setErrorMensual.bind(this)));
      } else {
        if (this.recibosForm.contains('pagoMensual')) {
          this.recibosForm.removeControl('pagoMensual');
        }
      }
      this.getMontoPagos();
    }
  }
  getMontoPagos() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistroPoliza).subscribe( data => {
      if (data && data.length > 0) {
        if (data.length > 1) {
          this.recibosForm.controls.pagoMensual.setValidators([Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]);
          this.recibosForm.controls.pagoMensual.setValue(data[0].cantidad);
          this.recibosForm.controls.pagoMensual.updateValueAndValidity();
        }
        this.recibosForm.controls.primerPago.setValue(data[data.length - 1].cantidad);
      }
    });
  }
  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (this.recibosForm.controls.primerPago.valid) {
        this.recibosForm.controls.pagoMensual.setErrors(+this.recibosForm.controls.primerPago.value >=
        +this.recibosForm.controls.pagoMensual.value ? null : {incorrect: true});
      }
    });
  }
  bajarRecibo() {
    this.recibosService.putBaja(this.idRegistroPoliza).subscribe({
      complete: () => {
        this.crearRecibo();
      }
    });
  }
  crearRecibo() {
    const periodo = this.periodicidad.find(arr => arr.id === this.recibosForm.controls.idPeriodicidad.value);
    let cantidadAPagar: number = this.recibosForm.controls.primerPago.value;
    const recibos: any[] = [];
    let fechaVigencia = moment(this.recibosForm.controls.fechaInicio.value);
    this.cantidad = this.recibosForm.controls.idTipoPago.value === 1 ? 1 : (periodo.id === 1 ? this.numeroPagos * .5 :
      this.numeroPagos * periodo.cantidadPagos);
    this.notificaciones.carga('Generando registro');
    for (let i = 0; i < this.cantidad ; i++) {
      if (i > 0) {
        switch (this.recibosForm.controls.idTipoPago.value) {
          case 2:
            fechaVigencia = moment(fechaVigencia).add(1, 'months');
            break;
          case 3:
            fechaVigencia = moment(fechaVigencia).add(3, 'months');
            break;
          case 4:
            fechaVigencia = moment(fechaVigencia).add(4, 'months');
            break;
          case 6:
            fechaVigencia = moment(fechaVigencia).add(2, 'months');
            break;
          case 5:
            fechaVigencia = moment(fechaVigencia).add(6, 'months');
            break;
        }
        cantidadAPagar = this.recibosForm.controls.pagoMensual.value;
      }
      const fechaN = this.recibosForm.controls.idTipoPago.value === 1 ? moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00') :
        this.recibosForm.controls.idPeriodicidad.value === 1 && this.recibosForm.controls.idTipoPago.value === 5 ?
          moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00') :
          moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00');
      recibos.push({
        idRegistro: this.idRegistroPoliza,
        idEmpleado: sessionStorage.getItem('Empleado'),
        numero: i + 1,
        cantidad: cantidadAPagar,
        fechaVigencia: fechaN,
      });
    }
    this.recibosService.postRecibos(recibos).subscribe({
      complete: () => {
        this.notificaciones.exito('¡Tus recbos han sido subidos exitosamente!');
        this.dialogRef.close();
      },
    });
  }
  dismiss() {
    this.dialogRef.close();
  }
}
