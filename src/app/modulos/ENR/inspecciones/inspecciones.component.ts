import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EstadoInspeccion} from '../../../@core/data/interfaces/venta-nueva/catalogos/estado-inspeccion';
import {Paises} from '../../../@core/data/interfaces/catalogos/paises';
import {Sepomex} from '../../../@core/data/interfaces/catalogos/sepomex';
import {InspeccionesService} from '../../../@core/data/services/venta-nueva/inspecciones.service';
import {SepomexService} from '../../../@core/data/services/catalogos/sepomex.service';
import {RegistroService} from '../../../@core/data/services/venta-nueva/registro.service';
import {ClienteVnService} from '../../../@core/data/services/venta-nueva/cliente-vn.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
@Component({
  selector: 'app-inspecciones',
  templateUrl: './inspecciones.component.html',
  styleUrls: ['./inspecciones.component.scss'],
})
export class InspeccionesComponent implements OnInit {
  inspeccionesForm: FormGroup = new FormGroup({
    cp: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{4,5}')])),
    calle: new FormControl('', Validators.required),
    numInt: new FormControl(''),
    numExt: new FormControl('', Validators.required),
    idColonia: new FormControl('', Validators.required),
    fechaInspeccion: new FormControl(null),
    kilometraje: new FormControl(null, Validators.pattern('[0-9]+')),
    placas: new FormControl({disabled: true, value: ''}),
    comentarios: new FormControl(null),
    idRegistro: new FormControl(''),
    respuestaDireccion: new FormControl('No'),
    urlPostventa: new FormControl('', [Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]),
  });
  opcionesRadioButton = ['No', 'Si'];
  estadosInspeccion: EstadoInspeccion[];
  paises: Paises[];
  coloniasSe: Sepomex[];
  @Input() idRegistro: number;
  @Input() idCliente: number;
  @Input() esStep: boolean;
  filesToUpload: FileList;
  archivoValido = false;
  permisos = JSON.parse(window.localStorage.getItem('User'));


  constructor(
    private inspeccionService: InspeccionesService,
    private sepomexService: SepomexService,
    private registroService: RegistroService,
    private router: Router,
    private route: ActivatedRoute,
    private clienteService: ClienteVnService,
    private notificaciones: NotificacionesService,
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<InspeccionesComponent>
    ) {}

  ngOnInit() {
    if (!this.esStep) {
      this.idRegistro = +this.route.snapshot.paramMap.get('idRegistro');
      this.idCliente = +this.route.snapshot.paramMap.get('idCliente');
    }
    this.getCliente();
    this.getRegistro();
  }

  inspeccionOnlie(event) {
    if (event.value === '2') {
      this.inspeccionesForm.controls.urlPostventa.clearValidators();
      this.inspeccionesForm.controls.urlPostventa.setValue(null);
    } else {
      this.inspeccionesForm.controls.urlPostventa.setValidators([Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]);
    }
    this.inspeccionesForm.controls.urlPostventa.updateValueAndValidity();
  }


  getCliente() {
    this.clienteService.getClienteById(this.data.idCliente).subscribe(data => {
      this.inspeccionesForm.patchValue(data);
      this.validarCp(true);
    });
  }

  disableInspeccionesForm(opcion) {
    if (opcion === 'No') {
      this.inspeccionesForm.controls.cp.setValue('');
      this.inspeccionesForm.controls.calle.setValue('');
      this.inspeccionesForm.controls.numInt.setValue('');
      this.inspeccionesForm.controls.numExt.setValue('');
      this.inspeccionesForm.controls.idColonia.setValue('');
      this.coloniasSe = [];
    } else {
      // this.inspeccionesForm.disable();
      this.inspeccionesForm.controls.fechaInspeccion.enable();
      this.inspeccionesForm.controls.kilometraje.enable();
      this.inspeccionesForm.controls.comentarios.enable();
      this.inspeccionesForm.controls.urlPostventa.enable();
      this.getCliente();
    }
  }
  getRegistro() {
    this.registroService.getForInspecciones(this.data.id).subscribe(data => {
      this.inspeccionesForm.controls.placas.setValue(data.numeroPlacas);
    });
  }

  validarCp(esGet: boolean) {
    if (this.inspeccionesForm.controls.cp.valid) {
      if (!esGet) { this.inspeccionesForm.controls.idColonia.setValue(null); }
      this.sepomexService.getColoniaByCp(this.inspeccionesForm.controls.cp.value).subscribe(res => {
        this.coloniasSe = res;
      });
    }
  }

  guardarInspeccion() {
    this.notificaciones.carga('Se está subiendo la inspección ingresada');
    const inspeccion = this.inspeccionesForm.getRawValue();
    inspeccion.idRegistro = this.data.id;
    this.inspeccionService.postOne(inspeccion, this.filesToUpload, this.data.id).subscribe({
      error: () => this.notificaciones.error('Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información'),
      complete: () => {
        this.notificaciones.exito('¡Registro finalizado con éxito!').then(() => {
          this.dialogRef.close();
        });
      },
    });
  }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUpload = (event.target as HTMLInputElement).files;
    const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
    const tamanioArchivo = this.filesToUpload.item(0).size * .000001;

    if (this.filesToUpload.length === 1) {
      // El servicio en Spring solo acepta como máximo 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        this.notificaciones.exito('¡Tu archivo está listo para ser guardado!');
        this.archivoValido = true;
      } else {
        this.notificaciones.error(!extensionValida ? 'Solo puedes subir archivos pdf' : 'Los archivos que subas deben pesar menos de 7 MB');
        this.archivoValido = false;
      }
    }
  }
}
