import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enr',
  template:  `
    <router-outlet></router-outlet>
  `,
})
export class ENRComponent {}
