import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoClienteCorreccionComponent } from './producto-cliente-correccion.component';

describe('ProductoClienteCorreccionComponent', () => {
  let component: ProductoClienteCorreccionComponent;
  let fixture: ComponentFixture<ProductoClienteCorreccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductoClienteCorreccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoClienteCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
