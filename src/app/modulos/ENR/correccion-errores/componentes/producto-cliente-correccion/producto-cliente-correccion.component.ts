import {Component, DoCheck, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, ValidationErrors, Validators} from '@angular/forms';
import {ProductoClienteService} from '../../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import { RegistroService } from 'src/app/@core/data/services/venta-nueva/registro.service';

@Component({
  selector: 'app-producto-cliente-correccion',
  templateUrl: './producto-cliente-correccion.component.html',
  styleUrls: ['./producto-cliente-correccion.component.scss']
})
export class ProductoClienteCorreccionComponent implements OnInit, DoCheck {
  constructor(private productoClienteService: ProductoClienteService,
              private notificaciones: NotificacionesService,
              private registroService: RegistroService, ) { }
  @Input() variablesPorMostrar: string[];
  @Input() variablesComentarios: string[];
  @Input() idProducto: number;
  @Input() camposPorCorregir: any[];
  @Output() isAutorizacion = new EventEmitter<any>();
  @Output() formularioCorrect = new EventEmitter<any>();

  datosProducto = new FormBuilder().group({
    marca: new FormControl(null),
    modelo: new FormControl(null),
    descripcion: new FormControl(null),
    numeroMotor: new FormControl(null, Validators.compose([
      Validators.pattern('^[A-Z0-9 ]+'), Validators.maxLength(30)])),
    numeroPlacas: new FormControl('S/N'),
    numeroSerie: new FormControl(null, Validators.compose([
        Validators.pattern('^([A-HJ-NPR-Z0-9]{8}|[A-HJ-NPR-Z0-9]{17})$')]),
      [ this.validarNoSerieNoExista.bind(this)]),
  });
  productoCliente;

  ngOnInit(): void {
    this.getInformacionProducto();
  }
  validarNoSerieNoExista(): Promise<ValidationErrors | null> {
    return new Promise((resolve) => {
      if (!this.variablesPorMostrar.includes('numeroSerie')) resolve(null);
      if (this.datosProducto.controls.numeroSerie.dirty) {
        this.registroService.getNoSerieExistente(this.datosProducto.controls.numeroSerie.value).subscribe(
          data => {
            if (!data) {
              resolve(null);
            } else {
              this.notificaciones.pregunta('¿Estas segur@ de subir el mismo número de serie? Actualmente ya existe una poliza activa con este número de serie y se encuetra en: '
                + data.noSerie, 'Número de serie en el sistema').then((resp) => {
                if (resp.isConfirmed) {
                  resolve(null);
                } else {
                  resolve({placaExiste: true});
                }
              });
            }
          });
      }
    });
  }

  getInformacionProducto() {
    this.productoClienteService.getProductoClienteById(this.idProducto).subscribe({
      next: data => {
        this.productoCliente = data;
        if (this.camposPorCorregir) {
          const array = ['marca', 'modelo', 'descripcion', 'numeroMotor', 'numeroPlacas', 'numeroSerie'];
          for (const campo of this.camposPorCorregir) {
            if (array.includes(campo.campo)) {
              this.datosProducto.controls[campo.campo].setValidators(Validators.required);
            }
            this.datosProducto.controls[campo.campo].setValue(campo.campo === 'numeroSerie' ? data.noSerie : data.datos[campo.campo]);
          }
        }
      },
    });
  }
  putProducto() {
    this.notificaciones.carga('Subiendo la corrección ingresada');

    this.productoCliente.datos.descripcion = this.datosProducto.controls.descripcion.value || this.productoCliente.datos.descripcion;
    this.productoCliente.datos.marca = this.datosProducto.controls.marca.value || this.productoCliente.datos.marca;
    this.productoCliente.datos.modelo = this.datosProducto.controls.modelo.value || this.productoCliente.datos.modelo;
    this.productoCliente.datos.numeroMotor = this.datosProducto.controls.numeroMotor.value || this.productoCliente.datos.numeroMotor;
    this.productoCliente.datos.numeroPlacas = this.datosProducto.controls.numeroPlacas.value || this.productoCliente.datos.numeroPlacas;
    this.productoCliente.noSerie = this.datosProducto.controls.numeroSerie.value || this.productoCliente.numeroSerie;

    this.productoClienteService.put(this.idProducto, this.productoCliente).subscribe({
      complete: () => {
        this.isAutorizacion.emit({titulo: 'Producto', tipoDocumento: 'producto'});
      },
    });
  }

  ngDoCheck(): void {
    this.formularioCorrect.emit(this.datosProducto.valid ? {isCorrecto: true} : {isCorrecto: false});
  }

}
