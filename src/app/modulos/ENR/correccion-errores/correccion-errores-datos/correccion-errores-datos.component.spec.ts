import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresDatosComponent } from './correccion-errores-datos.component';

describe('CorreccionErroresDatosComponent', () => {
  let component: CorreccionErroresDatosComponent;
  let fixture: ComponentFixture<CorreccionErroresDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorreccionErroresDatosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
