import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FiltrosTablasService} from '../../../../@core/filtros-tablas.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CorreccionErroresService} from '../../../../@core/data/services/venta-nueva/correccion-errores.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {VerificacionRegistro} from '../../../../@core/data/interfaces/verificacion/verificacion-registro';

@Component({
  selector: 'app-correccion-errores-documentos',
  templateUrl: './correccion-errores-documentos.component.html',
  styleUrls: ['./correccion-errores-documentos.component.scss']
})
export class CorreccionErroresDocumentosComponent implements OnInit, OnDestroy {
  // ==================variables tabla==================
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  estadoVerificacion: string;
  documentosPCorregir = new MatTableDataSource([]);
  displayedColumns: string[] = ['poliza', 'numero', 'fechaInicio', 'nombreCliente', 'nombre', 'fechaCreacion', 'fechaPago',
    'nombreComercial', 'descripcion', 'horasRestantes', 'Acciones'];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  max = new Date();
  // ==================fin variables tabla==================

  // ==================variables componente==================
  tipoCorreccion = '';
  fechaHoy: any = new Date();
  errorSpin = false;
  // ==================fin variables componente==================
  constructor(private filtrosTablasService: FiltrosTablasService,
              private route: ActivatedRoute,
              private correccionErroresService: CorreccionErroresService,
              private notificaciones: NotificacionesService,
              private router: Router) {
    this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'poliza' , nombre: 'PÓLIZA'},
                                                                                  {id: 'numero' , nombre: 'INTENTOS'},
                                                                                  {id: 'nombre' , nombre: 'EMPLEADO'},
                                                                                  {id: 'nombreCliente' , nombre: 'CLIENTE'},
                                                                                  {id: 'nombreComercial' , nombre: 'SOCIO'},
                                                                                  {id: 'descripcion' , nombre: 'DEPARTAMENTO'}]);
  }

  ngOnInit(): void {
    this.route.url.subscribe(() => {
      this.tipoCorreccion = this.route.snapshot.paramMap.get('tipo_correccion');
      if (this.tipoCorreccion === 'autorizacion' && this.displayedColumns.find(campo => campo === 'nombreMesa')) {
          this.displayedColumns.splice(4, 1);
      } else {
        this.displayedColumns.splice(4, 0, 'nombreMesa');
      }
      this.getDocumentosPCorregirA(false);
    });
  }

  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }

  getDocumentosPCorregirA(recarga: boolean) {
    if (recarga) { this.documentosPCorregir = null; }
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);
    const op = this.tipoCorreccion === 'autorizacion' ? this.correccionErroresService.getErroresDocumentosA(fechaInicio, fechaFin)
      : this.correccionErroresService.getErroresDocumentosENR(fechaInicio, fechaFin);
    op.subscribe({
      next:   data => {
        if (data.length === 0) {
          this.notificaciones.informacion('No tienes errores de documentos por mostrar, sigue así');
        }
        this.documentosPCorregir = new MatTableDataSource(data);
        this.documentosPCorregir.sort = this.sort;
        this.documentosPCorregir.paginator = this.paginator;

      },
      error: () => {
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }
    });
  }

  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();

    this.documentosPCorregir.filter = JSON.stringify(this.filterValues);
  }


  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.documentosPCorregir.filter = '';
  }
  // ====================================================
  // ===========fin metodos  tabla===========
  // ====================================================

  diferenciaDeHoras(fechaCreacion: any): number {
    const horaEnFormatoCorrecto: any = new Date(new Date(fechaCreacion).toLocaleString('en-US', {timeZone: 'America/Mexico_City'}));
    const diferencia = horaEnFormatoCorrecto - this.fechaHoy;
    const diff = 24 - Math.abs(diferencia) / 36e5;
    return +diff.toString().split('.')[0];
    /*const test = await this.contador.toPromise().then(() => id++);
+    return test.valueOf();*/
  }
  mostrarDocumentos(verificacionRegistro: VerificacionRegistro, input: number) {
    this.router.navigate(['/modulos/ENR/documentos-correccion/documento/' + this.tipoCorreccion
    + '/' + input + '/' + verificacionRegistro.id]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentosPCorregir.filter = filterValue.trim().toLowerCase();
  }
}
