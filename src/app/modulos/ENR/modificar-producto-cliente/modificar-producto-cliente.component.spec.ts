import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarProductoClienteComponent } from './modificar-producto-cliente.component';

describe('ModificarProductoClienteComponent', () => {
  let component: ModificarProductoClienteComponent;
  let fixture: ComponentFixture<ModificarProductoClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificarProductoClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarProductoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
