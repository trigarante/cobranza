import {Component, Inject, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {Registro} from '../../../@core/data/interfaces/venta-nueva/registro';
import {Recibos} from '../../../@core/data/interfaces/venta-nueva/recibos';
import {ProductoCliente} from '../../../@core/data/interfaces/venta-nueva/producto-cliente';
import {ClienteVn} from '../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {Auto} from '../../../@core/data/interfaces/cotizador/catalogo';
import {MarcaInterno} from '../../../@core/data/interfaces/cotizador/marca-interno';
import {ModeloInterno} from '../../../@core/data/interfaces/cotizador/modelo-interno';
import {DetalleInterno} from '../../../@core/data/interfaces/cotizador/detalle-interno';
import {TipoSubramo} from '../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubRamo} from '../../../@core/data/interfaces/comerciales/sub-ramo';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CatalogoInternoService} from '../../../@core/data/services/cotizador/catalogo-interno.service';
import {ActivatedRoute} from '@angular/router';
import {TipoSubramoService} from '../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';
import {ProductoClienteService} from '../../../@core/data/services/venta-nueva/producto-cliente.service';
import {DriveService} from '../../../@core/data/services/ti/drive.service';
import {ClienteVnService} from '../../../@core/data/services/venta-nueva/cliente-vn.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-modificar-producto-cliente',
  templateUrl: './modificar-producto-cliente.component.html',
  styleUrls: ['./modificar-producto-cliente.component.scss']
})
export class ModificarProductoClienteComponent implements OnInit {

  registro: Registro;
  recibos: Recibos[];
  datosProductoCliente: ProductoCliente;
  datosCliente: ClienteVn;
  // @Output() _idProductoCliente = new EventEmitter<number>();

  socketActivo = false;
  autoSelected: Auto = new Auto();
  marcas: MarcaInterno[];
  modelos: ModeloInterno[];
  descripciones: DetalleInterno[];
  subdescripciones: DetalleInterno[];
  detalles: DetalleInterno[];
  marcaValue: MarcaInterno;
  modeloValue: ModeloInterno;
  descripcioneValue: DetalleInterno;
  subdescripcioneValue: DetalleInterno;
  detalleVlaue: DetalleInterno;
  tipoSubRamos: TipoSubramo[];
  tipoSubRamo: TipoSubramo;
  cotizacionResponse: any;
  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  descuentos = Array.from(Array(20).keys(), n => n + 1);
  aseguradoras = [];
  crearProd = false;
  // formCotizacion: FormGroup;
  formDatos: FormGroup = new FormGroup({
    'numeroMotor': new FormControl('', Validators.compose([
      Validators.pattern('^[A-Z0-9 ]+$'), Validators.maxLength(30)])),
    'numeroPlacas': new FormControl(/**/'S/N'),
    'numeroSerie': new FormControl('', Validators.compose([ Validators.required,
      Validators.pattern('^[A-Z0-9]+$'), Validators.minLength(8), Validators.maxLength(17)])),
    // this.validarNoSerieNoExista.bind(this)),
    'catalogos': new FormControl(''),
    'marca': new FormControl('', Validators.required),
    'modelos': new
    FormControl('', Validators.required),
    'descripcion': new FormControl('', Validators.required),
    'subdescripcion': new FormControl('', Validators.required),
    'detalles': new FormControl('', Validators.required),
  });
  cliente: ClienteVn;
  idEmision: number;
  idSolicitud;
  subRamo: SubRamo[];
  socios: any = {
    value: '',
    aseguradora: '',
    idSubRamo: '',
  };
  /**Variable que almacena los datos del JSON*/
  datosRespuestas: any;
  respuestaVacia = false;
  // Web Sockets
  private stompClient = null;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  datos = {
    'idMarca': null,
    'marca': null,
    'idModelo': null,
    'modelo': null,
    'idDescripcion': null,
    'descripcion': null,
    'idSubDescripcion': null,
    'subDescripcion': null,
    'descuento': ' ',
    'idDetalle': null,
    'detalle': null,
    'numeroPlacas': null,
    'numeroMotor': null,
    'numeroSerie': null
  };
  constructor(public dialogRef: MatDialogRef<ModificarProductoClienteComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private catalogoService: CatalogoInternoService,
              private router: ActivatedRoute,
              private tipoSubRamoService: TipoSubramoService,
              private productoClienteService: ProductoClienteService,
              private dialogoService: MatDialogRef<any>,
              private driveService: DriveService,
              private clienteService: ClienteVnService,
              private notificaciones: NotificacionesService
  ) {
    this.tipoSubRamoService.get().pipe(
      map( result => {
        return result.filter(dataa => dataa.id === 1 || dataa.id === 2 ||
          dataa.id === 9);
      })).subscribe(dat => {
      this.tipoSubRamos = dat;
    });
  }

  ngOnInit() {
    this.getCliente();
    this.getProductoCliente();
  }

  getCliente() {
    this.clienteService.getClienteById(this.data.idCliente).subscribe( result => {
      this.cliente = result;
    });
  }

  getMarcas() {
    this.catalogoService.getMarcas(this.tipoSubRamo.id).subscribe(m => {
      this.marcas = m;
    });
  }

  getModelos() {
    this.catalogoService.getModelos().subscribe(m => {
      this.modelos = m;
    });
  }

  getDescripcion() {
    this.catalogoService.getDescripcionENR(this.marcaValue.id, this.modeloValue.id)
      .subscribe(m => {
        this.descripciones = m;
      });
  }

  getSubdescripcion() {
    this.catalogoService.getSubdescripcionENR(this.marcaValue.id, this.modeloValue.id, this.descripcioneValue.idDescripcion).subscribe(m => {
      this.subdescripciones = m;
    });
  }

  getDetalles() {
    this.catalogoService.getDetalles(this.marcaValue.id, this.modeloValue.id, this.descripcioneValue.idDescripcion,
      this.subdescripcioneValue.idSubDescripcion).subscribe({
      next: d => {
        this.detalles = d;
      }});

  }

  actualizarProducto(tipo) {
    this.notificaciones.carga('actualizando...');
    this.datos['idMarca'] = this.marcaValue.id;
    this.datos['marca'] = this.marcaValue.nombre;
    this.datos['modelo'] = this.modeloValue.nombre;
    this.datos['idModelo'] = this.modeloValue.id;
    this.datos['descripcion'] = this.descripcioneValue.descripcion;
    this.datos['idDescripcion'] = this.descripcioneValue.idDescripcion;
    this.datos['subDescripcion'] = this.subdescripcioneValue.subDescripcion;
    this.datos['idSubDescripcion'] = this.subdescripcioneValue.idSubDescripcion;
    this.datos['detalle'] = this.detalleVlaue.detalle;
    this.datos['idDetalle'] = this.detalleVlaue.id;
    this.datos['numeroPlacas'] = this.formDatos.controls['numeroPlacas'].value;
    this.datos['numeroMotor'] = this.formDatos.controls['numeroMotor'].value;
    this.datos['numeroSerie'] = this.formDatos.controls['numeroSerie'].value;
    const productoCliente: any = {
      'idProductoCliente': this.data.idProducto,
      'idSolicitud': this.idSolicitud,
      'idEmision': this.data.idEmision,
      'idCliente': this.data.idCliente,
      'datos': this.datos,
      'idSubRamo': 51,
      'noSerie': this.formDatos.controls['numeroSerie'].value,
    };
    this.productoClienteService.put(this.data.idProducto, productoCliente).subscribe({
      error: () => {
        this.notificaciones.error('¡Error al actualizar!', 'Error');
      },
      complete: () => {
        Swal.fire({
          icon: 'success',
          title: 'Datos actualizados',
          onAfterClose: () => {
            this.dialogRef.close();
          },
        });
      },
    });
  }

  getProductoCliente() {
    this.productoClienteService.getProductoClienteById(this.data.idProducto).subscribe( { next: data => {
      if (!data.datos) {
        this.crearProd = true;
      }
      else {
        this.idSolicitud = data?.idSolicitud;
        this.datos = data.datos;
        this.formDatos.controls['numeroPlacas'].setValue(this.datos?.numeroPlacas);
        this.formDatos.controls['numeroMotor'].setValue(this.datos?.numeroMotor);
        this.formDatos.controls['numeroSerie'].setValue(this.datos?.numeroSerie);
      }
      }, complete: () => {
        Swal.close();
      }});
  }

  verificarNumeroPlaca() {
    if (this.formDatos.controls['numeroPlacas'].value === '') {
      this.formDatos.controls['numeroPlacas'].setValue('S/N');
    }
  }

}
