import {Component,  OnInit,  ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatStepper} from '@angular/material/stepper';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';

@Component({
  selector: 'app-step-cotizador',
  templateUrl: './step-cotizador.component.html',
  styleUrls: ['./step-cotizador.component.scss']
})
export class StepCotizadorComponent implements OnInit {
  @ViewChild('stepper')  stepper: MatStepper;
  idSolicitud: number = parseFloat(this.route.snapshot.paramMap.get('id'));
  solicitud;
  step = [
    {idCliente: null},
    {idProducto: null},
    {idRegistro: null},
    {idRecibo: null},
    {idPago: null},
    {idInspeccion: null},
  ];
  constructor(private route: ActivatedRoute,
              private solicitudesService: SolicitudesvnService) {
  }

  ngOnInit() {
    this.getSolicitud();
  }

  selectionChange(event) {
    const aux: any = Object.keys(this.step[event.selectedIndex]);
    this.step[event.selectedIndex][aux] = (this.solicitud[aux] != null) ? this.solicitud[aux] : null;
  }

  nextSteper(num: number) {
    this.stepper.selectedIndex += num;
  }

  emisionHijo(value){
    /* flujo normal
    {numero: 0, id: 'idCliente', idCliente: result.idCliente, numAux: 1, idAux: 'idProducto'}
    this.stepper.nex();

    edicion
    {numero: 0, id: 'idCliente', idCliente: result.idCliente}

    recibos pasa a inspeccion
    {numero: 0, id: 'idCliente', idCliente: result.idCliente, numAux: 1, idAux: 'idProducto', avanzar: 2}
     */
    this.step[value.numero][value.id] = value[value.id];
    this.solicitud[value.id] = value[value.id];
    // Activar el siguiente step
    if (value.numAux) {
      this.step[value.numAux][value.idAux] = 0;
      this.solicitud[value.idAux] = 0;
    } else if (value.esRegistro) {
      this.step[value.numero++]['idRecibo'] = value[value.id];
      this.solicitud.idRecibo = value[value.id];
    }
    this.nextSteper(value.avanzar ? value.avanzar : 1);
  }

  getSolicitud() {
    this.solicitudesService.stepsById(this.idSolicitud).subscribe({
      next: data => {
      this.solicitud = data;
    },
    complete: () => {
      if (this.solicitud.idPago) {
        this.stepper.selectedIndex = 5;
        this.step[5]['idInspeccion'] = 0;
        this.solicitud.idInspeccion = 0;
      } else if (this.solicitud.idRecibo) {
        this.step[4]['idPago'] = 0;
        this.solicitud.idPago = 0;
        this.stepper.selectedIndex = 4;
      } else if (this.solicitud.idRegistro) {
        this.stepper.selectedIndex = 3;
      } else if (this.solicitud.idProducto) {
        this.stepper.selectedIndex = 2;
        this.step[2]['idRegistro'] = 0;
        this.solicitud.idRegistro = 0;
      } else {
        this.step[0]['idCliente'] = 0;
        this.solicitud.idCliente = 0;
      }
    }});
  }
}
