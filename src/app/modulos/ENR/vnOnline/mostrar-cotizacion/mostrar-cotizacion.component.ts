import { Component, OnInit } from '@angular/core';
import {CatalogoService} from '../../../../@core/data/services/cotizador/catalogo.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import Swal from 'sweetalert2';
import {SubRamoService} from '../../../../@core/data/services/comerciales/sub-ramo.service';
import {ProductoSolicitudvnService} from '../../../../@core/data/services/venta-nueva/producto-solicitudvn.service';
import {CotizacionesService} from '../../../../@core/data/services/venta-nueva/cotizaciones.service';
import {CotizacionesAli} from '../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {CotizacionesAliService} from '../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import {CotizarRequestOnline} from '../../../../@core/data/interfaces/cotizador/cotizar';
import {StepsCotizador} from '../../../../@core/data/interfaces/venta-nueva/steps-cotizador';
import {StepsCotizadorService} from '../../../../@core/data/services/venta-nueva/steps-cotizador.service';
import {ProductoClienteService} from '../../../../@core/data/services/venta-nueva/producto-cliente.service';
import {ProductoCliente, ProductoClienteOnline} from '../../../../@core/data/interfaces/venta-nueva/producto-cliente';

@Component({
  selector: 'app-mostrar-cotizacion',
  templateUrl: './mostrar-cotizacion.component.html',
  styleUrls: ['./mostrar-cotizacion.component.scss']
})
export class MostrarCotizacionComponent implements OnInit {

  cotizacion: any;
  urlServidor: string;
  urlCotizacion: string;
  imageInsureParam: string;
  urlImages = 'assets/images/logos-seguros/';
  imageSRC: string;
  nombreAseguradora: string;
  nombreAseguradorab: string;
  cotizacionResponse: any;
  cotizacionRequestOnline = null;
  idSolicitud: any;
  idProspecto: number;
  subMarca: string;

  // Variable para mostrar los datos de las coberturas
  coverageArray: Array<any>;
  vehicleDataText: string;
  dataArrayCotizacion: any;
  dataArrayCotizacion2: any;
  dataArrayCotizacion3: any;
  tabSelectedText: string;
  tabs = ['Amplia', 'Limitada', 'Rc'];
  selected = new FormControl(0);

  methodSelected: number = 0;
  methodsPayment = [
    { value: 0, text: 'Anual', checked: true },
    // { value: 1, text: 'Semestral', checked: false },
    // { value: 2, text: 'Trimestral', checked: false },
  ];

  primaTotal: number;
  primerPago: number;
  pagoSubsecuente: number;

  productoSolicitudForm: any = {
    idProspecto: null,
    idTipoSubRamo: null,
    datos: null,
  };
  productoSolicitudFormM: any = {
    id: null,
    idProspecto: null,
    idTipoSubRamo: null,
    datos: null,
  };
  cotizacionesForm: any = {
    idProducto: null,
    idPagina: null,
    idMedioDifusion: null,
    idTipoContacto: 1,
    idEstadoCotizacion: 1,
  };
  cotizacionesAliForm: any = {
    idCotizacion: null,
    idSubRamo: null,
    peticion: null,
    respuesta: null,
    // prima: null,
    // fechaActualizacion: null,
  };
  solicitudForm: any = {
    idCotizacionAli: null,
    idEmpleado: window.sessionStorage.getItem('Empleado'),
    idEstadoSolicitud: 1,
    comentarios: ' ',
    idEtiquetaSolicitud: 1,
    idFlujoSolicitud: 1,
    idSubarea: window.sessionStorage.getItem('idSubarea'),
  };

  datosSolicitudes: any = {
    cotizacionRequestOnline: '',
    cotizacionResponse: '',
    idProspecto: '',
    idTipoContacto: '',
    idEmpleado: '',
    idSubRamo: '',
  };
  idProductoSolicitud: any;
  idTipoModificar: number;
  idSolicitudD: number;
  datos: any;
  jsonDatos: ProductoClienteOnline;
  responseProducto: any;
  cotizacionAli: CotizacionesAli;
  peticionAli: any;
  tipo: string;

  constructor(
    private catalogoService: CatalogoService,
    private route: ActivatedRoute,
    private subRamoService: SubRamoService,
    private productoSolicitudService: ProductoSolicitudvnService,
    private cotizacionesService: CotizacionesService,
    private cotizacionesAliService: CotizacionesAliService,
    private solictudesVnService: SolicitudesvnService,
    private stepsService: StepsCotizadorService,
    private router: Router,
    private productoClienteService: ProductoClienteService
  ) {
    this.cotizacion = JSON.parse(this.route.snapshot.paramMap.get('cotizacion'));
    this.tipo = this.route.snapshot.paramMap.get('tipo');
    this.idProspecto = +this.route.snapshot.paramMap.get('idProspecto');
    this.urlServidor = this.route.snapshot.paramMap.get('urlServidor');
    this.urlCotizacion = this.route.snapshot.paramMap.get('urlCotizacion');
    this.nombreAseguradorab = this.route.snapshot.paramMap.get('nombreAseguradora');
    this.idTipoModificar = +this.route.snapshot.paramMap.get('idTipoModificar');
    this.idSolicitudD = +this.route.snapshot.paramMap.get('idSolicitudDetalles');
    this.subMarca = this.route.snapshot.paramMap.get('submarca');
    // console.log(this.cotizacion);
    // console.log(this.urlServidor);
    // console.log(this.urlCotizacion);
    this.inicio();
  }

  ngOnInit(): void {
    this.getImasgeInsure();
  }
  getImasgeInsure() {
    this.imageInsureParam = this.cotizacion.aseguradora;
    // console.log("Imagen:" + this.imageInsureParam);
    this.imageSRC = this.urlImages + this.imageInsureParam + '.svg';
  }

  inicio() {
    // se mostrara la informacion de la cotizacion seleccionada del auto
    let dataCotizacion: any;
    this.catalogoService
      .getCotizacion(this.urlServidor, this.urlCotizacion, this.cotizacion)
      .subscribe(
        (data) => {
          console.log('QUALITAS DATA => ', data);
          dataCotizacion = data;
          this.vehicleDataText = `${data["vehiculo"]["marca"]} ${data["vehiculo"]["modelo"]} ${data["vehiculo"]["descripcion"]}`;
        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {
          this.dataArrayCotizacion = dataCotizacion;
          this.setValue(0);
        }
      );
  }

  setValue(event) {
    this.selected.setValue(event);
    this.tabSelectedText = `Cobertura ${this.tabs[this.selected.value]}`;
    this.coverageArray = [];
    console.log('Data Array Cotizacion => ', this.dataArrayCotizacion);
    // this.primaTotal = Math.round(this.dataArrayCotizacion["cotizacion"][0]["amplia"][0]["anual"]["primaTotal"]);
    const coverages = this.dataArrayCotizacion["coberturas"];
    // se muestra el plan de cotizacion depediendo de la cobertura
    switch (event) {
      case 0:
        this.coverageArray = coverages[0]["amplia"];
        break;
      case 1:
        this.coverageArray = coverages[1]["limitada"];
        break;
      case 2:
        this.coverageArray = coverages[2]["rc"];
        break;
    }
    this.primaTotal = this._primaTotal(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
  }

  changeSelectedMethod() {
    // si se cambia la cobertura se mostrara la prima, el primer pago y el pago subsecuente para trimestral o semestral
    this.primaTotal = this._primaTotal(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    this.primerPago = this._primerPago(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());

    this.pagoSubsecuente = this._pagoSubsecuente(this.selected.value,
      this.tabs[this.selected.value].toLowerCase(), 0, this.methodsPayment[this.methodSelected].text.toLocaleLowerCase());
  }

  private _primaTotal(indexCobertura: number, cobertura: string, indexMetodoPago: number, metodoPago: string): number {
    let primaTotalCalculo: number = Math.round(
      this.dataArrayCotizacion["cotizacion"][indexCobertura][cobertura][indexMetodoPago][metodoPago]["primaTotal"]
    );
    return primaTotalCalculo;
  }
  private _primerPago(indexCobertura: number, cobertura: string, indexMetodoPago: number, metodoPago: string): number {
    let primerPago: number = Math.round(
      this.dataArrayCotizacion["cotizacion"][indexCobertura][cobertura][indexMetodoPago][metodoPago]["primerPago"]
    );
    return primerPago;
  }
  private _pagoSubsecuente(indexCobertura: number, cobertura: string, indexMetodoPago: number, metodoPago: string): number {
    let pagoSubsecuente: number = Math.round(
      this.dataArrayCotizacion["cotizacion"][indexCobertura][cobertura][indexMetodoPago][metodoPago]["pagosSubsecuentes"]
    );
    return pagoSubsecuente;
  }
crearEmision(){
  this.cotizar();
}
  cotizar() {
    // se cotiza y se guarda la informacion en la tabla cotizacionAli en respuesta para despues utilizarla para emision
    this.swalCargando();
    let dataCotizacion: any;
    this.cotizacionRequestOnline = new CotizarRequestOnline(this.cotizacion.aseguradora, this.cotizacion.clave,
      this.cotizacion.cp, this.cotizacion.descripcion, this.cotizacion.descuento, this.cotizacion.edad,
      this.cotizacion.fechaNacimiento,
      this.cotizacion.genero, this.cotizacion.marca,
      this.cotizacion.modelo, 'cotizacion', 'AMPLIA', 'PARTICULAR', this.subMarca);
    this.nombreAseguradora = this.cotizacion.aseguradora.toLowerCase();
    // se obtiene una cotizacion y se guarda en nuestra base de dato
    this.catalogoService
      .getCotizacion(this.urlServidor, this.urlCotizacion, this.cotizacion)
      .subscribe(
        (data) => {
          console.log('QUALITAS DATA => ', data);
          dataCotizacion = data;
          // this.vehicleDataText = `${data["vehiculo"]["marca"]} ${data["vehiculo"]["modelo"]} ${data["vehiculo"]["descripcion"]}`;
        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {
          this.dataArrayCotizacion2 = dataCotizacion;
          // this.setValue(0);

          console.log(this.dataArrayCotizacion2);

          this.dataArrayCotizacion3 = dataCotizacion;
          console.log(this.dataArrayCotizacion3)

          // delete this.dataArrayCotizacion3["Coberturas"];
          this.dataArrayCotizacion3["coberturas"] =
            this.dataArrayCotizacion2["coberturas"][this.selected.value][this.tabs[this.selected.value].toLowerCase()];

          // PROBAR DESPUES
          this.dataArrayCotizacion3["paquete"] = this.tabs[this.selected.value].toUpperCase();
          this.dataArrayCotizacion3["periodicidadDePago"] = this.methodsPayment[this.methodSelected].text.toUpperCase();

          // delete this.dataArrayCotizacion3["Cotizacion"];
          //  COTIZACION REAL
          this.dataArrayCotizacion3["cotizacion"] =
            this.dataArrayCotizacion2["cotizacion"][this.selected.value][this.tabs[this.selected.value].toLowerCase()][0]
              [this.methodsPayment[this.methodSelected].text.toLowerCase()];

          // // PRUEBA PARA PODER COTIZAR EN DEV GNP
          // this.dataArrayCotizacion3["cotizacion"] =  {
          //   "cotID": "",
          //   "verID": "",
          //   "cotIncID": "",
          //   "derechos": "450.0",
          //   "impuesto": "1271.29",
          //   "recargos": "0.0",
          //   "verIncID": "",
          //   "primaNeta": "8421.96",
          //   "resultado": "True",
          //   "primaTotal": "9216.84",
          //   "primerPago": "9216.84",
          //   "idCotizacion": "CIANNE210730490425",
          //   "pagosSubsecuentes": "0.0"
          // },

          // this.dataArrayCotizacion2["cotizacion"][this.selected.value][this.tabs[this.selected.value].toLocaleLowerCase()][0]
          // [this.methodsPayment[this.methodSelected].text.toLocaleLowerCase()];
          // console.log(this.dataArrayCotizacion3)
            // al areglo se le agrega el idSubRamo para identificar se borrara despues
          this.subRamoService.getTipoAndAlias(1, this.cotizacion.aseguradora).subscribe(rsp => {
            if (rsp !== null) {
              this.dataArrayCotizacion3['idSubRamo'] = rsp.id;
            }
            this.cotizacionResponse = this.dataArrayCotizacion3;
            // Se hara el lead de solicitud vn
            // si la solicitud es contactado se recotizara si no se recotiza se hara un nuevo lead
            if (this.idTipoModificar === 2){
              this.reCotizar();
            } else {
              this.crearEmisionn();
            }
          }, error => {
            this.cotizacionResponse.push(this.dataArrayCotizacion3);
          });
        }
      );
  }

  crearEmisionn() {
    let idSolicitud: number;

    // nuevo
    // se hara un lead
    // se hara una nueva solicitud vn y se direccionara al step correspondiente para llenar datos cliente,
    // vehiculo y poder emitir y pagar
    this.datosSolicitudes.cotizacionRequestOnline = this.cotizacionRequestOnline;
    this.datosSolicitudes.cotizacionResponse = this.cotizacionResponse;
    this.datosSolicitudes.idEmpleado = window.sessionStorage.getItem('Empleado');
    this.datosSolicitudes.idTipoContacto = 3;
    this.datosSolicitudes.idProspecto  = this.idProspecto;
    this.datosSolicitudes.idSubRamo = this.cotizacionResponse.idSubRamo;
    // servicio que creara un productoSolicitud del producto hara una cotizacion - creara una cotizacionALI
    // donde guardara la peticion y la respuesta de la cotizacion, de la cotAli ara una solictud Vn
    this.productoSolicitudService.postProductoOnline(this.datosSolicitudes).subscribe({
      next: (result) => {
        this.idSolicitud = result.idSolicitud;
        // solicitudes sockets
      },
      complete: () => {
        switch (this.nombreAseguradorab) {
          case 'QUALITAS':
            this.router.navigate(['modulos/ENR/step-online/' + this.idSolicitud + '/'
            + this.nombreAseguradorab ]);
            break;
          case 'GNP' :
            this.router.navigate(['modulos/ENR/step-online/' + this.idSolicitud + '/'
            + this.nombreAseguradorab ]);
            break;
          default:

            break;
        }
      },
      error: e => {
        this.swalError();
      }

      // this.productoSolicitudForm.idProspecto = this.idProspecto;
      // this.productoSolicitudForm.datos = JSON.stringify(this.cotizacionRequestOnline);
      // this.productoSolicitudForm.idTipoSubRamo = 1;
      //
      // this.productoSolicitudService.post(this.productoSolicitudForm).subscribe({
      //   next: (result) => {
      //
      //     this.idProductoSolicitud = this.cotizacionesForm.idProducto = result.id;
      //   },
      //   complete: () => {
      //
      //     this.cotizacionesForm.idTipoContacto = 3;
      //     this.cotizacionesService.post(this.cotizacionesForm).subscribe(
      //       result => {
      //
      //         this.cotizacionesAliForm.idCotizacion = result.id;
      //         this.cotizacionesAliForm.peticion = JSON.stringify(this.cotizacionRequestOnline);
      //         console.log(this.cotizacionResponse)
      //         console.log(this.cotizacionResponse[0]);
      //         console.log(this.cotizacionResponse.idSubRamo);
      //         /**servicio getbydescripcion en sub ramo para obtener el ID*/
      //         this.cotizacionesAliForm.idSubRamo = this.cotizacionResponse.idSubRamo;
      //         this.cotizacionesAliForm.respuesta = JSON.stringify(this.cotizacionResponse);
      //         this.cotizacionesAliForm.fechaActualizacion = new Date();
      //         this.cotizacionesAliService.post(this.cotizacionesAliForm).subscribe({
      //           next: data => {
      //             this.solicitudForm.idCotizacionAli = data.id;
      //
      //
      //             this.solictudesVnService.post(this.solicitudForm).subscribe({
      //               next: r => {
      //                 idSolicitud = r.id;
      //                 this.idSolicitud = r.id;
      //                 let json = r;
      //                 // json['tipoSolicitud'] = this.rutaActiva.snapshot.paramMap.get('tipo_contacto');
      //                 // this.solictudesVnService.postSolicitudSocket(json).subscribe({
      //                 //   next: () => {
      //                 //     // Swal.fire({
      //                 //     //   title: '¡Solicitud guardada con éxito!',
      //                 //     //   text: '¿Deseas comenzar ahora el proceso de emisión?',
      //                 //     //   type: 'question',
      //                 //     //   // showCancelButton: true,
      //                 //     //   // cancelButtonColor: '#799EB0',
      //                 //     //   confirmButtonText: 'Ir a comenzar proceso',
      //                 //     //   // cancelButtonText: 'Regresar a solicitudes',
      //                 //     //   // reverseButtons: true,
      //                 //     //   width: 500,
      //                 //     // }).then(value => {
      //                 //     //   if (value.value) {
      //                 //     //
      //                 //     //     switch (this.nombreAseguradorab) {
      //                 //     //       case 'QUALITAS':
      //                 //     //         this.router.navigate(['modulos/cobranza/step-online/' + idSolicitud + '/'
      //                 //     //         + this.nombreAseguradorab ]);
      //                 //     //         break;
      //                 //     //       case 'GNP' :
      //                 //     //         this.router.navigate(['modulos/cobranza/step-cotizador-online-funnel2/' + idSolicitud + '/'
      //                 //     //         + this.nombreAseguradorab ]);
      //                 //     //         break;
      //                 //     //       default:
      //                 //     //
      //                 //     //         break;
      //                 //     //     }
      //                 //     //     // this.router.navigate(['modulos/cobranza/step-cotizador-online-funnel2/' + idSolicitud + '/'
      //                 //     //     // + this.nombreAseguradorab ]);
      //                 //     //   } else {
      //                 //     //     // window.close();
      //                 //     //   }
      //                 //     //
      //                 //     //
      //                 //     // });
      //                 //   },
      //                 //   error: () => {
      //                 //     this.swalError();
      //                 //   },
      //                 // });
      //
      //                 switch (this.nombreAseguradorab) {
      //                   case 'QUALITAS':
      //                     // this.router.navigate(['modulos/cobranza/step-online/' + idSolicitud + '/'
      //                     // + this.nombreAseguradorab ]);
      //                     break;
      //                   case 'GNP' :
      //                     // this.router.navigate(['modulos/cobranza/step-online/' + idSolicitud + '/'
      //                     // + this.nombreAseguradorab ]);
      //                     break;
      //                   default:
      //
      //                     break;
      //                 }
      //               },
      //               error: e => {
      //                 this.swalError();
      //               }
      //             });
      //           },
      //           error: e => {
      //             this.swalError();
      //           }
      //         });
      //       }); // fin de cotizacion
      //   }
      //
      // }) // fin de producto
    });
  }
  swalCargando() {
    Swal.fire({
      title: 'Se está actualizando la informacón, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 8000);
  }
  swalError() {
    Swal.fire({
      title: 'Hubo un error al actualizar la información',
      text: 'Favor de contactar a TI',
    }).then((result) => {
      if (result.value) {
        window.close();
      }
    });
  }
  // vuelve a recotizar
  // De la tabla contactados se tomara un lead y volvera a poner datos del vehiculo y recotizara datos y creara
  // un lead
  reCotizar() {
    let infoCliente: StepsCotizador;
    let productoCliente;
    this.stepsService.getByIdOnline(this.idSolicitudD).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {

      },
      () => {
        // si existe producto cliente llenara los datos con  el numero de placas, serie y motor y datos del auto
        if (infoCliente.idProductoCliente !== null) {



          this.productoClienteService.getProductoClienteById(infoCliente.idProductoCliente).subscribe(dataPC => {

            this.jsonDatos = dataPC;
            this.responseProducto = this.jsonDatos.datos;
            console.log(this.jsonDatos);
            console.log(this.jsonDatos['numeroPlacas']);

            this.datos = this.cotizacion;

            this.datos['numeroPlacas'] = this.responseProducto.numeroPlacas;
            this.datos['numeroMotor'] = this.responseProducto.numeroMotor;
            this.datos['numeroSerie'] = this.responseProducto.numeroSerie;
            this.datos['submarca'] = this.subMarca;
            productoCliente = {
              id: dataPC.id,
              idSolictud: dataPC.idSolictud,
              idCliente: dataPC.idCliente,
              datos: JSON.stringify(this.datos),
              idSubRamo: dataPC.idSubRamo,
            };
            this.productoClienteService.put(dataPC.id, productoCliente).subscribe({
              error: () => {

              },
              complete: () => {

              },
            });
          })

        }
      });
    // modifica producto solicitud con los datos de cotizado nuevo
    this.solictudesVnService.getSolicitudesByIdOnline(this.idSolicitudD).subscribe(data => {

      this.productoSolicitudService.getProductoSolicitudOnline(data.idProducto).subscribe(dataP => {

        this.productoSolicitudFormM.id = dataP[0].id;
        this.productoSolicitudFormM.idProspecto = dataP[0].idProspecto;
        this.productoSolicitudFormM.datos = this.cotizacionRequestOnline;
        this.productoSolicitudFormM.idTipoSubRamo = dataP[0].idTipoSubRamo;

        this.productoSolicitudService.putOnline(dataP[0].id, this.productoSolicitudFormM).subscribe(dataPro => {
        });
      });
     // modifica la peticion de cotizacion ali, le deja datos que tenia y area los nuevos de cotizacion auto
      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.peticionAli = this.cotizacionAli.peticion;

        delete this.peticionAli['cp'];
        delete this.peticionAli['edad'];
        delete this.peticionAli['clave'];
        delete this.peticionAli['marca'];
        delete this.peticionAli['genero'];
        delete this.peticionAli['modelo'];
        delete this.peticionAli['paquete'];
        delete this.peticionAli['servicio'];
        delete this.peticionAli['subMarca'];
        delete this.peticionAli['descuento'];
        delete this.peticionAli['movimiento'];
        delete this.peticionAli['aseguradora'];
        delete this.peticionAli['descripcion'];
        delete this.peticionAli['fechaNacimiento'];

        this.peticionAli['cp'] = this.cotizacionRequestOnline.cp;
        this.peticionAli['edad'] = this.cotizacionRequestOnline.edad;
        this.peticionAli['clave'] = this.cotizacionRequestOnline.clave;
        this.peticionAli['marca'] = this.cotizacionRequestOnline.marca;
        this.peticionAli['genero'] = this.cotizacionRequestOnline.genero;
        this.peticionAli['modelo'] = this.cotizacionRequestOnline.modelo;
        this.peticionAli['paquete'] = this.cotizacionRequestOnline.paquete;
        this.peticionAli['servicio'] = this.cotizacionRequestOnline.servicio;
        this.peticionAli['subMarca'] = this.cotizacionRequestOnline.subMarca;
        this.peticionAli['descuento'] = this.cotizacionRequestOnline.descuento;
        this.peticionAli['movimiento'] = this.cotizacionRequestOnline.movimiento;
        this.peticionAli['aseguradora'] = this.cotizacionRequestOnline.aseguradora;
        this.peticionAli['descripcion'] = this.cotizacionRequestOnline.descripcion;
        this.peticionAli['fechaNacimiento'] = this.cotizacionRequestOnline.fechaNacimiento;
        this.cotizacionAli['peticion'] = this.peticionAli;
        /// aqui
        this.cotizacionAli['respuesta'] = this.cotizacionResponse;
        console.log(this.cotizacionAli)

        this.cotizacionesAliService.putOnline(data.idCotizacionAli, this.cotizacionAli).subscribe( result => {
          },
          error => { },
          () => {
            if (this.tipo === 'contactado') {
              // Swal.fire({
              //   title: '¡Cotizacion guardada con éxito!',
              //   text: '',
              //   type: 'question',
              //   showCancelButton: false,
              //   cancelButtonColor: '#799EB0',
              //
              //   // reverseButtons: true,
              //   width: 500,
              // }).then(value => {
              //   if (value.value) {
              //     window.close();
              //   } else {
              //     window.close();
              //   }
              // });
              switch (this.nombreAseguradorab) {
                case 'QUALITAS':
                  this.router.navigate(['modulos/cobranza/step-online/' + this.idSolicitudD + '/'
                  + this.nombreAseguradorab ]);
                  break;
                case 'GNP' :
                  this.router.navigate(['modulos/cobranza/step-online/' + this.idSolicitudD + '/'
                  + this.nombreAseguradorab ]);
                  break;
                default:

                  break;
              }
            }

            if (this.tipo === 'interno') {

              // Swal.fire({
              //   title: '¡Cotizacion guardada con éxito!',
              //   text: '¿Deseas continuar el proceso de cotizacion?',
              //   type: 'question',
              //   // showCancelButton: true,
              //   // cancelButtonColor: '#799EB0',
              //   confirmButtonText: 'Seguir proceso para Emitir',
              //   // reverseButtons: true,
              //   width: 500,
              // }).then(value => {
              //   if (value.value) {
              //
              //     switch (this.nombreAseguradorab) {
              //       case 'QUALITAS':
              //         this.router.navigate(['modulos/cobranza/step-online/' + this.idSolicitudD + '/'
              //         + this.nombreAseguradorab ]);
              //         break;
              //       case 'GNP' :
              //         this.router.navigate(['modulos/cobranza/step-cotizador-online-funnel2/' + this.idSolicitudD + '/'
              //         + this.nombreAseguradorab ]);
              //         break;
              //       default:
              //
              //         break;
              //     }
              //     // this.router.navigate(['modulos/cobranza/step-cotizador-online-funnel/' + this.idSolicitudD + '/'
              //     // + this.nombreAseguradorab]);
              //   } else {
              //   }
              // });

              switch (this.nombreAseguradorab) {
                case 'QUALITAS':
                  this.router.navigate(['modulos/cobranza/step-online/' + this.idSolicitudD + '/'
                  + this.nombreAseguradorab ]);
                  break;
                case 'GNP' :
                  this.router.navigate(['modulos/cobranza/step-online/' + this.idSolicitudD + '/'
                  + this.nombreAseguradorab ]);
                  break;
                default:

                  break;
              }
            }

          });
      });
    }); // fin de solicitud


  }
}
