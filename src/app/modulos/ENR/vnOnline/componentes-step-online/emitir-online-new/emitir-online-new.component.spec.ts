import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmitirOnlineNewComponent } from './emitir-online-new.component';

describe('EmitirOnlineNewComponent', () => {
  let component: EmitirOnlineNewComponent;
  let fixture: ComponentFixture<EmitirOnlineNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmitirOnlineNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitirOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
