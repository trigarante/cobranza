import {Component, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material/stepper';
import {CotizacionesAli} from '../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali';
import {ClienteVn} from '../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {Solicitudes, SolicitudesView} from '../../../../@core/data/interfaces/venta-nueva/solicitudes';
import {ActivatedRoute, Router} from '@angular/router';
import {StepsCotizadorService} from '../../../../@core/data/services/venta-nueva/steps-cotizador.service';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import Swal from 'sweetalert2';
import {StepsCotizador} from '../../../../@core/data/interfaces/venta-nueva/steps-cotizador';
import {RegistroService} from '../../../../@core/data/services/venta-nueva/registro.service';
import {CotizacionesAliService} from '../../../../@core/data/services/venta-nueva/cotizaciones-ali.service';

@Component({
  selector: 'app-new-step-online',
  templateUrl: './new-step-online.component.html',
  styleUrls: ['./new-step-online.component.scss']
})
export class NewStepOnlineComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  idSolicitud: number;
  solicitud: SolicitudesView;
  recibos: boolean;
  // Variables para el step de cliente
  idCliente: number = undefined;
  // Variables para el step de producto cliente
  idProductoCliente: number = undefined;
  // Variables para el step de registro poliza
  idRegistro: number = undefined;
  stepsIdentificados = false;
  datosCliente: ClienteVn;
  showStep = false;
  showStepEmitir = false;
  // por lo mientras solo para qualitas
  ocularStepRegistro = false;
  cotizacionAli: CotizacionesAli;
  responseEmision: any;
  coverageArray: Array<any>;
  cobertura: any;
  nombreCobertura: any;
  primaTotall: any;
  precioTotal: any;
  ahorro: any;
  filesToUploadPagado: FileList;
  nombreAseguradorab: string;

  arrayDownladedPDF: Array<{
    solicitudID: number,
    downloaded: boolean
  }> = [];
  stepIcons = [
    'face_retouching_natural',
    'airport_shuttle',
    'payment',
    'post_add',
    'content_paste_search'
  ];
  displayedColumns: string[] = ['nombre', 'sumaAsegurada'];
  dataSource: Array<any>;
  ocultarEmision: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private stepsService: StepsCotizadorService,
    private solicitudesService: SolicitudesvnService,
    private registroData: RegistroService,
    private cotizacionesAliService: CotizacionesAliService
  ) {

    // this.getCotizacion(this.idSolicitud);
  }

  ngOnInit(): void {
    this.idSolicitud = parseFloat(this.route.snapshot.paramMap.get('id_solicitud'));
    // console.log(this.idSolicitud);
    this.nombreAseguradorab = this.route.snapshot.paramMap.get('nombreAseguradora');
    this.downloadedpdfmethod();
    this.getSolicitud();
    this.getCotizacion(this.idSolicitud);
    this.getNuevaEmision();
  }
  private downloadedpdfmethod() {
    const itemDownloadPDF = { solicitudID: this.idSolicitud, downloaded: false };
    if (!localStorage.getItem('arrayDownloadedPdf')) {
      this.arrayDownladedPDF.push(itemDownloadPDF);
      localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownladedPDF));
    } else {
      this.arrayDownladedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
      const resultFind = this.arrayDownladedPDF.find(e => e.solicitudID === this.idSolicitud);
      if (resultFind === undefined) {
        this.arrayDownladedPDF.push(itemDownloadPDF);
        localStorage.removeItem('arrayDownloadedPdf');
        localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownladedPDF));
      }
    }
    console.log('Array Downloaded PDF => ', JSON.parse(localStorage.getItem('arrayDownloadedPdf')));
  }
  getSolicitud() {
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(data => {
        this.solicitud = data;
      }, error => { },
      () => {
        this.getStepActual();
      });
  }
  nextSteper(num?: number) {
    this.stepper.next();
  }
  activarProductoCliente(value: any) {
    this.idProductoCliente = 0;
    this.idCliente = value.idCliente;
    this.stepper.next();
  }
  activarCobro(value: number) {
    this.ocultarEmision = true;
    this.idProductoCliente = value;
    this.datosCliente = { ...this.datosCliente };
    switch (this.nombreAseguradorab) {
      case 'QUALITAS':
        break;
      default:

        break;
    }
    this.nextSteper();
  }
  activarCobrodesdeEmitir(value: any) {
    this.ocultarEmision = true;
    this.idProductoCliente = value;
    this.idRegistro = value.idRegistro;
    this.datosCliente = { ...this.datosCliente };
    this.nextSteper();
  }
  activarEmitir(value: number){
    this.idProductoCliente = value;
    this.showStepEmitir = true;
    this.stepper.next();
  }
  activarRegistroPoliza(value: any) {
    this.idRegistro = 0;
    this.idProductoCliente = value.idproductoc;
    this.datosCliente = { ...this.datosCliente };
    this.filesToUploadPagado = value.filepagado;
    this.nextSteper();
  }
  activarInspecciones(idRegistro: number) {
    this.idRegistro = idRegistro;
    this.stepper.next();
  }
  anterioStep() {
    this.stepper.previous();
  }
  getStepActual() {
    let infoCliente: StepsCotizador;
    this.stepsService.getByIdOnline(this.idSolicitud).subscribe(
      data => {
        infoCliente = data[0];
      },
      () => {
        Swal.fire({
          text: 'Ocurrió un error al cargar la solicitud, se te regresará al menú de solicitudes',
        }).then(() => {
          window.close();
        });
      }, () => {
        if (infoCliente.idProductoCliente !== null) {
          console.log(infoCliente.idCliente);
          this.idCliente = infoCliente.idCliente;
          this.idProductoCliente = infoCliente.idProductoCliente;
          // this.idRegistro = 0;
          this.datosCliente = { ...this.datosCliente };
          this.stepper.selectedIndex = 1;
          // console.log(this.stepper.selectedIndex);
          this.stepper.selectedIndex = 2;
        }
        if (infoCliente.idRegistro !== null) {
          this.idRegistro = infoCliente.idRegistro;
          this.datosCliente = { ...this.datosCliente };
          switch (this.nombreAseguradorab) {
            case 'QUALITAS':
              this.ocultarEmision = true;
              break;
            case 'GNP' :
              break;
            default:

              break;
          }
          // anterior step
          // this.registroData.getRegistroByIdOnline(this.idRegistro).subscribe(data => {
          //   if (infoCliente.recibos > 0 && data.archivo.length > 2) {
          //     if (infoCliente.pagos > 0) {
          //        this.stepper.selectedIndex = 5;
          //     } else {
          //        this.stepper.selectedIndex = 4;
          //     }
          //   } else {
          //      this.stepper.selectedIndex = 2;
          //   }
          // });
          if (infoCliente.pagos > 0) {
                 this.stepper.selectedIndex = 4;
              } else {
                 this.stepper.selectedIndex = 3;
              }
        }
        this.stepsIdentificados = true;
      });
  }
  ultimoStep(step: number) {
    switch (step) {
      case 1:
        this.stepper.selectedIndex = 5;
        break;
      case 2:
        this.stepper.selectedIndex = 6;
        break;
    }

  }

  getCotizacion(idSolicitud) {
    // let dataCotizacion: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.responseEmision = this.cotizacionAli.respuesta;
        const coverages = this.responseEmision['coberturas'];

        this.coverageArray = coverages;
        this.nombreCobertura = this.responseEmision['paquete'];
        this.primaTotall = this.responseEmision['cotizacion']['primaTotal'];
        console.log(this.responseEmision['cotizacion']['primaTotal'])
        this.precioTotal = Math.round((+this.primaTotall / 77) * 100);
        this.ahorro = Math.round((+this.precioTotal) - (+this.primaTotall));
        console.log(this.coverageArray)

        // this.responseEmision['cliente'] = this.clientejson;
        this.cotizacionAli['respuesta'] = this.responseEmision;
      }); // fin cotizacion

    }); // fin solicitud
  }
getNuevaEmision(){
    this.ocultarEmision = false;
    switch (this.nombreAseguradorab) {
    case 'QUALITAS':
      // BANCOS PARA QUALITAS
      this.showStepEmitir = true;
      this.ocultarEmision = false;
      this.ocularStepRegistro = false;
      this.stepIcons = [
        'face_retouching_natural',
        'airport_shuttle',
        'subtitles',
        'payment',
        'content_paste_search'
      ];
      break;
    case 'GNP' :
      this.showStepEmitir = false;
      this.ocultarEmision = true;
      this.ocularStepRegistro = true;
      break;
    default:

      break;
  }
}
}
