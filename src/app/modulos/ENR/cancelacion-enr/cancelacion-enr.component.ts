import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {MatSort, Sort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {AplicacionesViewENRService} from '../../../@core/data/services/ENR/aplicaciones-view-enr.service';

@Component({
  selector: 'app-cancelacion-enr',
  templateUrl: './cancelacion-enr.component.html',
  styleUrls: ['./cancelacion-enr.component.scss']
})
export class CancelacionEnrComponent implements OnInit {
  // -----variables tabla---
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  dataSource;
  displayedColumns: string[] = ['id', 'nombreEmpleado', 'poliza', 'motivo', 'fechaSolicitud'];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  permisos = JSON.parse(window.localStorage.getItem('User'));
  max: any = new Date();
  // -----fin variables tabla---
  extension = localStorage.getItem('extension');
  puedeLlamar: boolean;
  cargando = true;
  numeros: any[];
  errorSpin = false;
  mandarPlantilla = false;
  plantillaEnviada: boolean;
  idPuesto = +sessionStorage.getItem('idPuesto');

  constructor(
    private solicitudesvnService: SolicitudesvnService,
    private notificacionesService: NotificacionesService,
    private aplicacionesViewENRService: AplicacionesViewENRService
  ) {
  }

  ngOnInit(): void {
    this.getPolizasCanceladas();
  }
  getPolizasCanceladas() {
    const fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);
    this.aplicacionesViewENRService.getPolizasCanceladasENR(fechaCompleta1, fechaCompleta2).subscribe({
      next: data => {
        if (data.length === 0) {
          this.notificacionesService.advertencia('No hay solicitudes');
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error: () => {
        this.errorSpin = true;
        this.notificacionesService.error('Error al cargar los datos');
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  announceSortChange(sortState: Sort) {
    this.dataSource.sort = this.sort;
  }

}
