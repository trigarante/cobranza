import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionComponent } from './mostrar-documentos-autorizacion.component';

describe('MostrarDocumentosAutorizacionComponent', () => {
  let component: MostrarDocumentosAutorizacionComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
