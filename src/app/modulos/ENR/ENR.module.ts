import {MaterialModule} from '../material.module';
import {NgModule} from '@angular/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {getSpanishPaginatorIntl} from '../../traduccion-paginator';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {ENRComponent} from './ENR.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ENRRoutingModule} from './ENR-routing.module';
import {AdministracionPolizasComponent} from './administracion-polizas/administracion-polizas.component';
import {InspeccionesComponent} from './inspecciones/inspecciones.component';
import {ModificarProductoClienteComponent} from './modificar-producto-cliente/modificar-producto-cliente.component';
import {ModificarClienteComponent} from './modificar-cliente/modificar-cliente.component';
import {RevisionDatosProductoClienteComponent} from './revision-datos-producto-cliente/revision-datos-producto-cliente.component';
import {
  AutorizacionesProcesoComponent
} from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import {
  CorreccionErroresDocumentosComponent
} from './correccion-errores/correccion-errores-documentos/correccion-errores-documentos.component';
import {
  CorreccionErroresDatosComponent
} from './correccion-errores/correccion-errores-datos/correccion-errores-datos.component';
import {FlexModule} from '@angular/flex-layout';
import {SolicitudesVnInternaComponent} from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import {ReasignarSolicitudComponent} from './modals/reasignar-solicitud/reasignar-solicitud.component';
import {ProspectoCreateComponent} from './modals/prospecto-create/prospecto-create.component';
import {CotizadorComponent} from './cotizador/cotizador.component';
import {StepCotizadorComponent} from './step-cotizador/step-cotizador.component';
import {CreateEmisionComponent} from './componentes-steps/create-emision/create-emision.component';
import {DatosProductoComponent} from './componentes-steps/datos-producto/datos-producto.component';
import {RegistroPolizaComponent} from './componentes-steps/registro-poliza/registro-poliza.component';
import {RecibosComponent} from './componentes-steps/recibos/recibos.component';
import {PagosComponent} from './componentes-steps/pagos/pagos.component';
import {LogAseguradorasComponent} from './vnOnline/log-aseguradoras/log-aseguradoras.component';
import {ModalCotizacionComponent} from './vnOnline/modal-cotizacion/modal-cotizacion.component';
import {MostrarCotizacionComponent} from './vnOnline/mostrar-cotizacion/mostrar-cotizacion.component';
import {NewStepOnlineComponent} from './vnOnline/new-step-online/new-step-online.component';
import {SharedModule} from '../../shared/shared.module';
import {
  CreateEmisionOnlineNewComponent
} from './vnOnline/componentes-step-online/create-emision-online-new/create-emision-online-new.component';
import {
  DatosProductosOnlineNewComponent
} from './vnOnline/componentes-step-online/datos-productos-online-new/datos-productos-online-new.component';
import {EmitirOnlineNewComponent} from './vnOnline/componentes-step-online/emitir-online-new/emitir-online-new.component';
import {InspeccionesOnlineNewComponent} from './vnOnline/componentes-step-online/inspecciones-online-new/inspecciones-online-new.component';
import {PagosOnlineNewComponent} from './vnOnline/componentes-step-online/pagos-online-new/pagos-online-new.component';
import {
  RegistroPolizaOnlineNewComponent
} from './vnOnline/componentes-step-online/registro-poliza-online-new/registro-poliza-online-new.component';
import {InspeccionEnrComponent} from './componentes-steps/inspecciones/inspeccion-enr.component';
import {
  MostrarDocumentosAutorizacionComponent
} from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';
import {ViewerComponent} from './viewer/viewer.component';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {DocumentosCorreccionComponent} from './correccion-errores/documentos-correccion/documentos-correccion.component';
import {
  DatosClienteCorreccionComponent
} from './correccion-errores/componentes/datos-cliente-correccion/datos-cliente-correccion.component';
import {DatosPagoCorreccionComponent} from './correccion-errores/componentes/datos-pago-correccion/datos-pago-correccion.component';
import {
  DatosRegistroCorreccionComponent
} from './correccion-errores/componentes/datos-registro-correccion/datos-registro-correccion.component';
import {
  ProductoClienteCorreccionComponent
} from './correccion-errores/componentes/producto-cliente-correccion/producto-cliente-correccion.component';
import {ViewerCorreccionComponent} from './correccion-errores/componentes/viewer-correccion/viewer-correccion.component';
import {CancelacionEnrComponent} from './cancelacion-enr/cancelacion-enr.component';
import {CancelarRegistroComponent} from './modals/cancelar-registro/cancelar-registro.component';
import {BajaRecibosComponent} from './baja-recibos/baja-recibos.component';
import {AdministradorRecibosComponent} from './componentes-steps/administrador-recibos/administrador-recibos.component';
import {PagosBajasComponent} from './componentes-steps/pagos-baja-recibo/pagos-bajas.component';
import {LinkPagoComponent} from './link-pago/link-pago.component';


@NgModule({
  declarations: [
    ENRComponent,
    AdministracionPolizasComponent,
    InspeccionesComponent,
    ModificarProductoClienteComponent,
    ModificarClienteComponent,
    RevisionDatosProductoClienteComponent,
    AdministracionPolizasComponent,
    AutorizacionesProcesoComponent,
    CorreccionErroresDocumentosComponent,
    CorreccionErroresDatosComponent,
    SolicitudesVnInternaComponent,
    ReasignarSolicitudComponent,
    ProspectoCreateComponent,
    CotizadorComponent,
    StepCotizadorComponent,
    CreateEmisionComponent,
    DatosProductoComponent,
    RegistroPolizaComponent,
    RecibosComponent,
    PagosComponent,
    LogAseguradorasComponent,
    ModalCotizacionComponent,
    MostrarCotizacionComponent,
    NewStepOnlineComponent,
    CreateEmisionOnlineNewComponent,
    DatosProductosOnlineNewComponent,
    EmitirOnlineNewComponent,
    InspeccionesOnlineNewComponent,
    PagosOnlineNewComponent,
    RegistroPolizaOnlineNewComponent,
    InspeccionEnrComponent,
    MostrarDocumentosAutorizacionComponent,
    ViewerComponent,
    DocumentosCorreccionComponent,
    DatosClienteCorreccionComponent,
    DatosPagoCorreccionComponent,
    DatosRegistroCorreccionComponent,
    ProductoClienteCorreccionComponent,
    ViewerCorreccionComponent,
    CancelacionEnrComponent,
    CancelarRegistroComponent,
    BajaRecibosComponent,
    AdministradorRecibosComponent,
    PagosBajasComponent,
    LinkPagoComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    RouterModule,
    ENRRoutingModule,
    FlexModule,
    SharedModule,
    NgxExtendedPdfViewerModule
  ],
  providers: [
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
  exports: [
    MaterialModule,
  ],
  entryComponents: [
  ]
})
export class ENRModule { }
