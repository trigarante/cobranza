import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {AplicacionesViewENRService} from '../../../../@core/data/services/ENR/aplicaciones-view-enr.service';
import {RegistroService} from '../../../../@core/data/services/venta-nueva/registro.service';

@Component({
  selector: 'app-cancelar-registro',
  templateUrl: './cancelar-registro.component.html',
  styleUrls: ['./cancelar-registro.component.scss']
})
export class CancelarRegistroComponent implements OnInit {
  motivosCancelacion: any;
  motivoSeleccionado: any;

  constructor(
    public dialogRef: MatDialogRef<CancelarRegistroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private empleadoService: EmpleadosService,
    private solicitudesService: SolicitudesvnService,
    private notificaciones: NotificacionesService,
    private aplicacionesViewENRService: AplicacionesViewENRService,
    private registroService: RegistroService,
  ) { }

  ngOnInit(): void {
    this.getMotivosCancelacion();
  }
  getMotivosCancelacion() {
    this.aplicacionesViewENRService.getMotivosCancelacionENR().subscribe(data => {
      this.motivosCancelacion = data;
    });
  }
  postCancelacion() {
    const datos = {
      idRegistro: this.data.id,
      poliza: this.data.poliza,
      idEmpleadoBaja: sessionStorage.getItem('Empleado'),
      idMotivo: this.motivoSeleccionado
    };
    this.aplicacionesViewENRService.postCancelada(datos).subscribe({
      complete: () => {
        this.registroService.updateEstadoPoliza(this.data.id, 9, 28).subscribe({
          complete: () => {
            this.notificaciones.exito('La póliza se ha cancelado').then(() => {
              this.dialogRef.close(true);
            });
          }
        });
      },
      error: () => {
        this.notificaciones.advertencia('Favor de contactar a TI', 'Hubo un error al realizar la cancelación');
      },
    });
  }

  dismiss(registroCancealdo: boolean) {
    this.dialogRef.close(registroCancealdo);
  }

}
