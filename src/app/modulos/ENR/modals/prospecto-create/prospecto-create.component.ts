import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {ProspectoService} from '../../../../@core/data/services/venta-nueva/prospecto.service';
import {ProductoSolicitudvnService} from '../../../../@core/data/services/venta-nueva/producto-solicitudvn.service';
@Component({
  selector: 'app-prospecto-create',
  templateUrl: './prospecto-create.component.html',
  styleUrls: ['./prospecto-create.component.scss'],
})
export class  ProspectoCreateComponent implements OnInit {
  prospectoCreateForm: FormGroup = new FormGroup({
    nombre: new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
    numero: new FormControl('', Validators.required),
    correo: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    sexo: new FormControl('', Validators.required),
    edad: new FormControl('', [Validators.required, Validators.pattern('18|19|([2-9][0-9]){1}')]),
  });
  genero: any[] = [
    {label: 'MASCULINO', value: 'M'},
    {label: 'FEMENINO', value: 'F'}
  ];
  datos;
  idProspectoExis: any;
  productoSolicitud = [];
  solicitud: any;
  idSolicitud: number;


  constructor(
              private router: Router,
              protected prospectoService: ProspectoService,
              private productoSolicitudService: ProductoSolicitudvnService,
              @Inject(MAT_DIALOG_DATA) public data,
              private notificaciones: NotificacionesService,
              public dialogRef: MatDialogRef<ProspectoCreateComponent>) {
  }

  guardarProspecto() {
    this.notificaciones.carga();
    const op = this.idProspectoExis ? this.prospectoService.put(this.idProspectoExis, this.prospectoCreateForm.getRawValue()) :
      this.prospectoService.post(this.prospectoCreateForm.getRawValue());
    op.subscribe({
    next: (result) => {
      this.idProspectoExis = result.id;
      this.notificaciones.exito();
    },
    error: () => {
      this.swalError();
    }});
  }

  swalError() {
    this.notificaciones.error('Ocurrió un error al actualizar la solicitud');
  }

  validaCorreo(correo) {
    if (this.prospectoCreateForm.controls.correo.valid) {
      this.prospectoService.validarCorreo(correo).subscribe({
        next: data => {
          if (Object.entries(data).length !== 0) {
            this.idProspectoExis = data.id;
            if (data.productos?.length > 0) {
              for (const producto of data.productos) {
                this.productoSolicitud.push({
                  label: producto.datos.marca + ' ' + producto.datos.descripcion + ' ' + producto.datos.modelo,
                  valor: producto.id,
                });
              }
            }
            this.prospectoCreateForm.patchValue(data);
            this.prospectoCreateForm.controls.correo.disable();
          }
        },
        error: () => this.notificaciones.error(),
      });
    }
  }

  getSolictud(idSolicitud) {
    this.idSolicitud = idSolicitud;
    this.productoSolicitudService.getProductoSolicitud(idSolicitud).subscribe(data => {
      if (data) {
        this.solicitud = data.datos;
      }
    });
  }

  dismiss() {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  abrirCotizador() {
    this.router.navigate([`modulos/ENR/cotizador-vn/${this.idProspectoExis}`]);
    this.dialogRef.close();
  }
}
