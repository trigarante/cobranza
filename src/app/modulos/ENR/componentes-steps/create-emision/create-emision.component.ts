import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild,
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Sepomex} from '../../../../@core/data/interfaces/catalogos/sepomex';
import {Paises} from '../../../../@core/data/interfaces/catalogos/paises';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import {ClienteVnService} from '../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {PaisesService} from '../../../../@core/data/services/catalogos/paises.service';
import {SepomexService} from '../../../../@core/data/services/catalogos/sepomex.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-create-emision',
  templateUrl: './create-emision.component.html',
  styleUrls: ['./create-emision.component.scss']
})
export class CreateEmisionComponent implements OnInit {
  @Input() idCliente: number;
  fechaNacimientoMaxima = new Date();
  prospecto;
  colonias: Sepomex[];
  paisOrigen: Paises[];
  @Input() idSolicitud: number;
  @Output() datosSalida = new EventEmitter<any>();
  @ViewChild('tabGroup') razonSocial;
  clienteForm: FormGroup = new FormGroup({
    curp: new FormControl(null, Validators.compose([Validators.required,
      Validators.pattern('^[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[A-Z-0-9]{2}'),
      Validators.maxLength(18), Validators.minLength(18)])),
    rfc: new FormControl(null, Validators.compose([Validators.required,
      Validators.pattern('[A-Z]{4}[0-9]{6}([A-Z-0-9]{3})?'),
      Validators.maxLength(13), Validators.minLength(10)])),
    nombre: new FormControl(null, Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(150)])),
    paterno: new FormControl(null, Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    materno: new FormControl(null, Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    correo: new FormControl(null, Validators.compose([Validators.required, Validators.email])),
    cp: new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(5),
      Validators.minLength(4), Validators.pattern('[0-9]{4,5}')])),
    idColonia: new FormControl(null),
    calle: new FormControl(null, Validators.required),
    numInt: new FormControl(null),
    numExt: new FormControl(null, Validators.required),
    genero: new FormControl(null, Validators.required),
    telefonoFijo: new FormControl(null, Validators.pattern('[0-9]{10}')),
    telefonoMovil: new FormControl(null, Validators.compose([Validators.required, Validators.pattern('[0-9]{10,13}')])),
    fechaNacimiento: new FormControl(null, Validators.required),
    razonSocial: new FormControl(1),
    idPais: new FormControl(1, Validators.required),
    archivoSubido: new FormControl(0),
    archivo: new FormControl(null),
  });
  genders = [{label: 'MASCULINO', value: 'M'}, {label: 'FEMENINO', value: 'F'}];

  constructor(
    protected clienteService: ClienteVnService,
    protected cpService: SepomexService,
    protected paisesService: PaisesService,
    private solicitudesService: SolicitudesvnService,
    private notificaciones: NotificacionesService) {
  }

  /** Funcion inicial **/
  ngOnInit() {
    this.fechaNacimientoMaxima.setMonth(this.fechaNacimientoMaxima.getMonth() - 216);
    this.getPaises();
    if (this.idCliente) {
      this.getInfoCliente(this.idCliente); // aqui ya es registro poliza
    }
  }

  /** Actualizar requeridos en el formulario **/
  actualizarFormulario(tipoCliente: number) {
    this.clienteForm.controls.razonSocial.setValue(tipoCliente);

    if (tipoCliente === 2) {
      this.clienteForm.controls.curp.clearValidators();
      this.clienteForm.controls.paterno.clearValidators();
      this.clienteForm.controls.materno.clearValidators();
      this.clienteForm.controls.fechaNacimiento.clearValidators();
      this.clienteForm.controls.genero.clearValidators();
      this.clienteForm.controls.rfc.setValidators([
        Validators.pattern('[A-Z]{3}[0-9]{6}([A-Z-0-9]{3})?'),
        Validators.maxLength(12), Validators.minLength(9)]);
    } else {
      this.clienteForm.controls.curp.setValidators(Validators.compose([Validators.required,
        Validators.pattern('^[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[A-Z-0-9]{2}'),
        Validators.maxLength(18), Validators.minLength(18)]));
      this.clienteForm.controls.rfc.setValidators([
        Validators.required,
        Validators.pattern('[A-Z]{4}[0-9]{6}([A-Z-0-9]{3})?'),
        Validators.maxLength(13), Validators.minLength(10)]);
      this.clienteForm.controls.paterno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.clienteForm.controls.materno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.clienteForm.controls.fechaNacimiento.setValidators(Validators.required);
      this.clienteForm.controls.genero.setValidators(Validators.required);
    }
    this.clienteForm.controls.paterno.updateValueAndValidity();
    this.clienteForm.controls.materno.updateValueAndValidity();
    this.clienteForm.controls.fechaNacimiento.updateValueAndValidity();
    this.clienteForm.controls.genero.updateValueAndValidity();
    this.clienteForm.controls.rfc.updateValueAndValidity();
    this.clienteForm.controls.curp.updateValueAndValidity();

    if (tipoCliente === 2) {
      this.clienteForm.controls.genero.setValue(null);
    }
  }

  /** Actualizar cliente **/
  actualizarEmision() {
    this.notificaciones.carga('Actualizando cliente');
    const operacion = this.idCliente ? this.clienteService.put(this.idCliente, this.clienteForm.getRawValue()) :
      this.clienteService.post(this.clienteForm.value);
    operacion.subscribe({
      next: data => {
        if (!this.idCliente) {
          this.idCliente = data.id;
          this.clienteForm.controls[this.clienteForm.controls.razonSocial.value === 1 ? 'curp' : 'rfc'].disable();
        }
        this.notificaciones.exito('¡La información del cliente se ha actualizado con éxito!');
        this.datosProducto();
      },
    });
  }

  getPaises() {
    this.paisesService.get().subscribe(result => {
      this.paisOrigen = result;
    });
  }

  getColonias() {
    if (this.clienteForm.controls.cp.valid) {
      this.cpService.getColoniaByCp(this.clienteForm.controls.cp.value).subscribe(data => {
        this.colonias = data;
      });
    }
  }

  /** Reiniciar el formulario **/
  limpiarFormulario() {
    const idRazonSocial = this.clienteForm.controls.razonSocial.value;
    this.clienteForm.reset(null);
    this.clienteForm.controls.razonSocial.setValue(idRazonSocial);
  }

  /** Get informacion del cliente de la DB **/
  getInfoCliente(idCliente: number) {
    this.clienteService.getClienteById(idCliente).subscribe(result => {
      this.idCliente = result.id;
      this.clienteForm.controls.curp.disable();
      this.clienteForm.patchValue(result);
      if (result.cp.toString().length === 4) {
        this.clienteForm.controls.cp.setValue(('0' + result.cp));
      }
      if (+result.razonSocial === 2) {
        this.clienteForm.controls.curp.clearValidators();
        this.clienteForm.controls.rfc.setValidators([
          Validators.pattern('[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
          Validators.maxLength(12), Validators.minLength(9)]);
        this.clienteForm.controls.paterno.clearValidators();
        this.clienteForm.controls.materno.clearValidators();
        this.clienteForm.controls.fechaNacimiento.clearValidators();
        this.clienteForm.controls.fechaNacimiento.setValue(null);
        this.clienteForm.controls.genero.clearValidators();
        this.clienteForm.controls.rfc.disable();
        this.clienteForm.controls.paterno.updateValueAndValidity();
        this.clienteForm.controls.materno.updateValueAndValidity();
        this.clienteForm.controls.fechaNacimiento.updateValueAndValidity();
        this.clienteForm.controls.genero.updateValueAndValidity();
        this.clienteForm.controls.curp.updateValueAndValidity();
        this.clienteForm.controls.rfc.updateValueAndValidity();
      }
      this.getColonias();
    });
  }

  /** Pasar al siguiente step **/
  datosProducto() {
    this.datosSalida.emit({numero: 0, id: 'idCliente', idCliente: this.idCliente, numAux: 1, idAux: 'idProducto'});
  }

  /** Validar curp cuando es fisico **/
  validarCurp() {
    if (this.clienteForm.controls.curp.valid) {
      this.clienteService.curpExist(this.clienteForm.controls.curp.value)
        .subscribe(data => {
        if (data) {
          this.idCliente = data.id;
          if (this.clienteForm.controls.razonSocial.value !== data.razonSocial) {
            this.actualizarFormulario(data.razonSocial);
          }
          this.clienteForm.patchValue(data);
          this.clienteForm.controls.curp.disable();
          this.getColonias();
        }
      });
    }
  }

  /** Validar RFC cuando es moral **/
  validarRFC() {
    if (this.clienteForm.controls.razonSocial.value === 2 && this.clienteForm.controls.rfc.valid) {
      this.clienteService.rfcExist(this.clienteForm.controls.rfc.value).subscribe(result => {
        if (result) {
          this.idCliente = result.id;
          this.clienteForm.controls.rfc.disable();
          this.clienteForm.patchValue(result);
          this.getColonias();
        }
      });
    }
  }

  getProspecto() {
    this.notificaciones.carga();
    if (!this.prospecto) {
      this.solicitudesService.getDatosProspectoStepCliente(this.idSolicitud).subscribe({
        next: resp => {
        this.prospecto = resp;
        this.showDatosProspecto();
      }});
    } else {
      this.showDatosProspecto();
    }
  }

  showDatosProspecto() {
    const datosProspecto =
      '<style>#swal2-content {' +
      '  text-align: left;' +
      '  margin-left: 40px;' +
      '}</style>' +
      '<strong>Nombre: </strong>' + this.prospecto.nombre + '</br>' +
      '<strong>Email: </strong>' + this.prospecto.correo + '</br>' +
      '<strong>Teléfono: </strong>' + this.prospecto.numero +
      '</br>';
    this.notificaciones.exitoPersonalizado(datosProspecto, 'Los datos de la solicitud son: ');
  }

}
