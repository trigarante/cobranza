import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Recibos} from '../../../../@core/data/interfaces/venta-nueva/recibos';
import {RecibosService} from '../../../../@core/data/services/venta-nueva/recibos.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {RegistroService} from '../../../../@core/data/services/venta-nueva/registro.service';
import {PagarLinkPagoComponent} from '../../../cobranza/modals/pagar-link-pago/pagar-link-pago.component';

@Component({
  selector: 'app-administrador-recibos',
  templateUrl: './administrador-recibos.component.html',
  styleUrls: ['./administrador-recibos.component.scss']
})
export class AdministradorRecibosComponent implements OnInit {
  recibo;
  idRegistro = this.rutaActiva.snapshot.params.idRegistro;
  conjuntoRecibos: Recibos[];
  count: number;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  archivoPago: any;


  constructor(
    private recibosService: RecibosService,
    public dialog: MatDialog,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private registroService: RegistroService,
    private _location: Location

  ) {
 }

  ngOnInit() {
    this.count = 0;
    this.getRegistro();
    this.getArchivos();
  }

  getArchivos() {
    this.registroService.getRegistroByIdViewer(this.idRegistro).subscribe({
      next: data => {
        this.archivoPago = data.archivoPago;
      }
    });
  }

  siguiente() {
    if (this.count < this.conjuntoRecibos.length - 1) {
      this.count ++;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  regresar() {
    if (this.count > 0) {
      this.count--;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  getRegistro() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistro).subscribe(data => {
      this.conjuntoRecibos = data;
      this.recibo = this.conjuntoRecibos[0];
    });
  }

  pagarRecibo() {
    if (this.archivoPago) {
      this.router.navigate(['/modulos/ENR/pagos-recibos', this.recibo.idRegistro]);
    }
    else {
      if (this.rutaActiva.snapshot.params.idSolicitud) {
        this.router.navigate(['/modulos/ENR/pagos-pagos', this.recibo.id, this.recibo.idRegistro, this.rutaActiva.snapshot.params.idSolicitud]);
      } else {
        this.router.navigate(['/modulos/ENR/pagos-pagos', this.recibo.id, this.recibo.idRegistro]);
      }
    }
  }
  cerrar() {
    this._location.back();
  }
  pagarLinkPago() {
    this.dialog.open(PagarLinkPagoComponent, {
      width: '500px',
      data: {idRegistro: this.idRegistro, idRecibo: this.recibo.id}
    });
  }
}
