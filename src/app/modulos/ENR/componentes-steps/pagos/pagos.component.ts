import {Component, ElementRef, Input, Output, EventEmitter, OnInit, ViewChild} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import { FormaPago } from 'src/app/@core/data/interfaces/venta-nueva/catalogos/forma-pago';
import {FormaPagoService} from '../../../../@core/data/services/venta-nueva/catalogos/forma-pago.service';
import {BancoVn} from '../../../../@core/data/interfaces/venta-nueva/catalogos/banco-vn';
import {Carrier} from '../../../../@core/data/interfaces/venta-nueva/catalogos/carrier';
import {CarrierService} from '../../../../@core/data/services/venta-nueva/catalogos/carrier.service';
import {BancoVnService} from '../../../../@core/data/services/venta-nueva/catalogos/banco-vn.service';
import {RecibosService} from '../../../../@core/data/services/venta-nueva/recibos.service';
import {PagosService} from '../../../../@core/data/services/venta-nueva/pagos.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {LogueoAppService} from '../../../../@core/data/services/app/logueo-app.service';
import {LinkPagoService} from '../../../../@core/data/services/link-pago/link-pago.service';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss'],
})
export class PagosComponent implements OnInit {
  formaPagoData: FormaPago[];
  bancosArray: BancoVn[];
  carrierArray: Carrier[];
  idRecibo;
  formPago: FormGroup = new FormGroup( {
    cantidad: new FormControl({value: '', disabled: true}),
    fechaPago: new FormControl({value: new Date(), disabled: true}),
    idFormaPago: new FormControl(''),
  });
  formDatosPago: FormGroup;
  inputType = 'password';
  @ViewChild('fechaV') fechaV: ElementRef;
  ocultarEyePermanent = false;
  filesToUpload: FileList;
  archivoValido = false;
  mesesSinIntereses = [{id: 3, value: '3 meses'}, {id: 6, value: '6 meses'}, {id: 9, value: '9 meses'}, {id: 12, value: '12 meses'}];
  @Input() idRegistro: number;
  @Input() id: number;
  @Input() esStep: boolean;
  @Output() nextStepper = new EventEmitter();

  constructor(
              private pagosService: PagosService,
              private formaPagoService: FormaPagoService,
              private carrierService: CarrierService,
              private bancoService: BancoVnService,
              private recibosService: RecibosService,
              private route: ActivatedRoute,
              private router: Router,
              private notificacionesService: NotificacionesService,
              private appServices: LogueoAppService,
              private linkPagoService: LinkPagoService
  ) {
  }

  ngOnInit() {
    if (!this.esStep) {
      this.idRegistro =  +this.route.snapshot.paramMap.get('idRegistro');
    }
    this.getFormaPago();
    this.getBancos();
    this.getCarriers();
    this.getRecibos();
  }

  getBancos() {
    this.bancoService.get().subscribe(arr => { this.bancosArray = arr; });
  }

  getFormaPago() {
    this.formaPagoService.get().subscribe({
      next: result => {
        this.formaPagoData = result;
      },
      complete: () => {
        this.linkPagoService.getById(this.idRegistro).subscribe(data => {
          this.formPago.controls.idFormaPago.setValue(data.idFormaPago);
          this.formPago.controls.idFormaPago.updateValueAndValidity();
          this.seleccionFormaPago();
          this.formDatosPago.patchValue(data);
          this.formDatosPago.controls.codigo.setValue(data.csv);
          this.formDatosPago.controls.mesVencimiento.setValue(data.mesDeVigencia);
          this.formDatosPago.controls.anioVencimiento.setValue(data.anioDeVigencia.slice(-2));
          if (data.msi && +data.msi > 1) {
            this.formDatosPago.controls.activarMesesSinIntereses.setValue(1);
            const mes = this.mesesSinIntereses.find(dato => dato.id === +data.msi);
            this.formDatosPago.controls.mesesSinIntereses.setValue(mes.value);
          }
        });
      }
    });
  }

  getRecibos() {
    this.recibosService.getForPago(this.idRegistro).subscribe( data => {
      if (data) {
        this.formPago.controls.cantidad.setValue(data.cantidad);
        this.idRecibo = data.id;
      }
    });
  }

  getCarriers() {
    this.carrierService.get().subscribe(data => { this.carrierArray = data; });
  }

  ocultarInput() {
    const verinfo = (this.formDatosPago.controls.numeroTarjeta.valid && this.formDatosPago.controls.toggleCodigo.value ?
      this.formDatosPago.controls.codigoToken.valid : this.formDatosPago.controls.codigo.valid);
    if (!verinfo) {
      this.notificacionesService.error('Datos incompletos');
    } else {
      let countDown = 60;
      Swal.fire({
        icon: 'warning',
        text: `¡Tienes ${countDown} segundos para verificar tus datos!`,
        showConfirmButton: true,
        showCancelButton: true,
      }).then( resp => {
        if (resp.value) {
          this.inputType = 'text';
          this.ocultarEyePermanent = true;
          const time = setInterval(() => {
            countDown--;
            const palabra = countDown > 1 ? 'segundos' : 'segundo';
            const loader =
              `<div>
                <div class="row">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Queda </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; ${palabra} &nbsp;</p>
                  </div>
                </div>
                </div>`;
            switch (countDown) {
              case 0:
                clearInterval(time);
                this.inputType = 'password';
                const Toast0 = Swal.mixin({
                  toast: true,
                  position: 'bottom-end',
                  showConfirmButton: false,
                  timer: 2500,
                });
                Toast0.fire({
                  icon: 'success',
                  width: 165,
                  text: 'Tiempo terminado',
                });
                break;
              default:
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'bottom-end',
                  showConfirmButton: false,
                });
                Toast.fire({
                  width: 180,
                  html: loader,
                });
                break;
            }
          }, 1000);
        }
      });
    }
  }

  seleccionFormaPago() {
    let datosPago;
    switch (this.formPago.controls.idFormaPago.value) {
      case 1:
      case 2:
        datosPago = {
          idBanco: new FormControl('', Validators.required),
          idCarrier: new FormControl('', Validators.required),
          cargoRecurrente: new FormControl(false),
          toggleCodigo: new FormControl(false),
          mesesSinIntereses: new FormControl(''),
          activarMesesSinIntereses: new FormControl(''),
          comentarios: new FormControl(''),
          numeroTarjeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]),
          titular: new FormControl('', Validators.required),
          codigo: new FormControl('', Validators.compose(
            [Validators.minLength(3), Validators.maxLength(5), Validators.required])),
          codigoToken: new FormControl(''),
          mesVencimiento: new FormControl('', [Validators.required]),
          anioVencimiento: new FormControl('', [Validators.required]),
        };
        break;
      case 7:
        datosPago = {
          idBanco: new FormControl('', Validators.required),
          noAutorizacion: new FormControl('', Validators.required),
          comentarios: new FormControl(''),
        };
        break;
      default:
        datosPago = {
          comentarios: new FormControl(''),
        };
        break;
    }
    this.formDatosPago = new FormGroup(datosPago);
  }

  fechaTarjeta(value) {
    if (value.length >= 2 && value.indexOf('/') < 0) {
      this.fechaV.nativeElement.value =
        this.fechaV.nativeElement.value.substring(0, 2) + '/' +
        this.fechaV.nativeElement.value.substring(2, 6);
    }

    if (value.charAt(1) === '/') {
      this.fechaV.nativeElement.value = this.fechaV.nativeElement.value.substring(0, 1) + '' + '/' +
        this.fechaV.nativeElement.value.substring(2, 6);
    }
    this.fechaV.nativeElement.value =  this.fechaV.nativeElement.value.substring(0, 5);
  }

  desactivarBoton(): boolean {
    return !(this.formDatosPago && this.formDatosPago.valid && this.archivoValido);
  }

  subirADrive() {
   this.notificacionesService.carga('Subiendo el comprobante de pago ingresado');
   const pago = {
      idRecibo: this.idRecibo,
      idFormaPago: this.formPago.controls.idFormaPago.value,
      idEmpleado: +sessionStorage.getItem('Empleado'),
      fechaPago: this.formPago.controls.fechaPago.value,
      cantidad: this.formPago.controls.cantidad.value,
      datos: this.formDatosPago.value,
    };
   if(this.route.snapshot.paramMap.get('id')) {
     this.pagosService.subirDrive(this.filesToUpload, this.route.snapshot.paramMap.get('id'), pago).subscribe({
       next: data => {
         this.appServices.enviarEmail(this.idRegistro).subscribe(data => {
           console.log('correo enviado');
         });
         this.id = data.id;
         if (this.esStep) {
           this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
             this.nextStepper.emit({numero: 4, id: 'idPago', idPago: this.id, idAux: 'idInspeccion', numAux: 5});
           });
         } else {
           this.notificacionesService.exito().then(() => this.router.navigate(['modulos/ENR/administracion-poliza']));
         }
       },
       error: () => {
         this.notificacionesService.error();
       },
     });
   } else {
     this.pagosService.subirDriveReno(this.filesToUpload, this.idRegistro, pago).subscribe({
       next: data => {
         this.appServices.enviarEmail(this.idRegistro).subscribe(data => {
           console.log('correo enviado');
         });
         this.id = data.id;
         if (this.esStep) {
           this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
             this.nextStepper.emit({numero: 4, id: 'idPago', idPago: this.id, idAux: 'idInspeccion', numAux: 5});
           });
         } else {
           this.notificacionesService.exito().then(() => this.router.navigate(['modulos/ENR/administracion-poliza']));
         }
       },
       error: () => {
         this.notificacionesService.error();
       },
     });
   }

  }

  cambioCT() {
    if (this.formDatosPago.controls.toggleCodigo.value) {
      this.formDatosPago.controls.codigoToken.setValidators(Validators.compose(
        [Validators.minLength(3), Validators.maxLength(6), Validators.required]));
      this.formDatosPago.controls.codigo.clearValidators();
    } else {
      this.formDatosPago.controls.codigo.setValidators(Validators.compose(
        [Validators.minLength(3), Validators.maxLength(5), Validators.required]));
      this.formDatosPago.controls.codigoToken.clearValidators();
    }
    this.formDatosPago.controls.codigo.updateValueAndValidity();
    this.formDatosPago.controls.codigoToken.updateValueAndValidity();
  }

  borrarMeses() {
    if (!this.formDatosPago.controls.activarMesesSinIntereses.value) {
      this.formDatosPago.controls.mesesSinIntereses.setValue(null);
    }
  }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.archivoValido = false;
    this.filesToUpload = (event.target as HTMLInputElement).files;
    if (this.filesToUpload.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
      const tamanioArchivo = this.filesToUpload.item(0).size * .000001;
      if (this.filesToUpload.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          this.notificacionesService.exito('¡El archivo está listo para ser guardado!');
          this.archivoValido = true;
        } else {
          const titulo = !extensionValida ? 'Extensión no soportada' : 'Archivo demasiado grande';
          const texto = !extensionValida ? 'Solo puedes subir archivos pdf' : 'Los archivos que subas deben pesar menos de 7 MB';
          this.notificacionesService.error(texto, titulo);
        }
      }
    }
  }
}
