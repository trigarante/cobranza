import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ClienteVnService} from '../../../@core/data/services/venta-nueva/cliente-vn.service';
import {SepomexService} from '../../../@core/data/services/catalogos/sepomex.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {PaisesService} from '../../../@core/data/services/catalogos/paises.service';
import {SolicitudesvnService} from '../../../@core/data/services/venta-nueva/solicitudes.service';
import {ValidacionTelefonoService} from '../../../@core/data/services/venta-nueva/validacion-telefono.service';
import {DriveService} from '../../../@core/data/services/ti/drive.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClienteVn} from '../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {Sepomex} from '../../../@core/data/interfaces/catalogos/sepomex';
import {Paises} from '../../../@core/data/interfaces/catalogos/paises';
import Swal from 'sweetalert2';
import {map} from 'rxjs/operators';
import {RevisionDatosProductoClienteComponent} from '../revision-datos-producto-cliente/revision-datos-producto-cliente.component';
import {ProductoClienteService} from '../../../@core/data/services/venta-nueva/producto-cliente.service';

@Component({
  selector: 'app-modificar-cliente',
  templateUrl: './modificar-cliente.component.html',
  styleUrls: ['./modificar-cliente.component.scss']
})
export class ModificarClienteComponent implements OnInit {

  @ViewChild('tabGroup') razonSocial;

  /** Expresión regular para un número. */
  numero: RegExp = /[0-9]+/;
  emisionFisicaForm: FormGroup;
  emisionMoralForm: FormGroup;
  cliente: ClienteVn;
  clienteMoral: ClienteVn;
  /** Arreglo que se inicializa con los valores del campo sepomex desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  /**Variable que activa opcion de actualizar o crear si existe curp o rfc*/
  idTipo: boolean;
  /** Variable de tipo "item seleccionado" para almacenar el género (masculino y femenino). */
  genders: any[] = [{label: 'MASCULINO', value: 'M'}, {label: 'FEMENINO', value: 'F'}];
  /** Arreglo que se inicializa con los valores del campo paises desde el Microservicio. */
  paisOrigen: Paises[];
  /** Arreglo auxiliar para paisOrigen. */
  pais: any[];
  /**Variable que almacena el idCliente al validar si existe el curp o rfc*/
  idCliente: number;
  /** FUNCIONA CON EL LOADER DE LOS BOTONES QUE LLEVAN UNA PETICIÓN **/
  gruposCargados = false;
  /**--Variable que recibe la respuesta del servicio de validacion de telefono**/
  telefonoFijo1;
  telefonoFijo2;
  telefonoMovil1;
  telefonoMovil2;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  filesToUpload: FileList;
  archivoValido = false;
  bote = false;
  disabled: boolean;
  btnRegistroProducto: boolean;
  btnLoad = false;
  private textError = 5;
  areasCargados = true;
  seleccion: any;
  productoCliente: any;
  constructor(public dialogRef: MatDialogRef<ModificarClienteComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private dialog: MatDialog,
              protected clienteService: ClienteVnService,
              protected sepomexService: SepomexService,
              private notificaciones: NotificacionesService,
              protected paisesService: PaisesService,
              private solicitudesvnService: SolicitudesvnService,
              private telefonoService: ValidacionTelefonoService,
              private productoClienteService: ProductoClienteService,
              private driveService: DriveService) {
    this.btnRegistroProducto = true;
  }

  ngOnInit() {
    this.productoClienteService.getProductoClienteById(this.data.idProducto).subscribe( data => {
      this.productoCliente = data;
    });
    this.btnRegistroProducto = true;
    this.idTipo = true;
    this.disabled = true;
    this.getPaises();
    this.emisionFisicaForm = new FormGroup({
      'curp': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(18), Validators.minLength(18)])),
      'rfc': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
        Validators.maxLength(13), Validators.minLength(10)])),
      'nombre': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)])),
      'paterno': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)])),
      'materno': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)])),
      'correo': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'cp': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5),
        Validators.maxLength(5), Validators.pattern('[0-9]{5}')])),
      'idColonia': new FormControl('', Validators.compose([Validators.required])),
      'calle': new FormControl('', Validators.compose([Validators.required])),
      'numInt': new FormControl(''),
      'numExt': new FormControl('', Validators.required),
      'genero': new FormControl('', Validators.required),
      'telefonoFijo': new FormControl('', Validators.compose([Validators.minLength(10),
        Validators.maxLength(10), Validators.pattern('[0-9]+')])),
      'telefonoMovil': new FormControl('', Validators.compose([Validators.required, Validators.minLength(10),
        Validators.maxLength(10), Validators.pattern('[0-9]+')])),
      'fechaNacimiento': new FormControl('', Validators.compose([Validators.required])),
      'razonSocial': new FormControl(''),
      'idPais': new FormControl(null, Validators.required),
    });
    this.emisionFisicaForm.controls['razonSocial'].setValue(1);

    this.emisionMoralForm = new FormGroup({
      'rfc': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
        Validators.maxLength(12), Validators.minLength(9)])),
      'nombre': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(100)])),
      'correo': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'cp': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5),
        Validators.maxLength(5), Validators.pattern('[0-9]{5}')])),
      'idColonia': new FormControl('', Validators.compose([Validators.required])),
      'calle': new FormControl('', Validators.compose([Validators.required])),
      'numInt': new FormControl(''),
      'numExt': new FormControl('', Validators.required),
      'telefonoFijo': new FormControl('', Validators.compose([Validators.maxLength(10),
        Validators.minLength(10), Validators.required, Validators.pattern('[0-9]+')])),
      'telefonoMovil': new FormControl('', Validators.compose([Validators.maxLength(10),
        Validators.minLength(10), Validators.required, Validators.pattern('[0-9]+')])),
      'razonSocial': new FormControl(''),
      'idPais': new FormControl('', Validators.required),
    });
    this.emisionMoralForm.controls['razonSocial'].setValue(2);
    if (this.data !== null) {
      this.bote = true;
      this.btnRegistroProducto = true;
      this.getInfoCliente(this.data.idCliente); // aqui ya es registro poliza
    } else {
      return;
    }
  }

  actualizarEmision() {
    this.disabled = false;
    this.notificaciones.carga('Actualizando el cliente ingresado');
    if (this.idCliente) {
      if (this.razonSocial.selectedIndex === 1) {
        this.emisionMoralForm.controls['razonSocial'].setValue(2);
        this.clienteService.put(this.idCliente, this.emisionMoralForm.getRawValue()).subscribe({
          next: () => {
            this.productoCliente.idCliente = this.idCliente;
          },
          complete: () => {
            this.productoClienteService.put(this.data.idProducto, this.productoCliente).subscribe({
              complete: () => {
                Swal.fire({
                  title: 'Información actualizada',
                  text: '¡La información del cliente se ha actualizado con éxito!',
                  icon: 'success',
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  onClose: () => {
                    this.disabled = true;
                    this.gruposCargados = false;
                    this.dialogRef.close();
                  },
                });
              }
            });
          },
        });
      } else {
        this.emisionFisicaForm.controls['razonSocial'].setValue(1);
        this.gruposCargados = true;
        this.clienteService.put(this.idCliente, this.emisionFisicaForm.getRawValue()).subscribe({
          next: () => {
            this.productoCliente.idCliente = this.idCliente;
          },
          complete: () => {
            this.productoClienteService.put(this.data.idProducto, this.productoCliente).subscribe({
              complete: () => {
                Swal.fire({
                  title: 'Información actualizada',
                  text: '¡La información del cliente se ha actualizado con éxito!',
                  icon: 'success',
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  onClose: () => {
                    this.disabled = true;
                    this.gruposCargados = false;
                    this.dialogRef.close();
                  },
                });
              }
            });
          },
        });
      }
    }
    else {
      if (this.razonSocial.selectedIndex === 1) {
        this.emisionMoralForm.controls['razonSocial'].setValue(2);
        this.clienteService.post(this.emisionMoralForm.getRawValue()).subscribe({
          next: result => {
            this.productoCliente.idCliente = result.id;
          },
          complete: () => {
            this.productoClienteService.put(this.data.idProducto, this.productoCliente).subscribe({
              complete: () => {
                Swal.fire({
                  title: 'Información guardada',
                  text: '¡La información del cliente se ha guardado con éxito!',
                  icon: 'success',
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  onClose: () => {
                    this.disabled = true;
                    this.gruposCargados = false;
                    this.dialogRef.close();
                  },
                });
              }
            });
          },
        });
      } else {
        this.emisionFisicaForm.controls['razonSocial'].setValue(1);
        this.gruposCargados = true;
        this.clienteService.post(this.emisionFisicaForm.getRawValue()).subscribe({
          next: result => {
            this.productoCliente.idCliente = result.id;
          },
          complete: () => {
            this.productoClienteService.put(this.data.idProducto, this.productoCliente).subscribe({
              complete: () => {
                Swal.fire({
                  title: 'Información guardada',
                  text: '¡La información del cliente se ha guardado con éxito!',
                  icon: 'success',
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  onClose: () => {
                    this.disabled = true;
                    this.gruposCargados = false;
                    this.dialogRef.close();
                  },
                });
              }
            });
          },
        });
      }
    }
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.areasCargados = false;
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
        this.paisOrigen = result;
        this.pais = [];
        for (let i = 0; i < this.paisOrigen.length; i++) {
          this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
        }
      },
      () => {},
      () => {
        this.areasCargados = true;
      });
  }

  /** Función que obtiene las colonias y las almacena en el arreglo "colonias" */
  getColonias(cp) {
    if (this.emisionFisicaForm.controls['cp'].valid || this.emisionMoralForm.controls['cp'].valid) {
      this.sepomexService.getColoniaByCp(cp).subscribe(data => {
        this.coloniasSepomex = data;
        this.btnLoad = false;
      });

      const cpParse = Number(cp);
      this.sepomexService.getColoniaByCp(cpParse).subscribe(res => {
        if (res.length !== 0) {
          this.emisionMoralForm.controls['cp'].setErrors(null);
        } else {
          this.emisionMoralForm.controls['cp'].setErrors({'incorrect': true});
        }
      }, error => error);
    }

  }

  limpiarFormulario() {
    if (this.razonSocial.selectedIndex === 0) {
      this.emisionFisicaForm.controls['curp'].enable();
      this.emisionFisicaForm.reset('');
      this.emisionFisicaForm.controls['razonSocial'].setValue(1);
    } else {
      this.emisionMoralForm.reset('');
      this.emisionFisicaForm.controls['razonSocial'].setValue(2);
    }
    this.idTipo = true;
  }

  validarCurp(curp) {
    if (curp.length === 18) {
      this.clienteService.curpExist
      (curp).subscribe(result => {
        if (result) {
          this.bote = true;
          this.idTipo = false;
          this.cliente = result;
          this.idCliente = result.id;
          this.emisionFisicaForm.controls['curp'].setValue(this.cliente.curp);
          this.emisionFisicaForm.controls['nombre'].setValue(this.cliente.nombre);
          this.emisionFisicaForm.controls['paterno'].setValue(this.cliente.paterno);
          this.emisionFisicaForm.controls['materno'].setValue(this.cliente.materno);
          this.emisionFisicaForm.controls['correo'].setValue(this.cliente.correo);
          this.emisionFisicaForm.controls['cp'].setValue(this.cliente.cp);
          this.getColonias(this.cliente.cp);
          this.emisionFisicaForm.controls['idColonia'].setValue(this.cliente.idColonia);
          this.emisionFisicaForm.controls['calle'].setValue(this.cliente.calle);
          this.emisionFisicaForm.controls['numInt'].setValue(this.cliente.numInt);
          this.emisionFisicaForm.controls['numExt'].setValue(this.cliente.numExt);
          this.emisionFisicaForm.controls['genero'].setValue(this.cliente.genero);
          this.emisionFisicaForm.controls['telefonoFijo'].setValue(this.cliente.telefonoFijo);
          this.emisionFisicaForm.controls['telefonoMovil'].setValue(this.cliente.telefonoMovil);
          this.emisionFisicaForm.controls['fechaNacimiento'].setValue(this.cliente.fechaNacimiento);
          this.emisionFisicaForm.controls['rfc'].setValue(this.cliente.rfc);
          this.emisionFisicaForm.controls['idPais'].setValue(1);
        }
        else {
          this.idCliente = null;
          this.emisionFisicaForm.reset();
          this.emisionFisicaForm.controls['curp'].setValue(curp);
        }
      });
    }
  }

  validarRFC(rfc) {
    if (this.emisionMoralForm.controls.rfc.value.length === 13) {
      this.clienteService.rfcExist(this.emisionMoralForm.controls.rfc.value).subscribe(result => {
        if (result !== null) {
          this.idCliente = result.id;
          this.idTipo = true;
          this.cliente = result;
          this.emisionMoralForm.controls['nombre'].setValue(this.cliente.nombre);
          this.emisionMoralForm.controls['correo'].setValue(this.cliente.correo);
          this.emisionMoralForm.controls['cp'].setValue(this.cliente.cp);
          this.getColonias(this.cliente.cp);
          this.emisionMoralForm.controls['idColonia'].setValue(this.cliente.idColonia);
          this.emisionMoralForm.controls['calle'].setValue(this.cliente.calle);
          this.emisionMoralForm.controls['numInt'].setValue(this.cliente.numInt);
          this.emisionMoralForm.controls['numExt'].setValue(this.cliente.numExt);
          this.emisionMoralForm.controls['telefonoFijo'].setValue(this.cliente.telefonoFijo);
          this.emisionMoralForm.controls['telefonoMovil'].setValue(this.cliente.telefonoMovil);
          this.emisionMoralForm.controls['idPais'].setValue(this.cliente.idPais);
        }
        else {
          this.idCliente = null;
          this.emisionMoralForm.reset();
          this.emisionMoralForm.controls['rfc'].setValue(rfc);
        }
      });
    }
  }

  getInfoCliente(idCliente: number) {
    this.areasCargados = false;
    this.clienteService.getClienteById(idCliente).subscribe(result => {
        if (Number(result.razonSocial) === 1) {
          this.razonSocial.selectedIndex = 0;
          this.idTipo = false;
          this.cliente = result;
          // desde el get las colonias que empiezan con "0" se los quita, por esa razon el if
          let cpCero = String(this.cliente.cp);
          if (cpCero.toString().length === 4) {
            cpCero = `0${this.cliente.cp}`;
            this.emisionFisicaForm.controls['cp'].setValue(cpCero);
            this.btnLoad = true;
            this.getColonias(cpCero);
            this.emisionFisicaForm.controls['idColonia'].setValue(this.cliente.idColonia);
          } else {
            this.emisionFisicaForm.controls['cp'].setValue(this.cliente.cp);
            this.btnLoad = true;
            this.getColonias(this.cliente.cp);
            this.emisionFisicaForm.controls['idColonia'].setValue(this.cliente.idColonia);
          }
          this.idCliente = idCliente;
          this.emisionFisicaForm.controls['curp'].setValue(this.cliente.curp);
          this.emisionFisicaForm.controls['nombre'].setValue(this.cliente.nombre);
          this.emisionFisicaForm.controls['paterno'].setValue(this.cliente.paterno);
          this.emisionFisicaForm.controls['materno'].setValue(this.cliente.materno);
          this.emisionFisicaForm.controls['correo'].setValue(this.cliente.correo);
          this.emisionFisicaForm.controls['calle'].setValue(this.cliente.calle);
          this.emisionFisicaForm.controls['numInt'].setValue(this.cliente.numInt);
          this.emisionFisicaForm.controls['numExt'].setValue(this.cliente.numExt);
          this.emisionFisicaForm.controls['genero'].setValue(this.cliente.genero);
          this.emisionFisicaForm.controls['telefonoFijo'].setValue(this.cliente.telefonoFijo);
          this.emisionFisicaForm.controls['telefonoMovil'].setValue(this.cliente.telefonoMovil);
          this.emisionFisicaForm.controls['fechaNacimiento'].setValue(this.cliente.fechaNacimiento + 'T06:00:00');
          this.emisionFisicaForm.controls['rfc'].setValue(this.cliente.rfc);
          this.emisionFisicaForm.controls['idPais'].setValue(1);
        } else {
          this.razonSocial.selectedIndex = 1;
          this.idTipo = false;
          this.clienteMoral = result;
          this.idCliente = idCliente;
          this.emisionMoralForm.controls['rfc'].setValue(this.clienteMoral.rfc);
          this.emisionMoralForm.controls['nombre'].setValue(this.clienteMoral.nombre);
          this.emisionMoralForm.controls['cp'].setValue(this.clienteMoral.cp);
          this.getColonias(this.clienteMoral.cp);
          this.emisionMoralForm.controls['idColonia'].setValue(this.clienteMoral.idColonia);
          this.emisionMoralForm.controls['calle'].setValue(this.clienteMoral.calle);
          this.emisionMoralForm.controls['numInt'].setValue(this.clienteMoral.numInt);
          this.emisionMoralForm.controls['numExt'].setValue(this.clienteMoral.numExt);
          this.emisionMoralForm.controls['correo'].setValue(this.clienteMoral.correo);
          this.emisionMoralForm.controls['telefonoFijo'].setValue(this.clienteMoral.telefonoFijo);
          this.emisionMoralForm.controls['telefonoMovil'].setValue(this.clienteMoral.telefonoMovil);
          this.emisionMoralForm.controls['idPais'].setValue(1);
        }
      }, () => {},
      () => {
        this.areasCargados = true;
      });
  }
  modalError() {
    this.notificaciones.error('Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información', 'Error' );
  }

}
