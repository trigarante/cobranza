import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendientesVerificacionComponent } from './pendientes-verificacion.component';

describe('PendientesVerificacionComponent', () => {
  let component: PendientesVerificacionComponent;
  let fixture: ComponentFixture<PendientesVerificacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendientesVerificacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendientesVerificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
