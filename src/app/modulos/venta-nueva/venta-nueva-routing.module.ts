import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VentaNuevaComponent } from './venta-nueva.component';
import {CommonModule} from '@angular/common';
import {PendientesVerificacionComponent} from './pendientes-verificacion/pendientes-verificacion.component';
import {MiEstadoCuentaComponent} from './mi-estado-cuenta/mi-estado-cuenta.component';

const routes: Routes = [{
  path: '',
  component: VentaNuevaComponent,
  children: [
    {
      path: 'pendientes-verificacion',
      component: PendientesVerificacionComponent
    },
    {
      path: 'mi-estado-cuenta',
      component: MiEstadoCuentaComponent
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VentaNuevaRoutingModule { }
