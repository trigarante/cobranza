import {MaterialModule} from '../material.module';
import {NgModule} from '@angular/core';
import {VentaNuevaComponent} from './venta-nueva.component';
import {PendientesVerificacionComponent} from './pendientes-verificacion/pendientes-verificacion.component';
import {CommonModule} from '@angular/common';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {getSpanishPaginatorIntl} from '../../traduccion-paginator';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {RouterModule} from '@angular/router';
import {VentaNuevaRoutingModule} from './venta-nueva-routing.module';
import {MiEstadoCuentaComponent} from './mi-estado-cuenta/mi-estado-cuenta.component';


@NgModule({
  declarations: [
    VentaNuevaComponent,
    PendientesVerificacionComponent,
    MiEstadoCuentaComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    RouterModule,
    VentaNuevaRoutingModule
  ],
  providers: [
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
  exports: [
    MaterialModule,
  ],
  entryComponents: [
  ]
})
export class VentaNuevaModule { }
