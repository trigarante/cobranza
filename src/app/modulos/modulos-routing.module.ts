import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ModulosComponent} from './modulos.component';
import {AuthGuard} from '../@core/data/services/login/auth.guard';
import {DashboardComponent} from './Dashboard/dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: ModulosComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: () => import('./Dashboard/dashboard.module').then(m => m.DashboardModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'subsecuentes',
      loadChildren: () => import('./subsecuentes/subsecuentes.module').then(m => m.SubsecuentesModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'cobranza',
      loadChildren: () => import('./cobranza/cobranza.module').then(m => m.CobranzaModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'venta-nueva',
      loadChildren: () => import('./venta-nueva/venta-nueva.module').then(m => m.VentaNuevaModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'ENR',
      loadChildren: () => import('./ENR/ENR.module').then(m => m.ENRModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'telefonia',
      loadChildren: () => import('./telefonia/telefonia.module').then(m => m.TelefoniaModule),
      canActivate: [AuthGuard]
    },
    {
      path: '**',
      component: DashboardComponent,
      canActivate: [AuthGuard]
    },
  ]

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosRoutingModule {
}
