import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {interval, Subscription} from 'rxjs';
import moment from 'moment';
import {MonitoreoService} from '../../../@core/data/services/telefonia/monitoreo.service';
import {KeypadComponent} from '../modals/keypad/keypad.component';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import { LiveAnnouncer } from '@angular/cdk/a11y';

@Component({
  selector: 'app-en-llamadas-out',
  templateUrl: './en-llamadas-out.component.html',
  styleUrls: ['./en-llamadas-out.component.scss']
})
export class EnLlamadasOutComponent implements OnInit, OnDestroy, AfterViewInit{

  displayColums: string [] = ['id', 'nombre', 'extension', 'tipoLlamada' , 'fechaInicio', 'numero', 'campania', 'tiempo', 'acciones'];
  displayUserColums: string [] = ['extension', 'nombre', 'estado', 'campania'];
  dataSource: MatTableDataSource<any>;
  usersSource: MatTableDataSource<any>;
  timerPeticion: Subscription;
  pausas: any[];
  counter = 15;
  counter2 = 15;

  interval: any;
  extension = localStorage.getItem('extension');
  permisos = JSON.parse(window.localStorage.getItem('User'));
  puedeLlamar: boolean;
  pjsipEvents: Subscription;
  @ViewChild('paginadorLlamadas', {read: MatPaginator}) paginadorLlamadas: MatPaginator;
  @ViewChild('sortLlamadas', {read: MatSort}) sortLlamadas: MatSort;

  @ViewChild('paginadorUsuarios', {read: MatPaginator}) paginadorUsuarios: MatPaginator;
  @ViewChild('sortUsuarios', {read: MatSort}) sortUsuarios: MatSort;
  @ViewChild(MatSort) sort: MatSort;



  constructor(private monitoreoService: MonitoreoService,
              private _liveAnnouncer: LiveAnnouncer,
              private webRTCService: WebrtcService,
              public dialog: MatDialog) {
    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }

  ngOnInit() {
    this.getData();
    this.listenPjsipEvents();
  }

  listenPjsipEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.pjsipEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            this.puedeLlamar = true;
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.pjsipEvents) { this.pjsipEvents.unsubscribe(); }
  }

  initTimer() {
    this.timerPeticion = interval(15000).subscribe(() => {
      this.getLlamadas();
    });
  }

  intervalo() {
    this.interval = setInterval(() => {
      this.counter = this.counter - 1;
      if (this.counter === 0) {
        this.counter = 15;
      }
    }, 1000);
  }

  getLlamadas() {
   const resp = this.monitoreoService.getCurrentLlamadas();
   resp.subscribe((report: any[]) => {
      this.pausas = report;
      this.pausas.forEach((llamada) => {
        const fecha1 = moment();
        const fecha2 = moment(llamada.fechaInicio);
        llamada.tiempo = this.segudosAhoras(fecha1.diff(fecha2, 'seconds'));
      });
      this.dataSource = new MatTableDataSource(this.pausas);
      this.dataSource.paginator = this.paginadorLlamadas;
      this.dataSource.sort = this.sortLlamadas;
    });

  }

  getUsuarios() {
    this.monitoreoService.getUsersState().subscribe((report: any[]) => {
      this.usersSource = new MatTableDataSource(report);
      this.usersSource.sort = this.sortUsuarios;
      this.usersSource.paginator = this.paginadorUsuarios;
    });
  }

  getData() {
    this.getLlamadas();
    this.getUsuarios();
  }
  segudosAhoras(segundos) {
    let hour: any = Math.floor(segundos / 3600);
    hour = (hour < 10) ? '0' + hour : hour;
    let minute: any = Math.floor((segundos / 60) % 60);
    minute = (minute < 10) ? '0' + minute : minute;
    let second: any = segundos % 60;
    second = (second < 10) ? '0' + second : second;
    return hour + ':' + minute + ':' + second;
  }

  openSupervisar(extension, nombreEjecutivo, tipoSupervision, idLlamada) {
    // this.timerPeticion.unsubscribe();
    const dialog = this.dialog.open(KeypadComponent, {
      data: {
        extension,
        nombreEjecutivo,
        tipoSupervision,
        idLlamada,
      },
      disableClose: true,
    });
    dialog.afterClosed().subscribe((resp) => {
      this.getData();
      // this.observableChequeo = interval(15000).subscribe(() => {
      //   this.getEjecutivos();
      // });
      // this.initTimer();
    });
  }

  applyFilterLlamadas(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilterUsuarios(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.usersSource.filter = filterValue.trim().toLowerCase();

    if (this.usersSource.paginator) {
      this.usersSource.paginator.firstPage();
    }
  }

  ngAfterViewInit() {
  }
  announceSortChange(sortState: Sort) {
    this.dataSource.sort = this.sort;
  }

}
