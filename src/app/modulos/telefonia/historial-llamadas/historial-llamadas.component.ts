import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {HistorialLlamadasService} from '../../../@core/data/services/telefonia/historial-llamadas.service';
import {ActivatedRoute, Params} from '@angular/router';
import {GrabacionesService} from '../../../@core/data/services/telefonia/grabaciones.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-historial-llamadas',
  templateUrl: './historial-llamadas.component.html',
  styleUrls: ['./historial-llamadas.component.scss']
})
export class HistorialLlamadasComponent implements OnInit  {
  displayedColumns: string[] = ['idLlamada', 'idAsterisk', 'idSolicitud', 'fechaInicio', 'fechaFinal', 'acciones'];
  dataSource: any;
  idSolicitud: string;
  idRegistro: string;
  loading = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private historialLlamadasService: HistorialLlamadasService,
              private grabacionesService: GrabacionesService,
              private notificacionesService: NotificacionesService,
              private route: ActivatedRoute) {
    this.route.params.subscribe((params: Params) => {
      this.idSolicitud = params.idSolicitud;
      this.idRegistro = params.idRegistro;
    });
  }

  ngOnInit(): void {
    if (this.idSolicitud) {
      this.getByIdSolicitud();
    }
    if (this.idRegistro) {
      this.getByIdRegistro();
    }
  }

  getByIdSolicitud() {
    this.historialLlamadasService.getHistorialLlamadasByIdSolicitud(this.idSolicitud).subscribe((result: []) => {
      if (!result.length) { return this.notificacionesService.informacion('No se encontraron llamadas'); }
      result.forEach((llamada: any) => {
        llamada.link = '';
        llamada.cargando =  false;
      });
      this.dataSource = new MatTableDataSource<any>(result);
      this.dataSource.paginator = this.paginator;
    });
  }

  getByIdRegistro() {
    this.historialLlamadasService.getHistorialLlamadasByidRegistro(this.idRegistro).subscribe((result: []) => {
      if (!result.length) { return this.notificacionesService.informacion('No se encontraron llamadas'); }
      result.forEach((llamada: any) => {
        llamada.link = '';
        llamada.cargando =  false;
      });
      this.dataSource = new MatTableDataSource<any>(result);
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  escucharGrabacion(idAsterisk, llamada) {
    this.loading = true;
    llamada.cargando = true;
    this.grabacionesService.getGrabacion(idAsterisk).subscribe((grabacion: any) => {
      if (!grabacion.length) {
        this.loading = false;
        llamada.cargando = false;
        return this.notificacionesService.advertencia('Intenta más tarde', 'Grabación no disponible');
      } else {
        llamada.link = grabacion[0].webContentLink;
        llamada.cargando = false;
        this.loading = false;
      }
    }, () => {
      this.loading = false;
      llamada.cargando = false;
      this.notificacionesService.advertencia('Intenta más tarde', 'Grabación no disponible');
    });
  }
}
