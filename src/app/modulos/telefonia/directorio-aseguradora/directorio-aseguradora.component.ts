import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NumeroAseguradoraService} from '../../../@core/data/services/telefonia/numero-aseguradora.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LlamadaSalidaComponent} from '../modals/llamada-salida/llamada-salida.component';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';
import {Subscription} from 'rxjs';
import { EstadoUsuarioTelefoniaService } from 'src/app/@core/data/services/telefonia/estado-usuario-telefonia.service';

export interface direcAseg{
  aseguradora: string;
  numero: any;
  acciones: any;
}

@Component({
  selector: 'app-directorio-aseguradora',
  templateUrl: './directorio-aseguradora.component.html',
  styleUrls: ['./directorio-aseguradora.component.scss']
})
export class DirectorioAseguradoraComponent implements OnInit, OnDestroy {

  numerosAsegurado: direcAseg[] = [];
  displayColums: string [] = ['aseguradora', 'numero', 'acciones'];
  dataSource = new MatTableDataSource<direcAseg>(this.numerosAsegurado);
  extension = localStorage.getItem('extension');
  puedeLlamar: boolean;
  webRtcEvents: Subscription;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  idUsuario = sessionStorage.getItem('Usuario');

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private directorio: NumeroAseguradoraService, private snackBar: MatSnackBar,
              private estadoUTelefonia: EstadoUsuarioTelefoniaService,
              private webRTCService: WebrtcService) {
  }

  ngOnInit(): void {
    this.getDirectorio();
    this.listenWebrtcEvents();
    this.isConnected();
  }

  ngOnDestroy() {
    if (this.webRtcEvents) { this.webRtcEvents.unsubscribe(); }
  }

  setApplyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getDirectorio(){
    let resp = this.directorio.getNumerosAsegura();
    resp.subscribe((report: any[]) => {
      this.dataSource.data = report as direcAseg[];
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  llamarAseuradora(aseguradora: string, numero: any): void {
    this.estadoUTelefonia.cambiarEstado(3, this.idUsuario).subscribe();
    this.snackBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
        numero,
        idTipoLlamada: 3
      }
    }).afterDismissed().subscribe(() => {
      this.estadoUTelefonia.cambiarEstado(1, this.idUsuario).subscribe();
    });
  }

  isConnected() {
    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }
  listenWebrtcEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.webRtcEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 7000);
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }


}
