import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {WebrtcService} from '../../../../@core/data/services/telefonia/webrtc.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {SpeechService} from '../../../../@core/data/services/telefonia/speech.service';
import {SolicitudesvnTelefoniaService} from '../../../../@core/data/services/telefonia/solicitudesvn-telefonia.service';

@Component({
  selector: 'app-dialogo-llamada',
  templateUrl: './dialogo-llamada.component.html',
  styleUrls: ['./dialogo-llamada.component.scss']
})
export class DialogoLlamadaComponent implements OnInit, OnDestroy{

  llamadaEvents: Subscription;
  speech: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
              private router: Router,
              public matDialogRef: MatDialogRef<DialogoLlamadaComponent>,
              private solicitudesVn: SolicitudesvnTelefoniaService,
              public speechService: SpeechService,
              private webRtcService: WebrtcService) {
  }

  ngOnInit() {
    this.getSpeech();
    this.asignarSolicitud();
    this.llamadaEvents = this.webRtcService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          this.matDialogRef.close();
          break;
      }
    });
  }


  ngOnDestroy() {
    this.llamadaEvents.unsubscribe();
  }

  generarEmision(idSolicitud) {
    this.router.navigate([`modulos/cobranza/step-cotizador/${idSolicitud}`]);
    this.matDialogRef.close();
  }

  getSpeech() {
    this.speechService.getSpeechByDepartamento().subscribe((resp: any) => {
      this.speech = resp;
    });
  }

  asignarSolicitud() {
    if (this.data.solicitud.id) {
      this.solicitudesVn.asignarEmpleadoSolicitud(this.data.solicitud.id).subscribe();
    }
  }

}
