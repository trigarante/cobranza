import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Extensiones} from '../../../../@core/data/interfaces/telefonia/extensiones';
import {FormControl} from '@angular/forms';
import {WebrtcService} from '../../../../@core/data/services/telefonia/webrtc.service';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from '@angular/material/snack-bar';
import {ExtensionesService} from '../../../../@core/data/services/telefonia/extensiones.service';
import {MatDialog} from '@angular/material/dialog';
import {SolicitudesvnTelefoniaService} from '../../../../@core/data/services/telefonia/solicitudesvn-telefonia.service';
import {map, startWith} from 'rxjs/operators';
import {DialogoLlamadaComponent} from '../dialogo-llamada/dialogo-llamada.component';
import {LlamadasOutService} from '../../../../@core/data/services/telefonia/llamadas-out.service';
import {TipificarComponent} from '../tipificar/tipificar.component';
import {KeypadComponent} from '../keypad/keypad.component';
import {EstadoUsuarioTelefoniaService} from '../../../../@core/data/services/telefonia/estado-usuario-telefonia.service';

@Component({
  selector: 'app-llamada-salida',
  templateUrl: './llamada-salida.component.html',
  styleUrls: ['./llamada-salida.component.scss']
})
export class LlamadaSalidaComponent implements OnInit, OnDestroy {
  private llamadaEvents: Subscription;
  public options: Extensiones[];
  filteredOptions: Observable<Extensiones[]>;
  myControl = new FormControl();
  public transferir = false;
  public estadoMute = true;
  public holdStatus = true;
  holdAudio = false;
  public counter = 0;
  private intervalId: any;
  public dialogoData: any;
  private idLlamada: number;
  idUsuario = sessionStorage.getItem('Usuario');
  idEstadoLlamada = 3;

  constructor(private webRTCService: WebrtcService,
              private snackBarRef: MatSnackBarRef<LlamadaSalidaComponent>,
              private matDialog: MatDialog,
              private extensionesService: ExtensionesService,
              private dialogService: MatDialog,
              private llamadasOutService: LlamadasOutService,
              private edoUsuarioTelefoniaS: EstadoUsuarioTelefoniaService,
              private SolicitudesService: SolicitudesvnTelefoniaService,
              @Inject(MAT_SNACK_BAR_DATA) public data: any) { }

  ngOnInit(): void {
    if (this.data.idLlamada) {
      this.llamarcb();
    } else {
      this.llamar();
    }
    this.getEvents();
    this.getNumerosTransferencia();
    if (this.data.idTipoLlamada !== 3 && this.data.idSolicitud) { this.getSolicitud(); }
  }

  getSolicitud() {
    this.SolicitudesService.getSolicitudById(this.data.idSolicitud).subscribe((resp) => {
      this.dialogoData = resp;
      this.abrirDialogo();
    });
  }

  ngOnDestroy() {
    if (this.llamadaEvents) {this.llamadaEvents.unsubscribe(); }
    clearInterval(this.intervalId);
  }

  llamar() {
    this.webRTCService.sipCall('call-audio', this.data.numero);
    this.iniciarContador();
    if (this.data.idTipoLlamada !== 3) {
      this.edoUsuarioTelefoniaS.cambiarEstado(3, this.idUsuario).subscribe();
      this.llamadasOutService.iniciarLlamada(this.data.numero, this.data.idSolicitud, this.data.idRegistro)
        .subscribe((resp: any) => {
          this.idLlamada = resp.id;
        });
    }
  }

  llamarcb() {
    this.idLlamada = this.data.idLlamada;
    this.webRTCService.sipCall('call-audio', this.data.numero);
    this.iniciarContador();
    this.edoUsuarioTelefoniaS.cambiarEstado(3, this.idUsuario).subscribe();
    this.llamadasOutService.inicioCb(this.data.idLlamada).subscribe();
  }

  getEvents() {
    this.llamadaEvents = this.webRTCService.listenEvents().subscribe((resp) => {
      switch (resp.evento) {
        case 'disconnected':
        case 'terminated':
          this.finalizarLlamada();
          this.llamadaEvents.unsubscribe();
          this.snackBarRef.dismiss();
          break;
      }
    });
  }

  finalizarLlamada() {
    if (this.data.idTipoLlamada !== 3 && this.idLlamada) {
      this.llamadasOutService.finLlamada(this.idLlamada, this.idEstadoLlamada).subscribe();
      this.matDialog.open(TipificarComponent, {
        data: {
          idLlamada: this.idLlamada,
          predictivo: this.data.predictivo,
          idSolicitud: this.data.idSolicitud
        },
        disableClose: true
      });
    }
  }
  getNumerosTransferencia() {
    this.extensionesService.getContactos().subscribe((resp) => {
      this.options = resp;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return this._filter(value);
          }),
        );
    });
  }

  private _filter(value: string): Extensiones[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.nombreCampania.toLowerCase().includes(filterValue));
  }

  transfer(extension: string) {
    this.webRTCService.sipTransfer(extension);
    this.transferir = true;
    this.finalizarLlamada();
    this.llamadaEvents.unsubscribe();
    this.snackBarRef.dismiss();
  }

  mute(estadoMute) {
    this.estadoMute = (estadoMute === 1) ? false : true;
    this.webRTCService.sipToggleMute();
  }
  hold(holdState) {
    this.holdStatus = (holdState === 1) ? false : true;
    this.webRTCService.sipToggleMute();
    this.holdAudio = holdState === 1;
  }

  abrirDialogo() {
    if (this.dialogoData) {
      this.dialogService.open(DialogoLlamadaComponent, {
        data: {solicitud: this.dialogoData},
        width: '550px'
      });
    }
  }

  colgar() {
    this.idEstadoLlamada = 5;
    this.webRTCService.sipHangUp();
  }

  iniciarContador() {
    this.intervalId = setInterval(() => {
      this.counter = this.counter + 1;
    }, 1000);
  }

  openDialpad() {
    this.dialogService.open(KeypadComponent);
  }

}
