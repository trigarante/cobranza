import {Component, Inject, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LlamadasOutService} from '../../../../@core/data/services/telefonia/llamadas-out.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {EstadoUsuarioTelefoniaService} from '../../../../@core/data/services/telefonia/estado-usuario-telefonia.service';
import {TipificarService} from '../../../../@core/data/services/telefonia/tipificar.service';
import {SolicitudesvnService} from '../../../../@core/data/services/venta-nueva/solicitudes.service';
import {SocketsService} from '../../../../@core/data/services/telefonia/sockets/sockets.service';

@Component({
  selector: 'app-tipificar',
  templateUrl: './tipificar.component.html',
  styleUrls: ['./tipificar.component.scss']
})
export class TipificarComponent implements OnInit, OnDestroy {
  estadoSolcitudCreateForm: FormGroup = this.fb.group({
    idEtiquetaSolicitud: new FormControl('', Validators.compose([Validators.required])),
    idSubetiquetaSolicitud: new FormControl('', Validators.required),
    comentarios: new FormControl(''),
  });
  edosSolicitud: any[] = [];
  etiquetaSolicitud: any[] = [];
  subetiquetas: any[] = [];
  idUsuario = sessionStorage.getItem('Usuario');
  tiempo = 15;
  intervalTime;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialogRef: MatDialogRef<TipificarComponent>,
    private notificacionesService: NotificacionesService,
    private tipificaionesService: TipificarService,
    private llamadasOutService: LlamadasOutService,
    private solicitudesService: SolicitudesvnService,
    private edoUsuarioTelefoniaS: EstadoUsuarioTelefoniaService,
    private fb: FormBuilder,
    private socketsService: SocketsService
  ) { }

  ngOnInit(): void {
    this.edoUsuarioTelefoniaS.cambiarEstado(6, this.idUsuario).subscribe();
    this.definirFormulario();
    this.intervalTime = setInterval(async () => {
      if (this.tiempo === 0) {
        this.dialogRef.close();
      }
      this.tiempo--;
    }, 1000);
  }
  definirFormulario() {
    if (!this.data.predictivo ) {
      this.estadoSolcitudCreateForm.addControl('edoSolicitud', new FormControl(null, Validators.required));
      this.getEdoSolicitud();
    } else {
      this.tipificaionesService.getEtiquetaSolicitud(2).subscribe((resp: any) => {
        this.etiquetaSolicitud = resp;
      });
    }
  }
  getEdoSolicitud() {
   this.tipificaionesService.getEstadoSolicitud().subscribe((resp: any) => {
     this.edosSolicitud = resp;
   });
  }
  getEtiquetas() {
    this.tipificaionesService.getEtiquetaSolicitud(+this.estadoSolcitudCreateForm.controls.edoSolicitud.value)
      .subscribe((resp: any) => {
        this.etiquetaSolicitud = resp;
      });
  }
  getSubetiquetas() {
    this.tipificaionesService.getSubetiquetaSolicitud(+this.estadoSolcitudCreateForm.controls.idEtiquetaSolicitud.value)
      .subscribe((resp: any) => {
        this.subetiquetas = resp;
      });
  }

  tipificarLlamada() {
    this.notificacionesService.carga('Tipificando llamada');
    const idSubetiqueta = this.estadoSolcitudCreateForm.value.idSubetiquetaSolicitud;
    this.llamadasOutService.tipificarLlamada(this.data.idLlamada, idSubetiqueta, this.estadoSolcitudCreateForm.value.comentarios).subscribe({
      complete: () => {
        if (!this.data.predictivo && this.data.idSolicitud) {
          this.solicitudesService.cambiarEstadoSolicitud(this.data.idSolicitud, this.estadoSolcitudCreateForm.value.edoSolicitud)
            .subscribe();
        }
        this.edoUsuarioTelefoniaS.cambiarEstado(1, this.idUsuario).subscribe();
        this.notificacionesService.exito('Llamada tipificada correctamente');
        this.dialogRef.close();
      }
    });
  }
  ngOnDestroy() {
    this.socketsService.tipificacion(this.idUsuario);
    clearInterval(this.intervalTime);
  }
}
