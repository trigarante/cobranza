import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {LlamadasSalidaService} from '../../../../@core/data/services/telefonia/llamadas-salida.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-escuchar-grabacion',
  templateUrl: './escuchar-grabacion.component.html',
  styleUrls: ['./escuchar-grabacion.component.scss']
})
export class EscucharGrabacionComponent implements OnInit {
  asteriskBuzon: string;
  link: string;
  loading = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
              private matDialogRef: MatDialogRef<EscucharGrabacionComponent>,
              private llamadaSalidas: LlamadasSalidaService) { }

  ngOnInit(): void {
    this.data.tipoServer ? this.getNombreArchivoPjsip(this.data.idAsterisk) : this.getnombreArchivo(this.data.idAsterisk);
  }

  getNombreArchivoPjsip(idAsterisk) {
    this.llamadaSalidas.getArchivoPjsip(idAsterisk).subscribe((record) => {
      if (record.length > 0) {
        this.getGrabacion(record[0].recordingfile);
      } else {
        this.getSegundoIdPjsip();
      }
    });
  }

  getGrabacion(nombreArchivo: string) {
    this.llamadaSalidas.getGrabacionOutbound(nombreArchivo).subscribe((resp: any[]) => {
      if (resp.length === 0) {
        this.data.tipoServer ? this.getSegundoIdPjsip() : this.getSegundoAstId();
      } else {
        this.link = resp[0].webContentLink;
        this.loading = false;
      }
    });
  }

  getSegundoIdPjsip() {
    let numero = '44445';
    numero = numero + this.data.numero;
    this.llamadaSalidas.getSecondIdPjsip(this.data.idAsterisk, numero).subscribe((resp) => {
      this.asteriskBuzon = resp;
    }, () => {
      Swal.fire({
        icon: 'warning',
        text: 'Grabación no disponible, intenta después',
        showConfirmButton: false,
        timer: 2000,
      }).then(() => {
        this.matDialogRef.close();
      });
    }, () => {
      this.getnombreArchivo(this.asteriskBuzon);
    });
  }

  getnombreArchivo(idAsterisk: string) {
    this.llamadaSalidas.getNombreArchivo(idAsterisk).subscribe((grabacion) => {
      if (grabacion.length > 0) {
        this.getGrabacion(grabacion[0].recordingfile);
      } else {
        this.data.tipoServer ? this.getSegundoIdPjsip() : this.getSegundoAstId();
      }
    });
  }

  getSegundoAstId() {
    let numero = '44445';
    numero = numero + this.data.numero;
    this.llamadaSalidas.getSecondIdAsterisk(this.data.idAsterisk, numero).subscribe((resp) => {
      this.asteriskBuzon = resp;
    }, () => {
      Swal.fire({
        icon: 'warning',
        text: 'Grabación no disponible, intenta después',
        showConfirmButton: false,
        timer: 2000,
      }).then(() => {
        this.matDialogRef.close();
      });
    }, () => {
      this.getnombreArchivo(this.asteriskBuzon);
    });
  }

}
