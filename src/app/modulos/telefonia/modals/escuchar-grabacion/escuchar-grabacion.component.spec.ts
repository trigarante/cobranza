import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EscucharGrabacionComponent } from './escuchar-grabacion.component';

describe('EscucharGrabacionComponent', () => {
  let component: EscucharGrabacionComponent;
  let fixture: ComponentFixture<EscucharGrabacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EscucharGrabacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EscucharGrabacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
