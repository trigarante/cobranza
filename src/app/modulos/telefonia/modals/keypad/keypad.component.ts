import {Component, Inject} from '@angular/core';
import {WebrtcService} from '../../../../@core/data/services/telefonia/webrtc.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-keypad',
  templateUrl: './keypad.component.html',
  styleUrls: ['./keypad.component.scss']
})
export class KeypadComponent {
  num = '';
  constructor(private webRtc: WebrtcService, @Inject(MAT_DIALOG_DATA) public data,
              private webRTCService: WebrtcService,
              private dialogRef: MatDialogRef<KeypadComponent>) {
    if (this.data) { this.supervisar(); }
  }
  getValue() {
    const value = (<HTMLInputElement> document.getElementById('output')).value;
    return value;
  }

  takevalue(x) { // Funcion que va colocando un numero ingresado por el boton del dialpad
    let num = this.getValue();
    num += x;
    this.num = num;
    this.webRtc.sipSendDTMF(x.toString());
  }

  supervisar() {
    switch (this.data.tipoSupervision) {
      case 'espiar': {
        this.webRTCService.sipCall('call-audio', '*500');
        break;
      }
      case 'couch': {
        this.webRTCService.sipCall('call-audio', '*501');
        break;
      }
      case 'conferencia': {
        this.webRTCService.sipCall('call-audio', '*502');
        break;
      }

    }
  }

  colgar() {
    this.webRTCService.sipHangUp();
    this.dialogRef.close();
  }
}
