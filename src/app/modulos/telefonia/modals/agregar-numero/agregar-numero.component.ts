import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NuevoNumeroService} from '../../../../@core/data/services/telefonia/nuevo-numero.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-agregar-numero',
  templateUrl: './agregar-numero.component.html',
  styleUrls: ['./agregar-numero.component.scss']
})
export class AgregarNumeroComponent {
  numero: RegExp = /[0-9]+/;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private matDialogRef: MatDialogRef<AgregarNumeroComponent>,
              private notificacionesService: NotificacionesService,
              private nuevoNumero: NuevoNumeroService) { }
  close() {
    this.matDialogRef.close();
  }

  enviar(numero: string) {
    if (this.data.idSolicitud) {
      this.saveByIdSolicitud(numero);
    }
    if (this.data.idRegistro) {
      this.saveByIdRegistro(numero);
    }
  }

  saveByIdSolicitud(numero: string) {
    this.nuevoNumero.numeroSolicitud(this.data.idSolicitud, numero).subscribe({
      complete: () => {
        this.notificacionesService.exito('Número agregado');
        this.close();
      },
      error: () => {
        this.notificacionesService.error();
      }
    });
  }

  saveByIdRegistro(numero: string) {
    this.nuevoNumero.numeroRegistro(this.data.idRegistro, numero).subscribe({
      complete: () => {
        this.notificacionesService.exito('Número agregado');
        this.close();
      },
      error: () => {
        this.notificacionesService.error();
      }
    });
  }

}
