import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {LlamadasInService} from '../../../@core/data/services/telefonia/llamadas-in.service';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {LlamadaSalidaComponent} from '../modals/llamada-salida/llamada-salida.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {WebrtcService} from '../../../@core/data/services/telefonia/webrtc.service';

@Component({
  selector: 'app-historico-ib',
  templateUrl: './historico-ib.component.html',
  styleUrls: ['./historico-ib.component.scss']
})
export class HistoricoIbComponent implements OnInit, OnDestroy {
  permisos = JSON.parse(window.localStorage.getItem('User'));
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cols: any[] = ['id', 'fecha', 'ejecutivo', 'tipificacion', 'numero', 'acciones'];
  dataSource = new MatTableDataSource([]);
  max = new Date();
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  extension = localStorage.getItem('extension');
  puedeLlamar: boolean;
  tipo: string;
  cargando = true;
  numeros: any[];
  webRtcEvents: Subscription;

  constructor(private llamadasInService: LlamadasInService,
              private snackBar: MatSnackBar,
              private webRTCService: WebrtcService,
              private notificacionesService: NotificacionesService) {
    setTimeout(() => {
      const element = document.getElementById('reconnect');
      element ? this.puedeLlamar = false : this.puedeLlamar = true;
    }, 500);
  }

  ngOnInit(): void {
    this.listenWebrtcEvents();
    this.getLlamadas(this.rangeDates[0].toISOString().split('T')[0], this.rangeDates[1].toISOString().split('T')[0]);
  }

  getFechas() {
    this.dataSource = new MatTableDataSource([]);
    const fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);
    this.getLlamadas(fechaCompleta1, fechaCompleta2);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getLlamadas(fechaInicio, fechaFin) {
    this.llamadasInService
      .getLlamadas(fechaInicio, fechaFin).subscribe((resp: any[]) => {
      if (!resp.length) {
        this.notificacionesService.advertencia('No tienes llamadas en el rango de fechas seleccionado', 'Sin solicitudes');
      }
      this.dataSource = new MatTableDataSource(resp);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  llamar(numero) {
    this.snackBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
        numero,
        predictivo: 1
      }
    });
  }

  listenWebrtcEvents() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      this.webRtcEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            setTimeout(() => {
              this.puedeLlamar = true;
            }, 5000);
            break;
          }
          case 'disconnected':
          {
            this.puedeLlamar = false;
            break;
          }
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.webRtcEvents) { this.webRtcEvents.unsubscribe(); }
  }

}
