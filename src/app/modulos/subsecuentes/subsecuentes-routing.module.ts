import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubsecuentesComponent } from './subsecuentes.component';
import {CommonModule} from '@angular/common';
import {SolicitudesVnInternaComponent} from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import {AutorizacionesProcesoComponent} from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import { CorreccionErroresPolizaComponent } from './correccion-errores-poliza/correccion-errores-poliza.component';
import {CorreccionErroresPolizaDatosComponent} from './correccion-errores-poliza-datos/correccion-errores-poliza-datos.component';
import { StepCotizadorComponent } from './step-cotizador/step-cotizador.component';
import { ViewerComponent } from './viewer/viewer.component';
import { PagoRecibosComponent } from './administracion-componentes/pago-recibos/pago-recibos.component';
import {MostrarDocumentosAutorizacionComponent} from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';
import { MostrarDocumentosPolizaComponent } from './autorizacion-errores/mostrar-documentos-poliza/mostrar-documentos-poliza.component';
import { DocumentosCorreccionPolizaComponent } from './autorizacion-poliza/documentos-correccion-poliza/documentos-correccion-poliza.component';
import { CotizadorComponent } from '../subsecuentes/cotizador/cotizador.component';
import {AdministracionRecibosComponent} from './administracion-componentes/administracion-recibos/administracion-recibos.component';
import {LinkPagoComponent} from './link-pago/link-pago.component';

const routes: Routes = [{
  path: '',
  component: SubsecuentesComponent,
  children: [
    {
      path: 'solicitudes/:tipo',
      component: SolicitudesVnInternaComponent,
    },
    {
      path: 'cotizador-vn/:id',
      component: CotizadorComponent,
    },
    {
      path: 'administracion-polizas/:idTipo',
      component: AdministracionPolizasComponent,
    },
    {
      path: 'viewer/:id_tipo_documento/:id_autorizacion_registro',
      component: ViewerComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro/:id',
      component: PagoRecibosComponent,
    },
    {
      path: 'pagos-pagos/:idRecibo/:idRegistro',
      component: PagoRecibosComponent,
    },
    {
      path: 'detalle-pagos/:esDetalle/:idRecibo/:idRegistro/:id',
      component: PagoRecibosComponent,
    },
    {
      path: 'detalle-pagos/:esDetalle/:idRecibo/:idRegistro',
      component: PagoRecibosComponent,
    },
    {
      path: 'step-cotizador/:id',
      component: StepCotizadorComponent
    },
    {
      path: 'autorizacion-errores/:estadoAutorizacion',
      component: AutorizacionesProcesoComponent,
    },
    {
      path: 'autorizacion-errores/:pago/:estadoAutorizacion',
      component: AutorizacionesProcesoComponent,
    },
    {
      path: 'correccion-errores-poliza/documentos/:tipo_correccion',
      component: CorreccionErroresPolizaComponent,
    },
    {
      path: 'correccion-errores-poliza/datos/:tipo_correccion',
      component: CorreccionErroresPolizaDatosComponent,
    },
    {
      path: 'validar-documentos/:id_tipo_documento/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent,
    },
    {
      path: 'validar-documentos/:id_autorizacion_registro',
      component: MostrarDocumentosAutorizacionComponent,
    },
    {
      path: 'validar-pago-poliza/:id_autorizacion_pago/:idEstado',
      component: MostrarDocumentosPolizaComponent,
    },
    {
      path: 'documentos-correccion-pago/:tipoError/:etapaCorreccion/:idTipoDocumento/:id_autorizacion_pago',
      component: DocumentosCorreccionPolizaComponent,
    },
    {
      path: 'documentos-correccion-poliza/:tipoError/:etapaCorreccion/:idTipoDocumento/:id_autorizacion_registro',
      component: DocumentosCorreccionPolizaComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo/:idSolicitud',
      component: AdministracionRecibosComponent,
    },
    {
      path: 'recibos-administrador-recibos/:idRegistro/:idRecibo',
      component: AdministracionRecibosComponent,
    },
    {
      path: 'solicitudes-pago',
      component: LinkPagoComponent,
    },
  ],
}];

@NgModule({
    declarations: [ SubsecuentesComponent ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)],
        exports: [RouterModule],
})
export class SubsecuentesRoutingModule { }
