import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {ActivatedRoute} from '@angular/router';
import {DriveService} from '../../../@core/data/services/ti/drive.service';
import {RegistroService} from '../../../@core/data/services/venta-nueva/registro.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {
  @Input() carpetaRegistro;
  @Input() archivoPago;
  @Input() carpetaCliente;
  @Input() carpetaInspeccion;
  @Input() autorizacion;
  @Input() soloPago;
  arrayArchivos;
  pdf: any;
  idAutorizacionRegistro: number;
  filesToUpload: FileList;
  index = 1;
  documentos: any = [
    {
      tipo: 'Cliente',
      img: null,
      id: 1,
    },
    {
      tipo: 'Poliza',
      img: null,
      id: 2,
    },
    {
      tipo: 'Pago',
      img: null,
      id: 4,
    },
    {
      tipo: 'Inspeccion',
      img: null,
      id: 5,
    },
  ];

  constructor(private notificaciones: NotificacionesService,
    private activatedRoute: ActivatedRoute,
    private registroService: RegistroService,
    private driveService: DriveService,
    private location: Location) {
      this.idAutorizacionRegistro = parseFloat(this.activatedRoute.snapshot.paramMap.get('id_autorizacion_registro'));
     }

     ngOnInit(): void {
      if(this.soloPago)
        this.documentos = this.documentos.slice(2,3)

      if (!this.autorizacion && !this.soloPago) {
        this.registroService.getRegistroByIdViewer(this.idAutorizacionRegistro).subscribe({
          next: data => {
            this.arrayArchivos = [data.carpetaCliente, data.archivoPoliza, data.archivoPago, data.archivoInspeccion];
          },
          complete: () => this.getArchivo(0)
        });
      } else if(this.soloPago){
        this.arrayArchivos = [this.archivoPago];
        this.getArchivo(0)
      } else{
        this.arrayArchivos = [this.carpetaCliente, this.carpetaRegistro, this.archivoPago, this.carpetaInspeccion];
        this.getArchivo(0);
      }
    }
    cerrar(){
      this.location.back();
    }
    onclick(event) {
      this.pdf = null;
      this.notificaciones.carga('Cargando documento');
      this.index = event.index;
      this.getArchivo(this.index);
    }
    async getArchivo(index: number) {
      const doc = this.documentos[index];
      if (doc.img) {
        this.pdf = doc.img;
        this.notificaciones.cerrar();
      } else if (this.arrayArchivos[index]) {
        await this.driveService.getArchivo(this.arrayArchivos[index], this.documentos[index].id).subscribe({
          next: result => {
            this.pdf = result[0];
            doc.img = this.pdf;
            this.notificaciones.cerrar();
          },
          error: () => this.notificaciones.error(),
        });
      } else {
        this.notificaciones.error('Aún no se ha subido documento');
      }
    }
  actualizarArchivo(numero) {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUpload = (<HTMLInputElement>event.target).files;
    const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
    const tamanioArchivo = this.filesToUpload.item(0).size * .000001;
    if (this.filesToUpload.length === 1) {
      // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        this.notificaciones.carga();
        this.subirADrive(numero);
      } else {
        this.filesToUpload = null;
        this.notificaciones.error(!extensionValida ? 'Solo puedes subir archivos pdf' :
            'Los archivos que subas deben pesar menos de 7 MB',
          !extensionValida ? 'Extensión no soportada' : 'Archivo demasiado grande');
      }
    } else {
      this.filesToUpload = null;
    }
  }
  subirADrive(numero) {
    this.driveService.putFile(this.filesToUpload, this.idAutorizacionRegistro, this.documentos[numero].id).subscribe({
      next: () => {
        if (numero !== this.index) {
          this.documentos[numero].img = null;
          this.getDatos(0);
        } else {
          this.documentos[numero].img = this.pdf = null;
          this.getDatos(1, numero);
        }
        this.notificaciones.exito().then(() => this.cerrar)
      },
      error: () => this.notificaciones.error(),
    });
  }
  getDatos(setDefault, index?) {
    this.registroService.getRegistroByIdViewer(this.idAutorizacionRegistro).subscribe({
      next: data => {
        this.arrayArchivos = [data.carpetaCliente, data.archivoPoliza, data.archivoPago, data.archivoInspeccion];
      },
      complete: () => (setDefault) ? this.getArchivo(index) : null,
    });
  }
}
