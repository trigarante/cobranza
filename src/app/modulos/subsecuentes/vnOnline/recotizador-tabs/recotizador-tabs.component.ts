import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-recotizador-tabs',
  templateUrl: './recotizador-tabs.component.html',
  styleUrls: ['./recotizador-tabs.component.scss']
})
export class RecotizadorTabsComponent implements OnInit {
  @Input() idProspecto: number = this.rutaActiva.snapshot.params.idProspectoExis;
  // @Input() idSolicitudDetalles: number = this.rutaActiva.snapshot.params.idSolicitud;
  // @Input() idSolicitudDetalles: number = this.rutaActiva.snapshot.params.idSolicitudDestalles;
  @Input() tipoContacto = this.rutaActiva.snapshot.paramMap.get('tipo_contacto');
  @Input() idTipoModificar = this.rutaActiva.snapshot.paramMap.get('idTipoModificar');
  @Input() idSolicitud = this.rutaActiva.snapshot.paramMap.get('idSolicitud');
  @Input() idSolicitudDetalles = this.rutaActiva.snapshot.paramMap.get('idSolicitudDetalles');

  constructor(private rutaActiva: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
