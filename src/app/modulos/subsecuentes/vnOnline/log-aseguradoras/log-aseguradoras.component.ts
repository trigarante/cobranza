import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ModalCotizacionComponent} from "../modal/modal-cotizacion/modal-cotizacion.component";

@Component({
  selector: 'app-log-aseguradoras',
  templateUrl: './log-aseguradoras.component.html',
  styleUrls: ['./log-aseguradoras.component.scss']
})
export class LogAseguradorasComponent implements OnInit {

  aseguradoras: any[] = [{value: '', aseguradora: 'QUALITAS', activo: 1, service: 'CATALOGO_AUTOSQUALITAS',
    catalogo: 'catalogos/', urlCotizacion: 'qualitas_cotizaciones/autos',
    departamento: [{depa: 3}, {depa: 2} , {depa: 62} , {depa: 44},
       {depa: 63} , {depa: 85}, {depa: 80} , {depa: 81},
      {depa: 86} , {depa: 93} , {depa: 105} , {depa: 106} , {depa: 114} , {depa: 116} ]},
    {value: '', aseguradora: 'GNP', activo: 1, service: 'CATALOGO_AUTOSGNP',
      catalogo: 'gnp_catalogos_autos/', urlCotizacion: 'gnp_autos/cotizaciones',
      urlEmision: 'gnp_autos/emisiones_full', urlPago: 'pago_gnp_3D/crear_id_pago_3d',
      departamento: [{depa: 0}, {depa: 0}, {depa: 0}]},
    {value: '', aseguradora: 'ABA', activo: 0, departamento: [{depa: 102}, {depa: 145}, {depa: 144}]},
    // {value: '', aseguradora: 'ABA2', activo: 0},
    {value: '', aseguradora: 'AFIRME', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'AIG', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ANA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ATLAS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'AXA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'BANCOMER', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'BANORTE', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'BXMAS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ELAGUILA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ELPOTOSI', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'GENERALDESEGUROS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'HDI', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'HDI2', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'INBURSA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'LALATINO', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'MAPFRE', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'MIGO', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'PRIMEROSEGUROS', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'SURA', activo: 0, departamento: [{depa: 0}]},
    {value: '', aseguradora: 'ZURICH', activo: 0, departamento: [{depa: 0}]},
  ];
  @Input() idProspecto: number;
  @Input() idSolicitudDetalles: number;
  @Input() tipoContacto: string;
  @Input() idTipoModificar: number;
  idDepartamento =  +sessionStorage.getItem('idDepartamento');
  valor: boolean;
  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
    // se mostrara todas las aseugradoras para cotizar al dar clic se enviara que tipos de auto para poder cotizar
    this.getAseguradoras();
    this.c(5);
  }
  // se bloqueara la aseguradora si el ejecutivo no tiene el idDepartamento correspondiente a la aseguradora
  c(depa){
    let index;
    // se bloquea la aseguradora si no encuentra el idDepartamento del empleadoen el json
    for (index in this.aseguradoras) {
        // console.log(index)
        // console.log(this.aseguradoras[index])
        // console.log(this.aseguradoras[index].departamento.includes(fruta => fruta.depa === this.idDepartamento));
        const depas = this.aseguradoras[index].departamento;
        const finddepartamento = depas.find(departamentos => departamentos.depa === this.idDepartamento);
        // console.log(finddepartamento);
        if ( finddepartamento === undefined) {
          this.aseguradoras[index].activo = 0;
        }
      }

  }
  getAseguradoras(){

  }
  // se abrira un modal donde se seleccionara el tipo de autos para poder cotizar
  getMarcas(aseguradora , event, catalogo, urlCotizacion) {
    this.dialog.open(ModalCotizacionComponent, {
      width: 'auto',
      data: {
        nombreAseguradora: aseguradora,
        urlServidor: event,
        tipo: this.tipoContacto,
        idProspecto: this.idProspecto,
        urlCatalogo: catalogo,
        urlCotizacion,
        idTipoModificar: this.idTipoModificar,
        idSolicitudDetalles: this.idSolicitudDetalles,
      },
    });
  }


  }

