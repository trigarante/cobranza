import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosOnlineNewComponent } from './pagos-online-new.component';

describe('PagosOnlineNewComponent', () => {
  let component: PagosOnlineNewComponent;
  let fixture: ComponentFixture<PagosOnlineNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagosOnlineNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
