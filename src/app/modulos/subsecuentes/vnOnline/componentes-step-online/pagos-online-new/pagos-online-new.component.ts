import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClienteVn} from "../../../../../@core/data/interfaces/venta-nueva/cliente-vn";
import {MatStepper} from "@angular/material/stepper";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {Recibos} from "../../../../../@core/data/interfaces/venta-nueva/recibos";
import {FormaPago, FormaPagoOnline} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/forma-pago";
import {BancoExterno, BancoVn, MsiView} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/banco-vn";
import {Carrier} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/carrier";
import {TarjetasVn} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/tarjetas-vn";
import {CotizacionesAli} from "../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {Pagos} from "../../../../../@core/data/interfaces/venta-nueva/pagos";
import {ProductoCliente} from "../../../../../@core/data/interfaces/venta-nueva/producto-cliente";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PagosService} from "../../../../../@core/data/services/venta-nueva/pagos.service";
import {FormaPagoService} from "../../../../../@core/data/services/venta-nueva/catalogos/forma-pago.service";
import {CarrierService} from "../../../../../@core/data/services/venta-nueva/catalogos/carrier.service";
import {BancoVnService} from "../../../../../@core/data/services/venta-nueva/catalogos/banco-vn.service";
import {TarjetasVnService} from "../../../../../@core/data/services/venta-nueva/catalogos/tarjetas-vn.service";
import {RecibosService} from "../../../../../@core/data/services/venta-nueva/recibos.service";
import {RegistroService} from "../../../../../@core/data/services/venta-nueva/registro.service";
import {DriveService} from "../../../../../@core/data/services/ti/drive.service";
import {AdminVnViewService} from "../../../../../@core/data/services/venta-nueva/adminVnView.service";
import {CotizacionesAliService} from "../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {CatalogoService} from "../../../../../@core/data/services/cotizador/catalogo.service";
import {SolicitudesvnService} from "../../../../../@core/data/services/venta-nueva/solicitudes.service";
import {ActivatedRoute} from "@angular/router";
import {ClienteVnService} from "../../../../../@core/data/services/venta-nueva/cliente-vn.service";
import {SepomexService} from "../../../../../@core/data/services/catalogos/sepomex.service";
import {MsiExtenoGnp, urlAseguradoras} from "../../../../../@core/data/interfaces/cotizador/urlAseguradoras";
import Swal from "sweetalert2";
import {map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {BancoOnline} from "../../../../../@core/data/interfaces/cotizador/cotizar";
import {MessagePagoSuccessComponent} from "../../../../../shared/componentes/modal-pagos-online-new/componentes/message-pago-success/message-pago-success.component";
import {ProductoClienteService} from "../../../../../@core/data/services/venta-nueva/producto-cliente.service";
import {LogCotizadorService} from "../../../../../@core/data/services/cotizador/log-cotizador.service";
import {NotificacionesService} from "../../../../../@core/data/services/others/notificaciones.service";
import {RegistroOnline} from "../../../../../@core/data/interfaces/venta-nueva/registro";


@Component({
  selector: 'app-pagos-online-new',
  templateUrl: './pagos-online-new.component.html',
  styleUrls: ['./pagos-online-new.component.scss']
})
export class PagosOnlineNewComponent implements OnInit {

  @Input() idProductoCliente: number;
  @Input() idCliente: number;
  @Input() datosCliente: ClienteVn;
  @Input() idRegistro: number;
  idSolicitud: number;

  // @Output() stepSeleccionado = new EventEmitter<number>();
  // @Output() nextStepper = new EventEmitter<number>();
  @ViewChild('fechaV') fechaV: ElementRef;
  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('mostrarCodigoCheck') checkin: MatSlideToggle;

  @ViewChild('capture') capture: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;

  @Output() idCobroSaliente = new EventEmitter<{ idproductoc: number, filepagado: FileList}>();
  @Output() Inspeccione = new EventEmitter<number>();
  @Output() emisionMal = new EventEmitter<number>();

  // @Output() _idProductoCliente = new EventEmitter<number>();

  recibo: Recibos;
  formaPagoData: FormaPagoOnline[];
  bancosArray: BancoVn[];
  carrierArray: Carrier[];
  tarjetasArray: TarjetasVn[];
  archivoValido = false;
  activarMesesSinIntereses = false;
  inputType = 'password';
  filesToUpload: FileList;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  mesesSinIntereses = ['0 meses', '3 meses', '6 meses', '9 meses', '12 meses'];
  letras: RegExp = /[\\A-Za-zñÑ ]+/;
  idCarpetaDrive: string;
  ocultarEyePermanent: boolean;
  momentoCountdown = false;
  btnLoad = false;
  toggleActivo = true;
  // pago online
  token: string;
  cotizacionAli: CotizacionesAli;
  pagoOnline: Pagos;
  response: any;
  responseObtenerEmision: any;
  dataArrayPago: any;
  dataArrarObtenerPoliza: any;
  dataArrayValidacionTarjeta: any;
  responsepago: any;
  nombrebanco: any;
  responsePago: any;
  mostrarbotonPago: boolean = false;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urlPago: string;
  urlEmision: string;
  urlBanco: string;
  urlValidarTarjeta: string;
  urlMsi: string;
  urlObtenerEmision: string;
  // emision
  productoCliente: ProductoCliente;
  responseProducto: any;
  vehiculoJson: object;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  clientejson: object;
  jsonEmitir: any;
  responseEmision: any;
  responseEmisionPrima: any;
  dataArrayEmision: any;
  loader = false;
  bancosArrayExterno: BancoExterno[];
  ocultarFormulario = false;
  msi: MsiView[];
  msiGnp: MsiExtenoGnp[];
  valorMsi: any;
  // ocultarMsi: boolean = true;
   bancosOnline: BancoOnline[];
  Arraybanco: any;
  numTarj: string;
  banderaNumTarjeta = false;
  btnLoadP = false;
  valuePdfPago: any;
  pago: any;
  filesToUploadPago: FileList;
  jsonEmitirGnp: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
  };

  jsonPagoGnp: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
    banderaPoliza: 0,
  };

  jsonEmitirQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
  };

  jsonPagoQualitas: any = {
    idSolicitudVn: null,
    idEmpleado: null,
    request: null,
    response: null,
    error: null,
    poliza: null,
    banderaPagado: 0,
  };
  ocultarMsi = false;
  banderaEmitirMal = false;
  banderaPagoMal = false;
  registroOnline: RegistroOnline;


  // variable para mostrar el dialog cuando el pago sea true
  statusPayment = false;

  @ViewChild('reportContent') reportContent: ElementRef;

  // FORMS
  formPago: FormGroup = new FormGroup({
    cantidad: new FormControl({ value: '', disabled: true }),
    fechaPago: new FormControl({ value: new Date(), disabled: true }),
    idFormaPago: new FormControl(''),
  });

  formDatosPago: FormGroup = new FormGroup({
    idBanco: new FormControl('', Validators.required),
    idCarrier: new FormControl({ disabled: true, value: '' }, Validators.required),
    cargoRecurrente: new FormControl(false),
    toggleCodigo: new FormControl(false),
    mesesSinIntereses: new FormControl(''),
    comentarios: new FormControl(''),
    numeroTarjeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]),
    titular: new FormControl('', [Validators.required, Validators.pattern('[A-Z]+')]),
    nombreTarjeta: new FormControl('',[Validators.required, Validators.pattern('[A-Z]+')] ),
    codigo: new FormControl('', Validators.required),
    codigoToken: new FormControl(''),
    noClave: new FormControl(''),
    fechaVencimiento: new FormControl('', [Validators.required/*, Validators.pattern('[0-9]+')*/]),
  });
  private textError = 5;
  constructor(private pagosService: PagosService, private dc: ChangeDetectorRef,
              private formaPagoService: FormaPagoService,
              private carrierService: CarrierService,
              private bancoService: BancoVnService,
              private tarjetasService: TarjetasVnService,
              private recibosService: RecibosService,
              private registroService: RegistroService,
              private driveService: DriveService,
              private administracionPolizaService: AdminVnViewService,
              private cotizacionesAliService: CotizacionesAliService,
              private catalogoService: CatalogoService,
              private solicitudesService: SolicitudesvnService,
              private route: ActivatedRoute,
              private dialog: MatDialog, private router: ActivatedRoute,
              private productoClienteService: ProductoClienteService,
              private clienteService: ClienteVnService,
              private sepomexServicio: SepomexService,
              private logCotizadorService: LogCotizadorService,
              private notificacionesService: NotificacionesService,
  ) {
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urlPago = this.service.urlPago;
    this.urlEmision = this.service.urlEmision;
    this.urlBanco = this.service.urlBanco;
    this.urlValidarTarjeta = this.service.urlValidarTarjeta;
    this.urlMsi = this.service.urlMsi;
    this.urlObtenerEmision = this.service.urlObtenerEmision;
  }

  ngOnInit(): void {
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));
    this.getFormaPago();
    this.getBancos();
    this.getCarriers();
    // obtiene la cantidad
    this.getRecibos(this.idSolicitud);
    // this.getRegistro();
    this.ocultarEye();
    this.emitir(this.idSolicitud);
  }

  getBancos() {
    // this.bancoService.get().pipe(map(data => data.filter(res => res.activo = 1))).subscribe(
    //   arr => { this.bancosArray = arr; });

    // BANCOS PARA QUALITAS
    this.bancoService.getexterno().subscribe(data => {
      this.bancosArrayExterno = data;
    });

    // BANCOS PARA GNP

    // this.catalogoService.getBancosExterno(this.urlService, this.urlBanco).subscribe(dataBan => {
    //   // this.bancosOnline = dataBan["CATALOGO"];
    //   databanco = dataBan;
    //   this.Arraybanco = databanco;
    //   this.bancosOnline = this.Arraybanco["CATALOGO"]
    //   console.log(this.bancosOnline)
    // });

    switch (this.nombreAseguradora) {
      case 'QUALITAS':
        // BANCOS PARA QUALITAS
        this.bancoService.getexterno().subscribe(data => {
          this.bancosArrayExterno = data;
        });
        break;
      case 'GNP' :
        // BANCOS PARA GNP
        // let databanco: any;
        this.formDatosPago.controls.idBanco.disable();

        break;
      default:

        break;
    }
  }

  getFormaPago() {
    this.formaPagoService.getOnline().pipe(map((res) => {
      return res.filter(data => data.id === 1 || data.id === 2);
    })).subscribe(result => {
      this.formaPagoData = result;
    });
  }

  getRecibos(idSol) {


    // this.recibosService.getRecibosByIdRegistro(this.idRegistro).subscribe(data => {
    //   if (data) {
    //     this.formPago.controls['cantidad'].setValue(data.reverse()[0].cantidad);
    //     this.recibo = data[0];
    //   }
    // });

    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmisionPrima = this.cotizacionAli.respuesta;

        console.log(this.responseEmisionPrima["cotizacion"]["primaNeta"]);
        this.formPago.controls.cantidad.setValue(this.responseEmisionPrima["cotizacion"]["primaNeta"]);


        // #region comments
      });
    });
  }

  getCarriers() {
    this.carrierService.get().pipe(map(data => data.filter(res => res.activo = 1))).subscribe(
      data => { this.carrierArray = data; });
  }

  // getRegistro() {
  //   this.registroService.getRegistroById(this.idRegistro).subscribe(data => {
  //     this.idCarpetaDrive = data.archivo;
  //     console.log('Data ARchivo => ', data);
  //   });6
  // }

  verificarInputs() {
    this.cambioCT();
    if (this.formDatosPago.controls.idCarrier.value === '') {
      this.formDatosPago.controls.idCarrier.enable();
    }
  }

  // MSI
  gbanco(nombreBanco) {
    this.cambioCT();
    if (this.formDatosPago.controls.idCarrier.value === '') {
      this.formDatosPago.controls.idCarrier.enable();
    }

    switch (this.nombreAseguradora) {
      case 'QUALITAS':
        // msi PARA QUALITAS
        this.ocultarMsi = false;
        // this.bancoService.getMsi(this.nombreAseguradora, this.formDatosPago.controls['idBanco'].value).subscribe(dataMsi => {
        //     this.msi = dataMsi;
        //     if (this.msi.length === 0) {
        //       this.ocultarMsi = false;
        //     }
        //     // if (this.msi.length !== 0) {
        //     //   this.ocultarMsi = true;
        //     // }
        //     // se oculta el select de meses sin intereses
        //     // se puede cambiar
        //     this.ocultarMsi = false;
        //   },
         // error => {},
         // () => {
        //  });
        break;
      case 'GNP' :
        // BANCOS PARA GNP
        // let databanco: any;
        this.ocultarMsi = false;
        if (this.formDatosPago.controls.numeroTarjeta.value !== '') {
          this.ocultarMsi = true;
          this.numTarj = this.formDatosPago.controls.numeroTarjeta.value;
          this.numTarj = this.numTarj.substring(0, 6);
          this.catalogoService.getMsiExterno(this.nombreAseguradora, this.urlMsi, this.numTarj).subscribe(dataMsiGNP => {
              this.msiGnp = dataMsiGNP['ELEMENTO'];
            },
            error => {},
            () => {
            });
        } else {
          this.ocultarMsi = false;
        }
        break;
      default:

        break;
    }
  }

  verificaMesesSinIntereses() {
    if (this.formDatosPago.controls.mesesSinIntereses.value === '0 meses') {
      this.activarMesesSinIntereses = false;
      this.formDatosPago.controls.mesesSinIntereses.setValue('');
    }
  }

  ocultarEye() {
    if (this.inputType === 'text') {
      return true;
    } else {
      this.ocultarEyePermanent = true;
      return false;
    }
  }

  ocultarInput() {
    if (this.formDatosPago.controls.toggleCodigo.value === true) {
      // if (this.checkin.checked) {
      const tarjeta = this.formDatosPago.controls.numeroTarjeta.valid;
      const codigoToken = this.formDatosPago.controls.codigoToken.valid;
      this.toggleActivo = !(!tarjeta || !codigoToken);
    } else {
      const tarjeta = this.formDatosPago.controls.numeroTarjeta.valid;
      const codigo = this.formDatosPago.controls.codigo.valid;
      this.toggleActivo = !(!tarjeta || !codigo);
    }
    if (!this.toggleActivo) {
      Swal.fire({
        text: 'Datos incompletos',
      });
    } else {
      let countDown = 60;
      Swal.fire({
        text: `¡Tienes ${countDown} segundos para verificar tus datos!`,
        showConfirmButton: true,
        showCancelButton: true,
      }).then(resp => {
        if (resp.value) {
          this.momentoCountdown = true;
          this.inputType = 'text';
          const time = setInterval(() => {
            countDown--;
            const loader2 =
              `<div>
                <div class="row"">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Quedan </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; segundos &nbsp;</p>
                  </div>
                </div>
                </div>`;
            const loader3 =
              `<div>
                <div class="row">
                  <div class="spinner-grow text-info" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="row" style="padding-top: 5px">
                    <p>&nbsp;&nbsp;&nbsp;Queda </p> <p>&nbsp;<strong>${countDown}</strong></p><p>&nbsp; segundo &nbsp;</p>
                  </div>
                </div>
                </div>`;
            switch (countDown) {
              case 1:
                const Toast3 = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                });
                Toast3.fire({
                  width: 165,
                  html: loader3,
                });
                break;
              case 0:
                clearInterval(time);
                this.momentoCountdown = false;
                this.inputType = 'password';
                this.ocultarEyePermanent = false;
                const Toast0 = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer: 2500,
                });
                Toast0.fire({
                  width: 165,
                  text: 'Tiempo terminado',
                });
                break;
              default:
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                });
                Toast.fire({
                  width: 180,
                  html: loader2,
                });
                break;
            }
          }, 1000);
        }
      });
    }
  }

  seleccionFormaPago() {
    let datosPago;
    switch (this.formPago.controls.idFormaPago.value) {
      case 1:
      case 2:
        datosPago = {
          idBanco : new FormControl('', Validators.required),
          idCarrier: new FormControl({ disabled: true, value: '' }, Validators.required),
          cargoRecurrente: new FormControl(false),
          toggleCodigo: new FormControl(false),
          mesesSinIntereses: new FormControl(''),
          comentarios: new FormControl(''),
          numeroTarjeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]),
          titular: new FormControl('', [Validators.required, Validators.pattern('[A-Z]+')]),
          nombreTarjeta: new FormControl('', [Validators.required, Validators.pattern('[A-Z]+')]),
          codigo: new FormControl('', Validators.required),
          codigoToken: new FormControl(''),
          fechaVencimiento: new FormControl('', [Validators.required/*, Validators.pattern('[0-9]+')*/]),
        };
        break;
      case 7:
        datosPago = {
          idBanco: new FormControl('', Validators.required),
          noAutorizacion: new FormControl('', Validators.required),
          comentarios: new FormControl(''),
        };
        break;
      default:
        datosPago = {
          comentarios: new FormControl(''),
        };
        break;
    }
    // this.formDatosPago = new FormGroup(datosPago);

  }

  fechaTarjeta(value) {
    if (value.length >= 2 && value.indexOf('/') < 0) {
      this.fechaV.nativeElement.value =
        this.fechaV.nativeElement.value.substring(0, 2) + '/' +
        this.fechaV.nativeElement.value.substring(2, 6);
    }

    if (value.charAt(1) === '/') {
      this.fechaV.nativeElement.value = this.fechaV.nativeElement.value.substring(0, 1) + '' + '/' +
        this.fechaV.nativeElement.value.substring(2, 6);
    }
    this.fechaV.nativeElement.value = this.fechaV.nativeElement.value.substring(0, 5);
  }

  desactivarBoton(): boolean {
    if (this.formDatosPago !== undefined) {
      if (this.formDatosPago.valid && this.archivoValido) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  cambioCT() {
    if (this.formDatosPago.contains('codigo')) {
      this.formDatosPago.addControl('codigoToken', new FormControl('', Validators.compose(
        [Validators.minLength(3), Validators.maxLength(6)])));
      //   this.formDatosPago.removeControl('codigo');
    } else {
      if (this.formDatosPago.contains('codigoToken')) {
        //     this.formDatosPago.addControl('codigo', new FormControl('', Validators.compose(
        //       [Validators.minLength(3), Validators.maxLength(5), Validators.required])));
        this.formDatosPago.removeControl('codigoToken');
      }
    }
  }

  // obtiene los datos del servicio pago y los guarda en cotizacion ali
  enviarpagoOnline() {
    let dataPago: any;
    let dataValidacionTarjeta: any;
    let formaPago: string;


    // enviar los datos del pago en json al sesionLocal para obtenerlos despues en el step de pago
    // y mandarlo a la base de datos pagos
    // nueva logica para gnp y qualitas

    /* const pago: Pagos = {
       id: 0,
       idRecibo: 0,
       idFormaPago: this.formPago.controls['idFormaPago'].value,
       idEstadoPago: 1,
       idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
       fechaPago: this.formPago.controls['fechaPago'].value,
       cantidad: this.formPago.controls['cantidad'].value,
       archivo: '',
       datos: '',
     };

     if (this.formDatosPago.controls['mesesSinIntereses'].value === '') {
       this.valorMsi = "CL";
       console.log(this.valorMsi);
     } else {
       this.valorMsi = this.formDatosPago.controls['mesesSinIntereses'].value;
     }

     // if (this.formDatosPago.controls['mesesSinIntereses'].value) {
     //
     //   this.valorMsi = this.formDatosPago.controls['mesesSinIntereses'].value;
     //   console.log(this.valorMsi);
     // } else if (!this.formDatosPago.controls['mesesSinIntereses'].value) {
     //   this.valorMsi = "CL";
     //   console.log(this.valorMsi);
     // }
     const DatosPago = {
       'idBanco': this.formDatosPago.controls['idBanco'].value,
       'idCarrier': this.formDatosPago.controls['idCarrier'].value,
       'cargoRecurrente': false,
       'toggleCodigo': false,
       'mesesSinIntereses': 'CL',
       'comentarios': this.formDatosPago.controls['comentarios'].value,
       'numeroTarjeta': this.formDatosPago.controls['numeroTarjeta'].value,
       'titular': this.formDatosPago.controls['titular'].value,
       'nombreTarjeta': this.formDatosPago.controls['nombreTarjeta'].value ,
       'codigo': this.formDatosPago.controls['codigo'].value,
       'codigoToken': '',
       'noClave': this.formDatosPago.controls['noClave'].value,
       'fechaVencimiento': this.formDatosPago.controls['fechaVencimiento'].value,
     };

     localStorage.setItem('pago', JSON.stringify(pago));
     localStorage.setItem('datosPago', JSON.stringify(DatosPago));*/


    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(dataSol => {
      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;
        this.response = this.cotizacionAli.respuesta;
        // this.bancoService.getBancoById(this.formDatosPago.controls['idBanco'].value).subscribe(databanco => {
        delete this.response['pago'];
        // poner datos bien

        switch (this.nombreAseguradora) {
          case 'QUALITAS':
            // envia los pagos al localstorage para usarlos en el otro step registro
            const pago: Pagos = {
              id: 0,
              idRecibo: 0,
              idFormaPago: this.formPago.controls.idFormaPago.value,
              idEstadoPago: 1,
              idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
              fechaPago: this.formPago.controls.fechaPago.value,
              cantidad: this.formPago.controls.cantidad.value,
              archivo: '',
              datos: '',
            };

            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = 'CL';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }

            // if (this.formDatosPago.controls['mesesSinIntereses'].value) {
            //
            //   this.valorMsi = this.formDatosPago.controls['mesesSinIntereses'].value;
            //   console.log(this.valorMsi);
            // } else if (!this.formDatosPago.controls['mesesSinIntereses'].value) {
            //   this.valorMsi = "CL";
            //   console.log(this.valorMsi);
            // }
            const DatosPago = {
              idBanco: this.formDatosPago.controls.idBanco.value,
              idCarrier: this.formDatosPago.controls.idCarrier.value,
              cargoRecurrente: false,
              toggleCodigo: false,
              mesesSinIntereses: this.valorMsi,
              comentarios: this.formDatosPago.controls.comentarios.value,
              numeroTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              titular: this.formDatosPago.controls.titular.value,
              nombreTarjeta: this.formDatosPago.controls.nombreTarjeta.value,
              codigo: this.formDatosPago.controls.codigo.value,
              codigoToken: '',
              noClave: this.formDatosPago.controls.noClave.value,
              fechaVencimiento: this.formDatosPago.controls.fechaVencimiento.value,
            };

            localStorage.setItem('pago', JSON.stringify(pago));
            localStorage.setItem('datosPago', JSON.stringify(DatosPago));

            if (this.formPago.controls.idFormaPago.value === 1) {
              formaPago = 'CREDITO';
            } else if (this.formPago.controls.idFormaPago.value === 2) {
              formaPago = 'DEBITO';
            }

            this.response['pago'] = {
              msi: 'CL',
              banco: this.formDatosPago.controls.idBanco.value,
              mesExp: this.formDatosPago.controls.fechaVencimiento.value.substring(0, 2),
              anioExp: '20' + this.formDatosPago.controls.fechaVencimiento.value.substring(3, 6),
              carrier: this.formDatosPago.controls.idCarrier.value,
              noClabe: this.formDatosPago.controls.noClave.value,
              medioPago: formaPago,
              noTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              nombreTarjeta: this.formDatosPago.controls.nombreTarjeta.value,
              codigoSeguridad: this.formDatosPago.controls.codigo.value
            }
            // console.log(this.response['Pago']);
            // console.log(this.response['Aseguradora']);
            // console.log(this.idSolicitud);
            // console.log(this.token);

            this.cotizacionAli['respuesta'] = this.response;
            console.log('Response cotizacion Ali =>', this.cotizacionAli.respuesta);

            this.responsePago = this.cotizacionAli.respuesta;
            console.log(this.responsePago);

            // servicio real
            this.catalogoService.postPago(this.urlService, this.urlPago, this.responsePago)
              .subscribe(
                (data) => {
                  dataPago = data;

                },
                (error) => {
                  console.error('Error => ', error);
                },
                () => {
                  this.dataArrayPago = dataPago;
                  // this.responsepago = this.dataArrayPago;
                  console.log(this.dataArrayPago)

                  // log de pago

                  this.jsonPagoQualitas.request = this.responsePago;

                  this.jsonPagoQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonPagoQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonPagoQualitas.response = this.dataArrayPago;
                  this.jsonPagoQualitas.error = this.dataArrayPago['codigoError'];
                  this.jsonPagoQualitas.poliza = this.dataArrayPago['emision']['poliza'];
                  if (this.dataArrayPago['emision']['resultado'] === true) {
                    this.jsonPagoQualitas.banderaPagado = 1;
                  }
                  this.logCotizadorService.qualitaspago(this.jsonPagoQualitas).subscribe(log => {
                  });

                  // delete this.response['pago'];
                  // this.response['pago'] = this.dataArrayPago['pago'];
                  // delete this.response['emision'];
                  // this.response['emision'] = this.dataArrayPago['emision'];
                  // this.response['codigoError'] = this.dataArrayPago['codigoError'];
                  // this.response['urlRedireccion'] = this.dataArrayPago['urlRedireccion'];
                  // this.response['periodicidadDePago'] = this.dataArrayPago['periodicidadDePago'];
                  delete this.response;
                  this.response = this.dataArrayPago;
                  this.cotizacionAli['respuesta'] = this.response;

                  this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe( result => {
                  });

                  if (this.dataArrayPago['emision']['resultado'] === true) {
                    // muestre una ventana de pago exitoso
                    this.openDialogMessageSuccess();
                    this.banderaPagoMal = false;
                  } else if (this.dataArrayPago['codigoError'] !== '' && this.dataArrayPago['emision']['resultado'] === false){
                    // no ha pagado-
                    this.banderaPagoMal = true;
                    this.noPayment();
                    console.log('El pago ha sido negado');
                  }


                });

            // ejemplo responde


            //// delete this.response['emision'];

            // this.jsonPagoQualitas.request = this.response;
            // //
            // delete this.response['emision']['resultado'];
            // this.response['emision']['resultado'] = true;
            // // ejemplo de pago que devuelve
            // // this.response['emision'] = {
            // //   "poliza": "17160214702",
            // //   "derechos": "520",
            // //   "impuesto": "949.52",
            // //   "recargos": "",
            // //   "terminal": "",
            // //   "documento": "http://qbcenter.qualitas.com.mx/polizaprueba/c7160214768.pdf|" +
            // //     "http://qbcenter.qualitas.com.mx/polizaprueba/r7160214768.pdf|" +
            // //     "http://qbcenter.qualitas.com.mx/polizaprueba/p7160214768.pdf",
            // //   "primaNeta": "6445.6",
            // //   "resultado": true,
            // //   "primaTotal": "7930.56",
            // //   "primerPago": "",
            // //   "iDCotizacion": "",
            // //   "pagosSubsecuentes": ""
            // // }
            // // console.log(this.response['emision'])
            //
            //
            // // // this.nextStepper.emit(2);
            // //// this._idProductoCliente.emit(producto.id);
            //
            //
            // this.jsonPagoQualitas.idEmpleado = sessionStorage.getItem('Empleado');
            // this.jsonPagoQualitas.idSolicitudVn = this.idSolicitud;
            // this.jsonPagoQualitas.response = this.response;
            // this.jsonPagoQualitas.error = this.response['codigoError'];
            // this.jsonPagoQualitas.poliza =this.response['emision']['poliza'];
            // if (this.response['emision']['resultado'] === true) {
            //   this.jsonPagoQualitas.banderaPagado = 1;
            // }
            // this.logCotizadorService.qualitaspago(this.jsonPagoQualitas).subscribe(log => {
            // });
            // this.cotizacionAli['respuesta'] = this.response;
            //
            // this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
            //
            //   // si si pago que pase a la sig pag
            //   if (this.response['emision']['resultado'] === true) {
            //     this.banderaPagoMal = false;
            //     // muestre una ventana de pago exitoso
            //     this.openDialogMessageSuccess();
            //
            //   } else if (this.response['codigoError'] !== '' && this.response['emision']['resultado'] === false) {
            //     this.banderaPagoMal = true;
            //     // no ha pagado-
            //     this.noPayment();
            //
            //     console.log('El pago ha sido negado');
            //   }
            // });
            // fin simulacion
            // });// banco

            break;
          case 'GNP' :

            const pagognp: Pagos = {
              id: 0,
              idRecibo: 0,
              idFormaPago: this.formPago.controls.idFormaPago.value,
              idEstadoPago: 1,
              idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
              fechaPago: this.formPago.controls.fechaPago.value,
              cantidad: this.formPago.controls.cantidad.value,
              archivo: '',
              datos: '',
            };

            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = 'CL';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }

            const DatosPagognp = {
              idBanco: this.formDatosPago.controls.idBanco.value,
              idCarrier: this.formDatosPago.controls.idCarrier.value,
              cargoRecurrente: false,
              toggleCodigo: false,
              mesesSinIntereses: this.valorMsi,
              comentarios: this.formDatosPago.controls.comentarios.value,
              numeroTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              titular: this.formDatosPago.controls.titular.value,
              nombreTarjeta: this.formDatosPago.controls.nombreTarjeta.value,
              codigo: this.formDatosPago.controls.codigo.value,
              codigoToken: '',
              noClave: this.formDatosPago.controls.noClave.value,
              fechaVencimiento: this.formDatosPago.controls.fechaVencimiento.value,
            };

            localStorage.setItem('pago', JSON.stringify(pagognp));
            localStorage.setItem('datosPago', JSON.stringify(DatosPagognp));

            if (this.formPago.controls.idFormaPago.value === 1) {
              formaPago = 'R';
            } else if (this.formPago.controls.idFormaPago.value === 2) {
              formaPago = 'D';
            }


            if (this.formDatosPago.controls.mesesSinIntereses.value === '') {
              this.valorMsi = '';
              console.log(this.valorMsi);
            } else {
              this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;
            }
            this.valorMsi = this.formDatosPago.controls.mesesSinIntereses.value;

            this.response['pago'] = {
              msi: this.valorMsi.toString(),
              banco: this.formDatosPago.controls.idBanco.value,
              mesExp: this.formDatosPago.controls.fechaVencimiento.value.substring(0, 2),
              anioExp: '20' + this.formDatosPago.controls.fechaVencimiento.value.substring(3, 6),
              carrier: this.formDatosPago.controls.idCarrier.value,
              noClabe: this.formDatosPago.controls.noClave.value,
              medioPago: formaPago,
              noTarjeta: this.formDatosPago.controls.numeroTarjeta.value,
              nombreTarjeta: this.formDatosPago.controls.nombreTarjeta.value,
              codigoSeguridad: this.formDatosPago.controls.codigo.value
            }

            this.cotizacionAli['respuesta'] = this.response;
            console.log('Response cotizacion Ali =>', this.cotizacionAli.respuesta);

            this.responsePago = this.cotizacionAli.respuesta;
            console.log(this.responsePago);

            // servicio real
            this.catalogoService.postValidacionTarjeta(this.urlService, this.urlValidarTarjeta, this.responsePago)
              .subscribe(
                (data) => {
                  dataValidacionTarjeta = data;

                },
                (error) => {
                  console.error('Error => ', error);
                },
                () => {
                  this.dataArrayValidacionTarjeta = dataValidacionTarjeta;
                  // this.responsepago = JSON.parse(this.dataArrayPago);
                  console.log(this.dataArrayValidacionTarjeta)

                  delete this.response;
                  this.response = this.dataArrayValidacionTarjeta;
                  // se encripta tarjeta
                  delete this.responsePago['pago']['noTarjeta'];
                  this.responsePago['pago']['noTarjeta'] = this.response['pago']['noTarjeta'];
                  // log de pago
                  this.jsonPagoGnp.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonPagoGnp.idSolicitudVn = this.idSolicitud;
                  this.jsonPagoGnp.request = this.responsePago;
                  this.jsonPagoGnp.response = (this.response);
                  this.jsonPagoGnp.error = this.response['codigoError'];
                  this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
                    console.log(log);
                    console.log('entro a log validar tarjeta')
                  });


                  this.cotizacionAli['respuesta'] = this.response;

                  this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(
                    (datacot) => {
                      console.log(datacot);
                    },
                    (error) => {

                    },
                    () => {
                      if (this.dataArrayValidacionTarjeta['cotizacion']['verID'] !== '') {

                        console.log('tarjeta valida');
                        this.pagoGnp(dataSol.idCotizacionAli);
                        this.banderaPagoMal = false;
                      } else  if (this.dataArrayValidacionTarjeta['cotizacion']['verID'] === '') {
                        this.banderaPagoMal = true;
                        this.swalCargandotarjetanovalida();
                        console.log('tareta no valida');
                      }
                    });
                }); // fin de servicio real

            // simulacion

            // delete this.responsePago['cotizacion']['verID'];
            // this.responsePago['cotizacion']['verID'] = '0002';
            //
            // if (this.responsePago['cotizacion']['verID'] !== '') {
            //               this.pagoGnp(dataSol.idCotizacionAli);
            //               console.log('tarjeta valida');
            //             }else {
            //               console.log('tareta no valida');
            //             }
            // fin simulacion


            break;
          default:

            break;
        }
      });
    });
    // delete this.response['emision']['resultado'];
    //    this.response['emision']['resultado'] = true;
    // ejemplo de pago que devuelve
    // this.response['emision'] = {
    //   "poliza": "17160214702",
    //   "derechos": "520",
    //   "impuesto": "949.52",
    //   "recargos": "",
    //   "terminal": "",
    //   "documento": "http://qbcenter.qualitas.com.mx/polizaprueba/c7160214768.pdf|" +
    //     "http://qbcenter.qualitas.com.mx/polizaprueba/r7160214768.pdf|" +
    //     "http://qbcenter.qualitas.com.mx/polizaprueba/p7160214768.pdf",
    //   "primaNeta": "6445.6",
    //   "resultado": true,
    //   "primaTotal": "7930.56",
    //   "primerPago": "",
    //   "iDCotizacion": "",
    //   "pagosSubsecuentes": ""
    // }
    // console.log(this.response['emision'])
    //
    //
    //
    //   // // this.nextStepper.emit(2);
    //    //// this._idProductoCliente.emit(producto.id);
    //
    //  this.cotizacionAli['respuesta'] = JSON.stringify(this.response);
    //
    //  this.cotizacionesAliService.put(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
    //
    //    // si si pago que pase a la sig pag
    //    if (this.response['emision']['resultado'] === true) {
    //      // muestre una ventana de pago exitoso
    //      this.openDialogMessageSuccess();
    //
    //    } else {
    //      // no ha pagado-
    //      this.noPayment();
    //
    //      console.log('El pago ha sido negado');
    //    }
    //  });
    //  fin simulacion
    // // });// banco

  }

  private noPayment() {
    Swal.fire({
      title: 'El pago no ha sido procesado',
      timer: 6000,
      text: '!Por favor verificar datos de tarjeta!',
    });
  }


  private openDialogMessageSuccess() {


    switch (this.nombreAseguradora) {
      case 'QUALITAS':
        const dialogRef2 = this.dialog.open(MessagePagoSuccessComponent, {
          panelClass: 'paySuccesModal',
          data: {
            //  numpoliza: this.response['emision']['poliza'],
            // real
             numpoliza:  this.dataArrayPago['emision']['poliza'],
            // numpoliza: 738778374,
            blobImage: ''
          },
          disableClose: true
        });
        // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
        dialogRef2.afterClosed().subscribe((result) => {
          // En esta variable se almacena el blob que se retorna desde el modal y está en result
          const resultBlob = result;
          console.log('URL BLOB => ', resultBlob);
          // Se llama a la función y el resultado se asigna a la variable de tipo FileList
          // Se pasan como parámetros el blob y el nombre del archivo
          this.filesToUpload = this.dataUrlToFileList(resultBlob, 'Archivo de Prueba');
          // Una vez que se haya llamado a la función anterior, se llama a la de subirADrive
          // this.subirADrive();
          // localStorage.setItem('idfilesPagoPDF', JSON.stringify(this.filesToUpload));
          // localStorage.setItem('blob', JSON.stringify(resultBlob));
          console.log('entro al registro');
          console.log(this.filesToUpload);
          this.subirPagoDrive(this.idRegistro, this.filesToUpload);
          // this.idCobroSaliente.emit({idproductoc: this.idProductoCliente, filepagado: this.filesToUpload});
        });
        // // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
        // dialogRef.afterClosed().subscribe((result) => {
        //   console.log('entro al registro')
        //    this._idProductoCliente.emit(this.idProductoCliente);
        // });

        break;
      case 'GNP' :

        const dialogRef1 = this.dialog.open(MessagePagoSuccessComponent, {
          panelClass: 'paySuccesModal',
          data: {
             // numpoliza: this.response['emision']['poliza'],
            // real
              numpoliza:  this.dataArrarObtenerPoliza['emision']['poliza'],
            blobImage: ''
          },
          disableClose: true
        });
        // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
        dialogRef1.afterClosed().subscribe((result) => {
          // En esta variable se almacena el blob que se retorna desde el modal y está en result
          const resultBlob = result;
          console.log('URL BLOB => ', resultBlob);
          // Se llama a la función y el resultado se asigna a la variable de tipo FileList
          // Se pasan como parámetros el blob y el nombre del archivo
          this.filesToUpload = this.dataUrlToFileList(resultBlob, 'Archivo de Prueba');
          // Una vez que se haya llamado a la función anterior, se llama a la de subirADrive
          // this.subirADrive();
          // localStorage.setItem('idfilesPagoPDF', JSON.stringify(this.filesToUpload));
          // localStorage.setItem('blob', JSON.stringify(resultBlob));
          console.log('entro al registro');
          console.log(this.filesToUpload);
          this.idCobroSaliente.emit({idproductoc: this.idProductoCliente, filepagado: this.filesToUpload});
        });
        // // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
        // dialogRef.afterClosed().subscribe((result) => {
        //   console.log('entro al registro')
        //    this._idProductoCliente.emit(this.idProductoCliente);
        // });



        break;
      default:

        break;
    }

    // const dialogRef = this.dialog.open(MessagePagoSuccessComponent, {
    //   panelClass: 'paySuccesModal',
    //   data: {
    //      // numpoliza: this.response['emision']['poliza'],
    //     // real
    //       numpoliza:  this.dataArrayPago['emision']['poliza'],
    //     // numpoliza: 738778374,
    //     blobImage: ''
    //   },
    //   disableClose: true
    // });
    // // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
    // dialogRef.afterClosed().subscribe((result) => {
    //   // En esta variable se almacena el blob que se retorna desde el modal y está en result
    //   const resultBlob = result;
    //   console.log('URL BLOB => ', resultBlob);
    //   // Se llama a la función y el resultado se asigna a la variable de tipo FileList
    //   // Se pasan como parámetros el blob y el nombre del archivo
    //    this.filesToUpload = this.dataUrlToFileList(resultBlob, 'Archivo de Prueba');
    //   // Una vez que se haya llamado a la función anterior, se llama a la de subirADrive
    //   // this.subirADrive();
    //   // localStorage.setItem('idfilesPagoPDF', JSON.stringify(this.filesToUpload));
    //   // localStorage.setItem('blob', JSON.stringify(resultBlob));
    //   console.log('entro al registro');
    //   console.log(this.filesToUpload);
    //   this._idCobro.emit({idproductoc: this.idProductoCliente, filepagado: this.filesToUpload});
    // });
    // // Se usa el evento después de cerrar para subir el archivo a la base de datos y a drive
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log('entro al registro')
    //    this._idProductoCliente.emit(this.idProductoCliente);
    // });
  }

  // Método que convierte a un nuevo blob y lo asigna a un FileList
  private dataUrlToFileList(resultBlob: Blob, fileName: string)/* : FileList */ {
    // console.log('Data URL en dataurltofilelist', resultBlob);
    // Arreglo en el cual se asignará el nuevo blob
    const FileArray: File[] = [];
    // se crea un nuevo objeto del nuevo blob
    const blobObject = new Blob([resultBlob], { type: 'application/pdf' });
    // Se crear un nuevo objeto de tipo file pasando como parametro el blob
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El objecto file se agrega al arreglo
    FileArray.push(file);
    // console.log("Arreglo de archivos FileList => ", FileArray);
    // Se retorna el arreglo de tipo file simulado como un FileList
    return FileArray as unknown as FileList;
  }

  emitir(idSol) {
    // alert("a");
    // this.loader = true;
    // setTimeout(() => {
    //   this.loader = false;
    // }, 3000);
    console.log(this.idRegistro)
    let dataEmision: any;
    this.solicitudesService.getSolicitudesByIdOnline(idSol).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;

        switch (this.nombreAseguradora) {
          // NO ENTRA A EMITIR QUALITAS POR LA NUEVA LOGICA DE CAMBIO
          case 'QUALITASx':

            if (this.responseEmision['codigoError'] === 'Poliza ' + this.responseEmision['emision']['poliza'] + ' '
              + 'ha sido emitida no cobrada.' || this.responseEmision['emision']['poliza'] !== ''){
              console.log('poliza ya emitida no cobrada')
            }else {
              this.swalCargando2();
              this.responseEmision['aseguradora'];
              // console.log('entra a emision')
              // delete this.responseEmision['emision'];
              // // ejemplo de pago que devuelve
              // this.responseEmision['emision'] = {
              //   "poliza": "",
              //   "derechos": "",
              //   "impuesto": "",
              //   "recargos": "",
              //   "terminal": "",
              //   "documento": "",
              //   "primaNeta": "",
              //   "resultado": false,
              //   "primaTotal": "",
              //   "primerPago": "",
              //   "idCotizacion": "",
              //   "pagosSubsecuentes": ""
              // }
              console.log(this.responseEmision);
              // #region comments
              // servicio real
              this.catalogoService.emitir(this.urlService, this.urlEmision, this.responseEmision).subscribe(
                (data) => {
                  dataEmision = data;

                },
                (error) => {
                  console.error('Error => ', error);
                  // para gnp si sale error
                  this.dataArrayEmision = error.error;
                  console.log(this.dataArrayEmision);

                  this.jsonEmitirQualitas.request = this.responseEmision;

                  this.jsonEmitirQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonEmitirQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonEmitirQualitas.response = this.dataArrayEmision;
                  this.jsonEmitirQualitas.error = JSON.parse(this.dataArrayEmision['codigoError']);
                  this.jsonEmitirQualitas.poliza = this.dataArrayEmision['emision']['poliza'];
                  this.logCotizadorService.qualitasemitir(this.jsonEmitirQualitas).subscribe(log => {
                  });
                  delete this.responseEmision['emision'];

                  this.responseEmision['emision'] = this.dataArrayEmision['emision'];
                  this.responseEmision['codigoError'] = this.dataArrayEmision['codigoError'];
                  this.responseEmision['urlRedireccion'] = this.dataArrayEmision['urlRedireccion'];
                  this.responseEmision['periodicidadDePago'] = this.dataArrayEmision['periodicidadDePago'];
                  console.log(this.responseEmision['emision']['poliza'])
                  this.cotizacionAli['respuesta'] = this.responseEmision;
                  this.cotizacionesAliService.put(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                    },
                    errorb => {
                    },
                    () => {
                    });
                },
                () => {
                  this.dataArrayEmision = dataEmision;
                  console.log(this.dataArrayEmision);

                  this.jsonEmitirQualitas.request = this.responseEmision;

                  this.jsonEmitirQualitas.idEmpleado = sessionStorage.getItem('Empleado');
                  this.jsonEmitirQualitas.idSolicitudVn =  this.idSolicitud;
                  this.jsonEmitirQualitas.response = this.dataArrayEmision;
                  this.jsonEmitirQualitas.error = this.dataArrayEmision['codigoError'];
                  this.jsonEmitirQualitas.poliza = this.dataArrayEmision['emision']['poliza'];
                  this.logCotizadorService.qualitasemitir(this.jsonEmitirQualitas).subscribe(log => {
                  });

                  delete this.responseEmision['emision'];

                  this.responseEmision['emision'] = this.dataArrayEmision['emision'];
                  this.responseEmision['codigoError'] = this.dataArrayEmision['codigoError'];
                  this.responseEmision['urlRedireccion'] = this.dataArrayEmision['urlRedireccion'];
                  this.responseEmision['periodicidadDePago'] = this.dataArrayEmision['periodicidadDePago'];
                  console.log(this.responseEmision['emision']['poliza'])
                  this.cotizacionAli['respuesta'] = this.responseEmision;
                  this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                    },
                    error => {
                    },
                    () => {
                      if (this.responseEmision['codigoError'] === 'Poliza ' + this.responseEmision['emision']['poliza'] + ' '
                        + 'ha sido emitida no cobrada.') {
                        this.banderaEmitirMal = false;
                        // this.swalCargando3();
                        console.log('entro')
                      } else {
                        this.swalCargando4();
                        // this.emisionMal.emit();
                        this.banderaEmitirMal =  true;
                        // this.emisionMal.emit();
                        console.log('no emitida')
                      }
                    });
                }); // fin de servicio

              // #endregion comments
              // ejemplo simulacion
              // this.response = JSON.parse(this.cotizacionAli.respuesta);
              // // this.cotizacionAli.idSubRamo;
              // delete this.response['emision'];
              //
              // this.response['Emision'] = {
              //   "poliza": "723888309",
              //   "derechos": "520",
              //   "impuesto": "949.52",
              //   "recargos": "",
              //   "terminal": "",
              //   "documento": "http://qbcenter.qualitas.com.mx/polizaprueba/c7160211018.pdf|" +
              //     "http://qbcenter.qualitas.com.mx/polizaprueba/r7160211018.pdf|" +
              //     "http://qbcenter.qualitas.com.mx/polizaprueba/p7160211018.pdf",
              //   "primaNeta": "5525",
              //   "resultado": true,
              //   "primaTotal": "6884.02",
              //   "primerPago": "",
              //   "iDCotizacion": "",
              //   "pagosSubsecuentes": ""
              //
              // }
              //
              //
              // this.cotizacionAli['respuesta'] = JSON.stringify(this.response);
              //
              // this.cotizacionesAliService.put(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(
              //   result => {
              //   },
              //   error => { },
              //   () => {
              //
              //   });
              // //  fin de simulacion
            }
            break;
          case 'GNP' :
            this.swalCargando2();
            //this.responseEmision['aseguradora'];
            // console.log('entra a emision')
            // delete this.responseEmision['emision'];
            // // ejemplo de pago que devuelve
            // this.responseEmision['emision'] = {
            //   "poliza": "",
            //   "derechos": "",
            //   "impuesto": "",
            //   "recargos": "",
            //   "terminal": "",
            //   "documento": "",
            //   "primaNeta": "",
            //   "resultado": false,
            //   "primaTotal": "",
            //   "primerPago": "",
            //   "idCotizacion": "",
            //   "pagosSubsecuentes": ""
            // }
            console.log(this.responseEmision);

            // servicio real
            this.catalogoService.emitir(this.urlService, this.urlEmision, this.responseEmision).subscribe(
              (data) => {
                dataEmision = data;

              },
              (error) => {
                console.error('Error => ', error);
                // para gnp si sale error
                this.dataArrayEmision = error.error;
                console.log(this.dataArrayEmision);
                this.jsonEmitirGnp.request = this.responseEmision;
                delete this.responseEmision;

                this.responseEmision = this.dataArrayEmision;
                // json emitir

                this.jsonEmitirGnp.idEmpleado = sessionStorage.getItem('Empleado');
                this.jsonEmitirGnp.idSolicitudVn =  this.idSolicitud;
                this.jsonEmitirGnp.response = this.responseEmision;
                this.jsonEmitirGnp.error = this.responseEmision['codigoError'];

                this.logCotizadorService.gnpemitir(this.jsonEmitirGnp).subscribe(log => {
                });
                this.cotizacionAli['respuesta'] = this.responseEmision;
                this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                  },
                  errorb => {
                  },
                  () => {
                  });
              },
              () => {
                this.dataArrayEmision = dataEmision;
                console.log(this.dataArrayEmision);
                // envia la peticion de emision antes
                this.jsonEmitirGnp.request = this.responseEmision;


                delete this.responseEmision;

                this.responseEmision = this.dataArrayEmision;
                this.cotizacionAli['respuesta'] = this.responseEmision;
                // pone los datos de response al log emitir
                this.jsonEmitirGnp.idEmpleado =  sessionStorage.getItem('Empleado');
                this.jsonEmitirGnp.idSolicitudVn =  this.idSolicitud;
                this.jsonEmitirGnp.response = this.responseEmision;
                this.jsonEmitirGnp.error = this.responseEmision['codigoError'];

                this.logCotizadorService.gnpemitir(this.jsonEmitirGnp).subscribe(log => {
                 });
                this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
                  },
                  error => {
                  },
                  () => {
                    if (this.responseEmision['codigoError'] === '') {
                      this.banderaEmitirMal = false;
                      // this.swalCargando3();
                      console.log('entro')
                    } else if (this.responseEmision['codigoError'] !== '' && this.responseEmision['emision']['resultado'] === false) {
                      this.swalCargando4();
                      this.banderaEmitirMal =  false;
                      // this.emisionMal.emit();
                      console.log('no emitida')
                    }
                  });
              }); // fin de servicio
            break;
          default:

            break;
        }
      });
    });

  }

  swalCargando2() {
    Swal.fire({
      title: 'Se está emitiendo, espere un momento.',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
    // Swal.fire({
    //   type: 'error',
    //   title: 'Cancelled',
    //   text: 'Your offer is safe 🙂',
    //   showConfirmButton: false,
    //   timer: 2000,
    // });

  }
  // si se emitio bien
  swalCargando3() {
    Swal.fire({
      title: '¡Emison obtenida con exito!',
      text: 'Continua con Pago',
      // showCancelButton: true,
      // cancelButtonColor: '#799EB0',
      confirmButtonText: ' Comenzar pago',
      // cancelButtonText: 'Regresar a solicitudes',
      // reverseButtons: true,
      width: 500,
    }).then(value => {
      if (value.value) {
        window.close();


      } else {
        // window.close();
      }
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
  }

  swalCargando4() {
    Swal.fire({
      title: 'Se obtuvo un erro al amitir',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 6000);


  }
  // manda a llamar el servicio de pago de gnp
  pagoGnp(idCotizacionAli) {
    let dataPago: any;
    let dataEmi: any;

    console.log(this.responsePago);
    console.log(this.response);
    // servicio real
    // hace el cuadro de pago exitoso si pago y se obtiene la poliza de otro servicio postObtenerEmision
    this.catalogoService.postPago(this.urlService, this.urlPago, this.response)
      .subscribe(
        (data) => {
          dataPago = data;

        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {
          this.dataArrayPago = dataPago;
          // this.responsepago = this.dataArrayPago;
          console.log(this.dataArrayPago)


          // log de pago
          this.jsonPagoGnp.request = this.response;
          delete this.response;
          this.response = this.dataArrayPago;



          this.jsonPagoGnp.idEmpleado =  sessionStorage.getItem('Empleado');
          this.jsonPagoGnp.idSolicitudVn =  this.idSolicitud;
          this.jsonPagoGnp.response = this.response;
          this.jsonPagoGnp.error = this.response['codigoError'];
          this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
          });


          if (this.dataArrayPago['emision']['resultado'] === true) {
            // muestre una ventana de pago exitoso
            // para obtener la poliza en gnp se manda a llamar otro servicio
            this.catalogoService.postObtenerEmision(this.urlService, this.urlObtenerEmision, this.response).subscribe(
              (dataemi) => {
                dataEmi = dataemi;

              },
              (error) => {
                console.error('Error => ', error);
                this.dataArrarObtenerPoliza = error.error;
                // this.responsepago = this.dataArrayPago;
                console.log(this.dataArrarObtenerPoliza)

                this.cotizacionAli['respuesta'] = this.dataArrarObtenerPoliza;

                this.cotizacionesAliService.putOnline(idCotizacionAli, this.cotizacionAli).subscribe(result => {
                });
              },
              () => {
                this.dataArrarObtenerPoliza = dataEmi;
                // this.responsepago = this.dataArrayPago;
                console.log(this.dataArrarObtenerPoliza)

                this.cotizacionAli['respuesta'] = this.dataArrarObtenerPoliza;

                // log de pago para obtener la poliza
                this.jsonPagoGnp.request = this.response;

                delete this.response;
                this.responseObtenerEmision = this.dataArrayValidacionTarjeta;

                this.jsonPagoGnp.idEmpleado =  sessionStorage.getItem('Empleado');
                this.jsonPagoGnp.idSolicitudVn =  this.idSolicitud;
                this.jsonPagoGnp.response = this.responseObtenerEmision;
                this.jsonPagoGnp.error = this.responseObtenerEmision['codigoError'];
                this.jsonPagoGnp.poliza = this.responseObtenerEmision['emision']['poliza'];
                this.jsonPagoGnp.banderaPoliza = 1;
                this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
                });
                // guarda lo que trae el servicio , independientemente se trae error o no
                this.cotizacionesAliService.putOnline(idCotizacionAli, this.cotizacionAli).subscribe(result => {
                  },
                  errorb => {
                  },
                  () => {
                    // si emision poliza trae la poliza diferente de nulo es que trago la poliza y abre dialogo de pagado
                    if (this.dataArrarObtenerPoliza['emision']['poliza'] !== '') {
                      this.openDialogMessageSuccess();
                    }
                  });
              });

          } else {
            // no ha pagado-
            this.noPayment();
            console.log('El pago ha sido negado');
            this.cotizacionAli['respuesta'] = this.response;

            this.cotizacionesAliService.putOnline(idCotizacionAli, this.cotizacionAli).subscribe(result => {
            });
          }

          // this.cotizacionAli['respuesta'] = this.response;

          // this.cotizacionesAliService.put(idCotizacionAli, this.cotizacionAli).subscribe(result => {
          // });
        });







    // ejemplo responde
    //   console.log(this.response)
    // this.jsonPagoGnp.request = this.response;
    // // emulacion del pago correcto
    // delete this.response['emision']['resultado'];
    // this.response['emision']['resultado'] = true;
    //
    // this.jsonPagoGnp.idEmpleado =  sessionStorage.getItem('Empleado');
    // this.jsonPagoGnp.idSolicitudVn =  this.idSolicitud;
    // this.jsonPagoGnp.response = this.response;
    // this.jsonPagoGnp.error = this.responseEmision['codigoError'];
    // this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
    //   console.log(log)
    //   console.log('entro 2 ')
    // });
    //  // simulacion del obtencion de la poliza
    // this.jsonPagoGnp.request = this.responsePago;
    // delete this.response['emision']['poliza'];
    // this.response['emision']['poliza'] = 'XGNP000000003';
    //
    // this.jsonPagoGnp.idEmpleado =  sessionStorage.getItem('Empleado');
    // this.jsonPagoGnp.idSolicitudVn =  this.idSolicitud;
    // this.jsonPagoGnp.response = this.response;
    // this.jsonPagoGnp.error = this.responseEmision['codigoError'];
    // this.jsonPagoGnp.poliza = this.response['emision']['poliza'];
    // this.jsonPagoGnp.banderaPoliza = 1;
    // this.logCotizadorService.gnppago(this.jsonPagoGnp).subscribe(log => {
    //   console.log(log)
    //   console.log('entro 3 ')
    // });
    // console.log(this.response['emision']);
    // this.cotizacionAli['respuesta'] = this.response;
    //
    //
    // this.cotizacionesAliService.putOnline(idCotizacionAli, this.cotizacionAli).subscribe(result => {
    //
    //   // si si pago que pase a la sig pag
    //   if (this.response['emision']['resultado'] === true) {
    //     // muestre una ventana de pago exitoso
    //     delete this.response['emision']['poliza'];
    //     this.response['emision']['poliza'] = 'XGNP000000003';
    //     this.openDialogMessageSuccess();
    //
    //   } else {
    //     // no ha pagado-
    //     this.noPayment();
    //
    //     console.log('El pago ha sido negado');
    //   }
    // });
    //  fin simulacion
  }

  validarTarjeta() {
    let databanco: any;
    switch (this.nombreAseguradora) {
      case 'QUALITAS':

        break;
      case 'GNP' :
        this.formDatosPago.controls.idBanco.enable();
        if (this.formDatosPago.controls.numeroTarjeta.valid) {
          this.catalogoService.getBancosExterno(this.urlService, this.urlBanco).subscribe(dataBan => {
            // this.bancosOnline = dataBan["CATALOGO"];
            databanco = dataBan;
            this.Arraybanco = databanco;
            this.bancosOnline = this.Arraybanco["CATALOGO"]
            console.log(this.bancosOnline)
          });
        }

        break;
      default:

        break;
    }
  }

  swalCargandotarjetanovalida(){
    Swal.fire({
      title: 'Error al validar la tarjeta',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 3000);
  }
  // hacer pago
  subirPagoDrive(idRegistro, idfiles) {

    // nuevo migracion
    this.btnLoadP = true;
    // Swal.fire({
    //   title: 'Subiendo el comprobante de pago ingresado',
    //   allowOutsideClick: false,
    //   onBeforeOpen: () => {
    //     Swal.showLoading();
    //   },
    // });

    // this.valuePdfPago  = JSON.parse(localStorage.getItem('blob'));

    this.valuePdfPago = idfiles;
    console.log('direccion de pdf pago')
    console.log(this.valuePdfPago);
    // const doc = new jsPDF('p', 'px', 'a4');
    // const newblob = new Blob()

    // const request = new XMLHttpRequest();
    // request.open('GET', this.valuePdfPago , true);
    // request.responseType = 'blob';
    // request.onload = function() {
    //   const reader = new FileReader();
    //   reader.readAsDataURL(request.response);
    //   reader.onload =  function(e: any){
    //     console.log('DataURL:', e.target.result);
    //   };
    // };
    // request.send();
    this.filesToUploadPago = this.dataUrlToFileList(this.valuePdfPago, 'Archivo de Prueba');

    console.log(this.filesToUploadPago)
    console.log(idfiles)

    // nuevo

    this.pago = JSON.parse(localStorage.getItem('pago'));


    this.recibosService.getRecibosByIdRegistro(idRegistro).subscribe(dataR => {
      if (dataR) {
        this.recibo = dataR[0];

        const pago: Pagos = {
          id: 0,
          idRecibo: this.recibo.id,
          idFormaPago: this.pago.idFormaPago,
          idEstadoPago: 1,
          idEmpleado: this.pago.idEmpleado,
          fechaPago: this.pago.fechaPago,
          cantidad: this.pago.cantidad,
          archivo: '',
          datos: localStorage.getItem('datosPago'),
        };

        this.registroService.getRegistroByIdOnline(this.idRegistro).subscribe(data => {

          console.log(data);
          this.registroOnline = data;
          this.registroOnline['online'] = 1;
          console.log(this.registroOnline)

          this.registroService.putOnline(this.idRegistro, this.registroOnline).subscribe({
            complete: () => {
            },
          });
        });
        this.pagosService.subirDrive(idfiles, this.route.snapshot.paramMap.get('id_solicitud'), pago).subscribe({
          next: dataP => {
            this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
              this.Inspeccione.emit(idRegistro)
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
        });
      }
    });

  }
}


