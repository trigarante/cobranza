import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MarcasOnline} from "../../../../../@core/data/interfaces/cotizador/marcas-online";
import {ModelosOnline} from "../../../../../@core/data/interfaces/cotizador/modelos-online";
import {DescripcionesOnline} from "../../../../../@core/data/interfaces/cotizador/descripciones-online";
import {SubmarcasOnline} from "../../../../../@core/data/interfaces/cotizador/submarcas-online";
import {DetalleInterno} from "../../../../../@core/data/interfaces/cotizador/detalle-interno";
import {TipoSubramo} from "../../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo";
import {CotizacionesAli} from "../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {ProductoCliente} from "../../../../../@core/data/interfaces/venta-nueva/producto-cliente";
import {FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {TipoSubramoService} from "../../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service";
import {urlAseguradoras} from "../../../../../@core/data/interfaces/cotizador/urlAseguradoras";
import {CatalogoService} from "../../../../../@core/data/services/cotizador/catalogo.service";
import {ProductoClienteService} from "../../../../../@core/data/services/venta-nueva/producto-cliente.service";
import Swal from "sweetalert2";
import {RegistroService} from "../../../../../@core/data/services/venta-nueva/registro.service";
import {SolicitudesvnService} from "../../../../../@core/data/services/venta-nueva/solicitudes.service";
import {CotizacionesAliService} from "../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";

@Component({
  selector: 'app-datos-productos-online-new',
  templateUrl: './datos-productos-online-new.component.html',
  styleUrls: ['./datos-productos-online-new.component.scss']
})
export class DatosProductosOnlineNewComponent implements OnInit {
  @Input() idCliente: number;
  @Input() idSubRamo: number;
  @Input() idRegistroPoliza: number;
  @Input() idProductoCliente: number;
  @Input() datosCotizacion: string;
  @Input() datosRespuesta: string;
  @Output() idProductoClientesaliente = new EventEmitter<number>();
  @Output() idEmitir = new EventEmitter<number>();

  marca: string;
  modelo: string;
  submarca: string;
  descripcion: string;
  marcas: MarcasOnline[];
  modelos: ModelosOnline[];
  descripciones: DescripcionesOnline[];
  submarcas: SubmarcasOnline[];
  detalles: DetalleInterno[];
  tipoSubRamos: TipoSubramo[];
  idSolicitud: number;
  datos: any;
  responseEmision: any;
  cotizacionAli: CotizacionesAli;
  productoCliente: ProductoCliente;
  vehiculoJson: object;
  responseProducto: any;

  formDatos: FormGroup = new FormGroup({
    numeroMotor: new FormControl('', [Validators.pattern('^[A-Z0-9 ]+$'), Validators.maxLength(30)]),
    numeroPlacas: new FormControl(/**/'S/N'),
    numeroSerie: new FormControl('', Validators.compose([ Validators.required,
        Validators.pattern('^([A-HJ-NPR-Z0-9]{8}|[A-HJ-NPR-Z0-9]{17})$')]),
      this.validarNoSerieNoExista.bind(this)),
    recotizar: new FormControl(false),
    marca: new FormControl(''),
    modelo: new FormControl(''),
    descripcion: new FormControl(''),
    submarca: new FormControl(''),
  });
  tipoSubRamo: TipoSubramo;
  token: string;
  nombreAseguradora: string;
  service: any;
  urlService: string;
  urlCatalogo: string;

  constructor(private router: ActivatedRoute,
              private route: Router,
              private routerr: ActivatedRoute,
              private tipoSubRamoService: TipoSubramoService,
              private catalogoService: CatalogoService,
              private productoClienteService: ProductoClienteService,
              private registroService: RegistroService,
              private solicitudesService: SolicitudesvnService,
              private cotizacionesAliService: CotizacionesAliService) {
    this.idSolicitud =  +this.routerr.snapshot.paramMap.get('id_solicitud');
    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.tipoSubRamoService.getVN().subscribe(data => {
      this.tipoSubRamos = data;
    });
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora)
    // console.log(this.service);
    this.urlService = this.service.service;
    this.urlCatalogo = this.service.catalogo;
    this.getMarcas();
  }

  ngOnInit(): void {
    if (this.datosCotizacion === null) {
      this.datos = null;
    } else {
      this.datos = this.datosCotizacion;
      // console.log(this.datos)
    }
    if (this.idProductoCliente !== 0) {
      this.formDatos.controls.numeroSerie.clearAsyncValidators();
      this.getProductoCliente();
    }
  }
  getMarcas() {
    // console.log(this.urlService)
    this.catalogoService.getMarcas(this.urlService, this.urlCatalogo).subscribe(m => {
      this.marcas = m;
    });


  }

  getProductoCliente() {
    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe( { next: data => {
        const datos = data.datos;
        this.formDatos.controls.numeroPlacas.setValue(datos.numeroPlacas);
        this.formDatos.controls.numeroMotor.setValue(datos.numeroMotor);
        this.formDatos.controls.numeroSerie.setValue(datos.numeroSerie);
      }, complete: () => {
        this.formDatos.controls.numeroSerie.setAsyncValidators(this.validarNoSerieNoExista.bind(this));
      }});
  }
  validarNoSerieNoExista(): Promise<ValidationErrors | null> {
    return new Promise((resolve) => {
      this.registroService.getNoSerieExistente(this.formDatos.controls.numeroSerie.value).subscribe(
        data => {
          if (!data) {
            resolve(null);
          } else {
            Swal.fire({
              title: 'Número de serie en el sistema',
              html: 'Actualmente ya existe una poliza activa con este número de serie y se encuetra en: <strong>'
                + data.noSerie + '</strong>',
            }).then(() => {
              // this.route.navigate(['/modulos/dashboard']);
              resolve({placaExiste: true});
            });
          }
        });
    });
  }
  /** Swal Cargando **/
  swalCargando() {
    Swal.fire({
      title: 'Se está subiendo el producto ingresado',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    setTimeout(() => {
      Swal.close();
    }, 5000);
  }

  /** Swal Error **/
  swalError() {
    Swal.fire({
      title: 'Error',
      text: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
    });
  }

  /** Swal apra finalizar **/
  swalFinal(producto) {

    let textoPrimerAviso =
      '<style>#swal2-content {' +
      '  text-align: left;' +
      '  margin-left: 40px;' +
      '}</style>' +
      '<strong>Numero de Placas: </strong>' +
      this.formDatos.controls['numeroPlacas'].value +
      '</br>' +

      '<strong>Numero de motor: </strong>' +
      this.formDatos.controls['numeroMotor'].value +
      '</br>' +
      '<strong>Numero de Serie: </strong>' +
      this.formDatos.controls['numeroSerie'].value +
      '</br>' +
      '</br>';
    textoPrimerAviso += '</br></br><strong> <div style="text-align: center;"> <p><b>  ¿Estás seguro que desas continuar?' +
      '</p></b> </div> </strong>';
    if (producto) {
      Swal.fire({
        title: 'Información ingresada',
        text: '¡Tu producto ha sido creado exitosamente!',
      }).then(() => {  Swal.fire({
        title: 'Estos son los datos que ingresaste:',
        showCancelButton: true,
        cancelButtonText: 'Revisar nuevamente',
        confirmButtonText: '¡Sí, estoy seguro!',
        cancelButtonColor: 'red',
        confirmButtonColor: 'green',
        reverseButtons: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        html: textoPrimerAviso,
      }).then((firstWarning) => {
        // this.btnLoad = false;
        if (firstWarning.value) {
          Swal.fire({
            title: 'Confirmación',
            html: '<p>¡En el siguiente paso se Emitira y solo se podra registrar la poliza una sola vez, si ingresa otra vez podria ' +
              'ameritar una <strong>sanción económica</strong>!</p>' +
              '<p><b>¿Estás seguro que deseas continuar?</b></p>',
            cancelButtonColor: 'red',
            confirmButtonColor: 'green',
            showCancelButton: true,
            cancelButtonText: 'Revisar nuevamente',
            confirmButtonText: '¡Sí, estoy seguro!',
            reverseButtons: true,
          }).then(secondWarning => {
            if (secondWarning.value) {
              // Swal.fire({
              //   title: 'Generando emision',
              //   allowOutsideClick: false,
              //   onBeforeOpen: () => {
              //     Swal.showLoading();
              //   },
              // });
              // this._idProductoCliente.emit(producto.id);
              // this.gruposCargados = true;
              // this.registroEnUno = {
              //   registro: datosRegistro,
              //   recibos: recibos,
              // };

            } else {

            }
            // this.btnLoad = false;
          });
        }
      }); });
    } else {
      Swal.fire({
        title: 'Información actualizada',
        text: '¡Tu producto se actualizó exitosamente!',
      });
    }
  }
  // si modifica el producto
  /** Swal apra finalizar **/
  swalFinal2(idproducto) {

    let textoPrimerAviso =
      '<style>#swal2-content {' +
      '  text-align: left;' +
      '  margin-left: 40px;' +
      '}</style>' +
      '<strong>Numero de Placas: </strong>' +
      this.formDatos.controls['numeroPlacas'].value +
      '</br>' +

      '<strong>Numero de motor: </strong>' +
      this.formDatos.controls['numeroMotor'].value +
      '</br>' +
      '<strong>Numero de Serie: </strong>' +
      this.formDatos.controls['numeroSerie'].value +
      '</br>' +
      '</br>';
    textoPrimerAviso += '</br></br><strong>¿Estás seguro que desas continuar? </strong>';
    if (idproducto) {
      Swal.fire({
        title: 'Información ingresada',
        text: '¡Tu producto ha sido modificado exitosamente!',
      }).then(() => {  Swal.fire({
        title: 'Estos son los datos que ingresaste:',
        showCancelButton: true,
        cancelButtonText: 'Revisar nuevamente',
        confirmButtonText: '¡Sí, estoy seguro!',
        cancelButtonColor: 'red',
        confirmButtonColor: 'green',
        reverseButtons: true,
        allowOutsideClick: false,
        allowEscapeKey: false,
        html: textoPrimerAviso,
      }).then((firstWarning) => {
        // this.btnLoad = false;
        if (firstWarning.value) {
          Swal.fire({
            title: 'Confirmación',
            html: '<p>¡En el siguiente paso se Emitira y solo se podra registrar la poliza una sola vez, si ingresa otra vez podria ' +
              'ameritar una <strong>sanción económica</strong>!</p>' +
              '<p><b>¿Estás seguro que deseas continuar?</b></p>',
            cancelButtonColor: 'red',
            confirmButtonColor: 'green',
            showCancelButton: true,
            cancelButtonText: 'Revisar nuevamente',
            confirmButtonText: '¡Sí, estoy seguro!',
            reverseButtons: true,
          }).then(secondWarning => {
            if (secondWarning.value) {
              Swal.fire({
                title: 'Generando emision',
                // allowOutsideClick: false,
                // onBeforeOpen: () => {
                //   Swal.showLoading();
                // },
              });
          //    this._idProductoCliente.emit(idproducto);
              // this.gruposCargados = true;
              // this.registroEnUno = {
              //   registro: datosRegistro,
              //   recibos: recibos,
              // };

            } else {

            }
            // this.btnLoad = false;
          });
        }
      }); });
    } else {
      Swal.fire({
        title: 'Información actualizada',
        text: '¡Tu producto se actualizó exitosamente!',
      });
    }
  }

  crearProducto(esPost: boolean) {
    let producto: ProductoCliente;
    let productoCliente;
    this.swalCargando();
    this.datos['numeroPlacas'] = this.formDatos.controls.numeroPlacas.value;
    this.datos['numeroMotor'] = this.formDatos.controls.numeroMotor.value;
    this.datos['numeroSerie'] = this.formDatos.controls.numeroSerie.value;
    if (this.formDatos.controls.recotizar.value) {
      this.datos['marca'] = this.marcas.filter(marca => marca.text === this.formDatos.controls.marca.value)[0].text;
      this.datos['modelo'] = this.modelos.filter(modelo => modelo.text === this.formDatos.controls.modelo.value)[0].text;
      this.datos['submarca'] = this.submarcas.filter(submarca => submarca.text ===
        this.formDatos.controls.submarca.value)[0].text;
      this.datos['descripcion'] = this.descripciones.filter(descripcion => descripcion.text ===
        this.formDatos.controls.descripcion.value)[0].text;

    }
    productoCliente = {
      'idSolicitud': this.idSolicitud,
      'idCliente': this.idCliente,
      'datos': this.datos,
      'idSubRamo': this.idSubRamo,
    };
    // console.log(productoCliente);
    // console.log(this.idSubRamo);
    // console.log(this.idSolicitud);
    if (esPost) {
      this.productoClienteService.post(productoCliente).subscribe({
          next: data => {
            // this.productoClienteService.entryIdProductoCliente(data.id);
            // this.productoClienteService.entryAseguradora(0);
            producto = data;
          },
          error: () => {
            this.swalError();
          },
          complete: () => {

            this.modificarJsonCotizacion(producto.id);
            // this._idProductoCliente.emit(producto.id);
            // this.swalFinal(producto);
          },
        },
      );
    } else {
      this.productoClienteService.put(this.idProductoCliente, productoCliente).subscribe({
        error: () => {
          this.swalError();
        },
        complete: () => {
          this.modificarJsonCotizacion(this.idProductoCliente);
          // this._idProductoCliente.emit(this.idProductoCliente);
          // this.swalFinal2(this.idProductoCliente);
        },
      });
    }

  }
  modificarJsonCotizacion(idProducto) {
    // console.log(idProducto)
    this.solicitudesService.getSolicitudesById(this.idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;


        this.productoClienteService.getProductoClienteById(idProducto).subscribe(dataProducto => {
          this.productoCliente = dataProducto;
          this.responseProducto = this.productoCliente.datos;
          this.vehiculoJson = {
            "uso": "PARTICULAR",
            "clave": this.responseProducto.clave,
            "marca": this.responseProducto.marca,
            "codUso": "",
            "modelo": this.responseProducto.modelo,
            "noMotor": this.responseProducto.numeroMotor,
            "noSerie": this.responseProducto.numeroSerie,
            "codMarca": "",
            "noPlacas": this.responseProducto.numeroPlacas,
            "servicio": this.responseProducto.servicio,
            "subMarca": this.responseProducto.subMarca,
            "descripcion": this.responseProducto.descripcion,
            "codDescripcion": ""
          };
          // console.log(this.vehiculoJson);
          delete this.responseEmision['vehiculo'];
          this.responseEmision['vehiculo'] = this.vehiculoJson;
          this.cotizacionAli['respuesta'] = this.responseEmision;
          this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe({
            next: data => {
            },
            error: () => {
            },
            complete: () => {
               // this.idProductoClientesaliente.emit(idProducto);
               // caso qualitas
               // if (this.nombreAseguradora === 'QUALITAS'){
               //   this.idEmitir.emit(idProducto);
               // }
               switch (this.nombreAseguradora) {
                 case 'QUALITAS':
                  // se va al step emitir, pago, salta registro, inspeccion
                  this.idEmitir.emit(idProducto);
                  break;
                 case 'GNP' :
                  // va a al step pago, registro y inspeccion
                  this.idProductoClientesaliente.emit(idProducto);

                  break;
                default:

                  break;
              }

            },
          });
        });

      }); // fin cotizacion

    }); // fin solicitud
  }
  verificarNumeroPlaca() {
    if (this.formDatos.controls.numeroPlacas.value === '') {
      this.formDatos.controls.numeroPlacas.setValue('S/N');
    }
  }
  actualizarValidador() {
    if (this.formDatos.controls.recotizar.value) {

      this.formDatos.controls.marca.setValidators([Validators.required]);
      this.formDatos.controls.modelo.setValidators([Validators.required]);
      this.formDatos.controls.descripcion.setValidators([Validators.required]);
      this.formDatos.controls.submarca.setValidators([Validators.required]);
    } else {
      this.formDatos.controls.marca.clearValidators();
      this.formDatos.controls.modelo.clearValidators();
      this.formDatos.controls.descripcion.clearValidators();
      this.formDatos.controls.submarca.clearValidators();
    }
    this.formDatos.controls.marca.updateValueAndValidity();
    this.formDatos.controls.modelo.updateValueAndValidity();
    this.formDatos.controls.descripcion.updateValueAndValidity();
    this.formDatos.controls.submarca.updateValueAndValidity();
  }
  getModelos() {
    this.catalogoService.getModelos(this.urlService, this.urlCatalogo, this.marca).subscribe(m => {
      this.modelos = m;
    });
  }

  getSubmarca() {
    this.catalogoService.getSubMarca(this.urlService, this.urlCatalogo, this.marca, this.modelo)
      .subscribe(m => {
        this.submarcas = m;
      });

  }

  getDescripcion() {
    this.catalogoService.getDescripcion(this.urlService, this.urlCatalogo,
      this.marca, this.modelo,
      this.submarca).subscribe(m => {
      this.descripciones = m;
    });
  }

}

