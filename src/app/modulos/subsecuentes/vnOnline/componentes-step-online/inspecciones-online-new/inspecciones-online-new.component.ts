import {Component, Input, OnInit, ViewChild} from '@angular/core';
import Swal from 'sweetalert2';
import {MatSlideToggle} from '@angular/material/slide-toggle';
import {MatRadioButton} from '@angular/material/radio';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClienteVn} from '../../../../../@core/data/interfaces/venta-nueva/cliente-vn';
import {Sepomex} from '../../../../../@core/data/interfaces/catalogos/sepomex';
import {Paises} from '../../../../../@core/data/interfaces/catalogos/paises';
import {EstadoInspeccion} from '../../../../../@core/data/interfaces/venta-nueva/catalogos/estado-inspeccion';
import {ActivatedRoute, Router} from '@angular/router';
import {InspeccionesService} from '../../../../../@core/data/services/venta-nueva/inspecciones.service';
import {ConductorHabitualService} from '../../../../../@core/data/services/venta-nueva/conductor-habitual.service';
import {EstadoInspeccionService} from '../../../../../@core/data/services/venta-nueva/catalogos/estado-inspeccion.service';
import {PaisesService} from '../../../../../@core/data/services/catalogos/paises.service';
import {SepomexService} from '../../../../../@core/data/services/catalogos/sepomex.service';
import {SolicitudesvnService} from '../../../../../@core/data/services/venta-nueva/solicitudes.service';
import {ValidacionTelefonoService} from '../../../../../@core/data/services/venta-nueva/validacion-telefono.service';
import {RegistroService} from '../../../../../@core/data/services/venta-nueva/registro.service';
import {DriveService} from '../../../../../@core/data/services/ti/drive.service';
import {ClienteVnService} from '../../../../../@core/data/services/venta-nueva/cliente-vn.service';
import {RecibosService} from '../../../../../@core/data/services/venta-nueva/recibos.service';
import {AdminVnViewService} from '../../../../../@core/data/services/venta-nueva/adminVnView.service';
import {map} from 'rxjs/operators';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-inspecciones-online-new',
  templateUrl: './inspecciones-online-new.component.html',
  styleUrls: ['./inspecciones-online-new.component.scss']
})
export class InspeccionesOnlineNewComponent implements OnInit {
  @ViewChild('conductorCheck') checkin: MatSlideToggle;
  @ViewChild('check1') radCheck: MatRadioButton;
  @Input() idRegistro: number;
  @Input() idCliente: number;
  @Input() esStep: boolean;
  inspeccionesForm: FormGroup = new FormGroup({
    cp: new FormControl('', Validators.required),
    calle: new FormControl('', Validators.required),
    numInt: new FormControl(''),
    numExt: new FormControl('', Validators.required),
    idColonia: new FormControl('', Validators.required),
    fechaInspeccion: new FormControl(''),
    kilometraje: new FormControl('', Validators.pattern('[0-9]+')),
    placas: new FormControl({disabled: true, value: ''}),
    urlPostventa: new FormControl(''),
    comentarios: new FormControl(''),
  });
  inspeccionOnline: FormGroup = new FormGroup({
    urlInspeccionOnline: new FormControl(''),
  });
  opcionesRadioButton = ['No', 'Si'];
  respuestaDireccion = 'No';
  cliente: ClienteVn;
  conductorHabitualForm: FormGroup;
  colonias: Sepomex[];
  coloniasDos: Sepomex[];
  paises: Paises[];
  estadosInspeccion: EstadoInspeccion[];
  urlPostVenta: any;
  telefonoFijo;
  telefonoMovil;
  filesToUpload: FileList;
  archivoValido = false;
  carpetaDrive: string;
  coloniasSe: Sepomex[];
  unoCheked: boolean;

  btnLoad = false;
  private textError = 5;
  constructor(private inspeccionService: InspeccionesService,
              private conductorHabitualService: ConductorHabitualService,
              private estadoInspeccionService: EstadoInspeccionService,
              private paisesService: PaisesService,
              private sepomexService: SepomexService,
              private solicitudesService: SolicitudesvnService,
              private telefonoService: ValidacionTelefonoService,
              private registroService: RegistroService,
              private router: Router,
              private route: ActivatedRoute,
              private driveService: DriveService,
              private clienteService: ClienteVnService,
              private recibosService: RecibosService,
              private administracionPolizaService: AdminVnViewService,
              private notificaciones: NotificacionesService) { }

  ngOnInit(): void {
    console.log('entro a inspecciones new');
    this.solicitudesService.returnentryURLType().subscribe(vall => {
      this.urlPostVenta = vall;
    });
    this.conductorHabitualForm = new FormGroup({
      idPais: new FormControl('', Validators.required),
      idRegistro: new FormControl(10),
      nombre: new FormControl('', Validators.required),
      paterno: new FormControl('', Validators.required),
      materno: new FormControl('', Validators.required),
      correo: new FormControl('', Validators.required),
      cp: new FormControl('', Validators.required),
      idColonia: new FormControl('', Validators.required),
      calle: new FormControl('', Validators.required),
      numInt: new FormControl('', Validators.required),
      numExt: new FormControl('', Validators.required),
      genero: new FormControl('', Validators.required),
      telefonoFijo: new FormControl('', Validators.compose([Validators.minLength(10),
        Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]+')])),
      telefonoMovil: new FormControl('', Validators.compose([Validators.minLength(10),
        Validators.maxLength(10), Validators.required, Validators.pattern('[0-9]+')])),
      fechaNacimiento: new FormControl('', Validators.required),
      fechaRegistro: new FormControl('', Validators.required),
      urlPostventa: new FormControl('', Validators.required),
    });
    this.getEstadoInspeccion();
    this.getPaises();
    this.getRegistro();
  }

  getCliente() {
    this.clienteService.getClienteById(this.idCliente).subscribe(data => {
      this.cliente = data;
      this.inspeccionesForm.controls.cp.setValue(this.cliente.cp);
      this.inspeccionesForm.controls.calle.setValue(this.cliente.calle);
      this.getColonias(this.cliente.cp);
      this.inspeccionesForm.controls.numInt.setValue(this.cliente.numInt);
      this.inspeccionesForm.controls.numExt.setValue(this.cliente.numExt);
      this.inspeccionesForm.controls.idColonia.setValue(this.cliente.idColonia);
    });
  }

  getColonias(cp: number) {
    this.sepomexService.getColoniaByCp(cp).subscribe(resp => {
      this.coloniasSe = resp;
    });
  }

  disableInspeccionesForm(opcion) {
    if (opcion === 'No') {
      this.inspeccionesForm.enable();
      this.inspeccionesForm.controls.cp.setValue('');
      this.inspeccionesForm.controls.calle.setValue('');
      this.inspeccionesForm.controls.numInt.setValue('');
      this.inspeccionesForm.controls.numExt.setValue('');
      this.inspeccionesForm.controls.idColonia.setValue('');
      this.inspeccionesForm.controls.placas.disable();
      this.coloniasSe = [];
    } else {
      this.inspeccionesForm.disable();
      this.inspeccionesForm.controls.fechaInspeccion.enable();
      this.inspeccionesForm.controls.kilometraje.enable();
      this.inspeccionesForm.controls.comentarios.enable();
      this.getCliente();
    }
  }

  getEstadoInspeccion() {
    this.estadoInspeccionService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(
      dato => {
        this.estadosInspeccion = dato;
      },
    );
  }

  getPaises() {
    this.paisesService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(
      dato => {
        this.paises = dato;
      },
    );
  }

  getRegistro() {
    this.registroService.getRegistroByIdOnline(this.idRegistro).subscribe(data => {
      console.log(data.datos);
      const datosJSON = data.datos;
      console.log(data.datos);
      console.log(datosJSON.numeroPlacas);
      this.inspeccionesForm.controls.placas.setValue(datosJSON.numeroPlacas);
      this.carpetaDrive = data.archivo;
    });
  }

  validarCp(value, control) {
    const cpParse = parseInt(value, 10);
    if (value === '' || value.length < 4 || this.inspeccionesForm.controls.cp.invalid) {
      this.coloniasSe = [];
    } else {
      this.sepomexService.getColoniaByCp(cpParse).subscribe(res => {
        if (res.length !== 0) {
          if (control === 1) {
            this.inspeccionesForm.controls.cp.setErrors(null);
            this.coloniasSe = res;
          } else {
            this.conductorHabitualForm.controls.cp.setErrors(null);
            this.coloniasDos = res;
          }
        } else {
          if (control === 1) {
            this.inspeccionesForm.controls.cp.setErrors({'incorrect': true});
          } else {
            this.conductorHabitualForm.controls.cp.setErrors({'incorrect': true});
          }
        }
      }, error => error);
    }
  }

  // guardarInspeccion(idArchivoDrive: string) {
  //
  //   Swal.fire({
  //     title: 'Se está subiendo la inspección ingresada',
  //     allowOutsideClick: false,
  //     allowEscapeKey: false,
  //     onBeforeOpen: () => {
  //       Swal.showLoading();
  //     },
  //   });
  //
  //   const Json: any = {
  //     idRegistro: this.idRegistro,
  //     idEstadoInspeccion: 1,
  //     idColonia: this.inspeccionesForm.controls.idColonia.value,
  //     cp: this.inspeccionesForm.controls.cp.value,
  //     fechaInspeccion: this.inspeccionesForm.controls.fechaInspeccion.value,
  //     kilometraje: this.inspeccionesForm.controls.kilometraje.value,
  //     placas: this.inspeccionesForm.controls.placas.value,
  //     // urlPostventa: this.urlPostVenta,
  //     urlPostventa: this.inspeccionOnline.controls.urlInspeccionOnline.value,
  //     archivo: '',
  //     calle: this.inspeccionesForm.controls.calle.value,
  //     numExt: this.inspeccionesForm.controls.numExt.value,
  //     numInt: this.inspeccionesForm.controls.numInt.value,
  //     comentarios: this.inspeccionesForm.controls.comentarios.value,
  //   };
  //   this.inspeccionService.postOne(Json).subscribe({
  //     next: data => {
  //       const json = {
  //         id: Number(this.route.snapshot.paramMap.get('id_solicitud')),
  //         tipoSolicitud: this.route.snapshot.paramMap.get('tipo'),
  //       };
  //
  //       const jsonAux = {
  //         id: data.idRegistro,
  //         estado: 'EMITIDO',
  //       };
  //       this.administracionPolizaService.updateEstadoPolizaSockets(jsonAux).subscribe(() => {
  //         this.solicitudesService.removeSolicitudSockets(json).subscribe({
  //           error: () => {
  //             Swal.fire({
  //               title: 'Error',
  //               text: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
  //             });
  //           },
  //         });
  //       });
  //     },
  //     error: () => {
  //       Swal.fire({
  //         title: 'Error',
  //         text: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
  //       });
  //     }, complete: () => {
  //       Swal.fire({
  //         title: '¡Registro finalizado con éxito!',
  //       }).then(() => {
  //         window.close();
  //       });
  //     },
  //   });
  // }
 // duda nuevo triagrante se quita conductor HABITUAL base de datos
 //  guardarConductor() {
 //    this.conductorHabitualService.post(this.conductorHabitualForm.value).subscribe();
 //  }

  getTelefono(telefono, id) {
    if (telefono.length >= 9) {
      this.telefonoService.post(telefono).subscribe(data => {
        if (id === 1) {
          this.telefonoFijo = data;
          this.conductorHabitualForm.controls.telefonoFijo.setErrors({'incorrect': true});
        }
        if (id === 2) {
          this.telefonoMovil = data;
          this.conductorHabitualForm.controls.telefonoMovil.setErrors({'incorrect': true});
        }
      }, error => {
        if (id === 1) {
          this.telefonoFijo = error.error.text;
          if (this.telefonoFijo === 'FIJO') {
            this.conductorHabitualForm.controls.telefonoFijo.setErrors(null);
          }
          if (this.telefonoFijo !== 'FIJO') {
            this.conductorHabitualForm.controls.telefonoFijo.setErrors({'incorrect': true});
          }
        }
        if (id === 2) {
          this.telefonoMovil = error.error.text;
          if (this.telefonoMovil === 'MOVIL') {
            this.conductorHabitualForm.controls.telefonoMovil.setErrors(null);
          }
          if (this.telefonoMovil !== 'MOVIL') {
            this.conductorHabitualForm.controls.telefonoMovil.setErrors({'incorrect': true});
          }
        }
      });
    }
  }
  subirADrive() {
    this.btnLoad = true;
    // let idArchivo: string;
    Swal.fire({
      title: 'Subiendo los documentos de inspección',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });


    if (this.radCheck.checked ){
      console.log('entro a onli')
      const Json: any = {
        idRegistro: this.idRegistro,
        idEstadoInspeccion: 1,
        idColonia: this.inspeccionesForm.controls.idColonia.value,
        cp: this.inspeccionesForm.controls.cp.value,
        kilometraje: this.inspeccionesForm.controls.kilometraje.value,
        placas: this.inspeccionesForm.controls.placas.value,
        // urlPostventa: this.urlPostVenta,
        urlPostventa: this.inspeccionOnline.controls.urlInspeccionOnline.value,
        archivo: '',
        calle: this.inspeccionesForm.controls.calle.value,
        numExt: this.inspeccionesForm.controls.numExt.value,
        numInt: this.inspeccionesForm.controls.numInt.value,
        comentarios: this.inspeccionesForm.controls.comentarios.value,
      };
      this.inspeccionService.postOne(Json, this.filesToUpload, this.idRegistro).subscribe({
        next: dataC => {
          this.notificaciones.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
            this.router.navigate([this.esStep ? 'modulos/subsecuentes/solicitudes-interna' : 'modulos/subsecuentes/administracion-polizas']);
          });
        },
        error: () => {
          this.notificaciones.error('Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información');
        },
      });
    } else {
      const Json: any = {
        idRegistro: this.idRegistro,
        idEstadoInspeccion: 1,
        idColonia: this.inspeccionesForm.controls.idColonia.value,
        cp: this.inspeccionesForm.controls.cp.value,
        fechaInspeccion: this.inspeccionesForm.controls.fechaInspeccion.value,
        kilometraje: this.inspeccionesForm.controls.kilometraje.value,
        placas: this.inspeccionesForm.controls.placas.value,
        // urlPostventa: this.urlPostVenta,
        urlPostventa: this.inspeccionOnline.controls.urlInspeccionOnline.value,
        archivo: '',
        calle: this.inspeccionesForm.controls.calle.value,
        numExt: this.inspeccionesForm.controls.numExt.value,
        numInt: this.inspeccionesForm.controls.numInt.value,
        comentarios: this.inspeccionesForm.controls.comentarios.value,
      };
      this.inspeccionService.postOne(Json, this.filesToUpload, this.idRegistro).subscribe({
        next: dataC => {
          this.notificaciones.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
            this.router.navigate([this.esStep ? 'modulos/subsecuentes/solicitudes-interna' : 'modulos/subsecuentes/administracion-polizas']);
          });
        },
        error: () => {
          this.notificaciones.error('Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información');
        },
      });
    }





    // this.inspeccionService.postOne(Json, this.filesToUpload, this.idRegistro).subscribe({
    //   next: dataC => {
    //     this.notificaciones.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
    //       this.router.navigate([this.esStep ? 'modulos/subsecuentes/solicitudes-interna' : 'modulos/subsecuentes/administracion-polizas']);
    //     });
    //   },
    //   error: () => {
    //     this.notificaciones.error('Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información');
    //   },
    // });

    // this.driveService.uploadByIdFolderOP(this.filesToUpload, 'inspección', this.idRegistro, this.carpetaDrive).subscribe({
    //   next: result => {
    //     idArchivo = result;
    //   }, error: error => {
    //     this.btnLoad = false;
    //     let texto: string;
    //
    //     if (error.status !== 417 || error.status !== 400) {
    //       texto = 'Presiona OK para volver a intentarlo <b>5</b> segundos';
    //     } else {
    //       texto = error.error;
    //     }
    //     let timerInterval;
    //     let tiempo: number = 4;
    //     if (this.textError >= 1) {
    //       Swal.fire({
    //         title: 'Error',
    //         html: texto,
    //         showCancelButton: true,
    //         cancelButtonText: 'Cancelar',
    //         onBeforeOpen: () => {
    //           Swal.disableButtons();
    //           timerInterval = setInterval(() => {
    //             const content = Swal.getContent();
    //             const b = content.querySelector('b');
    //             b.innerText = `${tiempo}`;
    //             tiempo -= 1;
    //           }, 1000);
    //           setTimeout(() => {
    //             clearInterval(timerInterval);
    //             if (Swal.getConfirmButton() !== null) {
    //               Swal.enableButtons();
    //             }
    //           }, 5000);
    //         },
    //         preConfirm: () => {
    //           this.subirADrive();
    //           this.textError -= 1;
    //         },
    //         onClose: () => {
    //           clearInterval(timerInterval);
    //         },
    //       });
    //     } else {
    //       Swal.fire({
    //         title: 'Error',
    //         html: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
    //         preConfirm: () => {
    //           this.textError = 5;
    //         },
    //       });
    //     }
    //   }, complete: () => {
    //     this.btnLoad = false;
    //     this.guardarInspeccion(idArchivo);
    //   },
    // });
  }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.archivoValido = false;
    this.filesToUpload = (<HTMLInputElement>event.target).files;
    if (this.filesToUpload.length !== 0) {
      const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
      const tamanioArchivo = this.filesToUpload.item(0).size * .000001;

      if (this.filesToUpload.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          Swal.fire({
            title: 'Archivos listos',
            text: '¡El archivo está listo para ser guardado!',
          });
          this.archivoValido = true;
        } else {
          let titulo: string;
          let texto: string;

          if (!extensionValida) {
            titulo = 'Extensión no soportada';
            texto = 'Solo puedes subir archivos pdf';
          } else {
            titulo = 'Archivo demasiado grande';
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }

          Swal.fire({
            title: titulo,
            text: texto,
          });
        }
      }
    }
  }

}
