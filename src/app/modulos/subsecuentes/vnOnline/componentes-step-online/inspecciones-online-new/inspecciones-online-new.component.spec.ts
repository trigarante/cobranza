import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionesOnlineNewComponent } from './inspecciones-online-new.component';

describe('InspeccionesOnlineNewComponent', () => {
  let component: InspeccionesOnlineNewComponent;
  let fixture: ComponentFixture<InspeccionesOnlineNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspeccionesOnlineNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionesOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
