import {Component, Input,EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ClienteVn} from "../../../../../@core/data/interfaces/venta-nueva/cliente-vn";
import {MatCheckbox} from "@angular/material/checkbox";
import {Recibos} from "../../../../../@core/data/interfaces/venta-nueva/recibos";
import {FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {TipoPagoVn} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn";
import {Periodicidad} from "../../../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad";
import {
  SociosComercial,
  SociosComercialOnline
} from "../../../../../@core/data/interfaces/comerciales/socios-comercial";
import {Ramo} from "../../../../../@core/data/interfaces/comerciales/ramo";
import {SubRamo} from "../../../../../@core/data/interfaces/comerciales/sub-ramo";
import {ProductoNameCorto, ProductoSocios} from "../../../../../@core/data/interfaces/comerciales/producto-socios";
import moment from 'moment';
import {CotizacionesAli} from "../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {TipoSubramoService} from "../../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service";
import {ProductoSociosService} from "../../../../../@core/data/services/comerciales/producto-socios.service";
import {TipoPagoVnService} from "../../../../../@core/data/services/venta-nueva/catalogos/tipo-pago-vn.service";
// import {EstadoPolizaVnService} from "../../../../../@core/data/services/subsecuentes/catalogos/estado-poliza-vn.service";
import {RegistroService} from "../../../../../@core/data/services/venta-nueva/registro.service";
// import {FlujoPolizaVnService} from "../../../../../@core/data/services/subsecuentes/catalogos/flujo-poliza-vn.service";
import {SolicitudesvnService} from "../../../../../@core/data/services/venta-nueva/solicitudes.service";
import {ProductoClienteService} from "../../../../../@core/data/services/venta-nueva/producto-cliente.service";
import {RecibosService} from "../../../../../@core/data/services/venta-nueva/recibos.service";
import {SociosService} from "../../../../../@core/data/services/comerciales/socios.service";
import {RamoService} from "../../../../../@core/data/services/comerciales/ramo.service";
import {SubRamoService} from "../../../../../@core/data/services/comerciales/sub-ramo.service";
import {DriveService} from "../../../../../@core/data/services/ti/drive.service";
import {PeriodicidadService} from "../../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service";
import {AdminVnViewService} from "../../../../../@core/data/services/venta-nueva/adminVnView.service";
import {WhatsappMsgService} from "../../../../../@core/data/services/whatsappMsg/whatsapp-msg.service";
import {ClienteVnService} from "../../../../../@core/data/services/venta-nueva/cliente-vn.service";
import {ActivatedRoute} from "@angular/router";
import {CotizacionesAliService} from "../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {CatalogoService} from "../../../../../@core/data/services/cotizador/catalogo.service";
import {MatDialog} from "@angular/material/dialog";
import {SepomexService} from "../../../../../@core/data/services/catalogos/sepomex.service";
import {HttpClient} from "@angular/common/http";
import {PagosService} from "../../../../../@core/data/services/venta-nueva/pagos.service";
import {urlAseguradoras} from "../../../../../@core/data/interfaces/cotizador/urlAseguradoras";
import Swal from "sweetalert2";
import {map} from "rxjs/operators";
import {Pagos} from "../../../../../@core/data/interfaces/venta-nueva/pagos";
import {ModalViewPdfPolizaFunnel} from "../../../../../shared/componentes/modal-view-pdf-poliza-funnel/modal-view-pdf-poliza-funnel.component";
import {NotificacionesService} from "../../../../../@core/data/services/others/notificaciones.service";
import {ProductoCliente} from "../../../../../@core/data/interfaces/venta-nueva/producto-cliente";



@Component({
  selector: 'app-registro-poliza-online-new',
  templateUrl: './registro-poliza-online-new.component.html',
  styleUrls: ['./registro-poliza-online-new.component.scss']
})
export class RegistroPolizaOnlineNewComponent implements OnInit {
  @Input() idProductoCliente: number;
  @Input() idRegistroPoliza: number;
  @Input() idCliente: number;
  @Input() datosCliente: ClienteVn;
  @Input() filesToUploadPagado: FileList;
  @Output() idRegistroPolizaSaliente = new EventEmitter<number>();
  @Output() nextStepper = new EventEmitter<number>();
  @Output() Inspeccione = new EventEmitter<number>();
  @ViewChild('checkin') checkin: MatCheckbox;

  registroForm: FormGroup = new FormGroup({
    idTipoPago: new FormControl('', Validators.required),
    idSocio: new FormControl('', Validators.required),
    idRamo: new FormControl('', Validators.required),
    idSubRamo: new FormControl('', Validators.required),
    idProductoSocio: new FormControl('', Validators.required),
    poliza: new FormControl('', [Validators.required]),
    oficina: new FormControl(''),
    fechaInicio: new FormControl(new Date()),
    fechaFin: new FormControl(''),
    primaNeta: new FormControl('', [Validators.required, Validators.pattern('[0-9.]+')]),
    primerPago: new FormControl(''),
    periodo: new FormControl('', Validators.required),
  });
  numeroGuion: RegExp = /[Aa-zZ0-9-]/;
  desactivarBotones= false;
  tipoPagos: TipoPagoVn[];
  periodicidad: Periodicidad[];
  socios: SociosComercial[];
  sociosOnline: SociosComercialOnline[];
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  productoSocioName: ProductoNameCorto;
  productoCliente: ProductoCliente;
  urlPreventa: any;
  date = moment();
  filesToUpload: FileList;
  filesToUploadPago: FileList;
  archivoValido = false;
  numeroPagos: number;
  instrucciones = 'INTRODUCE LOS SIGUIENTES DATOS CORRECTAMENTE:';
  /** FUNCIONA CON EL LOADER DE LOS BOTONES QUE LLEVAN UNA PETICIÓN **/
  gruposCargados = false;
  private textError = 5;
  btnLoad = false;
  btnLoadP = false;
  cantidad: number;
  registroEnUno: any;
  socio: SociosComercial;
  socioOnline: SociosComercialOnline;
  idSolicitud: number;
  clienteParaWhats: ClienteVn;
  // emision
  cotizacionAli: CotizacionesAli;
  responseEmision: any;
  x: string;
  ocultarFormulario = false;
  // pago
  pago: any;
  datosPago: any;
  valuePdfPago: any;
  recibo: Recibos;
  idCarpetaDrive: string;
  filesToUploadCliente: FileList;
  archivoValidoCliente = false;
  nombreAseguradora: string;
  extensionesDeArchivoAceptadas = ['application/pdf'];
  service: any;
  urlService: string;
  urldocumentos: string;
  responsedocumentosPDF: any;
  ocultarboton = true;

  constructor(private subramoServices: TipoSubramoService,
              private productoSocioService: ProductoSociosService,
              private tipoPagoService: TipoPagoVnService,
              // private estadoPolizaService: EstadoPolizaVnService,
              private registroService: RegistroService,
              // private flujoPolizaService: FlujoPolizaVnService,
              private solicitudesService: SolicitudesvnService,
              private productoClienteService: ProductoClienteService,
              private recibosService: RecibosService,
              private sociosService: SociosService,
              private ramoService: RamoService,
              private subRamoService: SubRamoService,
              private driveService: DriveService,
              private periodicidadService: PeriodicidadService,
              private administradorPolizasService: AdminVnViewService,
              private whatsMsgService: WhatsappMsgService,
              // private socketsWhatsappService: SocketsWhatsappService,
              private clienteService: ClienteVnService,
              private route: ActivatedRoute,
              private cotizacionesAliService: CotizacionesAliService,
              private catalogoService: CatalogoService,
              private matDialog: MatDialog,
              private sepomexServicio: SepomexService,
              private httpClient: HttpClient,
              private router: ActivatedRoute,
              private pagosService: PagosService,
              private administracionPolizaService: AdminVnViewService,
              private notificaciones: NotificacionesService,
              private notificacionesService: NotificacionesService
              ) {

    this.nombreAseguradora = this.router.snapshot.paramMap.get('nombreAseguradora');
    this.service = urlAseguradoras.find(aseg => aseg.aseguradora === this.nombreAseguradora);
    this.urlService = this.service.service;
    this.urldocumentos = this.service.urldocumentos;
  }

  ngOnInit(): void {
    this.idSolicitud = Number(this.route.snapshot.paramMap.get('id_solicitud'));

    // this.productoClienteService.returnAseguradora().subscribe(
    //   result => {
    //     this.urlPreventa = result;
    //   });
    this.getTipoPago();
    this.getPeriodicidad();
    // this.getFlujoPoliza();
    this.getSocios();
    if (this.idRegistroPoliza !== 0) {
      this.getRegistroPoliza();
    }
    if (!this.datosCliente) {
      this.clienteService.getClienteById(this.idCliente).subscribe({
        next: value => {
          this.clienteParaWhats = value;
        },
      });
    }
    // ontener la poliza de los servicio ya obtenidos de pago
    this.obtenerPoliza(this.idSolicitud);
    //  // modificacion obtener el doc de poliza y descargarlo
    //  this.obtenerDocPoliza(this.idSolicitud);
  }

  getTipoPago() {
    this.tipoPagoService.get().pipe(map(data => {
      return data.filter(result => result.id === 1);
    })).subscribe(result => {
      this.tipoPagos = result;
    });
  }

  getPeriodicidad() {
    this.periodicidadService.get().subscribe(res => {
      this.periodicidad = res;
    });
  }

  getPeriodicidadFilter() {
    this.periodicidad = this.periodicidad.filter(perio => perio.id !== 1);
    this.registroForm.controls.periodo.setValue('');
  }

  getSocios() {
    this.sociosService.getByIdPaisStep(1).pipe(map(data => {
      return data.filter(res => res.activo === 1);
    })).subscribe(data => this.socios = data);
    // this.sociosService.get().pipe(map(data => {
    //   return data.filter(result => result.activo === 1);
    // })).subscribe( data => {this.socios = data; } );
  }

  getRamo(idSocio: number, idRamo?: number) {
    this.socio = this.socios.filter( socios => socios.id === this.registroForm.controls.idSocio.value)[0];

    if (!this.socio.expresionRegular) {
      this.registroForm.controls.poliza.setValidators(Validators.required);
    } else {
      if (this.socio.id !== 2 && this.socio.id !== 8) {
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
      } else {
        const arrExp = this.socio.expresionRegular.split(',');
        const expresionesRegulares =
          this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
        this.registroForm.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(expresionesRegulares)]));
      }
    }

    this.registroForm.controls.poliza.updateValueAndValidity();
    if (idRamo === undefined) {
      this.registroForm.controls.idRamo.setValue('');
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getByIdSocio(idSocio).subscribe( {next: data => {
        this.ramos = data;
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
        if (idRamo !== undefined) {
          this.registroForm.controls.idRamo.setValue(idRamo);
        }
      }});
  }
  getRamoNuevo(idSocio: number, idRamo?: number) {
    this.sociosService.getByIdPaisStep(1).subscribe(data => {
      this.sociosOnline = data;
      console.log(data);
      console.log(this.sociosOnline)
      this.socioOnline = this.sociosOnline.filter(sociosOnline => sociosOnline.id === idSocio)[0];
      console.log(this.socioOnline);
      if (!this.socioOnline.expresionRegular) {
        this.registroForm.controls.poliza.setValidators(Validators.required);
      } else {
        if (this.socioOnline.id !== 2 && this.socioOnline.id !== 8) {
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(`${this.socioOnline.expresionRegular}`)]));
        } else {
          const arrExp = this.socioOnline.expresionRegular.split(',');
          const expresionesRegulares =
            this.socioOnline.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
          this.registroForm.controls.poliza.setValidators(Validators.compose(
            [Validators.required, Validators.pattern(expresionesRegulares)]));
        }
      }

      this.registroForm.controls.poliza.updateValueAndValidity();
      if (idRamo === undefined) {
        this.registroForm.controls.idRamo.setValue('');
        this.registroForm.controls.idSubRamo.setValue('');
        this.registroForm.controls.idProductoSocio.setValue('');
        this.ramos = [];
        this.subRamos = [];
        this.productosSocio = [];
      }

      this.ramoService.getByIdSocio(idSocio).subscribe({
        next: dataR => {
          this.ramos = dataR;
          if (this.ramos.length === 0) {
            this.mensajeErrorCatalogos('ramos');
          }
        }, complete: () => {
          if (idRamo !== undefined) {
            this.registroForm.controls.idRamo.setValue(idRamo);
          }
        }
      });
    });
    // console.log(this.registroForm.controls.idSocio.value);
    // console.log(this.socios);
    // this.socio = this.socios.filter(socios => socios.id === this.registroForm.controls.idSocio.value)[0];
    // console.log(this.socio);
    // console.log('nuevi');
  }

  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (idSubRamo === undefined) {
      this.registroForm.controls.idSubRamo.setValue('');
      this.registroForm.controls.idProductoSocio.setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getSubramosByIdRamo(idRamo).subscribe({
      next: data => {
        this.subRamos = data;
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      }, complete: () => {
        if (idSubRamo !== undefined) {
          this.registroForm.controls.idSubRamo.setValue(idSubRamo);
        }
      }
    });
  }

  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getProductoSocioByIdSubRamo(subRamo).subscribe({
      next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        if (idProductoSocio !== undefined) {
          this.registroForm.controls.idProductoSocio.setValue(idProductoSocio);
        }
      }
    });
  }

  getProductoSocioNombre(subRamo: number, nombre: string, idProductoSocio?: number) {

    if (idProductoSocio === undefined) {
      this.registroForm.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }
    this.productoSocioService.getProductoSocioByIdSubRamoByNombre(subRamo, nombre).subscribe({
      next: result => {
        this.productoSocioName = result;
        console.log(this.productoSocioName)
        if (this.productoSocioName === null) {
          this.mensajeErrorCatalogos('productos socio');
        }
      }, complete: () => {
        this.registroForm.controls.idProductoSocio.setValue(this.productoSocioName.id);
        console.log(this.registroForm.controls.idProductoSocio.value)
      }
    });
  }

  mensajeErrorCatalogos(catalogo: string) {
    Swal.fire({
      title: 'Sin elementos disponibles',
      text: '¡Por favor agregue ' + catalogo + ' en el catálogo correspondiente para poder continuar!',
    });
  }

  getRegistroPoliza() {
    this.registroService.getRegistroById(this.idRegistroPoliza).subscribe(data => {
      // console.log(moment(data.fechaInicio).format('L'), 'getRegistroPoliza');
      this.registroForm.controls.idTipoPago.setValue(data.idTipoPago);
      this.registroForm.controls.idSocio.setValue(data.idSocio);
      this.registroForm.controls.poliza.setValue(data.poliza);
      this.registroForm.controls.fechaInicio.setValue(data.fechaInicio + 'T00:00:00');
      this.registroForm.controls.primaNeta.setValue(data.primaNeta);
      this.registroForm.controls.oficina.setValue(data.oficina);
      this.registroForm.controls.periodo.setValue(data.idPeriodicidad);
      this.registroForm.controls.idTipoPago.disable;
      this.registroForm.controls.idSocio.disable;
      this.registroForm.controls.poliza.disable;
      this.registroForm.controls.fechaInicio.disable;
      this.registroForm.controls.primaNeta.disable;
      this.getRamoNuevo(data.idSocio, data.idRamo);
      this.getSubRamo(data.idRamo, data.idSubramo);
      this.getProductoSocio(data.idSubramo, data.idProductoSocio);
      if (data.archivo.length > 3) {
        this.instrucciones = 'YA NO PUEDES EDITAR LAS OPCIONES INGRESADAS';
        this.desactivarBotones = true;
        this.getMontoPagos();
      }
    });
  }

  getNumeroPagos(): number {
    if (this.tipoPagos !== undefined && this.registroForm.controls.idTipoPago.valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.registroForm.controls.idTipoPago.value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.registroForm.addControl('pagoMensual', new FormControl('',
          [Validators.required, Validators.pattern('[0-9.]+')], this.setErrorMensual.bind(this)));
      } else {
        if (this.registroForm.contains('pagoMensual')) {
          this.registroForm.removeControl('pagoMensual');
        }
      }
      return this.numeroPagos;
    } else {
      return 0;
    }
  }

  getMontoPagos() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistroPoliza).subscribe(data => {
      if (data && data.length > 0) {
        if (data.length > 1) {
          this.registroForm.addControl('pagoMensual', new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]));
          this.registroForm.controls.pagoMensual.setValue(data[0].cantidad);
        }
        this.registroForm.controls.primerPago.setValue(data[data.length - 1].cantidad);
      }
    });
  }

  // crearRegistroenUNo

  crearRegistroEnUno() {
    console.log(this.idProductoCliente);
    console.log(this.filesToUploadPagado);
    const poliza = this.registroForm.controls.poliza.value;


    this.registroService.getPolizaExistente(poliza, this.registroForm.controls.idSocio.value,
      this.registroForm.controls.fechaInicio.value.toISOString().split('T')[0]).subscribe({
      next: value => {
        const advertencia = `
          <p>La póliza <strong>${poliza}</strong> ya ha sido registrada. </p>
        `;
        if (value.length > 0) {
          Swal.fire({
            title: '¡Advertencia!',
            html: advertencia,
          });
        } else {
          const fecha = moment(this.registroForm.controls.fechaInicio.value).format('YYYY-MM-DD');
          let idRegistroPoliza: number;
          this.gruposCargados = false;
          if (this.registroForm.controls.idTipoPago.value === 1) {
            this.cantidad = 1;
          } else {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls['periodo'].value).id === 1) {
              this.cantidad = this.numeroPagos * .5;
            } else {
              this.cantidad = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls['periodo'].value).cantidadPagos
              );
            }
          }
          const datosRegistro: any = {
            idProducto: this.idProductoCliente,
            idEmpleado: sessionStorage.getItem('Empleado'),
            archivo: '',
            idEstadoPoliza: 1,
            idTipoPago: this.registroForm.controls.idTipoPago.value,
            idProductoSocio: this.registroForm.controls.idProductoSocio.value,
            idFlujoPoliza: 1,
            idSocio: this.registroForm.controls.idSocio.value,
            poliza: this.registroForm.controls.poliza.value,
            oficina: this.registroForm.controls.oficina.value,
            fechaInicio: this.registroForm.controls.fechaInicio.value,
            primaNeta: this.registroForm.controls.primaNeta.value,
            idDepartamento: +sessionStorage.getItem('idDepartamento'),
            idPeriodicidad: this.registroForm.controls.periodo.value,
            online: 1,
          };
          let textoPrimerAviso = ''
          // let textoPrimerAviso =
          //   '<style>#swal2-content {' +
          //   '  text-align: left;' +
          //   '  margin-left: 40px;' +
          //   '}</style>' +
          //   '<strong>Tipo de pago: </strong>' +
          //   this.tipoPagos.find(val => val.id === datosRegistro.idTipoPago).tipoPago +
          //   '</br>' +
          //   // '<strong>Número de mensualidades: </strong> <a style="color: red; font-weight: bold">' +
          //   // this.cantidad +
          //   // '</a></br>' +
          //   '<strong>Socio: </strong>' +
          //   this.socios.find(r => r.id === this.registroForm.controls.idSocio.value).nombreComercial +
          //   '</br>' +
          //   '<strong>Ramo: </strong>' +
          //   this.ramos.find(r => r.id === this.registroForm.controls.idRamo.value).tipoRamo +
          //   '</br>' +
          //   '<strong>Subramo: </strong>' +
          //   this.subRamos.find(r => r.id === this.registroForm.controls.idSubRamo.value).tipoSubRamo +
          //   '</br>' +
          //   '<strong>Producto socio: </strong>' +
          //   this.productosSocio.find(r => r.id === this.registroForm.controls.idProductoSocio.value).tipoProducto +
          //   '</br>' +
          //   '<strong>Inicio de vigencia: </strong>' +
          //   fecha +
          //   '</br>' +
          //   // '<strong>Oficina: </strong>' +
          //   // this.registroForm.controls['oficina'].value +
          //   // '</br>' +
          //   '<strong>Poliza: </strong>' +
          //   this.registroForm.controls.poliza.value +
          //   '</br>' +
          //   '<strong>Prima neta: </strong> $' +
          //   this.registroForm.controls.primaNeta.value +
          //   '</br>';
          // // '<strong>Prima neta del primer pago: </strong> $' +
          // // this.registroForm.controls['primerPago'].value +
          // // '</br>'
          if (this.getNumeroPagos() > 1) {
            textoPrimerAviso +=
              '<strong>Prima neta de las mensualidades: </strong> $' +
              this.registroForm.controls.pagoMensual.value +
              '</br>';

          }
          let cantidadAPagar: number = this.registroForm.controls.primerPago.value;
          let cantidadAPagarprima: number = this.registroForm.controls.primaNeta.value;
          const recibos: any[] = [];
          let fechaVigencia = moment(this.registroForm.controls.fechaInicio.value);
          // let fechaVigencia = moment(this.registroForm.controls['fechaInicio'].value).format('YYYY-MM-DD');
          // Swal.fire({
          //   title: 'Generando registro',
          //   allowOutsideClick: false,
          //   onBeforeOpen: () => {
          //     Swal.showLoading();
          //   },
          // });
          if (this.registroForm.controls.idTipoPago.value !== 1) {
            if (this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).id === 1) {
              this.numeroPagos = this.numeroPagos * .5;
              this.cantidad = this.numeroPagos;
            } else {
              this.numeroPagos = this.numeroPagos * (
                this.periodicidad.find(arr => arr.id === this.registroForm.controls.periodo.value).cantidadPagos);
              this.cantidad = this.numeroPagos;
            }
          } else {
            this.numeroPagos = 1;
          }
          for (let i = 0; i < this.numeroPagos; i++) {
            if (i > 0) {
              switch (this.registroForm.controls.idTipoPago.value) {
                case 2:
                  fechaVigencia = moment(fechaVigencia).add(1, 'months');
                  break;
                case 3:
                  fechaVigencia = moment(fechaVigencia).add(3, 'months');
                  break;
                case 4:
                  fechaVigencia = moment(fechaVigencia).add(4, 'months');
                  break;
                case 6:
                  fechaVigencia = moment(fechaVigencia).add(2, 'months');
                  break;
                case 5:
                  fechaVigencia = moment(fechaVigencia).add(6, 'months');
                  break;
              }
              cantidadAPagar = this.registroForm.controls.pagoMensual.value;
              cantidadAPagarprima = this.registroForm.controls.primaNeta.value;
              // fechaVigencia = moment(fechaVigencia).add(1, 'months');
            }
            if (this.registroForm.controls.idTipoPago.value === 1) {
              recibos.push({
                idEstadoRecibos: 2,
                idEmpleado: sessionStorage.getItem('Empleado'),
                numero: i + 1,
                cantidad: cantidadAPagarprima,
                fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
              });
            } else {
              if (this.registroForm.controls.periodo.value === 1 && this.registroForm.controls.idTipoPago.value === 5) {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).add(6, 'months').format('YYYY-MM-DD') + 'T06:00:00'),
                });
              } else {
                recibos.push({
                  idEstadoRecibos: 2,
                  idEmpleado: sessionStorage.getItem('Empleado'),
                  numero: i + 1,
                  cantidad: cantidadAPagar,
                  fechaVigencia: moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
                });
              }
            }
          }
          textoPrimerAviso += '</br></br><strong>¿Estás seguro que desas continuar? </strong>';

          // Swal.fire({
          //   type: 'warning',
          //   title: 'Estos son los datos que ingresaste:',
          //   showCancelButton: true,
          //   cancelButtonText: 'Revisar nuevamente',
          //   confirmButtonText: '¡Sí, estoy seguro!',
          //   cancelButtonColor: 'red',
          //   confirmButtonColor: 'green',
          //   reverseButtons: true,
          //   allowOutsideClick: false,
          //   allowEscapeKey: false,
          //   html: textoPrimerAviso,
          // }).then((firstWarning) => {
          //   // this.btnLoad = false;
          //   if (firstWarning.value) {
          //     Swal.fire({
          //       type: 'warning',
          //       title: 'Confirmación',
          //       html: '<p>¡Recuerda que ya no podrás cambiar los datos ingresados en este paso y en caso de error podría ' +
          //         'ameritar una <strong>sanción económica</strong>!</p>' +
          //         '<p><b>¿Estás seguro que deseas continuar?</b></p>',
          //       cancelButtonColor: 'red',
          //       confirmButtonColor: 'green',
          //       showCancelButton: true,
          //       cancelButtonText: 'Revisar nuevamente',
          //       confirmButtonText: '¡Sí, estoy seguro!',
          //       reverseButtons: true,
          //     }).then(secondWarning => {
          //       if (secondWarning.value) {
          //         Swal.fire({
          //           title: 'Generando registro',
          //           allowOutsideClick: false,
          //           onBeforeOpen: () => {
          //             Swal.showLoading();
          //           },
          //         });
          //         this.gruposCargados = true;
          //         this.registroEnUno = {
          //           registro: datosRegistro,
          //           recibos: recibos,
          //         };
          //         this.registroService.postInOne(this.registroEnUno).subscribe(
          //           result => {
          //             idRegistroPoliza = result.id;
          //             const json = result;
          //             json['idPago'] = null;
          //             json['idEstadoPago'] = null;
          //             /**Valida si pertenece a QUALITAS*/
          //             if (this.urlPreventa === 1) {
          //               /**Si pertenece valida si el check esta en TRUE*/
          //               if (this.checkin.checked) {
          //                 this.solicitudesService.entryURLType(0);
          //               } else {
          //                 this.solicitudesService.entryURLType(1);
          //               }
          //             } else {
          //               this.solicitudesService.entryURLType(2);
          //             }
          //             this.administradorPolizasService.postPolizaSocket(result).subscribe();
          //             // this.subirADrive('poliza', idRegistroPoliza);
          //           },
          //           e => {
          //             this.gruposCargados = false;
          //             Swal.fire({
          //               title: 'Ops!',
          //               html: '<p><b>Hubo un error al registrar los datos, intentalo nuevamente</b></p>',
          //               type: 'error',
          //               onClose: () => {
          //                 window.location.reload();
          //               },
          //             });
          //           },
          //           () => {
          //             this.gruposCargados = false;
          //             this.subirADrive('poliza', idRegistroPoliza);
          //           },
          //         );
          //       } else {
          //         this.gruposCargados = false;
          //       }
          //       // this.btnLoad = false;
          //     });
          //   }
          // });

          this.gruposCargados = true;
          // this.registroEnUno = {
          //   registro: datosRegistro,
          //   recibos: recibos,
          // };
          this.registroService.postInOneOnline({...datosRegistro, recibos},
            this.filesToUpload, this.route.snapshot.paramMap.get('id_solicitud')).subscribe(
            result => {
              this.notificaciones.exito('Se guardo la póliza');
              this.idRegistroPoliza = result.idRegistro;
              console.log('entro bien a pago :)');
              this.subirPagoDrive(this.idRegistroPoliza);
            },
            () => {
              this.notificaciones.error('Hubo un error al registrar los datos');
            },
          );
          // this.registroService.postInOne(this.registroEnUno).subscribe(
          //   result => {
          //     idRegistroPoliza = result.id;
          //     const json = result;
          //     json['idPago'] = null;
          //     json['idEstadoPago'] = null;
          //     /**Valida si pertenece a QUALITAS*/
          //     // if (this.urlPreventa === 1) {
          //     //   /**Si pertenece valida si el check esta en TRUE*/
          //     //   if (this.checkin.checked) {
          //     //     this.solicitudesService.entryURLType(0);
          //     //   } else {
          //     //     this.solicitudesService.entryURLType(1);
          //     //   }
          //     // } else {
          //     //   this.solicitudesService.entryURLType(2);
          //     // }
          //     this.administradorPolizasService.postPolizaSocket(result).subscribe();
          //     // this.subirADrive('poliza', idRegistroPoliza);
          //   },
          //   e => {
          //     this.gruposCargados = false;
          //     Swal.fire({
          //       title: 'Ops!',
          //       html: '<p><b>Hubo un error al registrar los datos, intentalo nuevamente</b></p>',
          //       onClose: () => {
          //         window.location.reload();
          //       },
          //     });
          //   },
          //   () => {
          //     this.gruposCargados = false;
          //     this.subirADrive('poliza', idRegistroPoliza);
          //   },
          // ); //
        }
      },
      error: () => {
        Swal.fire({
          title: '¡Algo salió mal!',

        });
      },
    });
  }

  // subirADrive(tipoArchivo: string, idRegistroPoliza: number) {
  //   let idCarpeta: string;
  //   // Swal.fire({
  //   //   title: 'Subiendo la poliza ingresada',
  //   //   allowOutsideClick: false,
  //   //   onBeforeOpen: () => {
  //   //     Swal.showLoading();
  //   //   },
  //   // });
  //   console.log(this.filesToUpload)
  //   this.driveService.postOP(this.filesToUpload, tipoArchivo, this.idCliente, idRegistroPoliza)
  //     .subscribe({
  //       next: result => {
  //         idCarpeta = result;
  //       }, error: error => {
  //         let texto: string;
  //
  //         if (error.status !== 417 || error.status !== 400) {
  //           texto = 'Presiona OK para volver a intentarlo <b>5</b> segundos';
  //         } else {
  //           texto = error.error;
  //         }
  //         let timerInterval;
  //         let tiempo: number = 4;
  //         if (this.textError >= 1) {
  //           Swal.fire({
  //             title: 'Error',
  //             html: texto,
  //             showCancelButton: true,
  //             cancelButtonText: 'Cancelar',
  //             onBeforeOpen: () => {
  //               Swal.disableButtons();
  //               timerInterval = setInterval(() => {
  //                 const content = Swal.getContent();
  //                 const b = content.querySelector('b');
  //                 b.innerText = `${tiempo}`;
  //                 tiempo -= 1;
  //               }, 1000);
  //               setTimeout(() => {
  //                 clearInterval(timerInterval);
  //                 if (Swal.getConfirmButton() !== null) {
  //                   Swal.enableButtons();
  //                 }
  //               }, 5000);
  //             },
  //             preConfirm: () => {
  //               this.subirADrive('poliza', idRegistroPoliza);
  //               this.textError -= 1;
  //             },
  //             onClose: () => {
  //               clearInterval(timerInterval);
  //             },
  //           });
  //         } else {
  //           Swal.fire({
  //             title: 'Error',
  //             html: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
  //             preConfirm: () => {
  //               this.textError = 5;
  //             },
  //             onClose: () => {
  //               window.location.reload();
  //             },
  //           });
  //         }
  //       }, complete: () => {
  //         this.registroService.updateArchivo(idRegistroPoliza, idCarpeta).subscribe({
  //           complete: () => {
  //             this.desactivarBotones = true;
  //
  //             console.log('entro bien a pago :)');
  //             this.subirPagoDrive(idRegistroPoliza);
  //
  //             if (this.idRegistroPoliza) {
  //               this.subirPagoDrive(idRegistroPoliza);
  //               window.location.reload();
  //             } else {
  //               // this._idRegistroPoliza.emit(idRegistroPoliza);
  //             }
  //             // Swal.fire({
  //             //   title: 'Archivos subidos',
  //             //   text: '¡Tus archivos han sido subidos exitosamente!',
  //             //   type: 'success',
  //             // }).then(() => {
  //             //   console.log('entro bien a pago :)');
  //             //   this.subirPagoDrive(idRegistroPoliza);
  //             //
  //             //   if (this.idRegistroPoliza) {
  //             //     this.subirPagoDrive(idRegistroPoliza);
  //             //     window.location.reload();
  //             //   } else {
  //             //     // this._idRegistroPoliza.emit(idRegistroPoliza);
  //             //   }
  //             // });
  //           },
  //         }); // fin de registro update
  //         // this.subirPagoDrive(idRegistroPoliza);
  //       }
  //     });
  // }

  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    let extensionValida;
    let tamanioArchivo;
    this.archivoValido = false;
    // this.filesToUpload = (<HTMLInputElement>event.target).files;
    console.log('Almacenar Archivo => ', this.filesToUpload);
    if (this.filesToUpload.length !== 0) {
      if (!this.filesToUpload.item) {
        extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload[0].type);
        tamanioArchivo = this.filesToUpload[0].size * .000001;
      } else {
        extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
        tamanioArchivo = this.filesToUpload.item(0).size * .000001;
      }
      if (this.filesToUpload.length === 1) {
        // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
        if (extensionValida && tamanioArchivo < 7) {
          Swal.fire({
            title: 'Archivos listos',
            text: '¡El archivo está listo para ser guardado!',
          });
          this.archivoValido = true;
          this.crearRegistroEnUno();
        } else {
          let titulo: string;
          let texto: string;
          if (!extensionValida) {
            titulo = 'Extensión no soportada';
            texto = 'Solo puedes subir archivos pdf';
          } else {
            titulo = 'Archivo demasiado grande';
            texto = 'Los archivos que subas deben pesar menos de 7 MB';
          }
          Swal.fire({
            title: titulo,
            text: texto,
          });
        }
      }
    }
  }

  validarCampos(): boolean {
    if (this.desactivarBotones) {
      return true;
    } else {
      if (this.registroForm.valid && this.archivoValido) {
        return false;
      } else {
        return true;
      }
    }
  }

  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (Number(this.registroForm.controls.primerPago.value) >= Number(this.registroForm.controls.pagoMensual.value)) {
        this.registroForm.controls.pagoMensual.setErrors(null);
      } else {
        this.registroForm.controls.pagoMensual.setErrors({ 'incorrect': true });
      }
    });
  }

  obtenerPoliza(idSolicitud) {
    this.solicitudesService.getSolicitudesByIdOnline(idSolicitud).subscribe(data => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
          this.cotizacionAli = dataAli;
          console.log(this.cotizacionAli);
          this.responseEmision = this.cotizacionAli.respuesta;

          this.registroForm.controls.poliza.setValue(this.responseEmision['emision']['poliza']);
          this.registroForm.controls.poliza.disable();
          this.registroForm.controls.idTipoPago.setValue(1);
          this.registroForm.controls.idTipoPago.disable();
          this.registroForm.controls.primaNeta.setValue(this.responseEmision['emision']['primaNeta']);
          this.registroForm.controls.primaNeta.disable();

          this.subRamoService.getSubramoById(this.cotizacionAli.idSubRamo).subscribe(datasubramo => {
            // console.log(datasubramo);
            this.registroForm.controls.idSocio.setValue(datasubramo.idSocio);
            console.log(this.registroForm.controls.idSocio.value)
            this.getRamoNuevo(datasubramo.idSocio, datasubramo.idRamo);
            this.getSubRamo(datasubramo.idRamo, datasubramo.id);
            // this.getProductoSocio(datasubramo.id, 25);

            //  // obtener el nombre de cobertura producto socio
            // this.nombreCobertura = Object.keys(this.responseEmision['coberturas'])
            //    // nombre de cobertura
            // this.x = this.nombreCobertura[0];
            this.x = this.responseEmision['paquete'];
            this.x = this.x.toUpperCase();
            this.getProductoSocioNombre(datasubramo.id, this.x, datasubramo.idSocio);
            this.registroForm.controls.idSocio.disable();
            this.registroForm.controls.idRamo.disable();
            this.registroForm.controls.idSubRamo.disable();
            this.registroForm.controls.idProductoSocio.disable();

          });
          this.registroForm.controls.periodo.setValue(2);
          this.registroForm.controls.periodo.disable();

          this.registroForm.controls.fechaInicio.disable();
        },
        (error) => {
          console.error('Error => ', error);
        },
        () => {



          switch (this.nombreAseguradora) {
            case 'QUALITASX':

              let datadoc: any;
              this.catalogoService.pdf(this.urlService, this.urldocumentos, this.registroForm.controls.poliza.value).subscribe(
                (datad) => {
                  datadoc = datad;
                  console.log(datadoc)

                },
                (error) => {
                  console.error('Error => ', error);
                },
                () => {
                  this.responsedocumentosPDF = datadoc;
                  // console.log(this.responsedocumentosPDF);
                  this.openDialog();
                });

              break;
            case 'GNP':
              // this.responsedocumentosPDF =
              //   "https://www.eot.gnp.com.mx//urlFilenetWeb" +
              //   "/getDocument?id=AZkfTM7fAPb983zxHtubTgDz8POSEf3vKh4dppxOa2AN58tQIleKMS6hn5ezbjkH7gE4qCYLBS2VN8jsN1Ts3Q==";
              // ejemplo
              // this.responsedocumentosPDF = "https://eqpro.es/wp-content/uploads/2018/11/Ejemplo.pdf";

              // poner aqui el servicio real para traer el doc de poliza de gnp
              // this.openDialog();
              let datadocgnp: any;
              this.catalogoService.pdfgnp(this.urlService, this.urldocumentos, this.registroForm.controls['poliza'].value).subscribe(
                resultc => {
                  datadocgnp = resultc;
                  console.log(resultc);
                  console.log(datadocgnp);

                },
                (error) => {
                  console.error('E ', error.error.text);
                  this.responsedocumentosPDF = error.error.text;
                  this.openDialog();
                },
                () => {
                  this.responsedocumentosPDF = datadocgnp;
                  // console.log(this.responsedocumentosPDF);
                  this.openDialog();
                });


              break;
            default:

              break;
          }

        });
    });
  }

//   obtenerDocPoliza(idSolicitud) {
//
//     this.openDialog();
//
// }

  private openDialog() {
    console.log('ID Solicitud Dialog => ', this.idSolicitud);

    const arrayInfoDownloadedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
    const downloadedPDF = arrayInfoDownloadedPDF.find(e => e.solicitudID === this.idSolicitud).downloaded;

    console.log(`¿El ID Póliza ${this.idSolicitud} ya descargó el PDF? => ${downloadedPDF}`);

    if (downloadedPDF === false) {
      this.matDialog.open(ModalViewPdfPolizaFunnel, {
        width: '900px',
        height: '670px',
        disableClose: false,
        data: {
          idsolicitud: this.idSolicitud,
          documentos: this.getDocuments()
        }
      }).afterClosed().subscribe(() => {
        return;
      });
    }
  }

  getDocuments() {
    // simulacion
    // const arrayDocuments = this.response['Emision'].Documento.split('|');
    // real

    const arrayDocuments = this.responsedocumentosPDF.split('|');

    console.log('Array Documentos => ', arrayDocuments);
    return arrayDocuments;
    // switch (this.nombreAseguradora) {
    //
    //
    //   case 'QUALITAS':
    //     const arrayDocuments = this.responsedocumentosPDF.split('|');
    //
    //     console.log('Array Documentos => ', arrayDocuments);
    //     return arrayDocuments;
    //
    //     break;
    //   case 'GNP' :
    //     this.responsedocumentosPDF = 'http://jornadasciberseguridad.riasc.unileon.es/archivos/ejemplo_esp.pdf';
    //     const arrayDocuments1 = this.responsedocumentosPDF;
    //
    //     console.log('Array Documentos => ', arrayDocuments1);
    //     return arrayDocuments1;
    //
    //
    //     break;
    //   default:
    //
    //     break;
    // }

  }

  // Método para obtener el pdf desde la url y convertirlo a base64
  async dataUrlToBlob() {
    Swal.fire({
      title: 'Comenzando proceso de registro, por favor espere',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    // simulacion
    // const documentsArray = this.getDocuments();
    //  const policie = documentsArray[documentsArray.length - 1];
    // real
    console.log(this.responseEmision['aseguradora'].toLowerCase());
    console.log(this.registroForm.controls.poliza.value);
    const policie = this.findPolicieUrl( this.responseEmision['aseguradora'].toLowerCase() , this.registroForm.controls.poliza.value);
    // Se hace un request para indicar que se va a obtener un blob de la url
    const request = new XMLHttpRequest();
    console.log(request);
    request.open('GET', policie, true);
    console.log(request);
    request.responseType = 'blob';
    console.log(request);
    let blob: Blob;
    // se implementa una funcion que cuando carga se creará un nuevo objeto de un FileReader
    // tslint:disable-next-line:only-arrow-functions
    request.onload = function() {
      const reader = new FileReader();
      console.log(request.response);
      reader.readAsDataURL(request.response);
      reader.onload = function (e: any) {
        // Se asigna el string de base64 a una constante para usarlo mas adelante
        const urlInBase64 = e.target.result;
        console.log(e.target.result);
        // Se obtiene el base64 sin especificar el tipo de aplicacion o mimeType
        const byteString = atob(urlInBase64.split(",")[1]);
        // Se obtiene el tipo de aplicacion o el mimeType
        const mimeString = urlInBase64.split(',')[0].split(':')[1].split(';')[0];
        // Se crear un arraybuffer del tamaño del base64
        const ab = new ArrayBuffer(byteString.length);
        // Se convierte a un int
        const ia = new Uint8Array(ab);
        // El base64 ese convierte a un char y se pasa en cada posicion del arreglo
        for (let i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
        // a partir del arreglo al que se asigno el base64 se crea un blob nuevo para obtener el archivo pdf
        blob = new Blob([ia], {
          type: mimeString
        });
        console.log(blob)
      }
    };
    request.send();
    setTimeout(() => {
      // Se llama a la funcion para que se asigne al FileList el arreglo simulado como FileList
      // Se envian como parametros el blob y el nombre del archivo (el cual no tendra nignu efecto)
      console.log(blob)
      this.filesToUpload = this.blobToFileList(blob, 'Ejemplo');
      // this.subirADrive('poliza', this.idRegistroPoliza);
      // Se llama a la funcion de almacenar alrchivo para subirlo automaticamente sin seleccionar el pdf desde el equipo
      this.almacenarArchivo();
    }, 5000)
  }

  // Metodo para agregar el blob al array simular ser un FileList
  private blobToFileList(blobResult: Blob, fileName: string) {
    // Se crea un arreglo de tipo file el cual se asignara el archivo blob
    console.log(blobResult)
    const fileArray: File[] = [];
    // Se crea un nuevo blob y se especifica el mimeType
    const blobObject = new Blob([blobResult], {
      type: 'application/pdf'
    });
    // Se crea un File a partir del blob generado, se envia el nombre del archivo y el mimeType
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El file generado, se agrega al array
    fileArray.push(file);
    // Se retorna el array simulando que es un filelist
    return fileArray as unknown as FileList;
  }

  // Método para buscar la url que contiene el pdf
  findPolicieUrl(aseguradora: string, policieNumber: string) {
    // Arreglo el cual traera consigo los documentos del servicio y el que se usará para buscar la URL
    // nuevo servicio
    const documentsArray = this.responsedocumentosPDF.split('|');
    // const documentsArray = this.responseEmision['emision']['documento'].split('|');
    // Arreglo que almacenará todas las url
    // const documentsArray = "http://qbcenter.qualitas.com.mx/poliza/c7160186330.pdf|" +
    //   "http://qbcenter.qualitas.com.mx/poliza/r7160186330.pdf|" +
    //   "http://qbcenter.qualitas.com.mx/poliza/p7160186330.pdf".split('|');
    // En caso de que las aseguradoras tengan prefijos diferentes con los cuales identificar el pdf de la poliza
    // se añadirá el prefijo y la aseguradora en este array

    switch (this.nombreAseguradora) {
      case 'QUALITAS':

        const prefixArray = [
          { aseguradora: 'qualitas', prefix: 'p' }
        ];
        // Se encuentra el pregfijo y se almacena en la variable
        const prefixFound = prefixArray.find(e => e.aseguradora === aseguradora).prefix;
        // Para qualitas se está usando esta varible donde se completa el prefijo concatenando el prefijo con el número de la poliza
        const completePrefix = `${prefixFound}${policieNumber}`;
        // Variable donde se almacenará la url que tenga el prefijo espcificado
        let urlPolicie: string;
        // Ciclo con el cual se hace la comparación para verificar cual url del arreglo es la que contiene el prefijo
        for (let i = 0; i < documentsArray.length; i++) {
          // Se compara si el array en la posición de i incluye el prefijo y asignarlo a la variable
          if (documentsArray[i].includes(completePrefix)) {
            urlPolicie = documentsArray[i];
          }
        }
        console.log('URL Policie Contain => ', urlPolicie);
        return urlPolicie;
        break;
      case 'GNP' :

        // Variable donde se almacenará la url que tenga el prefijo espcificado
        let urlPolicie1: string;
        urlPolicie1 = documentsArray[0];
        console.log('URL Policie Contain => ', urlPolicie1);
        return urlPolicie1;
        break;
      default:

        break;
    }

  }

  // hacer pago
  subirPagoDrive(idRegistro) {

    // nuevo migracion
    this.btnLoadP = true;
    // Swal.fire({
    //   title: 'Subiendo el comprobante de pago ingresado',
    //   allowOutsideClick: false,
    //   onBeforeOpen: () => {
    //     Swal.showLoading();
    //   },
    // });

    // this.valuePdfPago  = JSON.parse(localStorage.getItem('blob'));

    this.valuePdfPago  = localStorage.getItem('blob');
    console.log('direccion de pdf pago')
    console.log(this.valuePdfPago);
    // const doc = new jsPDF('p', 'px', 'a4');
    // const newblob = new Blob()

    // const request = new XMLHttpRequest();
    // request.open('GET', this.valuePdfPago , true);
    // request.responseType = 'blob';
    // request.onload = function() {
    //   const reader = new FileReader();
    //   reader.readAsDataURL(request.response);
    //   reader.onload =  function(e: any){
    //     console.log('DataURL:', e.target.result);
    //   };
    // };
    // request.send();
    this.filesToUploadPago = this.dataUrlToFileList(this.valuePdfPago, 'Archivo de Prueba');

    console.log(this.filesToUploadPago)
    console.log(this.filesToUploadPagado);

    // this.valuePdfPago  = localStorage.getItem('idfilesPagoPDF');
    //
    // this.filesToUploadPago  = JSON.parse(this.valuePdfPago)


      // nuevo

    this.pago = JSON.parse(localStorage.getItem('pago'));


    this.recibosService.getRecibosByIdRegistro(idRegistro).subscribe(dataR => {
        if (dataR) {
          this.recibo = dataR[0];

          const pago: Pagos = {
            id: 0,
            idRecibo: this.recibo.id,
            idFormaPago: this.pago.idFormaPago,
            idEstadoPago: 1,
            idEmpleado: this.pago.idEmpleado,
            fechaPago: this.pago.fechaPago,
            cantidad: this.pago.cantidad,
            archivo: '',
            datos: localStorage.getItem('datosPago'),
          };

          this.pagosService.subirDrive(this.filesToUploadPagado, this.route.snapshot.paramMap.get('id_solicitud'), pago).subscribe({
            next: dataP => {
              this.notificacionesService.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
                this.Inspeccione.emit(idRegistro)
              });
            },
            error: () => {
              this.notificacionesService.error();
            },
          });
        }
      });

      // viejo
      //   this.driveService.uploadByIdFolderOP(this.filesToUploadPagado, 'pago-1', idRegistro, this.idCarpetaDrive).subscribe({
      //   next: dataD => {
      //     this.guardarPago(dataD, idRegistro);
      //   },
      //   error: error => {
      //     this.btnLoadP = false;
      //     let texto: string;
      //     if (error.status !== 417 || error.status !== 400) {
      //       texto = 'Presiona OK para volver a intentarlo, espera <b>5</b> segundos';
      //     } else {
      //       texto = error.error;
      //     }
      //     let timerInterval;
      //     let tiempo: number = 4;
      //     if (this.textError >= 1) {
      //       Swal.fire({
      //         title: 'Error',
      //         html: texto,
      //         showCancelButton: true,
      //         cancelButtonText: 'Cancelar',
      //         onBeforeOpen: () => {
      //           Swal.disableButtons();
      //           timerInterval = setInterval(() => {
      //             const content = Swal.getContent();
      //             const b = content.querySelector('b');
      //             b.innerText = `${tiempo}`;
      //             tiempo -= 1;
      //           }, 1000);
      //           setTimeout(() => {
      //             clearInterval(timerInterval);
      //             if (Swal.getConfirmButton() !== null) {
      //               Swal.enableButtons();
      //             }
      //           }, 5000);
      //         },
      //         preConfirm: () => {
      //           this.subirPagoDrive(idRegistro);
      //           this.textError -= 1;
      //         },
      //         onClose: () => {
      //           clearInterval(timerInterval);
      //         },
      //       });
      //     } else {
      //       Swal.fire({
      //         title: 'Error',
      //         html: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
      //         preConfirm: () => {
      //           this.textError = 5;
      //         },
      //       });
      //     }
      //   },
      //   complete: () => {
      //     this.btnLoadP = false;
      //   },
      // });
  }

  // Método que convierte a un nuevo blob y lo asigna a un FileList
  private dataUrlToFileList(resultBlob: Blob, fileName: string)/* : FileList */ {
    // console.log('Data URL en dataurltofilelist', resultBlob);
    // Arreglo en el cual se asignará el nuevo blob
    const FileArray: File[] = [];
    console.log(resultBlob)
    // se crea un nuevo objeto del nuevo blob
    const blobObject = new Blob([resultBlob], { type: 'application/pdf' });
    // Se crear un nuevo objeto de tipo file pasando como parametro el blob
    const file = new File([blobObject], fileName, {
      type: 'application/pdf'
    });
    // El objecto file se agrega al arreglo
    FileArray.push(file);
    console.log('Arreglo de archivos FileList => ', FileArray);
    // Se retorna el arreglo de tipo file simulado como un FileList
    return FileArray as unknown as FileList;
  }

  guardarPago(idArchivo: string, idRegistro) {
    // const pago: Pagos = {
    //   id: 0,
    //   idRecibo: this.recibo.id,
    //   idFormaPago: this.formPago.controls['idFormaPago'].value,
    //   idEstadoPago: 1,
    //   idEmpleado: parseInt(sessionStorage.getItem('Empleado'), 10),
    //   fechaPago: this.formPago.controls['fechaPago'].value,
    //   cantidad: this.formPago.controls['cantidad'].value,
    //   archivo: idArchivo,
    //   datos: JSON.stringify(this.formDatosPago.value),
    // };

    this.pago = JSON.parse(localStorage.getItem('pago'));


    this.recibosService.getRecibosByIdRegistro(idRegistro).subscribe(dataR => {
      if (dataR) {
        this.recibo = dataR[0];

        const pago: Pagos = {
          id: 0,
          idRecibo: this.recibo.id,
          idFormaPago: this.pago.idFormaPago,
          idEstadoPago: 1,
          idEmpleado: this.pago.idEmpleado,
          fechaPago: this.pago.fechaPago,
          cantidad: this.pago.cantidad,
          archivo: idArchivo,
          datos: localStorage.getItem('datosPago'),
        };
        this.pagosService.postOne(pago).subscribe({
          next: data => {
            const json = {
              id: idRegistro,
              idPago: data.id,
              idEstadoPago: 1,
            };
            this.administracionPolizaService.updatePagoSockets(json).subscribe();
          },
          complete: () => {

            Swal.fire({
              title: 'Archivos subidos',
              text: '¡Tus archivos han sido subidos exitosamente!',
              showConfirmButton: true,
            }).then(() => {
              console.log(' va a entrar añl sig step inspecciones')
              // this.nextStepper.emit(2);
              this.Inspeccione.emit(idRegistro)
              //  }
              // });
            });
          },
        }); // fin de pago}
      }
    });
  }
  // almacena doc de cliente

  /** Guardar archivo **/
  almacenarArchivoCliente() {
    this.filesToUploadCliente = ( <HTMLInputElement>event.target).files;
    const extensionValida = this.extensionesDeArchivoAceptadas.includes(this.filesToUploadCliente.item(0).type);
    const tamanioArchivo = this.filesToUploadCliente.item(0).size * .000001;

    if (this.filesToUploadCliente.length === 1) {
      // El servicio en Spring solo acepta como máximo 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        Swal.fire({
          title: 'Archivos listos',
          text: '¡Tu archivo está listo para ser guardado!',
        });
        this.archivoValidoCliente = true;
        if (this.archivoValidoCliente) {
          this.subirADriveCliente('cliente');
          this.ocultarboton = false;
        }
      } else {
        let titulo: string;
        let texto: string;

        if (!extensionValida) {
          titulo = 'Extensión no soportada';
          texto = 'Solo puedes subir archivos pdf';
        } else {
          titulo = 'Archivo demasiado grande';
          texto = 'Los archivos que subas deben pesar menos de 7 MB';
        }

        Swal.fire({
          title: titulo,
          text: texto,
        });
      }
    }
  }

  subirADriveCliente(tipoArchivo: string) {
    let idFolder: string;
    Swal.fire({
      title: 'Se están subiendo los archivos',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe({
      next: dataproducto => {
        this.productoCliente = dataproducto;
        this.productoClienteService.putInOneOnline(this.idProductoCliente, this.productoCliente, this.filesToUploadCliente).subscribe({
          next: dataC => {
            this.notificacionesService.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
            });
          },
          error: () => {
            this.notificacionesService.error();
          },
        });
      },
      error: () => {
        this.notificacionesService.error();
      },
    });

    // this.clienteService.putInOne(this.idCliente, this.filesToUploadCliente).subscribe({
    //   next: dataC => {
    //     this.notificacionesService.exito('¡Tu archivo han sido subidos exitosamente!').then(() => {
    //     });
    //   },
    //   error: () => {
    //     this.notificacionesService.error();
    //   },
    // });
    // this.driveService.postOP(this.filesToUploadCliente, tipoArchivo, this.idCliente, 0).subscribe({
    //   next: (data) => {
    //     idFolder = data;
    //   },
    //   error: error => {
    //     let texto: string;
    //     if (error.status !== 417 || error.status !== 400) {
    //       texto = 'Presiona OK para volver a intentarlo <b>5</b> segundos';
    //     } else {
    //       texto = error.error;
    //     }
    //     let timerInterval;
    //     let tiempo: number = 4;
    //     if (this.textError >= 1) {
    //       Swal.fire({
    //         title: 'Error',
    //         html: texto,
    //         showCancelButton: true,
    //         cancelButtonText: 'Cancelar',
    //         onBeforeOpen: () => {
    //           Swal.disableButtons();
    //           timerInterval = setInterval(() => {
    //             const content = Swal.getContent();
    //             const b = content.querySelector('b');
    //             b.innerText = `${tiempo}`;
    //             tiempo -= 1;
    //           }, 1000);
    //           setTimeout(() => {
    //             clearInterval(timerInterval);
    //             if (Swal.getConfirmButton() !== null) {
    //               Swal.enableButtons();
    //             }
    //           }, 5000);
    //         },
    //         preConfirm: () => {
    //           this.subirADriveCliente('cliente');
    //           this.textError -= 1;
    //         },
    //         onClose: () => {
    //           clearInterval(timerInterval);
    //         },
    //       });
    //     } else {
    //       Swal.fire({
    //         title: 'Error',
    //         html: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
    //         preConfirm: () => {
    //           this.textError = 5;
    //         },
    //       });
    //     }
    //   },
    //   complete: () => {
    //     this.clienteService.putArchivo(this.idCliente, idFolder).subscribe({
    //       complete: () => {
    //         Swal.fire({
    //           title: 'Archivos subidos',
    //           text: '¡Tus archivos han sido subidos exitosamente!',
    //         }).then();
    //
    //         if (this.archivoValidoCliente){
    //
    //         }
    //         this.clienteService.putArchivoSubido(this.idCliente, 1).subscribe(() => {
    //             if (this.idRegistroPoliza) {
    //               const json = {
    //                 idCliente: this.idCliente,
    //                 archivoSubido: 1,
    //               };
    //               this.administracionPolizaService.updateArchivoSubidoSockets(json).subscribe();
    //             }
    //           });
    //       },
    //     });
    //   },
    // });
  }

}

