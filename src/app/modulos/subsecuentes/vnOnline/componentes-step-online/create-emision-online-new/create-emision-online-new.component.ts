import {Component, Input, OnInit, Output, EventEmitter, ViewChild, ChangeDetectorRef} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClienteVn} from "../../../../../@core/data/interfaces/venta-nueva/cliente-vn";
import {Sepomex} from "../../../../../@core/data/interfaces/catalogos/sepomex";
import {Paises} from "../../../../../@core/data/interfaces/catalogos/paises";
import {CotizacionesAli} from "../../../../../@core/data/interfaces/venta-nueva/cotizaciones-ali";
import {ClienteVnService} from "../../../../../@core/data/services/venta-nueva/cliente-vn.service";
import {SepomexService} from "../../../../../@core/data/services/catalogos/sepomex.service";
import {PaisesService} from "../../../../../@core/data/services/catalogos/paises.service";
import {SolicitudesvnService} from "../../../../../@core/data/services/venta-nueva/solicitudes.service";
import {DriveService} from "../../../../../@core/data/services/ti/drive.service";
import {AdminVnViewService} from "../../../../../@core/data/services/venta-nueva/adminVnView.service";
import {ActivatedRoute} from "@angular/router";
import {CotizacionesAliService} from "../../../../../@core/data/services/venta-nueva/cotizaciones-ali.service";
import {ProspectoService} from "../../../../../@core/data/services/venta-nueva/prospecto.service";
import Swal from 'sweetalert2';
import moment from 'moment';

@Component({
  selector: 'app-create-emision-online-new',
  templateUrl: './create-emision-online-new.component.html',
  styleUrls: ['./create-emision-online-new.component.scss']
})
export class CreateEmisionOnlineNewComponent implements OnInit {
  @Input() idClientes: number;
  @Input() idSolicitudPros: number;
  @Input() idRegistroPoliza: number;
  @Output() datosSalida = new EventEmitter<any>();
  @ViewChild('tabGroup') razonSocial;

  registroEmision: any;
  /** Expresión regular para un número. */
  numero: RegExp = /[0-9]+/;
  emisionFisicaForm: FormGroup;
  emisionMoralForm: FormGroup;
  cliente: ClienteVn;
  /** Arreglo que se inicializa con los valores del campo sepomex desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  // Variable que activa opcion de actualizar o crear si existe curp o rfc*/
  idTipo: boolean;
  idCotizacionAli: number;
  // Informacion del cliente **/
  clienteForm: FormGroup = new FormGroup({
    curp: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z-0-9][A-Z-0-9]'),
      Validators.maxLength(18), Validators.minLength(18)])),
    rfc: new FormControl('', Validators.compose([Validators.required,
      Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
      Validators.maxLength(13), Validators.minLength(10)])),
    nombre: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(150)])),
    paterno: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    materno: new FormControl('', Validators.compose([Validators.required,
      Validators.minLength(3), Validators.maxLength(50)])),
    correo: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    cp: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(5),
      Validators.minLength(4), Validators.pattern('[0-9]{4,5}')])),
    idColonia: new FormControl(null, Validators.compose([Validators.required])),
    calle: new FormControl('', Validators.required),
    numInt: new FormControl(null),
    numExt: new FormControl('', Validators.required),
    genero: new FormControl('', Validators.required),
    telefonoFijo: new FormControl(null, Validators.pattern('[0-9]{10}')),
    telefonoMovil: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{10,13}')])),
    fechaNacimiento: new FormControl('', Validators.required),
    razonSocial: new FormControl(1),
    idPais: new FormControl(1, Validators.required),
    archivoSubido: new FormControl(0),
    archivo: new FormControl(null),
  });
  /** Arreglo que se inicializa con los valores del campo cp desde el Microservicio. */
  colonias: Sepomex[];
  /** Variable de tipo "item seleccionado" para almacenar el género (masculino y femenino). */
  genders = [{label: 'MASCULINO', value: 'M'}, {label: 'FEMENINO', value: 'F'}];
  /** Arreglo que se inicializa con los valores del campo paises desde el Microservicio. */
  paisOrigen: Paises[];
  // ** Variable que almacena el idCliente al validar si existe el curp o rfc*/
  idCliente: number;
  // ** FUNCIONA CON EL LOADER DE LOS BOTONES QUE LLEVAN UNA PETICIÓN **/
  extensionesDeArchivoAceptadas = ['application/pdf'];
  filesToUpload: FileList;
  archivoValido: false;
  private textError: 5;
  fechaNacimientoMaxima = new Date();
  textoBoton = 'Crear';
  idSolicitud: number;
  cotizacionAli: CotizacionesAli;
  response: any;
  clientejson: object;
  peticion: any;
  ciudad: string;
  poblacion: string;
  fechaNacCambio: any;
  codPostal: string;
  responseEmision: any;
  peticionE: any;
  changeForm2 = false;
  num: number;
  nombre: string;
  cpnuevo: number;
  fechacambio: any;
  fechaObject: Date;

  constructor(protected clienteService: ClienteVnService,
              protected cpService: SepomexService,
              protected paisesService: PaisesService,
              private solicitudesService: SolicitudesvnService,
              private driveService: DriveService,
              private administracionPolizaService: AdminVnViewService,
              private cdRef: ChangeDetectorRef,
              private route: ActivatedRoute,
              private cotizacionesAliService: CotizacionesAliService,
              private prospectoService: ProspectoService) {
    this.idSolicitud =   +this.route.snapshot.paramMap.get('id_solicitud');
  }

  ngOnInit(): void {
    this.fechaNacimientoMaxima.setMonth(this.fechaNacimientoMaxima.getMonth() - 216);
    this.getPaises();
    // console.log(this.idClientes)
    if (this.idClientes !== undefined) {
      this.textoBoton = 'Modificar';
      this.getInfoCliente(this.idClientes); // aqui ya es registro poliza
    }
    if (this.idClientes === undefined ) {
      // conseguir datos del codigo postal y sexo del cotizador
      this.conseguirDatos();
    }
  }
  // ** Funcion para que no marque error al actualizar el formulario **/
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().subscribe(result => {
      this.paisOrigen = result;
    });
  }
  // ** Actualizar requeridos en el formulario **/
  actualizarFormulario(tipoCliente: number) {
    this.clienteForm.controls.razonSocial.setValue(tipoCliente);

    if (tipoCliente === 2) {
      this.clienteForm.controls.curp.clearValidators();
      this.clienteForm.controls.paterno.clearValidators();
      this.clienteForm.controls.materno.clearValidators();
      this.clienteForm.controls.fechaNacimiento.clearValidators();
      this.clienteForm.controls.genero.clearValidators();
      this.clienteForm.controls.rfc.setValidators([
        Validators.pattern('[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
        Validators.maxLength(12), Validators.minLength(9)]);
    } else {
      this.clienteForm.controls.curp.setValidators(Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(18), Validators.minLength(18)]));
      this.clienteForm.controls.rfc.setValidators([
        Validators.required,
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
        Validators.maxLength(13), Validators.minLength(10)]);
      this.clienteForm.controls.paterno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.clienteForm.controls.materno.setValidators(Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(50)]));
      this.clienteForm.controls.fechaNacimiento.setValidators(Validators.required);
      this.clienteForm.controls.genero.setValidators(Validators.required);
    }
    this.clienteForm.controls.paterno.updateValueAndValidity();
    this.clienteForm.controls.materno.updateValueAndValidity();
    this.clienteForm.controls.fechaNacimiento.updateValueAndValidity();
    this.clienteForm.controls.genero.updateValueAndValidity();
    this.clienteForm.controls.rfc.updateValueAndValidity();
    this.clienteForm.controls.curp.updateValueAndValidity();
  }
  conseguirDatos() {
    console.log(this.idSolicitud)
    this.solicitudesService.getSolicitudesByIdOnline(this.idSolicitud).subscribe(data => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(data.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.response = this.cotizacionAli.respuesta;
        this.peticion = this.cotizacionAli.peticion;
        // console.log(this.peticion);
        // console.log(this.peticion.cp);
        this.cpnuevo = this.peticion.cp

        this.clienteForm.controls.cp.setValue(this.cpnuevo);
        console.log(this.clienteForm.controls.cp.value)
        // this.clienteForm.controls.cp.setValue('55816');
        // this.clienteForm.controls.cp.disable();

        this.getColonias2();

        if (this.peticion.genero === 'MASCULINO') {
          this.clienteForm.controls.genero.setValue('M');
          // this.clienteForm.controls.genero.disable();
        }
        if (this.peticion.genero === 'FEMENINO') {
          this.clienteForm.controls.genero.setValue('F');
          // this.clienteForm.controls.genero.disable();
        }

        this.prospectoService.getById(data.idProspecto).subscribe(
          dataPros => {
            console.log(dataPros);

            this.nombre = '';
            const fullName = dataPros.nombre.split(' ');
            this.num = fullName.length - 1;
            for (let i = 0 ; i <= (this.num - 2); i++) {
              this.nombre = this.nombre + ' ' + fullName[i] ;
            }
            // console.log(this.nombre);
            const PatApellido = fullName[this.num - 1];
            const MatApellido = fullName[this.num];

            // console.log(PatApellido);
            this.nombre = this.nombre.trim();
            this.clienteForm.controls.nombre.setValue(this.nombre);
            this.clienteForm.controls.paterno.setValue(PatApellido);
            this.clienteForm.controls.materno.setValue(MatApellido);
            this.clienteForm.controls.correo.setValue(dataPros.correo);
            this.clienteForm.controls.telefonoMovil.setValue(dataPros.numero);

          });
      });
    });
  }
  // Get informacion del cliente de la DB **/
  getInfoCliente(idCliente: number) {
    this.clienteService.getClienteById(idCliente).subscribe(result => {
      this.idCliente = result.id;
      this.textoBoton = 'Modificar';
      this.clienteForm.controls.curp.setValue(result.curp);
      this.clienteForm.controls.curp.disable();
      this.clienteForm.controls.rfc.setValue(result.rfc);
      this.clienteForm.controls.nombre.setValue(result.nombre);
      this.clienteForm.controls.paterno.setValue(result.paterno);
      this.clienteForm.controls.materno.setValue(result.materno);
      this.clienteForm.controls.correo.setValue(result.correo);
      this.clienteForm.controls.cp.setValue(result.cp);
      this.clienteForm.controls.idColonia.setValue(result.idColonia);
      this.clienteForm.controls.calle.setValue(result.calle);
      this.clienteForm.controls.genero.setValue(result.genero);
      this.clienteForm.controls.telefonoFijo.setValue(result.telefonoFijo);
      this.clienteForm.controls.telefonoMovil.setValue(result.telefonoMovil);
      this.fechacambio = moment(result.fechaNacimiento ).format('DD/MM/YYYY');
      this.clienteForm.controls.fechaNacimiento.setValue(new Date( this.fechacambio));
      this.clienteForm.controls.idPais.setValue(result.idPais);
      this.clienteForm.controls.archivoSubido.setValue(result.archivoSubido);
      this.clienteForm.controls.archivo.setValue(result.archivo);
      this.clienteForm.controls.numExt.setValue(result.numExt);
      this.clienteForm.controls.numInt.setValue(result.numInt);
      this.clienteForm.controls.razonSocial.setValue(result.razonSocial);
      if (+result.razonSocial === 2) {
        this.clienteForm.controls.curp.clearValidators();
        this.clienteForm.controls.rfc.setValidators([
          Validators.pattern('[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]([A-Z-0-9][A-Z-0-9][A-Z-0-9])?'),
          Validators.maxLength(12), Validators.minLength(9)]);
        this.clienteForm.controls.rfc.setValue(result.rfc);
        this.clienteForm.controls.paterno.clearValidators();
        this.clienteForm.controls.materno.clearValidators();
        this.clienteForm.controls.fechaNacimiento.clearValidators();
        this.clienteForm.controls.fechaNacimiento.setValue(null);
        this.clienteForm.controls.genero.clearValidators();
        this.clienteForm.controls.paterno.updateValueAndValidity();
        this.clienteForm.controls.materno.updateValueAndValidity();
        this.clienteForm.controls.fechaNacimiento.updateValueAndValidity();
        this.clienteForm.controls.genero.updateValueAndValidity();
        this.clienteForm.controls.curp.updateValueAndValidity();
        this.clienteForm.controls.rfc.updateValueAndValidity();
      }
      this.getColonias();
    });
  }
  // ** Validar curp cuando es fisico **/
  validarCurp() {
    if (this.clienteForm.controls.curp.valid) {
      this.clienteService.curpExist(this.clienteForm.controls.curp.value).subscribe(data => {
        if (data) {
          this.idCliente = data.id;
          this.textoBoton = 'Modificar';
          if (this.clienteForm.controls.razonSocial.value !== data.razonSocial) {
            this.actualizarFormulario(data.razonSocial);
          }
          this.clienteForm.controls.curp.setValue(data.curp);
          this.clienteForm.controls.curp.disable();
          this.clienteForm.controls.cp.setValue(data.cp);
          this.clienteForm.controls.rfc.setValue(data.rfc);
          this.clienteForm.controls.nombre.setValue(data.nombre);
          this.clienteForm.controls.paterno.setValue(data.paterno);
          this.clienteForm.controls.materno.setValue(data.materno);
          this.clienteForm.controls.correo.setValue(data.correo);
          this.clienteForm.controls.idColonia.setValue(data.idColonia);
          this.clienteForm.controls.calle.setValue(data.calle);
          this.clienteForm.controls.genero.setValue(data.genero);
          this.clienteForm.controls.telefonoFijo.setValue(data.telefonoFijo);
          this.clienteForm.controls.telefonoMovil.setValue(data.telefonoMovil);
          this.fechacambio = moment(data.fechaNacimiento ).format('DD/MM/YYYY');
          this.clienteForm.controls.fechaNacimiento.setValue(new Date( this.fechacambio));
          this.clienteForm.controls.idPais.setValue(data.idPais);
          this.clienteForm.controls.archivoSubido.setValue(data.archivoSubido);
          this.clienteForm.controls.archivo.setValue(data.archivo);
          this.clienteForm.controls.numExt.setValue(data.numExt);
          this.clienteForm.controls.numInt.setValue(data.numInt);
          this.clienteForm.controls.razonSocial.setValue(data.razonSocial);
          this.getColonias();
        }
      });
    }
  }
  // ** Validar RFC cuando es moral **/
  validarRFC() {
    if (this.clienteForm.controls.razonSocial.value === 2 && this.clienteForm.controls.rfc.valid) {
      this.clienteService.rfcExist(this.clienteForm.controls.rfc.value).subscribe(result => {
        if (result) {
          this.idCliente = result.id;
          this.clienteForm.controls.nombre.setValue(result.nombre);
          this.clienteForm.controls.correo.setValue(result.correo);
          this.clienteForm.controls.cp.setValue(result.cp);
          this.clienteForm.controls.idColonia.setValue(result.idColonia);
          this.clienteForm.controls.calle.setValue(result.calle);
          this.clienteForm.controls.numInt.setValue(result.numInt);
          this.clienteForm.controls.numExt.setValue(result.numExt);
          this.clienteForm.controls.telefonoFijo.setValue(result.telefonoFijo);
          this.clienteForm.controls.telefonoMovil.setValue(result.telefonoMovil);
          this.clienteForm.controls.idPais.setValue(result.idPais);
          this.getColonias();
        }
      });
    }
  }

  // ** Post cliente **/
  crearCliente() {
    Swal.fire({
      title: 'Creando cliente, por favor espere',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    console.log(this.clienteForm.value);
    this.clienteService.postOnline(this.clienteForm.value).subscribe({
      next: result => {
        this.idCliente = result.id;
        this.textoBoton = 'Actualizar';
      },
      complete: () => {


        this.modificarJsonCotizacion();



        // se guardan los datos del cliente en el json que se enviara a la emision
        if (this.archivoValido) {
          this.subirADrive('cliente');
        } else {
          Swal.fire({
            // type: 'info',
            title: '¡Cliente creado con éxito!, recuerda adjuntar su archivo desde el administrador de pólizas',
          });
        }
      },
    });
    this.solicitudesService.entryIdCliente(this.idCliente);
  }

  // ** Actualizar cliente **/
  actualizarEmision() {
    Swal.fire({
      title: 'Actualizando cliente',
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.clienteService.put(this.idCliente, this.clienteForm.getRawValue()).subscribe({
      complete: () => {
        const json = {
          idCliente: this.idCliente,
          nombreCliente: this.clienteForm.controls.nombre.value,
        };

        this.modificarJsonCotizacion();

        if (this.clienteForm.controls.razonSocial.value === 1) {
          json['apellidoPaternoCliente'] = this.clienteForm.controls.paterno.value;
          json['apellidoMaternoCliente'] = this.clienteForm.controls.materno.value;
        }
        // this.administracionPolizaService.updateClienteSockets(json).subscribe();
        if (!this.archivoValido) {
          Swal.fire({
            title: 'Información actualizada',
            text: '¡La información del cliente se ha actualizado con éxito!',
            allowOutsideClick: false,
            allowEscapeKey: false,
          });
         }
      },
    });

    if (this.archivoValido) {
      this.subirADrive('cliente');
    }
  }
  // ** Get colonias a partir del cp **/
  getColonias() {
    if (this.clienteForm.controls.cp.valid) {

      this.cpService.getColoniaByCp(this.clienteForm.controls.cp.value).subscribe(data => {
        this.colonias = data;
      });
    }
  }
  // ** Pasar al siguiente step **/
  datosProducto() {
    this.datosSalida.emit({activarProductoCliente: true, idCliente: this.idCliente});
  }

  subirADrive(tipoArchivo: string) {
    let idFolder: string;
    Swal.fire({
      title: 'Se están subiendo los archivos',
      allowOutsideClick: false,
      allowEscapeKey: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    this.driveService.postOP(this.filesToUpload, tipoArchivo, this.idCliente, 0).subscribe({
      next: (data) => {
        idFolder = data;
      },
      error: error => {
        let texto: string;
        if (error.status !== 417 || error.status !== 400) {
          texto = 'Presiona OK para volver a intentarlo <b>5</b> segundos';
        } else {
          texto = error.error;
        }
        let timerInterval;
        let tiempo: number = 4;
        if (this.textError >= 1) {
          Swal.fire({
            title: 'Error',
            html: texto,
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            onBeforeOpen: () => {
              Swal.disableButtons();
              timerInterval = setInterval(() => {
                const content = Swal.getContent();
                const b = content.querySelector('b');
                b.innerText = `${tiempo}`;
                tiempo -= 1;
              }, 1000);
              setTimeout(() => {
                clearInterval(timerInterval);
                if (Swal.getConfirmButton() !== null) {
                  Swal.enableButtons();
                }
              }, 5000);
            },
            preConfirm: () => {
              this.subirADrive('cliente');
              this.textError -= 1;
            },
            onClose: () => {
              clearInterval(timerInterval);
            },
          });
        } else {
          Swal.fire({
            title: 'Error',
            html: 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información',
            preConfirm: () => {
              this.textError = 5;
            },
          });
        }
      },
      complete: () => {
        this.clienteService.putArchivo(this.idCliente, idFolder).subscribe({
          complete: () => {
            Swal.fire({
              title: 'Archivos subidos',
              text: '¡Tus archivos han sido subidos exitosamente!',
            }).then(() => this.datosProducto());

            if (this.archivoValido)
              this.clienteService.putArchivoSubido(this.idCliente, 1).subscribe(() => {
                if (this.idRegistroPoliza) {
                  const json = {
                    idCliente: this.idCliente,
                    archivoSubido: 1,
                  };
                  this.administracionPolizaService.updateArchivoSubidoSockets(json).subscribe();
                }
              });
          },
        });
      },
    });
  }
  // ** Get colonias a partir del cp **/
  getColonias2() {

    console.log(this.clienteForm.controls.cp.valid)
    // if (this.clienteForm.controls.cp.valid) {
    //
    //   this.cpService.getColoniaByCp(+this.peticion.cp).subscribe(data => {
    //     this.colonias = data;
    //     console.log(data)
    //   });
    // }
    if (this.clienteForm.controls.cp.valid) {
      this.cpService.getColoniaByCp(this.peticion.cp).subscribe(data => {
        this.colonias = data;
      });
    }
  }
  modificarJsonCotizacion() {
    this.solicitudesService.getSolicitudesById(this.idSolicitud).subscribe(dataSol => {

      this.cotizacionesAliService.getCotizacionAliByIdOnline(dataSol.idCotizacionAli).subscribe(dataAli => {
        this.cotizacionAli = dataAli;

        this.responseEmision = this.cotizacionAli.respuesta;
        this.peticionE = this.cotizacionAli.peticion;
        this.clienteService.getClienteById(this.idCliente).subscribe(dataCliente => {
          this.cpService.getColoniaByCp(dataCliente.cp).subscribe(datacp => {

            if (dataCliente.numInt === null) {
              dataCliente.numInt = '';
            }
            // console.log(datacp)
            // se quitan acentos en poblacion y ciudad
            // si no existe cuidad manda vacio
            if (datacp[0].ciudad === null) {
              this.poblacion = '';
            } else if (datacp[0].ciudad !== null) {
              this.poblacion = datacp[0].ciudad.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
            }
            this.ciudad = datacp[0].delMun.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
            // console.log(this.poblacion);
            // console.log(this.ciudad);

            this.fechaNacCambio = dataCliente.fechaNacimiento;
            this.fechaNacCambio = moment(dataCliente.fechaNacimiento).format('DD/MM/YYYY');

            // se convierte codigo portal a string y si empieza con o0 se le agrega
            this.codPostal = dataCliente.cp.toString();
            // console.log(this.codPostal)
            this.codPostal = this.codPostal.padStart(5, '0');
            // console.log(this.codPostal)
            this.clientejson = {
              "rfc": dataCliente.rfc,
              "curp": dataCliente.curp,
              "edad": this.peticionE.edad,
              "email": dataCliente.correo,
              "genero": dataCliente.genero,
              "nombre": dataCliente.nombre,
              "telefono": dataCliente.telefonoMovil,
              "direccion": {
                "pais": dataCliente.nombrePaises,
                "calle": dataCliente.calle,
                "noExt": dataCliente.numExt,
                "noInt": dataCliente.numInt,
                "ciudad": this.ciudad,
                "colonia": dataCliente.colonia,
                "codPostal": this.codPostal,
                "poblacion": this.poblacion
              },
              "ocupacion": "",
              "apellidoMat": dataCliente.materno,
              "apellidoPat": dataCliente.paterno,
              "tipoPersona": "",
              "fechaNacimiento": this.fechaNacCambio
            };

            if (dataCliente.genero === 'M') {
              delete this.clientejson['genero'];
              this.clientejson['genero'] = "MASCULINO";
            }
            if (dataCliente.genero === 'F') {
              delete this.clientejson['genero'];
              this.clientejson['genero'] = "FEMENINO";
            }
            delete this.responseEmision['cliente'];
            delete this.responseEmision['idSubRamo'];
            this.responseEmision['cliente'] = this.clientejson;
            this.cotizacionAli['respuesta'] = this.responseEmision;

            this.cotizacionesAliService.putOnline(dataSol.idCotizacionAli, this.cotizacionAli).subscribe(result => {
            });
          }); // codigo postal
        }); // fin cliente
      }); // fin cotizacion

    }); // fin solicitud
  }

  changeFormNext(){
    this.changeForm2 = true;
  }

  changeFormBack(){
    this.changeForm2 = false;
  }
}

