import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoComponent } from './autorizaciones-proceso.component';

describe('AutorizacionesProcesoComponent', () => {
  let component: AutorizacionesProcesoComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
