import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AutorizacionRegistro } from 'src/app/@core/data/interfaces/venta-nueva/autorizacion/autorizacion-registro';
import { NotificacionesService } from 'src/app/@core/data/services/others/notificaciones.service';
import { AutorizacionPagoService } from 'src/app/@core/data/services/subsecuentes/autorizacion-pago.service';
import { AutorizacionRegistroService } from 'src/app/@core/data/services/venta-nueva/autorizacion/autorizacion-registro.service';
import { CampoCorregirAutorizacionService } from 'src/app/@core/data/services/venta-nueva/autorizacion/campo-corregir-autorizacion.service';
import { ErroresAnexosService } from 'src/app/@core/data/services/venta-nueva/autorizacion/errores-anexos.service';
import { ErroresAutorizacionService } from 'src/app/@core/data/services/venta-nueva/autorizacion/errores-autorizacion.service';
import { TipoDocumentoAutorizacionService } from 'src/app/@core/data/services/venta-nueva/autorizacion/tipo-documento-autorizacion.service';
import { ClienteVnService } from 'src/app/@core/data/services/venta-nueva/cliente-vn.service';
import { PagosService } from 'src/app/@core/data/services/venta-nueva/pagos.service';
import { ProductoClienteService } from 'src/app/@core/data/services/venta-nueva/producto-cliente.service';
import { RegistroService } from 'src/app/@core/data/services/venta-nueva/registro.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-mostrar-documentos-poliza',
  templateUrl: './mostrar-documentos-poliza.component.html',
  styleUrls: ['./mostrar-documentos-poliza.component.scss']
})
export class MostrarDocumentosPolizaComponent implements OnInit {
  textoDocumentoActual: string;
  documentoLegible = false;
  documentosPCorregir = [];
  autorizacionRegistro: AutorizacionRegistro;
  idAutorizacionRegistro: number = +this.route.snapshot.paramMap.get('id_autorizacion_registro');
  idAutorizacionPago: number = +this.route.snapshot.paramMap.get('id_autorizacion_pago');
  idTipoDocumento: number = +this.route.snapshot.paramMap.get('id_tipo_documento');
  idEstado: number = +this.route.snapshot.paramMap.get('idEstado');
  idCorreccionActual: number;
  verificacionPago;
  checkBoxes = [];
  proboolean: boolean = false;
  existeArchivo: boolean;
  existeErrorDato: boolean;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  constructor(private notificaciones: NotificacionesService,
    private clienteService: ClienteVnService,
    private pagoService: PagosService,
    private campoCorregirService: CampoCorregirAutorizacionService,
    private tipoDocumentosService: TipoDocumentoAutorizacionService,
    private registroService: RegistroService,
    private productoClienteService: ProductoClienteService,
    private autorizacionRegistroService: AutorizacionRegistroService,
    private route: ActivatedRoute,
    private erroresAutorizacionService: ErroresAutorizacionService,
    private router: Router,
    private location: Location,
    private erroresAnexosService: ErroresAnexosService,
    private autorizacionPagoService: AutorizacionPagoService) {
    if (this.idAutorizacionPago){
      this.erroresAnexosService.getErroresAnexosAPago(this.idAutorizacionPago).subscribe(data =>{
        this.existeArchivo = data;
      });
      this.erroresAutorizacionService.getErroresAutorizacionPago(this.idAutorizacionPago).subscribe(data => {
        this.existeErrorDato = data;
      });
    }
  }

  ngOnInit(): void {
    if (this.idTipoDocumento) {
      this.getDocumentosPCorregir(this.idTipoDocumento);
      this.getVerificacionRegistro();
    } else if (this.idAutorizacionPago){
      this.proboolean = true;
      this.route.queryParams.subscribe(params => this.verificacionPago = params);
      this.actualizarInfo(4, 'pago', this.getPago() );
      this.getDocumentosPCorregir(this.idCorreccionActual);
    }else {
      this.getEstadoCorreccion();
    }
  }

  getPago(){
   let op = this.pagoService.getPagoById(this.verificacionPago.idPago);
    op.subscribe({
      next: value => {
        this.getCamposPCorregir(value, 4);
      },
    })
  }
  getEstadoCorreccion() {
    this.autorizacionRegistroService.getById(this.idAutorizacionRegistro).subscribe({
      next: data => {
        this.autorizacionRegistro = data;
      },
      complete: () => {
        const { verificadoCliente, verificadoProducto, verificadoRegistro, verificadoPago} = this.autorizacionRegistro;
        if (verificadoCliente === 1){
          this.actualizarInfo(1, 'cliente', this.getInfo(1));
        } else if (verificadoProducto === 1){
          this.actualizarInfo(3, 'producto', this.getInfo(3));
        } else if (verificadoRegistro === 1){
          this.actualizarInfo(2, 'registro', this.getInfo(2));
        } else if (verificadoPago === 1){
          this.actualizarInfo(4, 'pago', this.getInfo(4));
        } else {
          this.actualizarInfo(1, 'cliente', this.getInfo(1));
        }
        this.getDocumentosPCorregir(this.idCorreccionActual);
      },
    });
  }
  actualizarInfo(correccion: number, texto: string, getInfo){
    this.idCorreccionActual = correccion;
    this.textoDocumentoActual = texto;
  }
  verificarCheck(esDocumento): boolean {
    return JSON.stringify(esDocumento ? this.documentosPCorregir : this.checkBoxes).includes('true');
  }

  getInfo(opcion) {
    let op;
    switch (opcion) {
      case 1:
        op = this.clienteService.getClienteById(this.autorizacionRegistro.idCliente);
        break;
      case 2:
        op = this.registroService.getRegistroById(this.autorizacionRegistro.idRegistro);
        break;
      case 3:
        op = this.productoClienteService.getProductoClienteById(this.autorizacionRegistro.idProducto);
        break;
      case 4:
        op = this.pagoService.getPagoById(this.autorizacionRegistro.idPago);
        break;
    }
    op.subscribe({
      next: value => {
        this.getCamposPCorregir(value, opcion);
      },
    });
  }

  async postErroresDocumentos() {
    /* Si se seleccionó al menos un documento a corregir, se mandará la información de lo contrario se notificará
       al usuario */
    if (JSON.stringify(this.documentosPCorregir).includes('true')) {
      const respuesta = await this.notificaciones.pregunta('¿Estás seguro que el documento no es legible?');
      if (respuesta.value) {
        this.notificaciones.carga('Mandando errores ingresados');

        const arregloDocumentos: any = [];
        const tipoDocumento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
        for (const documento of this.documentosPCorregir) {
          if (documento.activado) {
            arregloDocumentos.push({
              idTipoDocumento: documento.id,
              nombreDocumentoCorregir: documento.descripcion,
              comentario: documento.comentario,
            });
          }
        }
        const errorAnexo = {
          idAutorizacionRegistro: this.idAutorizacionRegistro,
          idEmpleado: +sessionStorage.getItem('Empleado'),
          idTipoDocumento: tipoDocumento,
          idEmpleadoCorreccion: this.autorizacionRegistro.idEmpleado,
          correcciones: arregloDocumentos,
        };
        this.erroresAnexosService.post(errorAnexo).subscribe({
          error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
          complete: () => this.putAutorizacionRegistro(2),
        });
      }
    }
  }
  async postErroresDocumentosPago() {
    /* Si se seleccionó al menos un documento a corregir, se mandará la información de lo contrario se notificará
       al usuario */
    if (JSON.stringify(this.documentosPCorregir).includes('true') && !this.existeArchivo) {
      const respuesta = await this.notificaciones.pregunta('¿Estás seguro que el documento no es legible?');
      if (respuesta.value) {
        this.notificaciones.carga('Mandando errores ingresados');
        const arregloDocumentos: any = [];
        for (const documento of this.documentosPCorregir) {
          if (documento.activado) {
            arregloDocumentos.push({
              idTipoDocumento: documento.id,
              nombreDocumentoCorregir: documento.descripcion,
              comentario: documento.comentario,
            });
          }
        }
        const errorAnexoPago = {
          idAutorizacionPago: this.idAutorizacionPago,
          idEmpleado: +sessionStorage.getItem('Empleado'),
          idEstadoCorreccion: 1,
          idTipoDocumento: 4,
          idEmpleadoCorreccion: this.verificacionPago.idEmpleado,
          correcciones: arregloDocumentos,
        };
        this.erroresAnexosService.postPago(errorAnexoPago).subscribe({
          error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
          complete: () => this.putAutorizacionPago(2),
        });
      }
    } else {
      this.putAutorizacionPago(2)
    }
  }
  async postErroresEnCampos() {
    const respuesta = await this.notificaciones.pregunta('¿Estás seguro que el documento tiene errores en los campos que seleccionaste?');
    if (respuesta.value) {
      this.notificaciones.carga('Mandando errores ingresados');
      const arregloDocumentos: any = [];
      const idTipoDocumento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
      for (const documento of this.checkBoxes) {
        if (documento.activo) {
          arregloDocumentos.push({
            idCampo: documento.id,
            campo: documento.campo,
            comentario: documento.comentario,
          });
        }
      }
      const errorAnexo = {
        idAutorizacionRegistro: this.idAutorizacionRegistro,
        idEmpleado: +sessionStorage.getItem('Empleado'),
        idEstadoCorreccion: 1,
        idTipoDocumento,
        idEmpleadoCorreccion: this.autorizacionRegistro.idEmpleado,
        correcciones: arregloDocumentos,
      };

      this.erroresAutorizacionService.post(errorAnexo).subscribe({
        error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
        complete: () => this.putAutorizacionRegistro(5),
      });
    }
  }
  async postErroresEnCamposPago() {
    if (!this.existeErrorDato) {
      const respuesta = await this.notificaciones.pregunta('¿Estás seguro que el documento tiene errores en los campos que seleccionaste?');
      if (respuesta.value) {
        this.notificaciones.carga('Mandando errores ingresados');
        const arregloDocumentos: any = [];
        for (const documento of this.checkBoxes) {
          if (documento.activo) {
            arregloDocumentos.push({
              idCampo: documento.id,
              campo: documento.campo,
              comentario: documento.comentario,
            });
          }
        }
        const errorDocumento = {
          idAutorizacionPago: this.idAutorizacionPago,
          idEmpleado: +sessionStorage.getItem('Empleado'),
          idTipoDocumento: 4,
          idEstadoCorreccion: 1,
          idEmpleadoCorreccion: this.verificacionPago.idEmpleado,
          correcciones: arregloDocumentos,
        };
        this.erroresAutorizacionService.postPago(errorDocumento).subscribe({
          error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
          complete: () => this.putAutorizacionPago(5),
        });
      }
    } else {
      this.putAutorizacionPago(5);
    }

  }
  cerrar(){
    this.location.back();
  }

  putAutorizacionRegistro(estado: number) {
    this.notificaciones.carga('Actualizando el registro');
    const documento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
    this.autorizacionRegistroService.documentoVerificadoVn(this.idAutorizacionRegistro, documento, estado).subscribe({
      error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
      complete: () => {
        this.notificaciones.exito('Documento comprobado').then( () => {
          if (this.idTipoDocumento || this.idCorreccionActual === 4) {
            this.router.navigate(['/modulos/sub-secuentes/autorizacion-errores/2']);
          } else {
            this.checkBoxes = [];
            this.documentosPCorregir = [];
            this.documentoLegible = false;
            this.getEstadoCorreccion();
          }
        });
      },
    });

  }

  putAutorizacionPago(estado: number) {
    this.notificaciones.carga('Actualizando el registro');
    const documento = !this.idTipoDocumento ? this.idCorreccionActual : this.idTipoDocumento;
    this.autorizacionPagoService.documentoVerificadoSub(this.idAutorizacionPago, documento, estado).subscribe({
      error: () => this.notificaciones.error('Ha ocurrido un error, intenta nuevamente o contacta al departamento de TI'),
      complete: () => {
        this.notificaciones.exito('Documento comprobado').then( () => {
          if (this.idTipoDocumento || this.idCorreccionActual === 4) {
            this.router.navigate(['/modulos/sub-secuentes/autorizaciones-proceso/' + this.idEstado]);
          } else {
            this.checkBoxes = [];
            this.documentosPCorregir = [];
            this.documentoLegible = false;
            this.getEstadoCorreccion();
          }
        });
      },
    });

  }

  getCamposPCorregir(info: any, idTabla: number) {
    const op = info.razonSocial === 2 ? this.campoCorregirService.getCamposDisponiblesForMoral(idTabla) :
      this.campoCorregirService.getCamposDisponibles(idTabla);
    op.subscribe({
      next: (result) => {
        if (idTabla === 3) {
          for (const campo of result) {
            this.checkBoxes.push({
              id: campo.id,
              idTabla: campo.idTabla,
              activo: false,
              nombre: campo.descripcion,
              campo: campo.campo,
              informacion: info.datos[campo.campo] === '' || !info.datos[campo.campo] ? 'No hay información' : info.datos[campo.campo],
              comentario: '',
            });
          }
        } else {

          for (const campo of result) {
            this.checkBoxes.push({
              id: campo.id,
              idTabla: campo.idTabla,
              activo: false,
              nombre: campo.descripcion,
              campo: campo.campo,
              informacion: !info[campo.campo] ? 'No hay información' : info[campo.campo],
              comentario: '',
            });
          }
        }
      },
    });
  }
  getDocumentosPCorregir(idTabla: number) {
    this.tipoDocumentosService.get(idTabla).subscribe({
      next: (data) => {
        for (const val of data) {
          this.documentosPCorregir.push({
            id: val.id,
            descripcion: val.descripcion,
            comentario: '',
            activado: false,
          });
        }
      },
    });
  }
  getVerificacionRegistro() {
    this.autorizacionRegistroService.getVnById(this.idAutorizacionRegistro).subscribe({
      next: data => this.autorizacionRegistro = data,
      complete: () => {
        switch (this.idTipoDocumento) {
          case 1:
            this.actualizarInfo(1, 'cliente', this.getInfo(1));
            break;
          case 2:
            this.actualizarInfo(2, 'registro', this.getInfo(2));
            break;
          case 3:
            this.actualizarInfo(3, 'producto', this.getInfo(3));
            break;
          case 4:
            this.actualizarInfo(4, 'pago', this.getInfo(4));
            break;
        }
      },
    });
  }

}