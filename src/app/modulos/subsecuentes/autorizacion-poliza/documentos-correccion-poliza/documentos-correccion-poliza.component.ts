import { Component, OnInit, ViewChild } from '@angular/core';
import { DatosClienteCorreccionComponent } from "../../correccion-errores/componentes/datos-cliente-correccion/datos-cliente-correccion.component";
import { DatosRegistroCorreccionComponent } from "../../correccion-errores/componentes/datos-registro-correccion/datos-registro-correccion.component";
import {DatosPagoCorreccionComponent } from "../../correccion-errores/componentes/datos-pago-correccion/datos-pago-correccion.component";
import { ProductoClienteCorreccionComponent } from "../../correccion-errores/componentes/producto-cliente-correccion/producto-cliente-correccion.component";
import { ErroresAutorizacion } from "../../../../@core/data/interfaces/venta-nueva/autorizacion/errores-autorizacion";
import { AutorizacionRegistro} from "../../../../@core/data/interfaces/venta-nueva/autorizacion/autorizacion-registro";
import { ActivatedRoute, Router } from "@angular/router";
import { ErroresAnexosService } from "../../../../@core/data/services/venta-nueva/autorizacion/errores-anexos.service";
import { ErroresAutorizacionService } from "../../../../@core/data/services/venta-nueva/autorizacion/errores-autorizacion.service";
import { AutorizacionRegistroService } from "../../../../@core/data/services/venta-nueva/autorizacion/autorizacion-registro.service";
import { AutorizacionPagoService } from "../../../../@core/data/services/subsecuentes/autorizacion-pago.service";
import { ErroresAnexosVService } from "../../../../@core/data/services/verificacion/errores-anexos-v.service";
import { ErroresVerificacionService } from "../../../../@core/data/services/verificacion/errores-verificacion.service";
import { NotificacionesService } from "../../../../@core/data/services/others/notificaciones.service";
import { VerificacionRegistroService } from "../../../../@core/data/services/verificacion/verificacion-registro.service";
import { DriveService } from "../../../../@core/data/services/ti/drive.service";
import { PagosService } from "../../../../@core/data/services/venta-nueva/pagos.service";
import { Location } from '@angular/common';

@Component({
  selector: 'app-documentos-correccion-poliza',
  templateUrl: './documentos-correccion-poliza.component.html',
  styleUrls: ['./documentos-correccion-poliza.component.scss']
})
export class DocumentosCorreccionPolizaComponent implements OnInit {
// Para activar los metodos de los hijos
@ViewChild(DatosClienteCorreccionComponent) componenteCliente: DatosClienteCorreccionComponent;
@ViewChild(DatosRegistroCorreccionComponent) componenteRegistro: DatosRegistroCorreccionComponent;
@ViewChild(DatosPagoCorreccionComponent) componentePago: DatosPagoCorreccionComponent;
@ViewChild(ProductoClienteCorreccionComponent) componenteProducto: ProductoClienteCorreccionComponent;

tituloComponente: string;
leyendaSuperior: string;
tipoError: string = this._route.snapshot.paramMap.get('tipoError');
etapaCorreccion: string = this._route.snapshot.paramMap.get('etapaCorreccion');
comentariosDocumentos: any;
idTipoDocumento: number = +this._route.snapshot.paramMap.get('idTipoDocumento');
idErrorAnexo: number;
variablesPorMostrar: string[] = [];
variablesComentarios: string[] = [];
camposPorCorregir;
idverificacionAutorizacionRegistro: number = +this._route.snapshot.paramMap.get('id_autorizacion_registro');
idAutorizacionPago: number = +this._route.snapshot.paramMap.get('id_autorizacion_pago');
formularioCorrecto: Boolean;
tipoArchivo: string;
datosPorCorregirA: ErroresAutorizacion;
autorizacionRegistro: AutorizacionRegistro;
valorRouta: string;
verificacionPago;
proboolean: boolean = false;
permisos = JSON.parse(window.localStorage.getItem('User'));

constructor(private _route: ActivatedRoute,
            private erroresAnexosAService: ErroresAnexosService,
            private erroresAutorizacionService: ErroresAutorizacionService,
            private autorizacionRegistroService: AutorizacionRegistroService,
            private autorizacionPagoService: AutorizacionPagoService,
            private erroresAnexosVService: ErroresAnexosVService,
            private erroresVerificacionService: ErroresVerificacionService,
            private notificaciones: NotificacionesService,
            private verificacionRegistroService: VerificacionRegistroService,
            private router: Router,
            private driveService: DriveService,
            private location: Location,
            private pagoService: PagosService) {
}

ngOnInit(): void {
  if (this.idAutorizacionPago){
    this.proboolean = true;
    this.datosIniciales(this.tipoError === 'documento' ? 'Validar documentos' : 'Validar datos',
      this.tipoError === 'documento' ? 'Comentarios para los siguientes documentos:' : 'Comentarios acerca de los datos introducidos:',
      this.tipoError === 'documento' ? 'documentos' : 'datos');
    this.tipoError === 'documento' ? this.getErroresDocumentosAPago() : this.getErroresEnDatosAPago();
    this.getAutorizacionPagos();
  }else {
    this.datosIniciales(this.tipoError === 'documento' ? 'Validar documentos' : 'Validar datos',
      this.tipoError === 'documento' ? 'Comentarios para los siguientes documentos:' : 'Comentarios acerca de los datos introducidos:',
      this.tipoError === 'documento' ? 'documentos' : 'datos');
    this.tipoError === 'documento' ? this.getErroresDocumentosA() : this.getErroresEnDatosA();
    this.getAutorizacionRegistro();
  }

}
datosIniciales(titulo: string, leyenda: string, valor: string){
  this.valorRouta = valor;
  this.tituloComponente = titulo;
  this.leyendaSuperior = leyenda;
}

getErroresDocumentosA() {
  const op = this.etapaCorreccion === 'autorizacion'
    ? this.erroresAnexosAService.getByIdAutorizacionRegitro(this.idverificacionAutorizacionRegistro, this.idTipoDocumento)
    : this.erroresAnexosVService.getByIdVerificacionRegistro(this.idverificacionAutorizacionRegistro, this.idTipoDocumento);
  // @ts-ignore
  op.subscribe(result => {
    this.comentariosDocumentos = result.correcciones;
    this.idErrorAnexo = result.id;
  });
}

getErroresDocumentosAPago() {
  const op = this.etapaCorreccion === 'autorizacion'
    ? this.erroresAnexosAService.getByIdAutorizacionPago(this.idAutorizacionPago)
    : this.erroresAnexosVService.getByIdVerificacionPago(this.idAutorizacionPago, this.idTipoDocumento);
  // @ts-ignore
  op.subscribe(result => {
    this.comentariosDocumentos = result.correcciones;
    this.idErrorAnexo = result.id;
  });
}

getErroresEnDatosA() {
  const op = this.etapaCorreccion === 'autorizacion'
    ? this.erroresAutorizacionService.getCamposConErrores(this.idverificacionAutorizacionRegistro, this.idTipoDocumento)
    : this.erroresVerificacionService.getCamposConErrores(this.idverificacionAutorizacionRegistro, this.idTipoDocumento);
  // @ts-ignore
  op.subscribe({
    next: data => {
      this.datosPorCorregirA = data;
      this.camposPorCorregir = this.datosPorCorregirA.correcciones;
      for (const campo of this.camposPorCorregir) {
        this.variablesPorMostrar.push(campo.campo);
        this.variablesComentarios.push(campo.comentario ? campo.comentario : 'Sin comentarios');
      }
    },
  });
}

getErroresEnDatosAPago() {
  const op = this.etapaCorreccion === 'autorizacion'
    ? this.erroresAutorizacionService.getCamposConErroresPagos(this.idAutorizacionPago)
    : this.erroresVerificacionService.getCamposConErrores(this.idverificacionAutorizacionRegistro, this.idTipoDocumento);
  // @ts-ignore
  op.subscribe({
    next: data => {
      this.datosPorCorregirA = data;
      this.camposPorCorregir = this.datosPorCorregirA.correcciones;
      for (const campo of this.camposPorCorregir) {
        this.variablesPorMostrar.push(campo.campo);
        this.variablesComentarios.push(campo.comentario ? campo.comentario : 'Sin comentarios');
      }
    },
  });
}

getAutorizacionRegistro() {
  const op = this.etapaCorreccion === 'autorizacion'
    ? this.autorizacionRegistroService.getVnById(this.idverificacionAutorizacionRegistro)
    : this.verificacionRegistroService.getById(this.idverificacionAutorizacionRegistro);
  op.subscribe({
    next: data => {
      this.autorizacionRegistro = data;
    },
    complete: () => {
      switch (this.idTipoDocumento) {
        case 1: {
          this.tipoArchivo = 'cliente';
          break;
        }
        case 2: {
          this.tipoArchivo = 'poliza';
          break;
        }
        case 3: {
          this.tipoArchivo = 'poliza';
          break;
        }
        case 4: {
          this.tipoArchivo = 'pago';
          break;
        }
      }
    },
  });
}

getAutorizacionPagos() {
  const op = this.etapaCorreccion === 'autorizacion'
    ? this.autorizacionPagoService.getPagoById(this.idAutorizacionPago)
    : this.verificacionRegistroService.getByIdPago(this.idAutorizacionPago);
  op.subscribe({
    next: data => {
      this.verificacionPago = data;
    },
    complete: () => {
      this.tipoArchivo = 'pago';
    },
  });
}

isAutorizacion(value: any) {
  this.etapaCorreccion === 'autorizacion' ? this.putAutorizacion(value.titulo,  value.tipoDocumento) :
    this.putVerificacion(value.titulo,  value.tipoDocumento);
}

putAutorizacion(titulo: string, tipoDocumento: string) {
  this.erroresAutorizacionService.correcccionDatos(this.idverificacionAutorizacionRegistro, this.idTipoDocumento,
    this.datosPorCorregirA.id).subscribe({
    complete: () => {
      this.notificaciones.exito(`${titulo} corregido ¡La información del ${tipoDocumento} se ha actualizado exitosamente! ` )
        .then(() => {
          this.router.navigate([`modulos/subsecuentes/correccion-errores-poliza/${this.valorRouta }/${this.etapaCorreccion}`]);
        } );
    }});
}

putVerificacion(titulo: string, tipoDocumento: string) {
  this.verificacionRegistroService.documentoVerificado(this.idverificacionAutorizacionRegistro, this.idTipoDocumento, 3).subscribe({
    complete: () => {
      this.erroresVerificacionService.putEstadoCorreccion(this.datosPorCorregirA.id).subscribe({
        complete: () => {
          this.notificaciones.exito(`${titulo} corregido ¡La información del ${tipoDocumento} se ha actualizado exitosamente! ` )
            .then(() => {
              this.router.navigate([`modulos/subsecuentes/correccion-errores-poliza/${this.valorRouta }/${this.etapaCorreccion}`]);
            } );
        }});
    },
  });
}

estaCorrecto(value: any) {
  this.formularioCorrecto = value.isCorrecto;
}

almacenarArchivo($event) {
  const filesToUpload = (event.target as HTMLInputElement).files;
  const extensionesDeArchivoAceptadas = ['application/pdf'];
  const extensionValida: Boolean = extensionesDeArchivoAceptadas.includes(filesToUpload.item(0).type);
  const tamanioArchivo = filesToUpload.item(0).size * .000001;
  if (filesToUpload.length === 1) {
    // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
    if (extensionValida && tamanioArchivo < 7) {
      if(this.idverificacionAutorizacionRegistro) {
        this.subirADrive(filesToUpload);
      }
    } else {
      this.notificaciones.error(!extensionValida ? 'Solo puedes subir archivos pdf' :
        'Los archivos que subas deben pesar menos de 7 MB',
        !extensionValida ? 'Extensión no soportada' : 'Archivo demasiado grande');
    }
  }
  if(this.idAutorizacionPago) {
    this.subirADrivePago(filesToUpload);
  }
}

subirADrive(fileToUpload: FileList) {
  this.notificaciones.carga('Se están subiendo los archivos');
  this.driveService.correccionDocumentos(fileToUpload, this.autorizacionRegistro.idRegistro, this.idTipoDocumento,
    this.idverificacionAutorizacionRegistro, this.idErrorAnexo, this.etapaCorreccion === 'autorizacion' ? 0 : 1).subscribe({
    next: () => this.notificaciones.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
      this.router.navigate([`modulos/subsecuentes/correccion-errores-poliza/${this.valorRouta }/${this.etapaCorreccion}`]);
    }),
    error: error => this.notificaciones.error(error.status !== 417 || error.status !== 400 ? 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información'
      : error.error)});
}

subirADrivePago(fileToUpload: FileList) {
  this.notificaciones.carga('Se están subiendo los archivos');
  this.driveService.correccionDocumentoPago(fileToUpload, this.verificacionPago.idPago, this.idTipoDocumento,
    this.idAutorizacionPago, this.idErrorAnexo, this.etapaCorreccion === 'autorizacion' ? 0 : 1, 2).subscribe({
    next: () => this.notificaciones.exito('¡Tus archivos han sido subidos exitosamente!').then(() => {
      this.router.navigate([`modulos/subsecuentes/correccion-errores-poliza/${this.valorRouta }/${this.etapaCorreccion}`]);
    }),
    error: error => this.notificaciones.error(error.status !== 417 || error.status !== 400 ? 'Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información'
      : error.error)});
}
activarMetodoCliente() {
  this.componenteCliente.putCliente();
}

activarMetodoRegistro() {
  this.componenteRegistro.putRegistro();
}

activarMetodoPago() {
  this.componentePago.putPago();
}

activarMetodoPagoSubSecuentes() {
  this.componentePago.putPagoSubsecuentes(this.idAutorizacionPago);
}

activarMetodoProducto() {
  this.componenteProducto.putProducto();
}
cerrar(){
  this.location.back();
}
}