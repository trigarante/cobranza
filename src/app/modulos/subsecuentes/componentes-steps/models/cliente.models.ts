import {Cliente} from "../../../../@core/data/interfaces/venta-nueva/cliente";

export class ClienteModel {
  public id: number;
  public archivo: string;
  public archivoSubido: number;
  public calle: string;
  public correo: string;
  public cp: string;
  public curp: string;
  public dni: string;
  public genero: string;
  public idColonia: number;
  public idColoniaPeru: number;
  public idPais: number;
  public materno: string;
  public nombre: string;
  public numExt: string;
  public numInt: string;
  public paterno: string;
  public razonSocial: number;
  public rfc: string;
  public ruc: string;
  public telefonoFijo: string;
  public telefonoMovil: string;

  constructor( cliente: Cliente ) {
    this.id = cliente.id;
    this.archivo = cliente.archivo;
    this.archivoSubido = cliente.archivoSubido;
    this.calle = cliente.calle;
    this.correo = cliente.correo;
    this.cp = cliente.cp;
    this.curp = cliente.curp;
    this.dni = cliente.dni;
    this.genero = cliente.genero;
    this.idColonia = cliente.idColonia;
    this.idColoniaPeru = cliente.idColoniaPeru;
    this.idPais = cliente.idPais;
    this.materno = cliente.materno;
    this.nombre = cliente.nombre;
    this.numExt = cliente.numExt;
    this.numInt = cliente.numInt;
    this.paterno = cliente.paterno;
    this.razonSocial = cliente.razonSocial;
    this.rfc = cliente.rfc;
    this.ruc = cliente.ruc;
    this.telefonoFijo = cliente.telefonoFijo;
    this.telefonoMovil = cliente.telefonoMovil;
  }
}
