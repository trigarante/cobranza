import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Recibos } from '../../../../@core/data/interfaces/venta-nueva/recibos';
import { RecibosService } from '../../../../@core/data/services/venta-nueva/recibos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-administrador-recibos',
  templateUrl: './administrador-recibos.component.html',
  styleUrls: ['./administrador-recibos.component.scss']
})
export class AdministradorRecibosComponent implements OnInit {
  recibo;
  idRegistro = this.rutaActiva.snapshot.params.idRegistro;
  conjuntoRecibos: Recibos[];
  count: number;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  
  constructor(
    private recibosService: RecibosService,
    public dialog: MatDialog,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this.count = 0;
    this.getRegistro();
  }
  siguiente() {
    if (this.count < this.conjuntoRecibos.length - 1) {
      this.count ++;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  regresar() {
    if (this.count > 0) {
      this.count--;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  getRegistro() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistro).subscribe(data => {
      this.conjuntoRecibos = data;
      this.recibo = this.conjuntoRecibos[0];
    });
  }

  pagarRecibo() {
    this.router.navigate(['/modulos/subsecuentes/pagos-pagos', this.recibo.id, this.recibo.idRegistro, this.rutaActiva.snapshot.params.idSolicitud]);
  }
  cerrar() {
    this._location.back();
  }

}