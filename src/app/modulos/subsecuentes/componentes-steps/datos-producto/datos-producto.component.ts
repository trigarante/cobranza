import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MarcaInterno } from '../../../../@core/data/interfaces/cotizador/marca-interno';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { DetalleInterno } from '../../../../@core/data/interfaces/cotizador/detalle-interno';
import { ActivatedRoute, Router } from '@angular/router';
import { ModeloInterno } from '../../../../@core/data/interfaces/cotizador/modelo-interno';
import { TipoSubramo } from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import { CatalogoInternoService } from '../../../../@core/data/services/cotizador/catalogo-interno.service';
import { TipoSubramoService } from '../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';
import { ProductoClienteService } from '../../../../@core/data/services/venta-nueva/producto-cliente.service';
import { RegistroService } from '../../../../@core/data/services/venta-nueva/registro.service';
import { NotificacionesService } from '../../../../@core/data/services/others/notificaciones.service';
import { ClienteVnService } from '../../../../@core/data/services/venta-nueva/cliente-vn.service';

@Component({
  selector: 'app-datos-producto',
  templateUrl: './datos-producto.component.html',
  styleUrls: ['./datos-producto.component.scss']
})
export class DatosProductoComponent implements OnInit {
  tipoSubRamos: TipoSubramo[];
  formDatos: FormGroup = new FormGroup({
    numeroMotor: new FormControl('', [Validators.pattern('^[A-Z0-9 ]+$'), Validators.maxLength(30)]),
    numeroPlacas: new FormControl(/**/'S/N'),
    numeroSerie: new FormControl('', Validators.compose([ Validators.required,
        Validators.pattern('^([A-HJ-NPR-Z0-9]{8}|[A-HJ-NPR-Z0-9]{17})$')]),
      this.validarNoSerieNoExista.bind(this)),
    recotizar: new FormControl(false),
    idTipoSubRamo: new FormControl(''),
    idMarca: new FormControl(''),
    marca: new FormControl(''),
    idModelo: new FormControl(''),
    modelo: new FormControl(''),
    idDescripcion: new FormControl(''),
    descripcion: new FormControl(''),
    idSubDescripcion: new FormControl(''),
    subDescripcion: new FormControl(''),
    idDetalle: new FormControl(''),
    detalle: new FormControl(''),
    descuento: new FormControl(''),
  });
  datos: any;
  marcas: MarcaInterno[];
  modelos: ModeloInterno[];
  descripciones;
  subdescripciones;
  detalles: DetalleInterno[];
  filesToUpload: FileList;
  archivoValido = false;
  @Input() idCliente: number;
  @Input() datosCotizacion: string;
  @Input() idSubRamo: number;
  @Input() idProductoCliente: number;
  @Output() _idProductoCliente = new EventEmitter();
  seRegistroArchivo = false;

  constructor(private catalogoService: CatalogoInternoService,
              private router: ActivatedRoute,
              private tipoSubRamoService: TipoSubramoService,
              private productoClienteService: ProductoClienteService,
              private registroService: RegistroService,
              private route: Router,
              protected clienteService: ClienteVnService,
              private notificaciones: NotificacionesService,) {
                this.tipoSubRamoService.getVN().subscribe(data => {
                  this.tipoSubRamos = data;
                });
               }

  ngOnInit(): void {
    if (this.idProductoCliente !== 0) {
      this.formDatos.controls.numeroSerie.clearAsyncValidators();
      this.getProductoCliente();
    } else {
      this.datos = this.datosCotizacion ? this.datosCotizacion : null;
    }
  }
  
  setvalue(identifiquer, value) {
    this.formDatos.controls[identifiquer].setValue(value);
  }

  crearProducto() {
    let productoCliente;
    this.notificaciones.carga('Se está subiendo el producto ingresado');
    this.datos.numeroPlacas = this.formDatos.controls.numeroPlacas.value;
    this.datos.numeroMotor = this.formDatos.controls.numeroMotor.value;
    productoCliente = {
      idSolicitud: +this.router.snapshot.paramMap.get('id'),
      idCliente: this.idCliente,
      datos: this.formDatos.controls.recotizar.value ? this.formDatos.getRawValue() : this.datos,
      idSubRamo: this.idSubRamo,
      noSerie: this.formDatos.controls.numeroSerie.value,
    };
    const operacion = this.idProductoCliente ? this.productoClienteService.putInOne(this.idProductoCliente, productoCliente,
        this.filesToUpload) : this.productoClienteService.postInOne(productoCliente, this.filesToUpload);
    operacion.subscribe({
      next: data => {
        let texto;
        if (this.filesToUpload) {
          this.seRegistroArchivo = true;
          texto = '¡Tu producto se creó exitosamente!';
        } else {
          texto = 'Recuerda subir el archivo despues en el administrador de pólizas';
        }
        this.notificaciones.exito(texto).then(() => {
            if (!this.idProductoCliente) {
              this._idProductoCliente.emit({numero: 1, id: 'idProducto', idProducto: data.id, numAux: 2, idAux: 'idRegistro'});
            }
          });
      },
      error: () => this.notificaciones.error('Hubo un error, favor de contactar al departamento de TI o intenta volver a guardar la información'),
    });
  }

  getMarcas() {
    this.formDatos.controls.idMarca.setValue(null);
    this.catalogoService.getMarcas(this.formDatos.controls.idTipoSubRamo.value).subscribe(m => {
      this.marcas = m;
    });
  }

  getModelos() {
    this.formDatos.controls.idModelo.setValue(null);
    this.catalogoService.getModelos().subscribe(m => {
      this.modelos = m;
    });
  }

  getDescripcion() {
    this.formDatos.controls.idDescripcion.setValue(null);
    this.catalogoService.getDescripcion(this.formDatos.controls.idMarca.value, this.formDatos.controls.idModelo.value)
      .subscribe(m => {
        this.descripciones = m;
      });
  }

  getSubdescripcion() {
    this.formDatos.controls.idSubDescripcion.setValue(null);
    this.catalogoService.getSubdescripcion(this.formDatos.controls.idMarca.value, this.formDatos.controls.idModelo.value,
      this.formDatos.controls.idDescripcion.value).subscribe(m => {
      this.subdescripciones = m;
    });
  }

  getDetalles() {
    this.catalogoService.getDetalles(this.formDatos.controls.idMarca.value, this.formDatos.controls.idModelo.value,
      this.formDatos.controls.idDescripcion.value, this.formDatos.controls.idSubDescripcion.value).subscribe({
      next: d => {
        this.detalles = d;
      }});
  }

  getProductoCliente() {
    this.productoClienteService.getProductoClienteById(this.idProductoCliente).subscribe( { next: data => {
        this.datos = data.datos;
        this.formDatos.controls.numeroPlacas.setValue(this.datos.numeroPlacas);
        this.formDatos.controls.numeroMotor.setValue(this.datos.numeroMotor);
        this.formDatos.controls.numeroSerie.setValue(data.noSerie);
        this.idCliente = this.datos.idCliente;
        if (data.archivo) {
          this.seRegistroArchivo = true;
        }
      }, complete: () => {
        this.formDatos.controls.numeroSerie.setAsyncValidators(this.validarNoSerieNoExista.bind(this));
      }});
  }

  validarNoSerieNoExista(): Promise<ValidationErrors | null> {
    return new Promise((resolve) => {
      this.registroService.getNoSerieExistente(this.formDatos.controls.numeroSerie.value).subscribe(
        data => {
          if (!data) {
            resolve(null);
          } else {
            this.notificaciones.errorPersonalizado('Actualmente ya existe una poliza activa con este número de serie y se encuetra en: <strong>'
              + data.noSerie + '</strong>', 'Número de serie en el sistema').then(() => {
              // this.route.navigate(['/modulos/dashboard']);
              resolve({placaExiste: true});
            });
          }
        });
    });
  }

  verificarNumeroPlaca() {
    if (this.formDatos.controls.numeroPlacas.value === '') {
      this.formDatos.controls.numeroPlacas.setValue('S/N');
    }
  }

  actualizarValidador() {
    if (this.formDatos.controls.recotizar.value) {
      this.formDatos.controls.idTipoSubRamo.setValidators([Validators.required]);
      this.formDatos.controls.idMarca.setValidators([Validators.required]);
      this.formDatos.controls.idModelo.setValidators([Validators.required]);
      this.formDatos.controls.idDescripcion.setValidators([Validators.required]);
      this.formDatos.controls.idSubDescripcion.setValidators([Validators.required]);
      this.formDatos.controls.idDetalle.setValidators([Validators.required]);
    } else {
      this.formDatos.controls.idTipoSubRamo.clearValidators();
      this.formDatos.controls.idMarca.clearValidators();
      this.formDatos.controls.idModelo.clearValidators();
      this.formDatos.controls.idDescripcion.clearValidators();
      this.formDatos.controls.idSubDescripcion.clearValidators();
      this.formDatos.controls.idDetalle.clearValidators();
    }
    this.formDatos.controls.idTipoSubRamo.updateValueAndValidity();
    this.formDatos.controls.idMarca.updateValueAndValidity();
    this.formDatos.controls.idModelo.updateValueAndValidity();
    this.formDatos.controls.idDescripcion.updateValueAndValidity();
    this.formDatos.controls.idSubDescripcion.updateValueAndValidity();
    this.formDatos.controls.idDetalle.updateValueAndValidity();
  }
  /** Guardar archivo **/
  almacenarArchivo() {
    const extensionesDeArchivoAceptadas = ['application/pdf'];
    this.filesToUpload = (event.target as HTMLInputElement).files;
    const extensionValida: Boolean = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
    const tamanioArchivo = this.filesToUpload.item(0).size * .000001;

    if (this.filesToUpload.length === 1) {
      // El servicio en Spring solo acepta como máximo 7 MB para subir archivos
      if (extensionValida && tamanioArchivo < 7) {
        this.notificaciones.informacion('¡Tu archivo está listo para ser guardado!');
        this.archivoValido = true;

      } else {
        this.notificaciones.error(!extensionValida ? 'Solo puedes subir archivos pdf' : 'Los archivos que subas deben pesar menos de 7 MB');
        this.archivoValido = false;
      }
    }
  }
}
