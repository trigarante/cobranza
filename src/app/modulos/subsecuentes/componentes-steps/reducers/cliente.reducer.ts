import {Action, createReducer, on} from '@ngrx/store';
import {crearCliente} from "../actions/cliente.actions";
import {ClienteModel} from "../models/cliente.models";

export const initialState: ClienteModel[] = [
  // new ClienteModel({
  //   id: 1,
  //   archivo: 'test',
  //   archivoSubido: 0,
  //   calle: 'test',
  //   correo: 'test',
  //   cp: 'test',
  //   curp: 'test',
  //   dni: 'test',
  //   genero: 'test',
  //   idColonia: 20000,
  //   idColoniaPeru: 0,
  //   idPais: 1,
  //   materno: 'test',
  //   nombre: 'test',
  //   numExt: 'test',
  //   numInt: 'test',
  //   paterno: 'test',
  //   razonSocial: 1,
  //   rfc: 'test',
  //   ruc: 'test',
  //   telefonoFijo: 'test',
  //   telefonoMovil: 'test',
  // })
];

const _clienteReducer = createReducer(
  initialState,
  on(crearCliente, (state, {cliente}) => [...state, new ClienteModel(cliente)]),
);

export function clienteReducer(state, action) {
  return _clienteReducer(state, action);
}
