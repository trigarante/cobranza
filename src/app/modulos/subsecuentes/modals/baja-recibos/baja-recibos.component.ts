import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { RecibosService } from "../../../../@core/data/services/venta-nueva/recibos.service";
import { TipoPagoVn } from "../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { NotificacionesService } from "../../../../@core/data/services/others/notificaciones.service";
import { RegistroService } from "../../../../@core/data/services/venta-nueva/registro.service";
import { map } from "rxjs/operators";
import * as  moment from 'moment';
import { TipoPagoVnService } from "../../../../@core/data/services/venta-nueva/catalogos/tipo-pago-vn.service";

@Component({
  selector: 'app-baja-recibos',
  templateUrl: './baja-recibos.component.html',
  styleUrls: ['./baja-recibos.component.scss']
})
export class BajaRecibosComponent implements OnInit {
  idRegistroPoliza = this.data.idRegistro;
  tipoPagos: TipoPagoVn[];
  recibosForm: FormGroup = this.fb.group({
    'idTipoPago': new FormControl('', Validators.required),
    'primerPago': new FormControl('', Validators.required),
  });
  numeros: RegExp = /[0-9.]/;
  numeroPagos: number;
  constructor(public dialogRef: MatDialogRef<BajaRecibosComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private notificaciones: NotificacionesService,
    private tipoPagoService: TipoPagoVnService,
    private registroService: RegistroService,
    private recibosService: RecibosService) { }

  ngOnInit(): void {
    this.getTipoPago();
  }
  getTipoPago() {
    this.tipoPagoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoPagos = result;
    });
  }

  getNumeroPagos(): number {
    if (this.tipoPagos !== undefined && this.recibosForm.controls['idTipoPago'].valid) {
      this.numeroPagos = this.tipoPagos.find(arr => arr.id === this.recibosForm.controls['idTipoPago'].value).cantidadPagos;
      if (this.numeroPagos > 1) {
        this.recibosForm.addControl('pagoMensual', new FormControl('', [Validators.required], this.setErrorMensual.bind(this)));
      } else {
        if (this.recibosForm.contains('pagoMensual')) {
          this.recibosForm.removeControl('pagoMensual');
        }
      }
      return this.numeroPagos;
    } else {
      return 0;
    }
  }

  setErrorMensual(): Promise<ValidationErrors | null> {
    return new Promise(() => {
      if (Number(this.recibosForm.controls['primerPago'].value) > Number(this.recibosForm.controls['pagoMensual'].value)) {
        this.recibosForm.controls['pagoMensual'].setErrors(null);
      } else {
        this.recibosForm.controls['pagoMensual'].setErrors({'incorrect': true});
      }
    });
  }

  crearRecibo() {
    let cantidadAPagar: number = this.recibosForm.controls['primerPago'].value;
    const recibos: any[] = [];
    let fechaVigencia = moment();
    // let fechaVigencia = moment(this.registroForm.controls['fechaInicio'].value).format('YYYY-MM-DD');

    this.notificaciones.carga('Generando recibos')

    for (let i = 0; i < this.numeroPagos ; i++) {
      if (i > 0) {
        cantidadAPagar = this.recibosForm.controls['pagoMensual'].value;
        fechaVigencia = moment(fechaVigencia).add(1, 'months');
      }
      recibos.push({
        'idEstadoRecibos': 1,
        'idRegistro': this.idRegistroPoliza,
        'idEmpleado': sessionStorage.getItem('Empleado'),
        'numero': i + 1,
        'cantidad': cantidadAPagar,
        'fechaVigencia': moment(moment(fechaVigencia).format('YYYY-MM-DD') + 'T06:00:00'),
        'fechaLiquidacion': '',
      });
    }

    this.recibosService.putActivo(this.idRegistroPoliza).subscribe({
      complete: () => {
        this.recibosService.post(recibos).subscribe({
          complete: () => {
            this.notificaciones.exito('¡Tus recbos han sido subidos exitosamente!')
            this.dialogRef.close();
          },
        });
      },
    });
  }

}