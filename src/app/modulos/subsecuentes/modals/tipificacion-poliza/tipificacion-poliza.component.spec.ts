import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipificacionPolizaComponent } from './tipificacion-poliza.component';

describe('TipificacionPolizaComponent', () => {
  let component: TipificacionPolizaComponent;
  let fixture: ComponentFixture<TipificacionPolizaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipificacionPolizaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipificacionPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
