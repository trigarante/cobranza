import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirArchivoClienteComponent } from './subir-archivo-cliente.component';

describe('SubirArchivoClienteComponent', () => {
  let component: SubirArchivoClienteComponent;
  let fixture: ComponentFixture<SubirArchivoClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubirArchivoClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubirArchivoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
