import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DriveService } from '../../../../@core/data/services/ti/drive.service';
import { NotificacionesService } from '../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-subir-archivo-cliente',
  templateUrl: './subir-archivo-cliente.component.html',
  styleUrls: ['./subir-archivo-cliente.component.scss']
})
export class SubirArchivoClienteComponent implements OnInit {
  filesToUpload: FileList;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private matDialogRef: MatDialogRef<SubirArchivoClienteComponent>,
  private driveService: DriveService,
  private notificacionesService: NotificacionesService,) { }

  ngOnInit(): void {
  }
  dismiss(reload) {
    this.matDialogRef.close(reload);
 }

 almacenarArchivo() {
   const extensionesDeArchivoAceptadas = ['application/pdf'];
   this.filesToUpload = (<HTMLInputElement>event.target).files;
   const extensionValida = extensionesDeArchivoAceptadas.includes(this.filesToUpload.item(0).type);
   const tamanioArchivo = this.filesToUpload.item(0).size * .000001;
   if (this.filesToUpload.length === 1) {
     // El servicio en Spring solo acepta a lo más 7 MB para subir archivos
     if (extensionValida && tamanioArchivo < 7) {
       this.notificacionesService.carga();
       this.subirADrive ();
     }
     else {
       this.filesToUpload = null;
       this.notificacionesService.error(!extensionValida ? 'Solo puedes subir archivos pdf' :
           'Los archivos que subas deben pesar menos de 7 MB',
         !extensionValida ? 'Extensión no soportada' : 'Archivo demasiado grande');
     }
   }
 }

 subirADrive() {
   this.driveService.postFile(this.filesToUpload, this.data,  1).subscribe({
     next: () => this.notificacionesService.exito().then(() => this.dismiss(true)),
     error: () => this.notificacionesService.error(),
     });
 }
}