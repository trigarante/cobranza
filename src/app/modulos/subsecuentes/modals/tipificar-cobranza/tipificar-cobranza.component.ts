import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormControl, Validators} from "@angular/forms";
import { NotificacionesService } from "../../../../@core/data/services/others/notificaciones.service";
import { tipificacionCobranza, tipificacionesCobranzaEtiqueta, tipificarPolizaCobranza } from "../../../../@core/data/interfaces/tipificaciones/tipificacionesCobranza";
import {TipificacionPolizaComponent} from "../tipificacion-poliza/tipificacion-poliza.component";
import { TipificacionesCobranzaService } from "../../../../@core/data/services/tipificaciones/tipificaciones-cobranza.service";

@Component({
  selector: 'app-tipificar-cobranza',
  templateUrl: './tipificar-cobranza.component.html',
  styleUrls: ['./tipificar-cobranza.component.scss']
})
export class TipificarCobranzaComponent implements OnInit {
  etiquetaEstadosPoliza: tipificacionesCobranzaEtiqueta[];
  estatusPoliza: any[];
  estadosPoliza: tipificacionCobranza[];
  tipificacionPoliza: tipificarPolizaCobranza[];

  tipificacionPolizaForm = this.fb.group({
    'id': new FormControl(''),
    'idRegistro': this.data.idRegistro,
    'idEstatusPolizaCobranza': new FormControl('', Validators.compose([Validators.required])),
    'descripcion': new FormControl(''),
    'activo': new FormControl(1),
    'idEtiquetaEstadoPolizaCobranza': new FormControl(''),
    'idEstadoPolizaCobranza': new FormControl(''),
  });

  constructor(private fb: FormBuilder,
              private notificaciones: NotificacionesService,
              private ref: MatDialogRef<TipificacionPolizaComponent>,
              private tipificacionesCobranzaService: TipificacionesCobranzaService,
              @Inject(MAT_DIALOG_DATA) public data,) { }

  ngOnInit(): void {
    this.getEstadoPoliza();
  }
  guardarTipificacion(){
    const INFO: tipificarPolizaCobranza = {
      idRegistro: this.tipificacionPolizaForm.get('idRegistro').value,
      idEstatusPolizaCobranza: this.tipificacionPolizaForm.get('idEstatusPolizaCobranza').value,
      descripcion: this.tipificacionPolizaForm.get('descripcion').value,
    }

    this.tipificacionesCobranzaService.postTipificar(INFO).subscribe({
      complete: () => {
        this.notificaciones.exito('Tipificación realizada');
        this.ref.close();
      },
      error: () => {
        this.notificaciones.error('Vuelve a intentarlo');
      }
    });
  }

  getEstadoPoliza() {
    //  this.estadosPoliza = [{estado: `${TipificacionesCobranzaService}`, id:5}, {estado: '', id:6}];
    this.tipificacionesCobranzaService.getAll(4).subscribe(data => {
      this.estadosPoliza = data
    })

  }

  getAllEtiquetaByEstadoPoliza() {
    // this.etiquetaEstadosPoliza = [{idEstadoSolicitud: 5, descripcion:`${this.tipificacionesCobranzaService}`}, {descripcion: 'chapulin', idEstadoSolicitud:6}];
    this.tipificacionesCobranzaService.getEtiqueta(this.tipificacionPolizaForm.controls.idEstadoPolizaCobranza.value).
    subscribe(data => this.etiquetaEstadosPoliza = data);
  }
  getAllEstatusByIdEtiqueta() {
    // this.estatusPoliza = [{descripcion: 'djJuarez', id:1}, {descripcion: 'SharkDj', id:2}, {descripcion: 'UzielitoMix', id:3}];
    this.tipificacionesCobranzaService.getSubEtiqueta(this.tipificacionPolizaForm.controls.idEtiquetaEstadoPolizaCobranza.value).
    subscribe(data => this.estatusPoliza = data);
  }

}
