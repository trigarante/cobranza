import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { NotificacionesService } from "../../../../@core/data/services/others/notificaciones.service";
import { SolicitudEndosoService } from "../../../../@core/data/services/endososCancelaciones/solicitud-endoso.service";
import { TipoEndosoService } from "../../../../@core/data/services/endososCancelaciones/tipo-endoso.service";
import { MotivoEndosoService } from "../../../../@core/data/services/endososCancelaciones/motivo-endoso.service";

@Component({
  selector: 'app-endoso-create',
  templateUrl: './endoso-create.component.html',
  styleUrls: ['./endoso-create.component.scss']
})
export class EndosoCreateComponent implements OnInit {
  endosoCreateform = this.fb.group({
    'idRegistro': this.data.idRegistro,
    'idEmpleadoSolicitante': sessionStorage.getItem('Empleado'),
    'idPenalizacionEndoso': 3,
    'comentarios': new FormControl('', Validators.compose([Validators.required])),
    'idTipoEndoso': new FormControl('', Validators.compose([Validators.required])),
    'idMotivoEndoso': new FormControl('', Validators.compose([Validators.required])),
  });

  tipoEndosos;
  motivoEndosos;

  constructor(private tipoEndosoService: TipoEndosoService,
              private fb: FormBuilder,
              private notificaciones: NotificacionesService,
              @Inject(MAT_DIALOG_DATA) public data,
              public dialogRef: MatDialogRef<EndosoCreateComponent>,
              private motivoEndosoService: MotivoEndosoService,
              private solicitudEndosoService:SolicitudEndosoService,) { }

  ngOnInit(): void {
    this.getTipoEndosos();
  }
  dismiss() {
    this.dialogRef.close();
  }
  getTipoEndosos() {
    this.tipoEndosoService.get().subscribe(data => this.tipoEndosos = data);
  }
  createEndoso() {
    this.notificaciones.carga();
    this.solicitudEndosoService.post(this.endosoCreateform.value).subscribe({ error: () => {
        this.notificaciones.error('Tu solicitud de endoso no ha sido ingresada');
      },
      complete: () => {
        this.notificaciones.exito('¡Tu solicitud ha sido ingresada exitosamente!').then(() => this.dialogRef.close(true))
      }});
  }
  getMotivoEndosos() {
    this.motivoEndosoService.getByIdTipoEndoso(this.endosoCreateform.controls['idTipoEndoso'].value).subscribe(data => {
      this.motivoEndosos = data;
    });
  }
}
