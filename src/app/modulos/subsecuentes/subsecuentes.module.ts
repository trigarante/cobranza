import { NgModule } from '@angular/core';
import { SolicitudesVnInternaComponent } from './solicitudes-vn-interna/solicitudes-vn-interna.component';
import { DatosSolicitudComponent } from './modals/datos-solicitud/datos-solicitud.component';
import { ProspectoCreateComponent } from './modals/prospecto-create/prospecto-create.component';
import { ChatWhatsappComponent } from './modals/chat-whatsapp/chat-whatsapp.component';
import { ReasignarSolicitudComponent } from './modals/reasignar-solicitud/reasignar-solicitud.component';

import { CommonModule } from '@angular/common';
import { SubsecuentesRoutingModule } from './subsecuentes-routing.module';
import { TelefoniaModule } from '../telefonia/telefonia.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material.module';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatBadgeModule } from '@angular/material/badge';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDialogModule } from '@angular/material/dialog';
import { getSpanishPaginatorIntl } from 'src/app/traduccion-paginator';

import { MatPaginatorIntl } from '@angular/material/paginator';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { DateAdapter } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { AutorizacionesProcesoComponent } from './autorizacion-errores/autorizaciones-proceso/autorizaciones-proceso.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { BajaRecibosComponent } from './modals/baja-recibos/baja-recibos.component';
import { SubirArchivoClienteComponent } from './modals/subir-archivo-cliente/subir-archivo-cliente.component';
import { EndosoCreateComponent } from './modals/endoso-create/endoso-create.component';
import { TipificarCobranzaComponent } from './modals/tipificar-cobranza/tipificar-cobranza.component';
import { TipificacionPolizaComponent } from './modals/tipificacion-poliza/tipificacion-poliza.component';
import { CorreccionErroresPolizaComponent } from './correccion-errores-poliza/correccion-errores-poliza.component';
import { CorreccionErroresPolizaDatosComponent } from './correccion-errores-poliza-datos/correccion-errores-poliza-datos.component';
import { AdministradorRecibosComponent } from './componentes-steps/administrador-recibos/administrador-recibos.component';
import { CreateEmisionComponent } from './componentes-steps/create-emision/create-emision.component';
import { DatosProductoComponent } from './componentes-steps/datos-producto/datos-producto.component';
import { InspeccionesComponent } from './componentes-steps/inspecciones/inspecciones.component';
import { PagosComponent } from './componentes-steps/pagos/pagos.component';
import { RecibosComponent } from './componentes-steps/recibos/recibos.component';
import { RegistroPolizaComponent } from './componentes-steps/registro-poliza/registro-poliza.component';
import { StepCotizadorComponent } from './step-cotizador/step-cotizador.component';
import { ViewerComponent } from './viewer/viewer.component';
import { PagoRecibosComponent } from './administracion-componentes/pago-recibos/pago-recibos.component';
import { MostrarDocumentosAutorizacionComponent } from './autorizacion-errores/mostrar-documentos-autorizacion/mostrar-documentos-autorizacion.component';
import { MostrarDocumentosPolizaComponent } from './autorizacion-errores/mostrar-documentos-poliza/mostrar-documentos-poliza.component';
import { DocumentosCorreccionPolizaComponent } from './autorizacion-poliza/documentos-correccion-poliza/documentos-correccion-poliza.component';
import { DatosClienteCorreccionComponent } from './correccion-errores/componentes/datos-cliente-correccion/datos-cliente-correccion.component';
import { DatosRegistroCorreccionComponent } from './correccion-errores/componentes/datos-registro-correccion/datos-registro-correccion.component';
import { DatosPagoCorreccionComponent } from './correccion-errores/componentes/datos-pago-correccion/datos-pago-correccion.component';
import { ProductoClienteCorreccionComponent } from './correccion-errores/componentes/producto-cliente-correccion/producto-cliente-correccion.component';
import {CotizadorComponent} from './cotizador/cotizador.component';
import {LogAseguradorasComponent} from './vnOnline/log-aseguradoras/log-aseguradoras.component';
import {
  CreateEmisionOnlineNewComponent
} from './vnOnline/componentes-step-online/create-emision-online-new/create-emision-online-new.component';
import {
  DatosProductosOnlineNewComponent
} from './vnOnline/componentes-step-online/datos-productos-online-new/datos-productos-online-new.component';
import {EmitirOnlineNewComponent} from './vnOnline/componentes-step-online/emitir-online-new/emitir-online-new.component';
import {InspeccionesOnlineNewComponent} from './vnOnline/componentes-step-online/inspecciones-online-new/inspecciones-online-new.component';
import {PagosOnlineNewComponent} from './vnOnline/componentes-step-online/pagos-online-new/pagos-online-new.component';
import {
  RegistroPolizaOnlineNewComponent
} from './vnOnline/componentes-step-online/registro-poliza-online-new/registro-poliza-online-new.component';
import {ModalCotizacionComponent} from './vnOnline/modal/modal-cotizacion/modal-cotizacion.component';
import {MostrarCotizacionComponent} from './vnOnline/mostrar-cotizacion/mostrar-cotizacion.component';
import {NewStepOnlineComponent} from './vnOnline/new-step-online/new-step-online.component';
import {RecotizadorTabsComponent} from './vnOnline/recotizador-tabs/recotizador-tabs.component';
import {AdministracionRecibosComponent} from './administracion-componentes/administracion-recibos/administracion-recibos.component';
import {LinkPagoComponent} from './link-pago/link-pago.component';

@NgModule({
  declarations: [
    SolicitudesVnInternaComponent,
    DatosSolicitudComponent,
    ReasignarSolicitudComponent,
    ProspectoCreateComponent,
    ChatWhatsappComponent,
    AutorizacionesProcesoComponent,
    AdministracionPolizasComponent,
    BajaRecibosComponent,
    SubirArchivoClienteComponent,
    EndosoCreateComponent,
    TipificarCobranzaComponent,
    TipificacionPolizaComponent,
    CorreccionErroresPolizaComponent,
    CorreccionErroresPolizaDatosComponent,
    AdministradorRecibosComponent,
    CreateEmisionComponent,
    DatosProductoComponent,
    InspeccionesComponent,
    PagosComponent,
    RecibosComponent,
    RegistroPolizaComponent,
    StepCotizadorComponent,
    ViewerComponent,
    PagoRecibosComponent,
    MostrarDocumentosAutorizacionComponent,
    MostrarDocumentosPolizaComponent,
    DocumentosCorreccionPolizaComponent,
    DatosClienteCorreccionComponent,
    DatosRegistroCorreccionComponent,
    DatosPagoCorreccionComponent,
    ProductoClienteCorreccionComponent,
    CotizadorComponent,
    LogAseguradorasComponent,
    CreateEmisionOnlineNewComponent,
    DatosProductosOnlineNewComponent,
    EmitirOnlineNewComponent,
    InspeccionesOnlineNewComponent,
    PagosOnlineNewComponent,
    RegistroPolizaOnlineNewComponent,
    ModalCotizacionComponent,
    MostrarCotizacionComponent,
    NewStepOnlineComponent,
    RecotizadorTabsComponent,
    AdministracionRecibosComponent,
    LinkPagoComponent
  ],
    imports: [
      CommonModule,
      SubsecuentesRoutingModule,
      TelefoniaModule,
      FlexLayoutModule,
      MaterialModule,
      NgxExtendedPdfViewerModule,
      MatProgressBarModule,
      MatBadgeModule,
      SharedModule,
      MatStepperModule,
      MatDialogModule,
    ],
    providers: [
        {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
        {
          provide: STEPPER_GLOBAL_OPTIONS,
          useValue: { displayDefaultIndicatorType: false },
        },
        { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    ],

    exports: [
      ChatWhatsappComponent
    ]
  })
  export class SubsecuentesModule { }
