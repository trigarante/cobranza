import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subsecuentes',
  template:  `
    <router-outlet></router-outlet>
  `,
})
export class SubsecuentesComponent {}