import { Component, DoCheck, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { NotificacionesService } from '../../../../../@core/data/services/others/notificaciones.service';
import { TipoPagoVnService } from '../../../../../@core/data/services/venta-nueva/catalogos/tipo-pago-vn.service';
import { SociosService } from '../../../../../@core/data/services/comerciales/socios.service';
import { RamoService } from '../../../../../@core/data/services/comerciales/ramo.service';
import { SubRamoService} from '../../../../../@core/data/services/comerciales/sub-ramo.service';
import { ProductoSociosService } from '../../../../../@core/data/services/comerciales/producto-socios.service';
import { RegistroService } from '../../../../../@core/data/services/venta-nueva/registro.service';
import { TipoPagoVn } from '../../../../../@core/data/interfaces/venta-nueva/catalogos/tipo-pago-vn';
import { SociosComercial } from '../../../../../@core/data/interfaces/comerciales/socios-comercial';
import { ProductoSocios } from '../../../../../@core/data/interfaces/comerciales/producto-socios';
import { Periodicidad } from '../../../../../@core/data/interfaces/venta-nueva/catalogos/periodicidad';
import { SubRamo } from '../../../../../@core/data/interfaces/comerciales/sub-ramo';
import { Ramo } from '../../../../../@core/data/interfaces/comerciales/ramo';
import { Registro } from '../../../../../@core/data/interfaces/venta-nueva/registro';
import { PeriodicidadService } from '../../../../../@core/data/services/venta-nueva/catalogos/periodicidad.service';

@Component({
  selector: 'app-datos-registro-correccion',
  templateUrl: './datos-registro-correccion.component.html',
  styleUrls: ['./datos-registro-correccion.component.scss']
})
export class DatosRegistroCorreccionComponent implements OnInit {
  @Input() variablesPorMostrar: string[];
  @Input() variablesComentarios: string[];
  @Input() idRegistro: number;
  @Output() isAutorizacion = new EventEmitter<any>();
  @Output() formularioCorrect = new EventEmitter<any>();

  datosRegistro: FormGroup = new FormBuilder().group({
    idEmpleado: new FormControl(''),
    idProducto: new FormControl(''),
    idEstadoPoliza: new FormControl(''),
    idProductoSocio: new FormControl(''),
    idFlujoPoliza: new FormControl(''),
    // fechaRegistro: new FormControl(''),
    archivo: new FormControl(''),
    // fechaFin: new FormControl(null),
    oficina: new FormControl(''),
    idTipoPago: new FormControl('', Validators.required),
    poliza: new FormControl('', Validators.required),
    fechaInicio: new FormControl('', Validators.required),
    primaNeta: new FormControl('', [Validators.required, Validators.pattern('[0-9]+([.][0-9]{1,2})?')]),
    alias: new FormControl('', Validators.required),
    tipoRamo: new FormControl('', Validators.required),
    tipoSubramo: new FormControl('', Validators.required),
    idPeriodicidad: new FormControl('', Validators.required),
    idSocio: new FormControl('', Validators.required),
    idDepartamento: new FormControl('', Validators.required),
  });
  tipoPagos: TipoPagoVn[];
  socios: SociosComercial[];
  socio: SociosComercial;
  ramos: Ramo[];
  subRamos: SubRamo[];
  productosSocio: ProductoSocios[];
  periodicidades: Periodicidad[];
  registro;
  constructor(private tipoPagoVnService: TipoPagoVnService,
              private sociosService: SociosService,
              private ramoService: RamoService,
              private subRamoService: SubRamoService,
              private productoSocioService: ProductoSociosService,
              private registroService: RegistroService,
              private periodicidadService: PeriodicidadService,
              private notificaciones: NotificacionesService) { }

  ngOnInit(): void {
    this.getInformacionRegistro();
  }
  getTipoPago() {
    this.tipoPagoVnService.get().subscribe(data => { this.tipoPagos = data; });
  }

  // Métodos para catálogos de registro
  getSocios() {
    this.sociosService.getByIdPaisStep(1).subscribe( {
      next: data => this.socios = data,
      complete: () => {
        this.getRamo(this.registro.idSocio, this.registro.idRamo);
        this.getSubRamo(this.registro.idRamo, this.registro.idSubRamo);
        this.getProductoSocio(this.registro.idSubRamo, +this.registro.productoSocio);
      },
    });
  }

  getRamo(idSocio: number, idRamo?: number) {
    this.socio = this.socios.filter( socios => socios.id === this.datosRegistro.controls.idSocio.value)[0];
    if (this.socio.expresionRegular === null) {
      this.datosRegistro.controls.poliza.setValidators(Validators.required);
    } else {
      if (this.socio.id !== 2 && this.socio.id !== 8) {
        this.datosRegistro.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(`${this.socio.expresionRegular}`)]));
      } else {
        const arrExp = this.socio.expresionRegular.split(',');
        const expresionesRegulares =
          this.socio.id === 2 ? `${arrExp[0]}|${arrExp[1].trim()}|${arrExp[2].trim()}` : `${arrExp[0]}|${arrExp[1].trim()}`;
        this.datosRegistro.controls.poliza.setValidators(Validators.compose(
          [Validators.required, Validators.pattern(expresionesRegulares)]));
      }
    }

    this.datosRegistro.controls.poliza.updateValueAndValidity();
    if (idRamo === undefined) {
      this.datosRegistro.controls.tipoRamo.setValue('');
      this.datosRegistro.controls.tipoSubramo.setValue('');
      this.datosRegistro.controls.idProductoSocio.setValue('');
      this.ramos = [];
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.ramoService.getByIdSocio(idSocio).subscribe( {next: data => {
        this.ramos = data;
        if (this.ramos.length === 0) {
          this.mensajeErrorCatalogos('ramos');
        }
      }, complete: () => {
      }});
  }

  mensajeErrorCatalogos(catalogo: string) {
    this.notificaciones.error(`Sin elementos disponibles ¡Por favor agregue ${catalogo} en el catálogo correspondiente para poder continuar!`);

  }

  getSubRamo(idRamo: number, idSubRamo?: number) {
    if (idSubRamo === undefined) {
      this.datosRegistro.controls.tipoSubramo.setValue('');
      this.datosRegistro.controls.idProductoSocio.setValue('');
      this.subRamos = [];
      this.productosSocio = [];
    }

    this.subRamoService.getByIdRamo(idRamo).subscribe( {next: data => {
        this.subRamos = data;
        if (this.subRamos.length === 0) {
          this.mensajeErrorCatalogos('subramos');
        }
      },
    });
  }

  getProductoSocio(subRamo: number, idProductoSocio?: number) {
    if (idProductoSocio === undefined) {
      this.datosRegistro.controls.idProductoSocio.setValue('');
      this.productosSocio = [];
    }

    this.productoSocioService.getByIdSubRamo(subRamo).subscribe({
      next: result => {
        this.productosSocio = result;
        if (this.productosSocio.length === 0) {
          this.mensajeErrorCatalogos('productos socio');
        }
      },
    });
  }

  getInformacionRegistro() {
    this.registroService.getRegistroById(this.idRegistro).subscribe({
      next: data => {
        this.registro = data;
        this.getPeriodicidades();
        this.getTipoPago();
        this.datosRegistro.patchValue(data);
        // this.datosRegistro.controls.fechaInicio.setValue(data.fechaInicio + 'T06:00:00');
        this.datosRegistro.controls.alias.setValue(data.idSocio);
        this.datosRegistro.controls.tipoRamo.setValue(data.idRamo);
        this.datosRegistro.controls.tipoSubramo.setValue(data.idSubRamo);
      }, complete: () => this.getSocios(),
    });
  }

  getPeriodicidades() {
    this.periodicidadService.get().subscribe(data => { this.periodicidades = data; });
  }

  ngDoCheck(): void {
    this.formularioCorrect.emit(this.datosRegistro.valid ? {isCorrecto: true} : {isCorrecto: false});
  }

  putRegistro() {
    this.notificaciones.carga('Subiendo la corrección ingresada');

    this.registroService.put(this.idRegistro, this.datosRegistro.value).subscribe({
      complete: () => {
        this.isAutorizacion.emit({titulo: 'Registro', tipoDocumento: 'registro'});
      },
    });
  }
}