import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TipoSubramo} from '../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {CatalogoInternoService} from '../../../@core/data/services/cotizador/catalogo-interno.service';
import {MarcaInterno} from '../../../@core/data/interfaces/cotizador/marca-interno';
import {ModeloInterno} from '../../../@core/data/interfaces/cotizador/modelo-interno';
import {DetalleInterno} from '../../../@core/data/interfaces/cotizador/detalle-interno';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';
import {ProductoSolicitudvnService} from '../../../@core/data/services/venta-nueva/producto-solicitudvn.service';
import {TipoSubramoService} from '../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-cotizador',
  templateUrl: './cotizador.component.html',
  styleUrls: ['./cotizador.component.scss'],
})
export class CotizadorComponent implements OnInit {
  formCotizacion: FormGroup = new FormGroup({
    idTipoSubRamo: new FormControl(''),
    idMarca: new FormControl('', Validators.required),
    marca: new FormControl('', Validators.required),
    idModelo: new FormControl('', Validators.required),
    modelo: new FormControl('', Validators.required),
    idDescripcion: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required),
    idSubDescripcion: new FormControl('', Validators.required),
    subDescripcion: new FormControl('', Validators.required),
    descuento: new FormControl(''),
    idDetalle: new FormControl('', Validators.required),
    detalle: new FormControl('', Validators.required),
  });
  @Input() idProspecto: number = this.rutaActiva.snapshot.params.id;
  solicitud: any;
  marcas: MarcaInterno[];
  modelos: ModeloInterno[];
  descripciones;
  subdescripciones;
  detalles: DetalleInterno[];
  idSolicitudDetalles: number = this.rutaActiva.snapshot.params.idSolicitud;
  tipoSubRamos: TipoSubramo[];
  constructor(private catalogoService: CatalogoInternoService,
              private tipoSubRamoService: TipoSubramoService,
              private productoSolicitudService: ProductoSolicitudvnService,
              private router: Router,
              private location: Location,
              private notificaciones: NotificacionesService,
              private rutaActiva: ActivatedRoute) {
    this.tipoSubRamoService.get().subscribe(data => {
      this.tipoSubRamos = data;
    });
  }

  setvalue(identifiquer, value) {
    this.formCotizacion.controls[identifiquer].setValue(value);
  }

  ngOnInit() {
    if (this.idSolicitudDetalles) {
      this.getProductoSolicitud();
    }
  }
  getProductoSolicitud() {
    this.productoSolicitudService.getProductoSolicitud(this.idSolicitudDetalles).subscribe(data => {
      if (data) {
        this.solicitud = data.datos;
      }
    });
  }

  resetForm() {
    this.formCotizacion.reset();
  }

  getMarcas() {
    this.formCotizacion.controls.idMarca.setValue(null);
    this.catalogoService.getMarcas(this.formCotizacion.value.idTipoSubRamo).subscribe(m => {
      this.marcas = m;
    });
  }

  getModelos() {
    this.formCotizacion.controls.idModelo.setValue(null);
    this.catalogoService.getModelos().subscribe(m => {
      this.modelos = m;
    });
  }

  getDescripcion() {
    this.formCotizacion.controls.idDescripcion.setValue(null);
    this.catalogoService.getDescripcion(this.formCotizacion.value.idMarca, this.formCotizacion.value.idModelo)
      .subscribe(m => {
        this.descripciones = m;
      });
  }

  getSubdescripcion() {
    this.formCotizacion.controls.idSubDescripcion.setValue(null);
    this.catalogoService.getSubdescripcion(this.formCotizacion.value.idMarca, this.formCotizacion.value.idModelo,
      this.formCotizacion.value.idDescripcion).subscribe(m => {
      this.subdescripciones = m;
    });
  }

  getDetalles() {
    this.catalogoService.getDetalles(this.formCotizacion.value.idMarca, this.formCotizacion.value.idModelo,
      this.formCotizacion.value.idDescripcion, this.formCotizacion.value.idSubDescripcion).subscribe({
      next: d => {
        this.detalles = d;
      },
      });

  }

  guardarsinCotizar() {
    this.notificaciones.carga('Se está actualizando la informacón, espere un momento.');
    const {idTipoSubRamo, ...datos} = this.formCotizacion.getRawValue();
    const productoSolicitudForm = {
      idProspecto: +this.rutaActiva.snapshot.params.id,
      idTipoSubRamo,
      datos: {...datos}
    };
    this.productoSolicitudService.postAll(productoSolicitudForm).subscribe({
      next: (data) => {
        this.notificaciones.pregunta(
            '¿Deseas comenzar ahora el proceso de emisión?',
            '¡Solicitud guardada con éxito!',
            'Ir a emisión',
            'Regresar')
          .then(value => {
            if (value.value) {
              this.router.navigate(['modulos/subsecuentes/step-cotizador/' + data.idSolicitud]);
            } else {
              this.location.back();
            }
          });
      },
      error: () => {
        this.notificaciones.advertencia('Favor de contactar a TI', 'Hubo un error al actualizar la información');
      },
    });
  }
}
