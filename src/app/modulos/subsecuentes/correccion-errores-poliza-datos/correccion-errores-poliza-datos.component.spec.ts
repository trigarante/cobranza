import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresPolizaDatosComponent } from './correccion-errores-poliza-datos.component';

describe('CorreccionErroresPolizaDatosComponent', () => {
  let component: CorreccionErroresPolizaDatosComponent;
  let fixture: ComponentFixture<CorreccionErroresPolizaDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorreccionErroresPolizaDatosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresPolizaDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
