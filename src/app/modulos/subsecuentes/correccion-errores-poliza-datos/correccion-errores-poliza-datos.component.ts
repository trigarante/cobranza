import { Component, OnInit, ViewChild } from '@angular/core';
import { VerificacionRegistro } from "../../../@core/data/interfaces/verificacion/verificacion-registro";
import { MatTableDataSource } from "@angular/material/table";
import { FiltrosTablasService } from "../../../@core/filtros-tablas.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CorreccionErroresService } from "../../../@core/data/services/venta-nueva/correccion-errores.service";
import { NotificacionesService } from "../../../@core/data/services/others/notificaciones.service";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";

@Component({
  selector: 'app-correccion-errores-poliza-datos',
  templateUrl: './correccion-errores-poliza-datos.component.html',
  styleUrls: ['./correccion-errores-poliza-datos.component.scss']
})
export class CorreccionErroresPolizaDatosComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterValues = {};
  estadoVerificacion: string;
  datosPCorregir = new MatTableDataSource([]);
  displayedColumns: string[] = ['poliza', 'numero', 'fechaInicio', 'nombreCliente', 'nombre', 'fechaCreacionError', 'fechaPago',
    'nombreComercial', 'descripcion', 'horasRestantes', 'Acciones'];
  filterSelectObj = [];
  rangeDates =  [
    new Date(new Date().setDate(new Date().getDate() - 15)),
    new Date(new Date().setDate(new Date().getDate() + 1)),
  ];
  max: any = new Date();
  permisos = JSON.parse(window.localStorage.getItem('User'));
  tipoCorreccion = '';
  fechaHoy: any = new Date();
  errorSpin = false;

  constructor(private filtrosTablasService: FiltrosTablasService,
              private route: ActivatedRoute,
              private correccionErroresService: CorreccionErroresService,
              private notificaciones: NotificacionesService,
              private router: Router) {
                this.filterSelectObj = this.filtrosTablasService.createFilterSelect([{id: 'poliza' , nombre: 'PÓLIZA'},
                {id: 'numero' , nombre: 'INTENTOS'},
                {id: 'nombre' , nombre: 'EMPLEADO'},
                {id: 'nombreCliente' , nombre: 'CLIENTE'},
                {id: 'nombreComercial' , nombre: 'SOCIO'},
                {id: 'descripcion' , nombre: 'DEPARTAMENTO'}, ]);
  }

  ngOnInit(): void {
    this.route.url.subscribe(() => {
      this.tipoCorreccion = this.route.snapshot.paramMap.get('tipo_correccion');
      if (this.tipoCorreccion === 'autorizacion') {
        this.displayedColumns.splice(4, 1);
      } else {
        this.displayedColumns.splice(4, 0, 'nombreMesa');
      }
      this.getDatosPCorregirA(false);
    });
  }
  ngOnDestroy() {
    this.filtrosTablasService.filterSelect = [];
  }

  getDatosPCorregirA(recarga: boolean) {
    if (recarga) { this.datosPCorregir = null; }
    this.errorSpin = false;
    const fechaInicio = (this.rangeDates[0].toISOString().split('T')[0]);
    const fechaFin = (this.rangeDates[1].toISOString().split('T')[0]);
    const op = this.tipoCorreccion === 'autorizacion'
      ? this.correccionErroresService.getErroresDatosA(fechaInicio, fechaFin)
      : this.correccionErroresService.getErroresDatosSubs(fechaInicio, fechaFin);
    op.subscribe({
      next:   data => {
        if (data.length === 0) {
          this.notificaciones.informacion('No tienes errores de datos por mostrar, sigue así');
        }

        this.datosPCorregir = new MatTableDataSource(data);
        this.datosPCorregir.sort = this.sort;
        this.datosPCorregir.paginator = this.paginator;

        // Lineas para los filtros
        this.filterSelectObj.filter((o) => {
          o.options = this.filtrosTablasService.getFilterObject(data, o.columnProp);
        });

        // Lineas para los filtros
        this.datosPCorregir.filterPredicate = this.filtrosTablasService.createFilter();

      },
      error: () => {
        this.errorSpin = true;
        this.notificaciones.error('Error al cargar los datos');
      }
    });
  }

  // Lineas para los filtros
  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.value.trim().toLowerCase();

    this.datosPCorregir.filter = JSON.stringify(this.filterValues);
  }

  // Lineas para los filtros
  resetFilters() {
    this.filterValues = {};
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    });
    this.datosPCorregir.filter = '';
  }

  mostrarDocumentos(verificacionRegistro: VerificacionRegistro, tipoError: string, input: number) {
    this.router.navigate(['/modulos/subsecuentes/documentos-correccion-poliza/' + tipoError + '/' + this.tipoCorreccion
    + '/' + input + '/' + verificacionRegistro.id]);
  }

  diferenciaDeHoras(fechaCreacion: any): number {
    const horaEnFormatoCorrecto: any = new Date(new Date(fechaCreacion).toLocaleString('en-US', {timeZone: 'America/Mexico_City'}));
    const diferencia = horaEnFormatoCorrecto - this.fechaHoy;
    const diff = 24 - Math.abs(diferencia) / 36e5;
    return +diff.toString().split('.')[0];
  }

}
