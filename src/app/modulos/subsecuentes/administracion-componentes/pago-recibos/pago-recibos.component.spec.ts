import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoRecibosComponent } from './pago-recibos.component';

describe('PagoRecibosComponent', () => {
  let component: PagoRecibosComponent;
  let fixture: ComponentFixture<PagoRecibosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagoRecibosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
