import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionRecibosComponent } from './administracion-recibos.component';

describe('AdministracionRecibosComponent', () => {
  let component: AdministracionRecibosComponent;
  let fixture: ComponentFixture<AdministracionRecibosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracionRecibosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
