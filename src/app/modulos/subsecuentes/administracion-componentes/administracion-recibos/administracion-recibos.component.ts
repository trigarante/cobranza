import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {Recibos} from '../../../../@core/data/interfaces/venta-nueva/recibos';
import {RecibosService} from '../../../../@core/data/services/venta-nueva/recibos.service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {PagarLinkPagoComponent} from '../../../cobranza/modals/pagar-link-pago/pagar-link-pago.component';
@Component({
  selector: 'app-administracion-recibos',
  templateUrl: './administracion-recibos.component.html',
  styleUrls: ['./administracion-recibos.component.scss']
})
export class AdministracionRecibosComponent implements OnInit {

  recibo;
  idRegistro = this.rutaActiva.snapshot.params.idRegistro;
  idTipo: string;
  idSolicitud = this.rutaActiva.snapshot.paramMap.get('idSolicitud');
  conjuntoRecibos: Recibos[];
  count: number;
  permisos = JSON.parse(window.localStorage.getItem('User'));


  constructor(
    private recibosService: RecibosService,
    public dialog: MatDialog,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private _location: Location

  ) {
  }

  ngOnInit() {
    this.rutaActiva.queryParams.subscribe(params => {
      this.idTipo = params['idTipo'];
    });
    this.count = 0;
    this.getRegistro();
  }

  siguiente() {
    if (this.count < this.conjuntoRecibos.length - 1) {
      this.count ++;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  regresar() {
    if (this.count > 0) {
      this.count--;
    }
    this.recibo = this.conjuntoRecibos[this.count];
  }

  getRegistro() {
    this.recibosService.getRecibosByIdRegistro(this.idRegistro).subscribe(data => {
      this.conjuntoRecibos = data;
      this.recibo = this.conjuntoRecibos[0];
    });

  }

  pagarRecibo() {
    if (this.idSolicitud) {
      this.router.navigate(['/modulos/subsecuentes/pagos-pagos', this.recibo.id, this.recibo.idRegistro, this.idSolicitud]);
    } else {
      this.router.navigate(['/modulos/subsecuentes/pagos-pagos', this.recibo.id, this.recibo.idRegistro]);
    }
  }
  cerrar() {
    this._location.back();
  }
  detallesRecibo(){
    if (this.idSolicitud) {
      this.router.navigate(['/modulos/subsecuentes/detalle-pagos/1', this.recibo.id, this.recibo.idRegistro, this.idSolicitud]);
    } else {
      this.router.navigate(['/modulos/subsecuentes/detalle-pagos/1', this.recibo.id, this.recibo.idRegistro]);
    }
  }
  pagarLinkPago() {
    this.dialog.open(PagarLinkPagoComponent, {
      width: '500px',
      data: {idRegistro: this.idRegistro, idRecibo: this.recibo.id}
    });
  }

}
