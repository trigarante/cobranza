import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {PremioDesempenoService} from '../../../@core/data/services/administracion-personal/premio-desempeno.service';
import moment from 'moment';
import {NotificacionesService} from '../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-visualizar-kpi',
  templateUrl: './visualizar-kpi.component.html',
  styleUrls: ['./visualizar-kpi.component.css']
})
export class VisualizarKpiComponent implements OnInit {
  visualizarIndicadoresForm: FormGroup;
  complementoKPI: any[];
  counter: number;
  mostrarDetalles = false;
  indicadoresArrayAux: any;
  calificacionAux: any[] = [];
  rangeDates: Date[];
  fechaCompleta1: any;
  fechaCompleta2: any;
  habilitar = false;
  idEmpleado: number;
  fechasArr: any[] = [];
  premioDesempeno: any[] = [];
  fecha: any;
  valueAux: any[] = [];

  constructor(
    private premioDesempenoService: PremioDesempenoService,
    private notificacionesService: NotificacionesService,
    private fb: FormBuilder,
  ) {
    this.rangeDates = [
      new Date(new Date().setDate(new Date().getDate() - 1)),
      new Date(new Date().setDate(new Date().getDate())),
    ];
    this.fechaCompleta1 = moment(this.rangeDates[0]).format('YYYY-MM-DD h:mm:ss');
    this.fechaCompleta2 = moment(this.rangeDates[1]).format('YYYY-MM-DD h:mm:ss');
  }

  ngOnInit(): void {
    this.idEmpleado = +sessionStorage.getItem('Empleado');
    this.visualizarIndicadoresForm = this.fb.group({
      cantidadAsignada: ['', Validators.required],
      cantidadDescuento: ['', Validators.required],
      calificacionFinal: ['', Validators.required],
      fecha: ['', Validators.required],
      complementoKPI: this.fb.array([this.agregarIndicadoresDeDesempenoGroup()])
    });
    this.getIndicadoresDesempeno();
  }

  agregarIndicadoresDeDesempenoGroup() {
    return this.fb.group({
      indicador: new FormControl(''),
      definicion: new FormControl(''),
      objetivo: ['', Validators.required],
      unidad: [''],
      // peso: ['', Validators.required],
      calificacion: [''],
      real: ['', Validators.required],
      // resultado: ['', Validators.required],
    });
  }

  agregarIndicadoresDeDesempeno(cantidad, complementoKPI) {
    this.mostrarDetalles = true;
    this.counter = cantidad;
    const arrCalFinal = [];
    for (let i = 0; i < complementoKPI.length; i++) {
      if (complementoKPI[i].indicador !== '') {
        arrCalFinal.push(complementoKPI[i].calificacion);
        this.indicadoresArray.push(this.agregarIndicadoresDeDesempenoGroup());
        this.indicadoresArray.controls[i].controls.indicador.setValue(complementoKPI[i].indicador);
        this.indicadoresArray.controls[i].controls.definicion.setValue(complementoKPI[i].definicion);
        this.indicadoresArray.controls[i].controls.unidad.setValue(complementoKPI[i].unidad);
        this.indicadoresArray.controls[i].controls.calificacion.setValue(`${complementoKPI[i].calificacion}%`);
        this.indicadoresArray.controls[i].controls.objetivo.setValue(complementoKPI[i].objetivo);
        this.indicadoresArray.controls[i].controls.real.setValue(`${complementoKPI[i].real}%`);
        this.calificacionAux.push(complementoKPI[i].calificacion);
      }
    }
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    this.visualizarIndicadoresForm.controls.calificacionFinal.setValue(`${arrCalFinal.reduce(reducer)}%`);
    this.indicadoresArrayAux = this.indicadoresArray.controls;
  }

  get indicadoresArray(): any {
    return this.visualizarIndicadoresForm.get('complementoKPI') as FormArray;
  }

  getIndicadoresDesempeno(){
    // const fecha1 = moment(this.rangeDates[0]).format('YYYY-MM-DD h:mm:ss');
    // const fecha2 = moment(this.rangeDates[1]).format('YYYY-MM-DD h:mm:ss');
    this.premioDesempenoService.getIndicadores(this.idEmpleado).subscribe({
      next: value => {
        this.premioDesempeno = value;
        this.premioDesempeno.map(d => {
          this.fechasArr.push(d.fechaAsignacion);
        });
        // if (value.length > 0) {
        //   // this.habilitar  = true;
        //
        //   const formatter = new Intl.NumberFormat('en-US', {
        //     style: 'currency',
        //     currency: 'USD',
        //   });
        //   this.visualizarIndicadoresForm.controls.cantidadAsignada.setValue(formatter.format(value[0].cantidadTotal));
        //   this.visualizarIndicadoresForm.controls.cantidadDescuento.setValue(formatter.format(value[0].cantidadDescuento));
        //   this.complementoKPI = value[0].estructura.complementoKPI;
        //   this.agregarIndicadoresDeDesempeno(value[0].estructura.complementoKPI.length, value[0].estructura.complementoKPI);
        // } else {
        //   this.notificacionesService.advertencia('No hay datos que mostrar para este rango de fechas');
        // }
      }
    });
  }

  mostrarFormulario(fecha) {
    this.habilitar = true;
    this.valueAux = [];
    this.indicadoresArray.controls = [];
    this.visualizarIndicadoresForm.controls.calificacionFinal.setValue('');
    this.valueAux = this.premioDesempeno.filter(res => {
      return res.fechaAsignacion === fecha;
    });

    this.visualizarIndicadoresForm.controls.calificacionFinal.setValue('');

    // tslint:disable-next-line:prefer-for-of
    // for (let i = 0; i < this.valueAux[0].estructura.complementoKPI.length; i++) {
    //   if (this.valueAux[0].estructura.complementoKPI[i].indicador !== '') {

        // this.indicadoresArray.push(this.agregarIndicadoresDeDesempenoGroup());
        // this.indicadoresArray.controls[i].controls.indicador.setValue(complementoKPI[i].indicador);
        // this.indicadoresArray.controls[i].controls.definicion.setValue(complementoKPI[i].definicion);
        // this.indicadoresArray.controls[i].controls.unidad.setValue(complementoKPI[i].unidad);
        // this.indicadoresArray.controls[i].controls.calificacion.setValue(`${complementoKPI[i].calificacion}%`);
        // this.indicadoresArray.controls[i].controls.objetivo.setValue(complementoKPI[i].objetivo);
        // this.indicadoresArray.controls[i].controls.real.setValue(`${complementoKPI[i].real}%`);
        // this.calificacionAux.push(this.valueAux[0].complementoKPI[i].calificacion);
    //   }
    // }

    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
    });
    this.visualizarIndicadoresForm.controls.cantidadAsignada.setValue(formatter.format(this.valueAux[0].cantidadTotal));
    this.visualizarIndicadoresForm.controls.cantidadDescuento.setValue(formatter.format(this.valueAux[0].cantidadDescuento));
    // this.visualizarIndicadoresForm.controls.calificacionFinal.setValue(arrCalFinal.reduce(reducer));
    this.complementoKPI = this.valueAux[0].estructura.complementoKPI;
    this.agregarIndicadoresDeDesempeno(this.valueAux[0].estructura.complementoKPI.length, this.valueAux[0].estructura.complementoKPI);
  }
}
