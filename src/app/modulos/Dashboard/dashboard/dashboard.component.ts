import {Component, OnInit} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  smallBreak: boolean;
  constructor(
    private breakpointObserver: BreakpointObserver
  ) {
    this.smallBreak = breakpointObserver.isMatched('(max-width: 414px)');
  }

  ngOnInit() {
  }
}
