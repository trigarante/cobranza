import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {VisualizarKpiComponent} from './visualizar-kpi/visualizar-kpi.component';
import {VacacionesComponent} from './vacaciones/vacaciones.component';
import {NominaComponent} from './nomina/nomina.component';
import {IncapacidadesComponent} from './incapacidades/incapacidades.component';
import {TabsComponent} from './tabs/tabs.component';
import {AsistenciasComponent} from './asistencias/asistencias.component';
import {UltimoMovimientoComponent} from './ultimo-movimiento/ultimo-movimiento.component';
import {PromocionesComponent} from './promociones/promociones.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: TabsComponent,
      },
      {
        path: 'tabs',
        component: TabsComponent,
      },
      {
        path: 'vacaciones',
        component: VacacionesComponent
      },
      {
        path: 'asistencias',
        component: AsistenciasComponent
      },
      {
        path: 'ultimos-movimientos',
        component: UltimoMovimientoComponent
      },
      {
        path: 'nomina',
        component: NominaComponent
      },
      {
        path: 'justificantes',
        component: IncapacidadesComponent
      },
      {
        path: 'visualizar-kpi',
        component: VisualizarKpiComponent
      },
      {
        path: 'promociones',
        component: PromocionesComponent
      },
      {
        path: '**',
        redirectTo: ''
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
