import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashPjsipComponent } from './dash-pjsip.component';

describe('DashPjsipComponent', () => {
  let component: DashPjsipComponent;
  let fixture: ComponentFixture<DashPjsipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashPjsipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashPjsipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
