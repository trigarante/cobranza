import {Component, OnInit, ViewChild} from '@angular/core';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import {MatSort} from '@angular/material/sort';
import {DashBoardNew} from '../../../@core/data/interfaces/reportes/dash-board-new';
import {MatPaginator} from '@angular/material/paginator';
import {XlsxService} from '../../../@core/data/services/reportes/xlsx.service';
import {DashBoardNewService} from '../../../@core/data/services/reportes/dash-board-new.service';
import {MatTableDataSource} from '@angular/material/table';
const moment = _rollupMoment || _moment;
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';

@Component({
  selector: 'app-dash-pjsip',
  templateUrl: './dash-pjsip.component.html',
  styleUrls: ['./dash-pjsip.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class DashPjsipComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['subarea', 'ejecutivo', 'leads', 'leadsError', 'error%', 'noContactados', 'noC%', 'enMarcacion1a4'
    , 'enM14%', 'enMarcacion5', 'enM5%', 'contactados', 'con%', 'contactoEfectivo', 'conE%', 'horaLogueo', 'tiempoLogueo'
    , 'pausas', 'tiempoPausaa', 'tiempoProductivo', 'llamadas'];
  dashBoard: DashBoardNew[];
  rangeDates: Date[];
  dashBoardFiltro: DashBoardNew[] = [{idEmpleado: 0, idSubarea: 0, subarea: 'TODAS', ejecutivo: 'a', leads: 0,
    llamadas: 0, contactoErroneo: 0, contactados: 0, enMarcacion5: 0, contactoEfectivo: 0, horaLogueo: 0, tiempoLogueo: 0, pausas: 0,
    tiempoPausaa: ' ', tiempoProductivo: 0, noContactados: 0, enMarcacion1a4: 0}];
  puesto: string = sessionStorage.getItem('idPuesto');
  fechaCompleta1: any;
  dataSource: any;
  fechaCompleta2: any;
  totalDays: number;
  idSubarea: number = 0;
  leadsTotal: number;
  llamadasTotal: number;
  erororTotal: number;
  contactadosTotal: number;
  noContactadosTotal: number;
  enMarcacion14Total: number;
  enMarcacion5Total: number;
  contactoEfectivoTotal: number;
  pausasTotal: number;
  horaTotal: number;
  tiempoTotal: number;
  tiempoProductivoTotal: number;
  conteoLogueo: number;
  conteoPausa: number;
  conteoHora: number;

  constructor(private xlsxService: XlsxService,
              private dashBoardNewService: DashBoardNewService) {
    this.rangeDates = [
      new Date(new Date().setDate(new Date().getDate() )),
      new Date(new Date().setDate(new Date().getDate() )),
    ];
    this.fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    this.fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);
    this.totalDays = moment(this.rangeDates[1]).diff(moment(this.rangeDates[0]).valueOf(), 'days');
    this.dashBoardNewService.getDepartamentos().subscribe(data => {
      data.forEach(value => {
        this.dashBoardFiltro.push(value);
      });
    });
  }

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.dashBoardNewService.getDashBoardPjsip(this.fechaCompleta1, this.fechaCompleta2, this.idSubarea).subscribe(data => {
      this.dashBoard = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.leadsTotal = this.dashBoard.map(t => t.leads).reduce((acc, value) => acc + value, 0);
      this.llamadasTotal = this.dashBoard.map(t => t.llamadas).reduce((acc, value) => acc + value, 0);
      this.erororTotal = this.dashBoard.map(t => t.contactoErroneo).reduce((acc, value) => acc + value, 0);
      this.contactadosTotal = this.dashBoard.map(t => t.contactados).reduce((acc, value) => acc + value, 0);
      this.noContactadosTotal = this.dashBoard.map(t => t.noContactados).reduce((acc, value) => acc + value, 0);
      this.enMarcacion14Total = this.dashBoard.map(t => t.enMarcacion1a4).reduce((acc, value) => acc + value, 0);
      this.enMarcacion5Total = this.dashBoard.map(t => t.enMarcacion5).reduce((acc, value) => acc + value, 0);
      this.contactoEfectivoTotal = this.dashBoard.map(t => t.contactoEfectivo).reduce((acc, value) => acc + value, 0);
      this.conteoLogueo = this.dashBoard.filter(value => {
        return value.tiempoLogueo !== 0;
      }).length;
      this.conteoHora = this.dashBoard.filter(value => {
        return value.horaLogueo !== 0;
      }).length;
      this.conteoPausa = this.dashBoard.filter(value => {
        return value.pausas !== 0;
      }).length;
      this.pausasTotal = this.dashBoard.map(t => t.pausas).reduce((acc, value) => acc + value, 0) / this.conteoPausa;
      this.horaTotal = this.dashBoard.map(t => t.horaLogueo).reduce((acc, value) => acc + value, 0) / this.conteoHora;
      this.tiempoTotal = this.dashBoard.map(t => t.tiempoLogueo).reduce((acc, value) => acc + value, 0) / this.conteoLogueo;
      this.tiempoProductivoTotal = this.dashBoard.map(t => t.tiempoProductivo).reduce((acc, value) => acc + value, 0)
        / this.conteoHora;
      console.log((this.tiempoProductivoTotal * 1000) + 21600000 );
      console.log(this.dashBoard.map(t => t.tiempoProductivo).reduce((acc, value) => acc + value, 0));
      console.log(this.conteoHora);
    });
  }
  getFilter() {
    this.leadsTotal = undefined;
    this.llamadasTotal = undefined;
    this.contactadosTotal = undefined;
    this.enMarcacion14Total = undefined;
    this.enMarcacion5Total = undefined;
    this.contactoEfectivoTotal = undefined;
    this.pausasTotal = undefined;
    this.dataSource = undefined;
    this.dashBoardNewService.getDashBoardPjsip(this.fechaCompleta1, this.fechaCompleta2, this.idSubarea).subscribe(data => {
      this.dashBoard = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.leadsTotal = this.dashBoard.map(t => t.leads).reduce((acc, value) => acc + value, 0);
      this.llamadasTotal = this.dashBoard.map(t => t.llamadas).reduce((acc, value) => acc + value, 0);
      this.erororTotal = this.dashBoard.map(t => t.contactoErroneo).reduce((acc, value) => acc + value, 0);
      this.contactadosTotal = this.dashBoard.map(t => t.contactados).reduce((acc, value) => acc + value, 0);
      this.noContactadosTotal = this.dashBoard.map(t => t.noContactados).reduce((acc, value) => acc + value, 0);
      this.enMarcacion14Total = this.dashBoard.map(t => t.enMarcacion1a4).reduce((acc, value) => acc + value, 0);
      this.enMarcacion5Total = this.dashBoard.map(t => t.enMarcacion5).reduce((acc, value) => acc + value, 0);
      this.contactoEfectivoTotal = this.dashBoard.map(t => t.contactoEfectivo).reduce((acc, value) => acc + value, 0);
      this.conteoLogueo = this.dashBoard.filter(value => {
        return value.tiempoLogueo !== 0;
      }).length;
      this.conteoHora = this.dashBoard.filter(value => {
        return value.horaLogueo !== 0;
      }).length;
      this.conteoPausa = this.dashBoard.filter(value => {
        return value.pausas !== 0;
      }).length;
      this.pausasTotal = this.dashBoard.map(t => t.pausas).reduce((acc, value) => acc + value, 0) / this.conteoPausa;
      this.horaTotal = this.dashBoard.map(t => t.horaLogueo).reduce((acc, value) => acc + value, 0) / this.conteoHora;
      this.tiempoTotal = this.dashBoard.map(t => t.tiempoLogueo).reduce((acc, value) => acc + value, 0) / this.conteoLogueo;
      this.tiempoProductivoTotal = this.dashBoard.map(t => t.tiempoProductivo).reduce((acc, value) => acc + value, 0)
        / this.conteoHora;
    });
  }
  getFechas() {
    this.dashBoardFiltro = [{idEmpleado: 0, idSubarea: 0, subarea: 'TODAS', ejecutivo: 'a', leads: 0,
      llamadas: 0, contactoErroneo: 0, contactados: 0, enMarcacion5: 0, contactoEfectivo: 0, horaLogueo: 0, tiempoLogueo: 0, pausas: 0,
      tiempoPausaa: ' ', tiempoProductivo: 0, noContactados: 0, enMarcacion1a4: 0}];
    this.dashBoardNewService.getDashBoardFiltroPjsip(this.fechaCompleta1, this.fechaCompleta2).subscribe(data => {
      data.forEach(value => {
        this.dashBoardFiltro.push(value);
      });
    });
    this.totalDays = moment(this.rangeDates[1]).diff(moment(this.rangeDates[0]).valueOf(), 'days');
    this.leadsTotal = undefined;
    this.llamadasTotal = undefined;
    this.contactadosTotal = undefined;
    this.enMarcacion14Total = undefined;
    this.enMarcacion5Total = undefined;
    this.contactoEfectivoTotal = undefined;
    this.pausasTotal = undefined;
    this.dataSource = undefined;
    this.fechaCompleta1 = (this.rangeDates[0].toISOString().split('T')[0]);
    this.fechaCompleta2 = (this.rangeDates[1].toISOString().split('T')[0]);
    this.dashBoardNewService.getDashBoardPjsip(this.fechaCompleta1, this.fechaCompleta2, this.idSubarea).subscribe(data => {
      this.dashBoard = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.leadsTotal = this.dashBoard.map(t => t.leads).reduce((acc, value) => acc + value, 0);
      this.llamadasTotal = this.dashBoard.map(t => t.llamadas).reduce((acc, value) => acc + value, 0);
      this.erororTotal = this.dashBoard.map(t => t.contactoErroneo).reduce((acc, value) => acc + value, 0);
      this.contactadosTotal = this.dashBoard.map(t => t.contactados).reduce((acc, value) => acc + value, 0);
      this.noContactadosTotal = this.dashBoard.map(t => t.noContactados).reduce((acc, value) => acc + value, 0);
      this.enMarcacion14Total = this.dashBoard.map(t => t.enMarcacion1a4).reduce((acc, value) => acc + value, 0);
      this.enMarcacion5Total = this.dashBoard.map(t => t.enMarcacion5).reduce((acc, value) => acc + value, 0);
      this.contactoEfectivoTotal = this.dashBoard.map(t => t.contactoEfectivo).reduce((acc, value) => acc + value, 0);
      this.conteoLogueo = this.dashBoard.filter(value => {
        return value.tiempoLogueo !== 0;
      }).length;
      this.conteoHora = this.dashBoard.filter(value => {
        return value.horaLogueo !== 0;
      }).length;
      this.conteoPausa = this.dashBoard.filter(value => {
        return value.pausas !== 0;
      }).length;
      this.pausasTotal = this.dashBoard.map(t => t.pausas).reduce((acc, value) => acc + value, 0) / this.conteoPausa;
      this.horaTotal = this.dashBoard.map(t => t.horaLogueo).reduce((acc, value) => acc + value, 0) / this.conteoHora;
      this.tiempoTotal = this.dashBoard.map(t => t.tiempoLogueo).reduce((acc, value) => acc + value, 0) / this.conteoLogueo;
      this.tiempoProductivoTotal = this.dashBoard.map(t => t.tiempoProductivo).reduce((acc, value) => acc + value, 0)
        / this.conteoHora;
    });
    this.dashBoardNewService.getDepartamentos().subscribe(data => {
      data.forEach(value => {
        this.dashBoardFiltro.push(value);
      });
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  descargaLlamadas() {
    this.dashBoardNewService.getDescargaLlamadas(this.fechaCompleta1, this.fechaCompleta2).subscribe({
      next: value => {
        value.forEach((aux, index) => {
          value[index].fechaRegistro = moment(aux.fechaRegistro).format('DD-MM-YYYY HH:mm:ss');
          value[index].fechaInicio = moment(aux.fechaInicio).format('DD-MM-YYYY HH:mm:ss');
        });
        const columns = ['fechaRegistro', 'fechaInicio', 'horaInicio', 'area', 'departamento',
          'ejecutivo', 'numero', 'duracion', 'idSolicitud', 'subetiqueta', 'tipo', 'estado'];
        this.xlsxService.imprimirXLS(value, columns, 'Llamadas de Salida');
      },
    });
  }
  descargaLead() {
    this.dashBoardNewService.getDescargaLeads(this.fechaCompleta1, this.fechaCompleta2).subscribe({
      next: value => {
        value.forEach((aux, index) => {
          value[index].fechaSolicitud = moment(aux.fechaSolicitud).format('DD-MM-YYYY HH:mm:ss');
        });
        const columns = ['idSolicitud', 'idDepartamento', 'fechaSolicitud', 'area', 'departamento', 'ejecutivo', 'numero',
          'llamadas', 'llamadasEfectivas', 'callbackEfectivo', 'minutosatencion'];
        this.xlsxService.imprimirXLS(value, columns, 'Leads Marcados');
      },
    });
  }

  descargaDashBoard() {
    const columns = ['idEmpleado',  'idDepartamento',  'ejecutivo',  'leads',  'llamadas', 'leadsError'
      , 'noContactados',  'contactados', 'enMarcacion1a4', 'enMarcacion5', 'contactoEfectivo',  'horaLogueo',  'tiempoLogueo'];
    this.xlsxService.imprimirXLS(this.dashBoard, columns, 'Dashboard');
  }

  descargaDash(empleado: number, tipo: number) {
    this.dashBoardNewService.getDescargaDash(this.fechaCompleta1, this.fechaCompleta2, empleado, tipo).subscribe(value => {
      const columns = ['id', 'descripcion',  'ejecutivo',  'llamadas'];
      this.xlsxService.imprimirXLS(value, columns, 'Dashboard');
    });
  }

}
