import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../../@core/data/services/others/notificaciones.service';
import {SolicitudesVacacionesService} from '../../../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';

@Component({
  selector: 'app-cancelar-vacaciones-ejecutivo',
  templateUrl: './cancelar-vacaciones-ejecutivo.component.html',
  styleUrls: ['./cancelar-vacaciones-ejecutivo.component.scss']
})
export class CancelarVacacionesEjecutivoComponent implements OnInit {

  comentarios = new FormControl('', Validators.compose([Validators.required]));

  constructor(
    public dialogRef: MatDialogRef<CancelarVacacionesEjecutivoComponent>,
    private notificaciones: NotificacionesService,
    private vacacionesService: SolicitudesVacacionesService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
  }

  guardar() {
    this.notificaciones.carga(`Se están guardando los comentarios`);
    this.vacacionesService.update(this.data, {
      idEmpleadoEjecutivo: +sessionStorage.getItem('Empleado'),
      fechaCancelacion: new Date(),
      idEstadoVacacion: 4,
      comentariosEjecutivo: this.comentarios.value,
    }).subscribe(() => this.notificaciones.exito().then(() => this.cerrar(true)));
  }

  cerrar(reload: boolean) {
    this.notificaciones.cerrar();
    this.dialogRef.close(reload);
  }
}
