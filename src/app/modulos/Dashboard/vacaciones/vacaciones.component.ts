import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-vacaciones',
  templateUrl: './vacaciones.component.html',
  styleUrls: ['./vacaciones.component.scss']
})
export class VacacionesComponent implements OnInit {
  tabActual = 0;
  permisos = JSON.parse(window.localStorage.getItem('User')).VAC;
  constructor() { }

  ngOnInit(): void {
  }

  tabClick(event) {
    this.tabActual =  event.index;
  }
}
