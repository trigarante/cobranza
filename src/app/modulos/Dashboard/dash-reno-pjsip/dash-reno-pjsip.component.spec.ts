import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashRenoPjsipComponent } from './dash-reno-pjsip.component';

describe('DashRenoPjsipComponent', () => {
  let component: DashRenoPjsipComponent;
  let fixture: ComponentFixture<DashRenoPjsipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashRenoPjsipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashRenoPjsipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
