import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-dash-header',
  templateUrl: './dash-header.component.html',
  styleUrls: ['./dash-header.component.scss']
})
export class DashHeaderComponent implements OnInit {
  smallBreak: boolean;
  nombre = localStorage.getItem('nombreE');
  infoArea = {
    area: localStorage.getItem('area'),
    depto: localStorage.getItem('depto'),
    sede: localStorage.getItem('sede'),
    empleado: localStorage.getItem('nombreE'),
    puesto: localStorage.getItem('puesto'),
    puestoE: localStorage.getItem('puestoE'),
    usuario: sessionStorage.getItem('email'),
    id: sessionStorage.getItem('Empleado'),
  };
  constructor( private breakpointObserver: BreakpointObserver
  ) {
    this.smallBreak = this.breakpointObserver.isMatched('(max-width: 414px)');
  }

  ngOnInit(): void {
  }

}
