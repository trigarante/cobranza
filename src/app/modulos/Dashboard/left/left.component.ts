import {Component, OnInit} from '@angular/core';
import {CortesService} from '../../../@core/data/services/administracion-personal/asistencias/cortes.service';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css']
})
export class LeftComponent implements OnInit {
  hidden: boolean;
  permisos = JSON.parse(window.localStorage.getItem('User'));
  constructor(private cortesService: CortesService) {
    this.cortesService.traigoCorte().subscribe({
      next: corte => this.hidden = corte ? true : false
    });
  }

  ngOnInit(): void {
  }
}
