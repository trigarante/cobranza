import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ultimo-movimiento',
  templateUrl: './ultimo-movimiento.component.html',
  styleUrls: ['./ultimo-movimiento.component.scss']
})
export class UltimoMovimientoComponent implements OnInit {

  tabActual = 0;
  permisos = JSON.parse(window.localStorage.getItem('User')).UM;

  constructor() { }

  ngOnInit(): void {
  }

  tabClick(event) {
    this.tabActual =  event.index;
  }

}
