import {Component, Input, OnInit} from '@angular/core';
import {
  UltimosMovimientosService
} from '../../../../@core/data/services/administracion-personal/asistencias/ultimos-movimientos.service';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';
import {
  SolicitudesVacacionesService
} from '../../../../@core/data/services/administracion-personal/solicitudes-vacaciones.service';
import {PageEvent} from '@angular/material/paginator';
import {AsistenciasService} from '../../../../@core/data/services/administracion-personal/asistencias/asistencias.service';
import {
  EstadoAsistenciaService
} from '../../../../@core/data/services/administracion-personal/asistencias/estado-asistencia.service';

@Component({
  selector: 'app-ultimos-movimientos',
  templateUrl: './ultimos-movimientos.component.html',
  styleUrls: ['./ultimos-movimientos.component.scss']
})
export class UltimosMovimientosComponent implements OnInit {
  activados;
  solicitudesPendientes = [];
  solicitudesCompletas = [];
  datos: any[] = [];
  paginator = [];
  length = 0;
  pageSize = 10;
  dataSource = new MatTableDataSource([]);
  data = null;
  currentTab = 100;
  hijos;
  estados;
  formulario: FormGroup = new FormGroup({
    datos: new FormArray([]),
  });
  empleado = +sessionStorage.getItem('Empleado');
  yaRegistro = false;



  //
  fecha = new Date();
  @Input() Estado;

  constructor(
    private dialog: MatDialog,
    private notificaciones: NotificacionesService,
    private empleadoService: EmpleadosService,
    private estadoAsistenciaService: EstadoAsistenciaService,
    private asistenciasService: AsistenciasService,
    private fb: FormBuilder,
    private notificacionesService: NotificacionesService,
    private vacacionesService: SolicitudesVacacionesService,
    private ultimosMovimientosService: UltimosMovimientosService
  ) { }

  ngOnInit(): void {
    this.getSolicitudes();
  }
// tslint:disable-next-line:jsdoc-format
  /** Get inicial **/
  getSolicitudes() {
    const estado = this.Estado === 0 ? this.activados = 3 : this.activados = 4;
    const op = this.asistenciasService.getByEmpleadoSupervisor(estado);
    op.subscribe( values => {
      this.dataSource = new MatTableDataSource<any>(values);
      if (values.length === 0) { this.notificaciones.informacion(`No tienes solicitudes pendientes`); }
      // this.updateDatos();
      this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
      this.length = values.length;
    });
  }

// tslint:disable-next-line:jsdoc-format
  /** Cambiar el paginador **/
  onPage(event: PageEvent) {
    const start = event.pageIndex * event.pageSize;
    const final = event.pageIndex === 0 ? start + event.pageSize : start + event.pageSize + 1;
    this.paginator = this.dataSource.data.slice(start, final);
  }
// tslint:disable-next-line:jsdoc-format
  /** Actualizar información **/
  updateDatos() {
    this.datos = this.activados === 1 ? this.solicitudesPendientes : this.solicitudesCompletas ;
    this.dataSource = new MatTableDataSource(this.datos);
    this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
  }
// tslint:disable-next-line:jsdoc-format
  /** Si modifican un dato en los modals **/
  update() {
    this.activados === 1 ? this.solicitudesPendientes = [] : this.solicitudesCompletas = [];
    this.getSolicitudes();
  }
  onOpen(idArchivo, index) {
    this.data = null;
    this.currentTab = index;
    this.ultimosMovimientosService.getByArchivo(idArchivo).subscribe(archivo => this.data = archivo[0]);
  }

  // tslint:disable-next-line:jsdoc-format
  /** Filtro **/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.datos = this.dataSource.filteredData;
    this.onPage( { pageIndex: 0, pageSize: this.pageSize, length: this.datos.length });
  }

  get getDatosArray() {
    return this.formulario.get('datos') as FormArray;
  }

  limpiar() {
    this.getDatosArray.clear();
    this.hijos = null;
  }
  async guardar() {
    const respuesta = await this.notificacionesService.pregunta('¿Quieres guardar la información?');
    if (respuesta.isConfirmed) {
      this.notificacionesService.carga();
      this.asistenciasService.post(this.formulario.get('datos').value).subscribe(() => {
        this.notificacionesService.exito().then(() => this.limpiar());
      });
    }
  }

}

