import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarJustificanteComponent } from './solicitar-justificante.component';

describe('SolicitarJustificanteComponent', () => {
  let component: SolicitarJustificanteComponent;
  let fixture: ComponentFixture<SolicitarJustificanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitarJustificanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarJustificanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
