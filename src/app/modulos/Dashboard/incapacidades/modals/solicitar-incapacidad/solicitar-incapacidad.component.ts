import { JustificacionesService } from '../../../../../@core/data/services/administracion-personal/justificaciones.service';
import { NotificacionesService } from '../../../../../@core/data/services/others/notificaciones.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {Component, Inject, OnInit} from '@angular/core';
import { CandidatoService } from 'src/app/@core/data/services/atraccion-talento/candidato/candidato.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { MotivoJustificacionService } from 'src/app/@core/data/services/administracion-personal/motivo-justificacion.service';
import {EmpleadosService} from '../../../../../@core/data/services/rrhh/empleados.service';
import {EmpresaServiceService} from '../../../../../@core/data/services/administracion-personal/catalogos/empresa-service.service';
import {TipoJustificacionService} from '../../../../../@core/data/services/administracion-personal/tipo-justificacion.service';

@Component({
  selector: 'app-solicitar-incapacidad',
  templateUrl: './solicitar-incapacidad.component.html',
  styleUrls: ['./solicitar-incapacidad.component.css']
})
export class SolicitarIncapacidadComponent implements OnInit {

  file = {
    name: null,
    data: null
  };
  motivos: any[] = [];
  empleados: any[] = [];
  respaldo: any[] = [];
  tipoJustificacion: any[] = [];
  max = new Date();
  form = new FormGroup({
    idEmpleado: new FormControl(null, Validators.required),
    idEmpleadoSupervisor: new FormControl(+ sessionStorage.getItem('Empleado')),
    // fechaSolicitud: new FormControl(null, Validators.required),
    fechaInicio: new FormControl(null, Validators.required),
    fechaFin: new FormControl(null, Validators.required),
    comentarios: new FormControl(null, Validators.required),
    // razonSocial: new FormControl(null, Validators.required),
    idMotivo: new FormControl(null),
    // folio: new FormControl(null, Validators.required),
    // idTipoJustificacion: new FormControl(null, Validators.required),
  });
  input = new FormControl();
  empresas = [];
  mostrarMotivo = false;
  constructor(
    private candidatoService: CandidatoService,
    private notificaciones: NotificacionesService,
    private justificaciones: JustificacionesService,
    private matRef: MatDialogRef<SolicitarIncapacidadComponent>,
    private motivoJustificacion: MotivoJustificacionService,
    private empleadosService: EmpleadosService,
    private empresasService: EmpresaServiceService,
    private tipoJustificacionService: TipoJustificacionService,
    @Inject(MAT_DIALOG_DATA) public data,
  ){ }

  ngOnInit(): void {
    const array = [5, 7, 8, 10];
    if (array.includes(this.data.idEstadoAsistencia)) {
      this.mostrarMotivo = true;
      this.form.controls.idMotivo.setValidators(Validators.required);
    }
    if ( this.data.fecha ){
      this.form.controls.fechaInicio.setValue(new Date(new Date().setDate(new Date(this.data.fecha).getDate() + 1)));
      this.form.controls.fechaInicio.disable();
      this.form.controls.fechaFin.setValue(new Date(new Date().setDate(new Date(this.data.fecha).getDate() + 1)));
      this.form.controls.fechaFin.disable();
    }
    this.empresasService.getEmpresa().subscribe( data => this.empresas = data);
    if (this.data.asistencia) {
      this.motivoJustificacion.getByEstadoAsistencia(this.data.idEstadoAsistencia).subscribe((motivos: any) => this.motivos = motivos);
      this.form.controls.idEmpleado.setValue(this.data.id);
      this.form.controls.idEmpleado.disable();
      this.form.controls.idEmpleado.updateValueAndValidity();
    } else {
      this.motivoJustificacion.getAll().subscribe((motivos: any) => this.motivos = motivos);
      this.tipoJustificacionService.getActivos().subscribe((data: any) => this.tipoJustificacion = data);
    }
    this.empleadosService.getPlazasHijo().subscribe((data: any) => this.empleados = this.respaldo = data);
  }

  async onSubmit() {
    const pregunta = await this.notificaciones.pregunta('¿Es correcta la información?');
    if (pregunta.isConfirmed) {
      this.matRef.close({...this.form.getRawValue(), archivo: this.file.data.item(0)});
    }
  }

  onKey(value) {
    const filter = value.toLowerCase();
    this.empleados = this.respaldo.filter(empleado => empleado.nombreEmpleado.toLowerCase().startsWith(filter));
  }

  subirDocumento(files: FileList) {
    if (files.length > 0) {
      this.file.data = files;
      this.file.name = files.item(0).name;
    }
  }
  cerrar(){
    this.matRef.close();
  }
}
