import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-asistencias',
  templateUrl: './asistencias.component.html',
  styleUrls: ['./asistencias.component.scss']
})
export class AsistenciasComponent implements OnInit {

  tabActual = 0;
  permisos = JSON.parse(window.localStorage.getItem('User')).ASIS;
  constructor() { }

  ngOnInit(): void {
  }

  tabClick(event) {
    this.tabActual =  event.index;
  }

}
