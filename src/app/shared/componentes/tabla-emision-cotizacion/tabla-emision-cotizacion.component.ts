import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-tabla-emision-cotizacion',
  templateUrl: './tabla-emision-cotizacion.component.html',
  styleUrls: ['./tabla-emision-cotizacion.component.scss']
})
export class TablaEmisionCotizacionComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'sumaAsegurada', 'deducible'];
  dataSource: Array<any>;

  @ViewChild('paginador', { read: MatPaginator }) paginador: MatPaginator;
  @ViewChild('sort', { read: MatSort }) sort: MatSort;

  @Input() dataVehicle: any;
  @Input() nombreAseguradora: string;
  constructor() { }

  ngOnInit(): void {
  }

}
