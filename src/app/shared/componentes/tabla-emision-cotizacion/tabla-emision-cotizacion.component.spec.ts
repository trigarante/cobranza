import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaEmisionCotizacionComponent } from './tabla-emision-cotizacion.component';

describe('TablaEmisionCotizacionComponent', () => {
  let component: TablaEmisionCotizacionComponent;
  let fixture: ComponentFixture<TablaEmisionCotizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaEmisionCotizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaEmisionCotizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
