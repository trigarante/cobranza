import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-modal-view-pdf-poliza-funnel',
  templateUrl: './modal-view-pdf-poliza-funnel.component.html',
  styleUrls: ['./modal-view-pdf-poliza-funnel.component.scss']
})
export class ModalViewPdfPolizaFunnel implements OnInit {
  completePDF = false;
  // Variable para validar que el archivo ha sido descargado y no vuelva a aparecer el Dialog
  solicitudID: number;
  arrayDownloadedPDF: Array<{ solicitudID: number, downloaded: boolean }> = [];
  documentsArray: any;

  constructor(@Inject(MAT_DIALOG_DATA) public dataExternalDialog: any) {
    this.arrayDownloadedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
    this.documentsArray = this.dataExternalDialog.documentos;
    // this.urlPDF = this.documentsArray[0];
  }

  ngOnInit(): void {
    console.log('Informaciión contenida en el dataExternalDialog => ', this.dataExternalDialog);
  }
  downloadFile() {
    /* const evt: HTMLElement = this.FileInput.nativeElement;
    evt.click(); */
    for (let index = 0; index < this.documentsArray.length; index++) {
      FileSaver.saveAs(this.documentsArray[index], `${this.dataExternalDialog.idsolicitud}_Poliza_Numero_${index + 1}`);
    }
    this.findItemDownloaded();

  }

  private findItemDownloaded() {
    this.arrayDownloadedPDF = JSON.parse(localStorage.getItem('arrayDownloadedPdf'));
    this.arrayDownloadedPDF.find(e => e.solicitudID === this.dataExternalDialog.idsolicitud).downloaded = true;
    localStorage.setItem('arrayDownloadedPdf', JSON.stringify(this.arrayDownloadedPDF));
  }

  loadComplete() {
    this.completePDF = true;
  }

}
