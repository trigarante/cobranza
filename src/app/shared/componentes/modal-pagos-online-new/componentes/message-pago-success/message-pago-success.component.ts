import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-message-pago-success',
  templateUrl: './message-pago-success.component.html',
  styleUrls: ['./message-pago-success.component.scss']
})
export class MessagePagoSuccessComponent implements OnInit {
  @ViewChild('reportContent') reportContent: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;
  numPoliza: any;

  constructor(@Inject(MAT_DIALOG_DATA) public dataExternalDialog: any
  ) {
    this.numPoliza = this.dataExternalDialog.numpoliza;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.takeCapture();
  }

  /* Este método se usa para realizar la captura de pantalla del modal */
  private takeCapture() {
    // Se crea un objeto de tipo nativeElement del elemento al cual se le tomará captura
    const content = this.reportContent.nativeElement;
    // Se hace uso de la función y se pasa como parametro el objeto del elemento
    html2canvas(content, {
      allowTaint: true,
      scrollY: -window.scrollY,
    }).then((canvas) => {
      // Una vez que se haya procesado el elemnto, retornará un <canvas>
      // Se obtendrá la url del canvas y se almacenará en la variable
      const imgData = canvas.toDataURL('image/png', 1.0);
      // Se crea un nuevo documento
      const doc = new jsPDF('p', 'px', 'a4');
      // Se obtienen las dimensiones de la página con las propiedades internas del documento
      // Esto para centrar la imagen dentro del documento
      const pageWidth = doc.internal.pageSize.width;
      const pageHeight = doc.internal.pageSize.height;
      const widthRatio = pageWidth / canvas.width;
      const heightRatio = pageHeight / canvas.height;
      const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;
      const canvasWidth = canvas.width * ratio;
      const canvasHeight = canvas.height * ratio;
      const marginX = (pageWidth - canvasWidth) / 2;
      const marginY = (pageHeight - canvasHeight) / 2;
      // Se agrega la imagen al documento con extensión png, y los parametros de las dimensiones para centrarla
      doc.addImage(imgData, 'PNG', marginX , marginY, canvasWidth , canvasHeight);
      // Se crea un blob de la imagen que será pdf
      const newBlob = new Blob([doc.output('blob')], {
        type: 'application/pdf',
      });
      // Se asigna a la variable el blobl que retorna el propio documento
      this.dataExternalDialog.blobImage = doc.output('blob');
      // Se crea una url temporal en Cache o arraybuffer del documento
      const blobUrl = URL.createObjectURL(newBlob);
      localStorage.setItem('blob', blobUrl);
      localStorage.setItem('idfilesPagoPDF', JSON.stringify(blobUrl));
      // Se abre la url temporal en el navegador
      window.open(blobUrl);
      // Se guarda el documento en el equipo del usuario.
      // doc.save('modal-prueba.pdf');
    }).catch((error) => {
      console.error('Error Canvas to PDF => ', error.message);
    });
  }

}
