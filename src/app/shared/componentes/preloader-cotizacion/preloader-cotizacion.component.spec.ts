import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreloaderCotizacionComponent } from './preloader-cotizacion.component';

describe('PreloaderCotizacionComponent', () => {
  let component: PreloaderCotizacionComponent;
  let fixture: ComponentFixture<PreloaderCotizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreloaderCotizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreloaderCotizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
