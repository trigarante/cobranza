import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaEmisionCotizacionComponent } from './componentes/tabla-emision-cotizacion/tabla-emision-cotizacion.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTabsModule} from '@angular/material/tabs';
import { PreloaderCotizacionComponent } from './componentes/preloader-cotizacion/preloader-cotizacion.component';
import { MessagePagoSuccessComponent } from './componentes/modal-pagos-online-new/componentes/message-pago-success/message-pago-success.component';
import { ModalViewPdfPolizaFunnel } from './componentes/modal-view-pdf-poliza-funnel/modal-view-pdf-poliza-funnel.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';




@NgModule({
  declarations: [
    TablaEmisionCotizacionComponent,
    PreloaderCotizacionComponent,
    MessagePagoSuccessComponent,
    ModalViewPdfPolizaFunnel
  ],
  exports: [
    PreloaderCotizacionComponent,
    TablaEmisionCotizacionComponent,
    ModalViewPdfPolizaFunnel,
    MessagePagoSuccessComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    PdfViewerModule,
    MatProgressBarModule,
    MatTabsModule,
  ]
})
export class SharedModule { }
