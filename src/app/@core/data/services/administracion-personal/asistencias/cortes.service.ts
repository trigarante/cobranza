import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CortesService {
  private url = environment.CORE_URL + '/administracion-personal/cortes';
  constructor(private http: HttpClient) { }
  traigoCorte(): Observable<any> {
    return this.http.get(this.url + '/ultimo');
  }
}
