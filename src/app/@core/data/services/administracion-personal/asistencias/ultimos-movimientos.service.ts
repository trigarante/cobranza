import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UltimoMovimiento} from '../../../interfaces/administracion-personal/ultimoMovimiento';
@Injectable({
  providedIn: 'root'
})
export class UltimosMovimientosService {
  private url = environment.CORE_URL + '/administracion-personal/ultimos-movimientos';
  private docs = environment.CORE_DOCUMENTOS + '/administracion-personal/ultimos-movimientos';
  constructor(private http: HttpClient) { }

  postMovimiento(ultimoMovimiento): Observable<UltimoMovimiento >{
    const formData = new FormData();
    formData.append('file', ultimoMovimiento.documento);
    formData.append('datos', JSON.stringify(ultimoMovimiento));
    return this.http.post<UltimoMovimiento >(this.url , formData);
  }
  unaEnProceso(idAsistencia): Observable<boolean>{
    return this.http.get<boolean >(`${this.url}/unoEnProceso/${idAsistencia}`);
  }
  getByArchivo(idArchivo: string): Observable<any> {
    const headers = new HttpHeaders().set('id', idArchivo);
    return this.http.get<any>(`${this.url}/obtenerArchivo`, { headers });
  }
}
