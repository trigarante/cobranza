import { TestBed } from '@angular/core/testing';

import { UltimosMovimientosService } from './ultimos-movimientos.service';

describe('UltimosMovimientosService', () => {
  let service: UltimosMovimientosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UltimosMovimientosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
