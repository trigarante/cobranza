import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AsistenciasService {

  private url = environment.CORE_URL + '/administracion-personal/asistencias';
  private url3 = environment.CORE_URL + '/administracion-personal/ultimos-movimientos';
  private doc = environment.CORE_DOCUMENTOS + '/administracion-personal/asistencias';
  private url2 = environment.CORE_URL + '/administracion-personal';

  constructor(private http: HttpClient) { }
  getAsistencias(fInicio, fFin, idDepartamento): Observable<any>{
    const headers = new HttpHeaders()
      .append('iddepartamento', idDepartamento.toString())
      .append('finicio', fInicio.toString())
      .append('ffin', fFin.toString())
      .append('idempleado', sessionStorage.getItem('Empleado'));
    return this.http.get<any>(this.url + `/asistencias/fechas`, {headers});
  }
  getUltimos(fInicio, fFin, idDepartamento, idArea): Observable<any>{
    const headers = new HttpHeaders()
      .append('iddepartamento', idDepartamento.toString())
      .append('idarea', idArea.toString())
      .append('finicio', fInicio.toString())
      .append('ffin', fFin.toString());
    return this.http.get<any>(this.url3 + `/ultimosMovimientos/fechas/1`, {headers});
  }
  test(): Observable<any>{
    return this.http.get<any>(this.url + `/tests` );
  }
  getEstadosAsistencia(): Observable<any>{
    return this.http.get<any>(this.url2 + `/estado-asistencia` );
  }
  updateEstadosAsistencia(idAsistencia, idEstadoAsistencia, idEmpleadoAEditar, idEmpleadoEditor): Observable<any>{
    return this.http.put<any>(this.url + `/actualizar`, {id: idAsistencia, idEstadoAsistencia, idEmpleadoAEditar, idEmpleadoEditor} );
  }
  post(data) {
    const formData = new FormData();
    if (data.justificante) {
      formData.append('file', data.justificante.archivo, data.justificante.archivo.name);
    }
    formData.append('datos', JSON.stringify(data));
    return this.http.post(this.doc, formData);
  }

  getRegistroAsistencia() {
    const headers = new HttpHeaders().set('id', sessionStorage.getItem('Empleado'));
    return this.http.get(this.url + '/registroAsistenciaByEmpleado', {headers});
  }
  getEmpleadosFaltantes(): Observable<number[]> {
    const headers = new HttpHeaders().set('id', sessionStorage.getItem('Empleado'));
    return this.http.get<number[]>(this.url + '/faltanHijos', {headers});
  }
  getRegistroAsistenciaXempleado(idEmpleado): Observable<any> {
    const headers = new HttpHeaders().set('id', idEmpleado.toString());
    return this.http.get<any>(this.url + '/registroAsistenciaByEmpleado', {headers});
  }
  getByEmpleadoSupervisor(idEstado: number): Observable<any> {
    const headers = new HttpHeaders().append('id', sessionStorage.getItem('Empleado')).set('idestado', idEstado.toString());
    return this.http.get(this.url3 + '/byEmpleadoSupervisor', {headers});
  }

  verAsistencias(inicio, fin): Observable<any> {
    const headers = new HttpHeaders().set('idempleado', sessionStorage.getItem('Empleado')).set('inicio', inicio.toString())
      .set('fin', fin.toString());
    return this.http.get(this.url + '/verAsistencias', {headers});
  }
}
