import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MotivoJustificacionService {

  private baseURL = environment.CORE_URL + '/administracion-personal/motivos-justificacion';
  constructor(
    private http: HttpClient
  ) { }

  getAll() {
    return this.http.get(this.baseURL);
  }
  getByEstadoAsistencia(id) {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.get(this.baseURL + '/byidEstadoAsistencia', {headers});
  }
}
