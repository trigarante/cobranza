import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PremioDesempenoService {
  private baseURL;
  constructor(
    private http: HttpClient
  ) {
    this.baseURL = environment.CORE_URL + '/administracion-personal/';
  }

  getIndicadores(empleado): Observable<any> {
    return this.http.get<any>(this.baseURL + `premio-desempeno/empleado/${empleado}`);
  }
}
