import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ImagenesGaceta} from '../../interfaces/rrhh/imagenesGaceta';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ImagenesGacetaService {
  private baseURL2 = environment.CORE_ESCUCHA + '/desarrollo-organizacional/gaceta';

  constructor(private http: HttpClient) {
  }

  getGacetaActual(): Observable<ImagenesGaceta[]> {
    return this.http.get<ImagenesGaceta[]>(`${this.baseURL2}/activa`);
  }

  getByIdGaceta(idGaceta: number): Observable<ImagenesGaceta[]> {
    return this.http.get<ImagenesGaceta[]>(`${this.baseURL2}/getByIdGaceta/${idGaceta}`);
  }
}

