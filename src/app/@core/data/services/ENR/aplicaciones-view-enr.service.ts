import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {AplicacionesViewCobranza} from '../../interfaces/cobranza/aplicacionesViewCobranza';

@Injectable({
  providedIn: 'root'
})
export class AplicacionesViewENRService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL + '/administracion-polizas';
  }
  getPolizasENR(fechaInicio, fechaFin, puesto): Observable<AplicacionesViewCobranza[]> {
    const headers = new HttpHeaders().append('idPuesto', puesto.toString());
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/ENR/' + sessionStorage.getItem('Empleado')
      + '/' + fechaInicio + '/' + fechaFin, {headers});
  }
  getPoliza(poliza): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/polizas/buscar-poliza/' + poliza);
  }
  getPolizaByNumero(numero): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/polizas/buscar-numero/' + numero);
  }
  getPolizaENRByNumero(numero): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/ENR/' + sessionStorage.getItem('Empleado') + '/polizas/buscar-numero/' + numero);
  }
  getByPolizaENR(poliza): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/ENR/' + sessionStorage.getItem('Empleado') + '/polizas/buscar-poliza/' + poliza);
  }
  getMotivosCancelacionENR(): Observable<any> {
    return this.http.get<any>(this.baseURL + '/ENR/MotivosCancelacion');
  }
  postCancelada(datos) {
    return this.http.post<any>(this.baseURL + '/ENR/Cancelacion', datos);
  }
  getPolizasCanceladasENR(fechaInicio, fechaFin) {
    return this.http.get<any>(this.baseURL + '/ENR/PolizasCanceladas' + '/' + fechaInicio + '/' + fechaFin);
  }
}
