import { TestBed } from '@angular/core/testing';

import { SolicitudesCarruselService } from './solicitudes-carrusel.service';

describe('SolicitudesCarruselService', () => {
  let service: SolicitudesCarruselService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SolicitudesCarruselService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
