import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {SolicitudesPago} from '../../interfaces/venta-nueva/solicitudesPago';

@Injectable({
  providedIn: 'root',
})
export class SolicitudesPagoIvrService {
  baseURL: string;
  appUrl: string;
  private authUrl;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL + '/solicitudes-ivr';
  }
  getAll(): Observable<any>{
    return this.http.get<any>(this.baseURL);
  }

}
