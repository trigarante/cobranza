import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from "rxjs";
import { AutorizacionPago } from '../../interfaces/subsecuentes/autorizacion-pago';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionPagoService {
  baseURL: string;
  baseURL2: string;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL + '/autorizacion-pago';
    this.baseURL2 = environment.CORE_URL;
   }
   documentoVerificadoSub(idAutorizacionPago: number, idDocumentoVerificado: number, estado: number): Observable<String> {
    return this.http.put<string>(this.baseURL2 + '/autorizacion-pago/datos', {
      idAutorizacionPago, idDocumentoVerificado, estado});
  }

  get(): Observable<AutorizacionPago[]> {
    return this.http.get<AutorizacionPago[]>(this.baseURL + '/' + sessionStorage.getItem('Empleado'));
  }

  getTabla(estadoVerificacion, fechaInicio, fechaFin): Observable<any[]> {
    return this.http.get<any[]>
    (this.baseURL + '/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado')+ `/${fechaInicio}/${fechaFin}`);
  }
  getTablaSubsecuentes(estadoVerificacion, fechaInicio, fechaFin): Observable<any[]> {
    return this.http.get<any[]>
    (this.baseURL + '/tabla/subsecuentes/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado')+ `/${fechaInicio}/${fechaFin}`);
  }

  postVerificacionPago(verificacionPago): Observable<String> {
    return this.http.post<string>(this.baseURL2 + 'v1/verificacion-registro/pago', verificacionPago);
  }

  // getByIdFlujoPoliza(): Observable<AutorizacionPago[]> {
  //   return this.http.get<AutorizacionPago[]>(this.baseURL + '/' + sessionStorage.getItem('Empleado'));
  // }
  //
  // getByIdFlujoPolizaSubPeru(): Observable<AutorizacionPago[]> {
  //   return this.http.get<AutorizacionPago[]>(this.baseURL + '/' + sessionStorage.getItem('Empleado'));
  // }

  getById(id: number): Observable<AutorizacionPago> {
    return this.http.get<AutorizacionPago>(this.baseURL + '/by-id/' + id);
  }

  put(autorizacionPago: AutorizacionPago): Observable<any> {
    return this.http.put(this.baseURL, autorizacionPago);
  }

  concluirAutorizacion(id: number): Observable<any> {
    return this.http.put(this.baseURL + '/concluir-autorizacion/' + id, null, {responseType: 'text' as 'json'});
  }

  // Pagos

  getPagoById(id: number): Observable<any> {
    return this.http.get<any>(this.baseURL2 + '/autorizacion-pago/get-by-id/' + id);
  }
}
