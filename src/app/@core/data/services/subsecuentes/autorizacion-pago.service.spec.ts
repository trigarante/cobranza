import { TestBed } from '@angular/core/testing';

import { AutorizacionPagoService } from './autorizacion-pago.service';

describe('AutorizacionPagoService', () => {
  let service: AutorizacionPagoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutorizacionPagoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
