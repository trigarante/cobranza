import {Injectable} from '@angular/core';
import {ProductoNameCorto, ProductoSocios} from '../../interfaces/comerciales/producto-socios';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoSociosService  {
  private baseURL = environment.CORE_URL + '/producto-socio';

  constructor(private http: HttpClient) {
  }

  // NEW
  getByIdSubRamo(id: number): Observable<ProductoSocios[]> {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.get<ProductoSocios[]>(this.baseURL + '/getByIdSubRamo/', {headers});
  }

  getProductoSocioByIdSubRamoByNombre(idSubramo: number, nombre: string): Observable<ProductoNameCorto> {
    return this.http.get<ProductoNameCorto>(this.baseURL + '/id-subramoNombre/' + idSubramo + '/'
      + nombre);
  }
  // FIN NEW

  get(): Observable<ProductoSocios[]> {
    return this.http.get<ProductoSocios[]>(this.baseURL + '/productos-socio');
  }

  getProductoSocioByIdSubRamo(idSubramo: number): Observable<ProductoSocios[]> {
    return this.http.get<ProductoSocios[]>(this.baseURL + '/productos-socio/id-subramo/' + idSubramo);
  }

  getProductoSociosById(idProductoSocios): Observable<ProductoSocios> {
    return this.http.get<ProductoSocios>(this.baseURL + '/productos-socio/' + idProductoSocios);
  }
  post(productoSocios: ProductoSocios): Observable<ProductoSocios> {
    return this.http.post<ProductoSocios>(this.baseURL + '/productos-socio', productoSocios);
  }

  put(idProductoSocios, productoSocios: ProductoSocios): Observable<ProductoSocios> {
    return this.http.put<ProductoSocios>(this.baseURL + '/productos-socio/' + idProductoSocios + '/'
      + sessionStorage.getItem('Empleado') , productoSocios);
  }
}
