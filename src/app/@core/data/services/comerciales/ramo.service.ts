import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Ramo} from '../../interfaces/comerciales/ramo';

@Injectable({
  providedIn: 'root',
})
export class RamoService{

  private baseURL = environment.CORE_URL + '/ramo';

  constructor(private http: HttpClient) {
  }

  //  NEW
  getByIdSocio(idSocio: number): Observable<Ramo[]> {
    const headers = new HttpHeaders().append('id', idSocio.toString());
    return this.http.get<Ramo[]>(this.baseURL + '/getByIdSocio', {headers});
  }
  // FIN

  get(): Observable<Ramo[]> {
    return this.http.get<Ramo[]>(this.baseURL + '/ramo');
  }
  post(ramo: Ramo): Observable<Ramo> {
    return this.http.post<Ramo>(this.baseURL + '/ramo', ramo);
  }
  put(idRamo, ramo: Ramo): Observable<Ramo> {
    return this.http.put<Ramo>(this.baseURL + '/ramo/' + idRamo + '/' + sessionStorage.getItem('Empleado'), ramo);
  }
}
