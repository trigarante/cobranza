import {getTestBed, TestBed} from '@angular/core/testing';
import { ProductoSociosService } from './producto-socios.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';


fdescribe('ProductoSociosService', () => {
  let injector: TestBed;
  let service: ProductoSociosService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idSubRamo: 1,
    prioridad: 1,
    prioridades: 'ALTA',
    nombre: 'testing',
    activo: 0,
    idRamo: 30,
    idTipoSubRamo: 1,
    tipoRamo: 'SEGUROS',
    descripcion: 'TEST FINALL',
    nombreComercial: 'KK',
    alias: 'KK',
    idEstadoSocio: 2,
    estado: 'INACTIVO',
    idEstado: 2,
    tipoSubRamo: 'AUTO',
    idTipoProducto: 1,
  },
    {
      id: 'productossocio',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ProductoSociosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ProductoSociosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/productos-socio');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/productos-socio', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/productos-socio');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/productos-socio', '/1/74');
      });
    });
  }
});
