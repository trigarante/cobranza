import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {AplicacionesViewCobranza} from '../../interfaces/cobranza/aplicacionesViewCobranza';

@Injectable({
  providedIn: 'root'
})
export class AplicacionesViewCobranzaService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL + '/administracion-polizas';
  }
  getPolizasCobranza(fechaInicio, fechaFin, puesto): Observable<AplicacionesViewCobranza[]> {
    const headers = new HttpHeaders().append('idPuesto', puesto.toString());
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado')
      + '/' + fechaInicio + '/' + fechaFin, {headers});
  }
  getSinPagar(fechaInicio, fechaFin): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/sin-pagar' + '/' + fechaInicio + '/' + fechaFin);
  }
  getSinAplicar(fechaInicio, fechaFin): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/sin-aplicar' + '/' + fechaInicio + '/' + fechaFin);
  }
  getPolizasTotales(fechaInicio, fechaFin): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/polizas-totales' + '/' + fechaInicio + '/' + fechaFin);
  }
  getPoliza(poliza): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/polizas/buscar-poliza/' + poliza);
  }
  getPolizaByNumero(numero): Observable<AplicacionesViewCobranza[]> {
    return this.http.get<AplicacionesViewCobranza[]>(this.baseURL + '/cobranza/' + sessionStorage.getItem('Empleado') + '/polizas/buscar-numero/' + numero);
  }
  getByPolizas(poliza, value): Observable<any[]> {
    const headers = new HttpHeaders().append('poliza', poliza.toString()).append('valor', value.toString());
    return this.http.get<any[]>(this.baseURL  + '/subsecuentes/buscar-poliza', {headers});
  }
  getBySerie(numero, value): Observable<any[]> {
    const headers = new HttpHeaders().append('numero', numero.toString()).append('valor', value.toString());
    return this.http.get<any[]>(this.baseURL + '/subsecuentes/buscar-serie', {headers});
  }
  getPolizasSubsecuentes(fechaInicio, fechaFin): Observable<any[]> {
    return this.http.get<any[]>(
      this.baseURL  + '/subsecuentes/' + sessionStorage.getItem('Empleado') + '/' + fechaInicio + '/' + fechaFin);
  }
  getPolizasInternas(fechaInicio, fechaFin): Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + '/subsecuentes/' + sessionStorage.getItem('Empleado') + '/internas' + '/' + fechaInicio + '/' + fechaFin);
  }
  getPolizasTotalesSubs(fechaInicio, fechaFin): Observable<AplicacionesViewCobranza[]> {
  return this.http.get<AplicacionesViewCobranza[]>(
    this.baseURL + '/subsecuentes/' + sessionStorage.getItem('Empleado') + '/polizas-totales' + '/' + fechaInicio + '/' + fechaFin);
  }
  getSinAplicarSubs(fechaInicio, fechaFin): Observable<AplicacionesViewCobranza[]> {
  return this.http.get<AplicacionesViewCobranza[]>(
    this.baseURL + '/subsecuentes/' + sessionStorage.getItem('Empleado') + '/sin-aplicar' + '/' + fechaInicio + '/' + fechaFin);
  }
  getSinPagarSubs(fechaInicio, fechaFin): Observable<AplicacionesViewCobranza[]> {
  return this.http.get<AplicacionesViewCobranza[]>(
    this.baseURL + '/subsecuentes/' + sessionStorage.getItem('Empleado') + '/sin-pagar' + '/' + fechaInicio + '/' + fechaFin);
  }
}
