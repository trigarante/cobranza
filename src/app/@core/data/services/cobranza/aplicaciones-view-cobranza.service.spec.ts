import { TestBed } from '@angular/core/testing';

import { AplicacionesViewCobranzaService } from './aplicaciones-view-cobranza.service';

describe('AplicacionesViewCobranzaService', () => {
  let service: AplicacionesViewCobranzaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AplicacionesViewCobranzaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
