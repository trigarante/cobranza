import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogueoAppService {
  private urlApps;

  constructor(
    private http: HttpClient,
  ) {
    this.urlApps = environment.CORE_APP_V1;
  }

  getLogueosApps(): Observable<any[]> {
    return this.http.get<any[]>(this.urlApps + '/logueos-apps');
  }

  getDescargasPoliza(): Observable<any[]> {
    return this.http.get<any[]>(this.urlApps + '/descargas-polizas');
  }

  getSeguimientoMail(): Observable<any[]> {
    return this.http.get<any[]>(this.urlApps + '/seguimiento-mail');
  }

  getFullReporte(): Observable<any[]> {
    return this.http.get<any[]>(this.urlApps + '/reporte');
  }

  getReporteById(idEmpresa): Observable<any[]> {
    return this.http.get<any[]>(this.urlApps + '/reporte/' + idEmpresa);
  }

  getEmpresas(): Observable<any[]> {
    return this.http.get<any[]>(this.urlApps + '/empresas');
  }
  enviarEmail(idRegistro): Observable<any> {
    const params = new HttpParams().append("idRegistro", idRegistro.toString())
    return this.http.get<any[]>(this.urlApps + 'contact/reminder', {params});
  }
}
