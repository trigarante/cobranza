import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresAnexos} from '../../../interfaces/venta-nueva/autorizacion/errores-anexos';

@Injectable({
  providedIn: 'root'
})
export class ErroresAnexosService  {
  private baseURL;
  private CORE_URL;
  private CORE_VN3;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/errores-anexos/';
    this.CORE_URL = environment.CORE_URL + '/errores-anexos/';
    this.CORE_VN3 = environment.CORE_URL + '/errores-anexos-pago/';
  }
  // NEW
  getErroresAnexosAPago(idAutorizacionPago: number): Observable<boolean> {
    return this.http.get<any>(this.CORE_URL + 'erroresAnexosAPago/' + idAutorizacionPago);
  }
  postPago(error): Observable<string> {
    return this.http.post<string>(this.CORE_URL + 'pago', error);
  }
  getByIdAutorizacionRegitro(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAnexos> {
    return this.http.get<ErroresAnexos>(
      this.CORE_URL + idAutorizacionRegistro + '/' + idTipoDocumento);
  }
  post(error): Observable<string> {
    return this.http.post<string>(this.CORE_URL, error);
  }

  putEstadoCorreccion(idErrorAnexo: number): Observable<any> {
    return this.http.put(this.baseURL + 'update-estado/' + idErrorAnexo, null);
  }
  // FIN

  get(): Observable<ErroresAnexos[]> {
    return this.http.get<ErroresAnexos[]>(this.baseURL);
  }

  put(idError: number, error: ErroresAnexos): Observable<any> {
    return this.http.put(this.baseURL, error);
  }
   // Pagos

   getByIdAutorizacionPago(idAutorizacionPago: number): Observable<any> {
    return this.http.get<any>(this.CORE_VN3 + idAutorizacionPago);
  }

}
