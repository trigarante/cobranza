import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CampoCorregir} from '../../../interfaces/verificacion/campo-corregir';

@Injectable({
  providedIn: 'root',
})
export class CampoCorregirAutorizacionService  {
  private CORE_URL;


  constructor(private http: HttpClient) {
    this.CORE_URL = environment.CORE_URL + '/campo-corregir';

  }

  getCamposDisponibles(idTabla: number): Observable<CampoCorregir[]> {
    return this.http.get<CampoCorregir[]>(this.CORE_URL + '/' + idTabla);
  }

  getCamposDisponiblesForMoral(idTabla: number): Observable<CampoCorregir[]> {
    return this.http.get<CampoCorregir[]>(this.CORE_URL + '/forMoral/' + idTabla);
  }
}

