import { TestBed } from '@angular/core/testing';

import { TipoDocumentoAutorizacionService } from './tipo-documento-autorizacion.service';

describe('TipoDocumentoAutorizacionService', () => {
  let service: TipoDocumentoAutorizacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoDocumentoAutorizacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
