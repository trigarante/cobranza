import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {  AutorizacionRegistro,  AuthRegistro} from '../../../interfaces/venta-nueva/autorizacion/autorizacion-registro';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionRegistroService  {

  private baseURL;
  private baseURL2;
  private CORE_URL;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL + '/autorizacion-registro';

  }
  // NEW
  getById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.CORE_URL + '/get-by-id/' + id);
  }
  getByIdRegistro(id: number): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/get-by-idRegistro/' + id);
  }
  getTabla(estadoVerificacion, fechaInicio, fechaFin): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>
    (this.CORE_URL + '/tabla/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado') + `/${fechaInicio}/${fechaFin}`);
  }
  getTablaSubsecuentes(estadoVerificacion, fechaInicio, fechaFin): Observable<any[]> {
    return this.http.get<any[]>
    (this.CORE_URL + '/tabla/subsecuentes/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado') + `/${fechaInicio}/${fechaFin}`);
  }
  getTablaENR(estadoVerificacion, fechaInicio, fechaFin): Observable<any[]> {
    return this.http.get<any[]>
    (this.CORE_URL + '/tabla/ENR/' + estadoVerificacion + '/' + sessionStorage.getItem('Empleado') + `/${fechaInicio}/${fechaFin}`);
  }
  getVnById(id: number): Observable<AutorizacionRegistro> {
    return this.http.get<AutorizacionRegistro>(this.CORE_URL + '/get-by-id/' + id);
  }

  put(id: number, autorizacion): Observable<AutorizacionRegistro> {
    return this.http.put<AutorizacionRegistro>(this.CORE_URL + '/' + id, autorizacion);
  }
  documentoVerificadoVn(idAutorizacionRegistro: number, idDocumentoVerificado: number, estado: number): Observable<string> {
    return this.http.put<string>(this.CORE_URL + '/datos/', {
      idAutorizacionRegistro, idDocumentoVerificado, estado});
  }

  putInOne(id: number, autorizacion: AutorizacionRegistro, file: FileList, idError): Observable<AutorizacionRegistro> {
    return this.http.put<AutorizacionRegistro>(this.CORE_URL + '/' + id, autorizacion);
  }

  documentoVerificado(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<string> {
    const headers = new HttpHeaders().append('idautorizacionregistro', idAutorizacionRegistro.toString()).
    append('iddocumentoverificado', documentoVerificado.toString()).
    append('estado', estado.toString());
    return this.http.put<string>(this.CORE_URL + '/datos/', null, {headers});
  }


  // FIN

  get(): Observable<AutorizacionRegistro[]> {
    return this.http.get<AutorizacionRegistro[]>(this.baseURL2 + 'v1/autorizacion-registro/' + sessionStorage.getItem('Empleado'));
  }

  post(idRegistro: number): Observable<AutorizacionRegistro> {
    return this.http.post<AutorizacionRegistro>(this.baseURL + 'v1/autorizacion-registro/' + idRegistro + '/'
      + sessionStorage.getItem('Empleado'), null);
  }

}

