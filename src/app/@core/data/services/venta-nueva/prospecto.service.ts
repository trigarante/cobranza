import {Injectable} from '@angular/core';
import {Prospecto, ProspectoO} from '../../interfaces/venta-nueva/prospecto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProspectoService  {

  private baseURL;
  private nodeURL;
  private baseURL2;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.nodeURL = environment.CORE_URL + '/prospecto';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  // NEW
  validarCorreo(correo: string): Observable<Prospecto> {
    const headers = new HttpHeaders().append('correo', correo);
    return this.http.get<Prospecto>(this.nodeURL + '/getByCorreo', {headers} );
  }

  post(prospecto: Prospecto): Observable<Prospecto> {
    return this.http.post<Prospecto>(this.nodeURL, prospecto);
  }

  put(idProspecto, prospecto: Prospecto): Observable<Prospecto> {
    const headers = new HttpHeaders().append('id', idProspecto.toString());
    return this.http.put<Prospecto>(this.nodeURL, prospecto, {headers});
  }
  getById(id): Observable<ProspectoO> {
    return this.http.get<ProspectoO>( this.nodeURL + '/getById/' + id);
  }
  // FIN NEW

  get(): Observable<Prospecto[]> {
    return this.http.get<Prospecto[]>(this.baseURL2 + 'v1/prospectovn');
  }

  validaCorreo(correo): Observable<Prospecto> {
    return this.http.get<Prospecto>(this.baseURL2 + 'v1/prospectovn/correo/' + correo);
  }

  getProspectoByPhone(telefono: string): Observable<Prospecto> {
    return this.http.get<Prospecto>(`${this.nodeURL}/venta-nueva/prospecto/${telefono}`);
  }

}
