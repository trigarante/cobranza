import { TestBed } from '@angular/core/testing';

import { BancoVnService } from './banco-vn.service';

describe('BancoVnService', () => {
  let service: BancoVnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BancoVnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
