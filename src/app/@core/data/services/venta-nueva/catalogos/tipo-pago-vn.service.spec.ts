import { TestBed } from '@angular/core/testing';

import { TipoPagoVnService } from './tipo-pago-vn.service';

describe('TipoPagoVnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoPagoVnService = TestBed.get(TipoPagoVnService);
    expect(service).toBeTruthy();
  });
});
