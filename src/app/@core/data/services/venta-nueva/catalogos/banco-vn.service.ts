import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {BancoExterno, BancoVn} from '../../../interfaces/venta-nueva/catalogos/banco-vn';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BancoVnService  {

  private baseURL;
  private CORE_URL;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.CORE_URL = environment.CORE_URL;
  }

  // NEW
  get(): Observable<BancoVn[]> {
    return this.http.get<BancoVn[]>(this.CORE_URL + '/bancos');
  }
  getexterno(): Observable<BancoExterno[]> {
    return this.http.get<BancoExterno[]>( 'https://ws-qualitas.com/catalogos/bancos');
  }
  // FIN

  post(banco): Observable<BancoVn> {
    return this.http.post<BancoVn>(this.baseURL + 'v1/bancos', banco);
  }

  put(id, banco): Observable<BancoVn> {
    return this.http.put<BancoVn>(this.baseURL + 'v1/bancos' + id, banco);
  }
}
