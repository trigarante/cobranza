import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {FormaPago, FormaPagoOnline} from '../../../interfaces/venta-nueva/catalogos/forma-pago';


@Injectable({
  providedIn: 'root',
})
export class FormaPagoService  {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.CORE_URL;
  }
  // NEW
  get(): Observable<FormaPago[]> {
    return this.http.get<FormaPago[]>(this.baseURL + '/forma-pago');
  }
  getOnline(): Observable<FormaPagoOnline[]> {
    return this.http.get<FormaPagoOnline[]>(this.baseURL + '/forma-pago');
  }

  getActivos(): Observable<FormaPago[]> {
    return this.http.get<FormaPago[]>(this.baseURL + '/forma-pago/activos');
  }
  post(formaPago): Observable<FormaPago> {
    return this.http.post<FormaPago>(this.baseURL + '/forma-pago', formaPago );
  }
  put(idformaPago, formaPago): Observable<FormaPago> {
    return this.http.put<FormaPago>(this.baseURL + '/forma-pago' + idformaPago, formaPago);
  }
  // FIN

}
