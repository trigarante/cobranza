import { TestBed } from '@angular/core/testing';

import { TarjetasVnService } from './tarjetas-vn.service';

describe('TarjetasVnService', () => {
  let service: TarjetasVnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TarjetasVnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
