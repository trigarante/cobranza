import { TestBed } from '@angular/core/testing';

import { EstadoInspeccionService } from './estado-inspeccion.service';

describe('EstadoInspeccionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoInspeccionService = TestBed.get(EstadoInspeccionService);
    expect(service).toBeTruthy();
  });
});
