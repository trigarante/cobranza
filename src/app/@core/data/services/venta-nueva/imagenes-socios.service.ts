import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ImagenesSocios} from '../../interfaces/venta-nueva/imagenes-socios';

@Injectable({
  providedIn: 'root',
})
export class ImagenesSociosService {
  url: string;
  private baseURL2;

  constructor(private http: HttpClient) {
    this.url = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/imagenes-socios';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura +  'v1/imagenes-socios';

  }

  getAllImages(): Observable<ImagenesSocios[]> {
    return this.http.get<ImagenesSocios[]>(this.baseURL2);
  }

  getByIdImage(idSocio): Observable<ImagenesSocios[]> {
    return this.http.get<ImagenesSocios[]>(this.baseURL2 + '/' + idSocio);
  }
}
