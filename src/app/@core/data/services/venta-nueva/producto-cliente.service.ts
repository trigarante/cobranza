import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {ProductoCliente} from '../../interfaces/venta-nueva/producto-cliente';

@Injectable({
  providedIn: 'root',
})
export class ProductoClienteService {

  private baseURL = environment.CORE_URL + '/producto-cliente';
  private DOCUMENTOS = environment.CORE_DOCUMENTOS + '/producto-cliente';
  public aseguradora = new BehaviorSubject<number>(0);
  public idCliente = new BehaviorSubject<number>(0);
  public idProductoCliente = new BehaviorSubject<number>(0);
  public idRecibo = new BehaviorSubject<number>(0);


  constructor(private http: HttpClient) {
  }
  // NEW
  postInOne(productoCliente: ProductoCliente,  files: FileList): Observable<any> {
    const formData = new FormData();
    if (files) {
      formData.append('file', files.item ? files.item(0) : files[0], 'poliza');
    }
    formData.append('producto', JSON.stringify(productoCliente));
    return this.http.post<any>(this.DOCUMENTOS + '/All', formData);
  }

  putInOne(idProductoCliente, productoCliente: ProductoCliente, files: FileList): Observable<any> {
    const formData = new FormData();
    if (files) {
      formData.append('file', !files.item ? files[0] : files.item(0), 'poliza');
    }
    formData.append('producto', JSON.stringify(productoCliente));
    formData.append('idP', idProductoCliente.toString());
    return this.http.put<any>(this.baseURL + '/All', formData);
  }

  putInOneOnline(idProductoCliente, productoCliente: ProductoCliente, files: FileList): Observable<any> {
    const formData = new FormData();
    if (files) {
      formData.append('file', !files.item ? files[0] : files.item(0), 'poliza');
    }
    formData.append('producto', JSON.stringify(productoCliente));
    formData.append('idP', idProductoCliente.toString());
    return this.http.put<any>(this.baseURL + '/AllOnline', formData);
  }
  getProductoClienteById(idProductoCliente: number): Observable<any> {
    const headers = new HttpHeaders().append('idproductocliente', idProductoCliente.toString());
    return this.http.get<ProductoCliente>(this.baseURL + '/getById', {headers});
  }

  put(idProductoCliente, productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.put<ProductoCliente>(this.baseURL, {idProductoCliente, productoCliente});
  }
  // FIN


  // NO SE USAN
  get(): Observable<ProductoCliente[]> {
    return this.http.get<ProductoCliente[]>(this.baseURL);
  }

  post(productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.post<ProductoCliente>(this.baseURL, productoCliente);
  }
}
