import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {VerificacionRegistro} from '../../interfaces/verificacion/verificacion-registro';
import {Observable} from 'rxjs';
import {AutorizacionRegistro} from '../../interfaces/venta-nueva/autorizacion/autorizacion-registro';

@Injectable({
  providedIn: 'root',
})
export class VerificacionesPendientesService {
  private CORE_URL;


  constructor(private http: HttpClient) {
    this.CORE_URL = environment.CORE_URL + '/verificacion-registro/pendientes';
  }

  // NEW
  get(estadoVerificacion, fechaInicio, fechaFin): Observable<AutorizacionRegistro[]> {
    return this.http.get<VerificacionRegistro[]>(this.CORE_URL + '/' + sessionStorage.getItem('Empleado') +
      `/${fechaInicio}/${fechaFin}`);
}
// FIN
}

