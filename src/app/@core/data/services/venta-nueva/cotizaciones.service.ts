import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Cotizaciones} from '../../interfaces/venta-nueva/cotizaciones';

@Injectable({
  providedIn: 'root'
})
export class CotizacionesService  {

  returnEmisionParam: any;

  private baseURL;
  private baseURL2;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES + 'v1/cotizaciones';
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura + 'v1/cotizaciones';

  }

  get(): Observable<Cotizaciones[]> {
    return this.http.get<Cotizaciones[]>(this.baseURL2);
  }

  getByIdProducto(idProducto: number): Observable<Cotizaciones> {
    return this.http.get<Cotizaciones>(this.baseURL2 + '/idProducto/' + idProducto );
  }

  post(cotizacion: Cotizaciones): Observable<Cotizaciones> {
    return this.http.post<Cotizaciones>(this.baseURL, cotizacion);
  }

  getCotizacionById(id): Observable<Cotizaciones> {
    return this.http.get<Cotizaciones>(this.baseURL2 + '/' + id);
  }

  put(idCotizacion, cotizacion: Cotizaciones): Observable<Cotizaciones> {
    return this.http.put<Cotizaciones>(this.baseURL + '/' + idCotizacion + '/'
      + sessionStorage.getItem('Empleado') , cotizacion);
  }

  createEmisionParam(aseguradora) {
    this.returnEmisionParam = aseguradora;
  }
  returnEmision(): Observable<string> {
    return this.returnEmisionParam;
  }
}

