import { TestBed } from '@angular/core/testing';

import { ClienteVnService } from './cliente-vn.service';

describe('ClienteVnService', () => {
  let service: ClienteVnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClienteVnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
