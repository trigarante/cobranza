import { TestBed } from '@angular/core/testing';

import { MiEstadoCuentaService } from './mi-estado-cuenta.service';

describe('MiEstadoCuentaService', () => {
  let service: MiEstadoCuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MiEstadoCuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
