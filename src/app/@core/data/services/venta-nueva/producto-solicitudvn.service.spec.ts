import { TestBed } from '@angular/core/testing';

import { ProductoSolicitudvnService } from './producto-solicitudvn.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';

describe('ProductoSolicitudvnService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ProductoSolicitudvnService, HttpClient],
  }));

  it('should be created', () => {
    const service: ProductoSolicitudvnService = TestBed.get(ProductoSolicitudvnService);
    expect(service).toBeTruthy();
  });
});
