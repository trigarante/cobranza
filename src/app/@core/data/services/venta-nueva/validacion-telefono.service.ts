import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ValidacionTelefono} from '../../interfaces/venta-nueva/validacion-telefono';

@Injectable({
  providedIn: 'root',
})
export class ValidacionTelefonoService {
  private baseURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
  }
  post(telefono: ValidacionTelefono): Observable<ValidacionTelefono> {
    return this.http.post<ValidacionTelefono>(this.baseURL + 'v1/pnn/numero/' + telefono, '');
  }
}
