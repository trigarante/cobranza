import { Injectable } from '@angular/core';
import {ProductoSolicitudvn, ProductoSolicitudvnCorto} from '../../interfaces/venta-nueva/producto-solicitud';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoSolicitudvnService {
  private baseURL;
  private baseURL2;

  constructor(private http: HttpClient) {
    // super();
    this.baseURL = environment.CORE_URL;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;

  }

  // NEW
  getProductoSolicitud(id): Observable<ProductoSolicitudvn> {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.get<ProductoSolicitudvn>(this.baseURL + '/producto-solicitud/getById', {headers});
  }

  postAll(datos): Observable<any> {
    const headers = new HttpHeaders().append('idempleado', sessionStorage.getItem('Empleado'))
      .append('iddepartamento', sessionStorage.getItem('idDepartamento'));
    return this.http.post<any>(this.baseURL + '/producto-solicitud/All', datos, {headers});
  }
  post(nombre: ProductoSolicitudvn): Observable<ProductoSolicitudvn> {
    // quite el v1
    return this.http.post<ProductoSolicitudvn>(this.baseURL + '/producto-solicitud', nombre);
  }

  postProductoOnline(datosGenerales): Observable<any> {
    // quite el v1
    const headers = new HttpHeaders().append('idempleado', sessionStorage.getItem('Empleado'))
      .append('iddepartamento', sessionStorage.getItem('idDepartamento'));
    return this.http.post<ProductoSolicitudvn>(this.baseURL + '/producto-solicitud/solicitudOnline', datosGenerales, {headers});
  }
  // Online
  getProductoSolicitudOnline(id): Observable<ProductoSolicitudvnCorto> {
    const headers = new HttpHeaders().append('id', id.toString());
    return this.http.get<ProductoSolicitudvnCorto>(this.baseURL + '/producto-solicitud/getByIdProducto', {headers});
  }
  putOnline(idProducto, productoSolicitudvn: ProductoSolicitudvnCorto): Observable<ProductoSolicitudvnCorto> {
    return this.http.put<ProductoSolicitudvnCorto>(this.baseURL + '/producto-solicitud/producto/' + idProducto , productoSolicitudvn);
  }
  // FIN NEW

  get(): Observable<ProductoSolicitudvn[]> {
    return this.http.get<ProductoSolicitudvn[]>(this.baseURL2 + 'v1/producto-solicitudvn');
  }

  put(id, nombre: ProductoSolicitudvn): Observable<ProductoSolicitudvn> {
    return this.http.put<ProductoSolicitudvn>(this.baseURL + 'v1/producto-solicitudvn/' + id + '/'
      + sessionStorage.getItem('Empleado'), nombre);
  }

  getByIdProspecto(idProspecto): Observable<ProductoSolicitudvn []> {
    return this.http.get<ProductoSolicitudvn []>(this.baseURL2 + 'v1/producto-solicitudvn/id-prospecto/' + idProspecto);
  }
}
