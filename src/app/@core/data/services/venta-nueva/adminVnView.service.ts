import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {Solicitudes, SolicitudesVN} from '../../interfaces/venta-nueva/solicitudes';
import {AdminVnView} from '../../interfaces/venta-nueva/adminVnView';


@Injectable({
  providedIn: 'root'
})
export class AdminVnViewService {
  private baseURL;
  private nodeURL;
  private CORE_URL;
  public idCliente = new BehaviorSubject<number>(0);
  public idRegistroPoliza = new BehaviorSubject<number>(0);
  public  urlPostventa = new BehaviorSubject<number>(0);
  private socketURL;
  private baseURL2;


  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL;
  }

  // NEW
  getAllRegistroByIdUsuario(fechaInicio, fechaFin): Observable<AdminVnView[]> {
    return this.http.get<AdminVnView[]>(this.CORE_URL + '/admin-polizas/' + sessionStorage.getItem('Empleado') + `/${fechaInicio}/${fechaFin}`);
  }

  // FIN
  get(): Observable<AdminVnView[]> {
    return this.http.get<AdminVnView[]>(this.baseURL2 + 'v1/admin-vn/getPolizas/' + sessionStorage.getItem('Empleado'));
  }

  getRegistroById(idRegistro): Observable<AdminVnView> {
    return this.http.get<AdminVnView>(this.baseURL2 + 'v1/admin-vn/' + idRegistro);
  }
  // Sockets
  postPolizaSocket(json): Observable<AdminVnView> {
    return this.http.post<AdminVnView>(this.socketURL + 'vnAdministradorPolizas/saveVnPoliza', json);
  }

  updateClienteSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateCliente', json);
  }

  updateNumeroSerieSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateNumeroSerie', json);
  }

  updateArchivoSubidoSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateArchivoSubido', json);
  }

  updatePagoSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updatePago', json);
  }

  updateEstadoPolizaSockets(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnAdministradorPolizas/updateEstadoPoliza', json);
  }


}
