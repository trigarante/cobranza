import { TestBed } from '@angular/core/testing';

import { ConductorHabitualService } from './conductor-habitual.service';

describe('ConductorHabitualService', () => {
  let service: ConductorHabitualService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConductorHabitualService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
