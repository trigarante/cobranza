import { TestBed } from '@angular/core/testing';

import { VerificacionesPendientesService } from './verificaciones-pendientes.service';

describe('VerificacionesPendientesService', () => {
  let service: VerificacionesPendientesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VerificacionesPendientesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
