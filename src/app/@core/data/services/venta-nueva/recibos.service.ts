import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {Recibos, RecibosComplemento} from '../../interfaces/venta-nueva/recibos';

@Injectable({
  providedIn: 'root'
})
export class RecibosService {

  private baseURL;
  private baseURL2;
  private CORE_URL;

  public idRecibo = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) {

    this.baseURL = environment.CORE_URL;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.CORE_URL = environment.CORE_URL + '/recibo';


  }

  // NEW
  getRecibosByIdRegistro(idRegistro: number, id?): Observable<any[]> {
    const headers = new HttpHeaders();
    if (id) {
      headers.append('idrecibo', id.toString());
    }
    return this.http.get<any[]>(this.CORE_URL + '/registro/' + idRegistro, {headers});
  }

  getForPago(idRegistro: number): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/forPago/' + idRegistro);
  }

  getForPagoByIdRecibo(idPago: number): Observable<any> {
    return this.http.get<any>(this.CORE_URL + '/forPagoByid/' + idPago);
  }
  putBaja(idRegistro: number): Observable<any> {
    return this.http.put<any>(this.CORE_URL + '/bajaRecibos', {idRegistro});
  }
  postRecibos(recibos: Recibos[]): Observable<string> {
    return this.http.post<string>(this.CORE_URL + '/recibos/', recibos);
  }
  // FIN

  // por migrar********************
  putActivo(idRecibo: number) {
    return this.http.put(this.baseURL + 'v1/recibo/baja-recibos/' + idRecibo, null);
  }
  post(recibos: Recibos[]): Observable<string> {
    return this.http.post<string>(this.baseURL + '/recibo/', recibos,
      {responseType: 'text' as 'json'});
  }
}

