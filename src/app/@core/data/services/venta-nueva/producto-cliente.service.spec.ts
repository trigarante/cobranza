import { TestBed } from '@angular/core/testing';

import { ProductoClienteService } from './producto-cliente.service';

describe('ProductoClienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductoClienteService = TestBed.get(ProductoClienteService);
    expect(service).toBeTruthy();
  });
});
