import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {
  Solicitudes,
  SolicitudesView,
  SolicitudesVN,
  SolicitudesVNInterno
} from '../../interfaces/venta-nueva/solicitudes';

@Injectable({
  providedIn: 'root',
})
export class SolicitudesvnService {
  private baseURL;
  public idCliente = new BehaviorSubject<number>(0);
  public  urlPostventa = new BehaviorSubject<number>(0);
  private socketURL;
  private baseURL2;
  private serviciosNode;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS_OPERACIONES;
    this.socketURL = environment.GLOBAL_SOCKET;
    this.baseURL2 = environment.GLOBAL_SERVICIOS_OPERACIONES_Lectura;
    this.serviciosNode = environment.CORE_URL + '/solicitudes-vn';
  }

  // NEW
  stepsById(idSolicitud): Observable<any> {
    return this.http.get<any>(this.serviciosNode + '/steps/' + idSolicitud);
  }

  getFechas(fechaInicial: string, fechaFinal: string, idTipoContacto, idEstadoSolicitud): Observable<SolicitudesVNInterno[]> {
    return this.http.get<SolicitudesVNInterno[]>(`${this.serviciosNode}/todas/${sessionStorage.getItem('Empleado')}/${fechaInicial}/${fechaFinal}/${idTipoContacto}/${idEstadoSolicitud}`);
  }
  getDatosProspectoStepCliente(idSolicitud): Observable<any> {
    const headers = new HttpHeaders().append('id', idSolicitud.toString());
    return this.http.get<any>(this.serviciosNode + '/stepCliente', {headers});
  }

  getSolicitudesById(idSolicitud): Observable<Solicitudes> {
    return this.http.get<Solicitudes>(this.serviciosNode + '/get-by-id/' + idSolicitud);
  }
  getSolicitudesByIdOnline(idSolicitud): Observable<SolicitudesView> {
    return this.http.get<SolicitudesView>(this.serviciosNode + '/get-by-idOnline/' + idSolicitud);
  }
  entryIdCliente(value) {
    this.idCliente.next(value);
  }
  returnentryURLType(): Observable<number> {
    return this.urlPostventa.asObservable();
  }
  // FIN NEW

  get(): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL2 + 'v1/solicitudes-vn/' + sessionStorage.getItem('Usuario'));
  }

  post(solicitud: Solicitudes): Observable<Solicitudes> {
    return this.http.post<Solicitudes>(this.baseURL + 'v1/solicitudes-vn', solicitud);
  }

  put(idSolicitud, solicitud): Observable<Solicitudes> {
    return this.http.put<Solicitudes>(this.baseURL + 'v1/solicitudes-vn/' + idSolicitud + '/'
      + sessionStorage.getItem('Empleado'), solicitud);
  }

  updateEmpleadoSolicitud(idSolicitud: number, idEmpleado: number): Observable<any> {
    return this.http.put(this.serviciosNode + '/update-empleado/' + idSolicitud + '/' + idEmpleado, null);
  }

  reasignarEmpleadoSocket(json): Observable<SolicitudesVN> {
    return this.http.put<SolicitudesVN>(this.socketURL + 'vnsolicitudes/updateEmpleado', json);
  }

  cambiarEstadoSolicitud(idSolicitud, idEstadoSolicitud) {
    return this.http.put(`${this.serviciosNode}/cambiar-estado/${idSolicitud}/${idEstadoSolicitud}`, null);
  }
}
