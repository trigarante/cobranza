import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

interface TipoEndoso {
  id: number;
  descripcion: string;
  activo: number;
}

@Injectable({
  providedIn: 'root'
})
export class TipoEndosoService {
  CORE_URL = environment.CORE_URL + '/tipo-endoso';


  constructor(private http: HttpClient){}

  /* INICIO DE SERVICIOS VN POSTGRES */
  get(): Observable<TipoEndoso[]> {
    return this.http.get<TipoEndoso[]>(this.CORE_URL);
  }
  /*FIN DE SERVICIOS VN POSTGRES */
}
