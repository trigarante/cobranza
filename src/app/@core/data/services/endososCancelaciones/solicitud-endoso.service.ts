import { Injectable } from '@angular/core';
import {SolicitudEndoso} from '../../interfaces/endososCancelaciones/solicitudEndoso';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SolicitudEndosoPendiente} from '../../interfaces/endososCancelaciones/view/solicitudEndosoPendiente';

@Injectable({
  providedIn: 'root'
})
export class SolicitudEndosoService  {
  private baseURL;
  private CORE_URL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.CORE_URL = environment.CORE_URL + '/solicitud-endoso';

  }
// NEW
  getSolicitudEndosoPendiente(fechaInicio, fechaFin): Observable<SolicitudEndosoPendiente[]> {
    return this.http.get<SolicitudEndosoPendiente[]>(this.CORE_URL + '/get-endosoPendiente' + '/'
      + sessionStorage.getItem('Empleado') + `/${fechaInicio}/${fechaFin}`);
  }

  post(solicitudEndoso: SolicitudEndoso ): Observable<SolicitudEndoso> {
    return this.http.post<SolicitudEndoso>(this.CORE_URL, solicitudEndoso);
  }
// FIN
  getByIdEmpleado(idEstadoSolicitudEndoso: number): Observable<SolicitudEndoso[]> {
    return this.http.get<SolicitudEndoso[]>(this.baseURL + 'v1/solicitud-endoso/get-by-estado/' + idEstadoSolicitudEndoso + '/'
      + sessionStorage.getItem('Empleado'));
  }
  get(idEstadoSolicitudEndoso: number): Observable<SolicitudEndoso[]> {
    return this.http.get<SolicitudEndoso[]>(this.baseURL + 'v1/solicitud-endoso/get-by-estado/' + idEstadoSolicitudEndoso );
  }

  getSolicitudEndosoById(idSolicitudEndoso): Observable<SolicitudEndoso> {
    return this.http.get<SolicitudEndoso>(this.baseURL + 'v1/solicitud-endoso/' + idSolicitudEndoso);
  }

  put(idSolicitudEndoso, solicitudEndoso: SolicitudEndoso): Observable<SolicitudEndoso> {
    return this.http.put<SolicitudEndoso>(this.baseURL + 'v1/solicitud-endoso/' + idSolicitudEndoso, solicitudEndoso);
  }

  Completadas(): Observable<SolicitudEndoso[]> {
    return this.http.get<SolicitudEndoso[]>(this.baseURL + 'v1/solicitud-endoso/completadas');
  }
}
