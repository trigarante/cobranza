import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

export interface MotivoEndoso {
  id: number;
  descripcion: string;
  activo: number;
}
@Injectable({
  providedIn: 'root'
})
export class MotivoEndosoService  {

  private baseURL;
  private CORE_URL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.CORE_URL = environment.CORE_URL + '/motivo-endoso';


  }
  /* INICIO DE SERVICIOS VN POSTGRES */
  getByIdTipoEndoso(idMotivoEndoso: number): Observable<MotivoEndoso[]> {
    return this.http.get<MotivoEndoso[]>(this.CORE_URL + '/' + idMotivoEndoso);
  }
  /*FIN DE SERVICIOS VN POSTGRES */
}
