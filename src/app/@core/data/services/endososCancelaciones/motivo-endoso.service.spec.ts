import { TestBed } from '@angular/core/testing';

import { MotivoEndosoService } from './motivo-endoso.service';

describe('MotivoEndosoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MotivoEndosoService = TestBed.get(MotivoEndosoService);
    expect(service).toBeTruthy();
  });
});
