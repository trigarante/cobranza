import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {MarcasOnline} from '../../interfaces/cotizador/marcas-online';
import {DescripcionesOnline} from '../../interfaces/cotizador/descripciones-online';
import {ModelosOnline} from '../../interfaces/cotizador/modelos-online';
import {SubmarcasOnline} from '../../interfaces/cotizador/submarcas-online';

@Injectable({
  providedIn: 'root',
})
export class CatalogoService {

  constructor(private http: HttpClient) {

  }

  emitir(aseguradora: string, urlEmision: string, jsonEmision): Observable<JSON> {

    return this.http.post<JSON>( environment[aseguradora] + urlEmision,
      jsonEmision, { headers:
          {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }
  postPago(aseguradora: string, urlPago: string, jsonPago):
    Observable<JSON> {
    return this.http.post<JSON>(environment[aseguradora] + urlPago, jsonPago, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });

  }
  pdf(aseguradora: string, urldocumentos: string, poliza: string): Observable<string> {
    return this.http.get<string>(environment[aseguradora] + urldocumentos +
      '/imprimir?noPoliza=' + poliza, { headers: null,  responseType: 'text' as 'json'} );
  }

  getCotizacion(aseguradora: string, urlCotizacion: string, jsonCotizacion): Observable<JSON> {
    return this.http.post<JSON>( environment[aseguradora] + urlCotizacion, jsonCotizacion,
      { headers:
          {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }
  getMarcas(aseguradora: string, catalogo: string): Observable<MarcasOnline[]> {
    return this.http.get<MarcasOnline[]>(environment[aseguradora] + catalogo + 'marcas_autos');
  }

  getModelos(aseguradora: string, catalogo: string, marca: string): Observable<ModelosOnline[]> {
    return this.http.get<ModelosOnline[]>( environment[aseguradora] + catalogo  +
      'modelos_autos?&marca=' + marca);
  }

  getSubMarca(aseguradora: string, catalogo: string, marca: string, modelo: string): Observable<SubmarcasOnline[]> {
    return this.http.get<SubmarcasOnline[]>(environment[aseguradora] + catalogo +
      'submarcas_autos?marca=' + marca + '&modelo=' + modelo);
  }

  getDescripcion(aseguradora: string, catalogo: string, marca: string, modelo: string, submarca: string):
    Observable<DescripcionesOnline[]> {
    return this.http.get<DescripcionesOnline[]>(    environment[aseguradora] + catalogo +
      'descripciones_autos?marca=' + marca + '&modelo=' + modelo + '&submarca=' + submarca);

  }

  // gnp catalogos
  // // gnp bancos
  getBancosExterno(aseguradora: string, urlBanco: string): Observable<JSON> {
    return this.http.get<JSON>(environment[aseguradora] + urlBanco);
  }

  getTipoTarjetasExterno(aseguradora: string, urlTipoTarjetas: string): Observable<JSON> {
    return this.http.get<JSON>(environment[aseguradora] + urlTipoTarjetas);
  }
  getMsiExterno(aseguradora: string, urlMsi: string, numTarjeta: string): Observable<JSON> {
    return this.http.get<JSON>('https://web-gnp.mx/pago_gnp_3D/obtener_planes_pago?bin=' + numTarjeta);
  }

  postValidacionTarjeta(aseguradora: string, urlValidarTarjeta: string, jsonValidar):
    Observable<JSON> {
    return this.http.post<JSON>(environment[aseguradora] + urlValidarTarjeta, jsonValidar, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });

  }
  postObtenerEmision(aseguradora: string, urlObtenerEmision: string, jsonObtener):
    Observable<JSON> {
    return this.http.post<JSON>(environment[aseguradora] + urlObtenerEmision, jsonObtener, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });

  }
  pdfgnp(aseguradora: string, urldocumentos: string, poliza: string): Observable<any> {
    return this.http.post<any>(  'https://web-gnp.mx/' + urldocumentos +
      '/impresion?numeroPoliza=' + poliza, null, { headers:
        {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } });
  }

}
