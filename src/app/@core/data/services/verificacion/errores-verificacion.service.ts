import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErroresVerificacion} from '../../interfaces/verificacion/errores-verificacion';

@Injectable({
  providedIn: 'root',
})
export class ErroresVerificacionService {
  private baseURL;
  private CORE_URL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS + 'v1/errores-verificacion';
    this.CORE_URL = environment.CORE_URL + '/errores-verificacion';
  }
  // NEW
  getCamposConErrores(idVerificacionRegistro: number, idTipoDocumento: number): Observable<ErroresVerificacion> {
    return this.http.get<ErroresVerificacion>(this.CORE_URL + '/' + idVerificacionRegistro + '/' + idTipoDocumento);
  }

  putEstadoCorreccion(idErrorVerificacion: number): Observable<any> {
    return this.http.put(this.CORE_URL + '/' + idErrorVerificacion, null);
  }

  // FIN

  get(): Observable<ErroresVerificacion[]> {
    return this.http.get<ErroresVerificacion[]>(this.baseURL);
  }

  post(error: ErroresVerificacion): Observable<any> {
    return this.http.post<string>(this.baseURL, error);
  }


}
