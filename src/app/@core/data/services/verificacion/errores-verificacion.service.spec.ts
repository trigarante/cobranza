import { TestBed } from '@angular/core/testing';

import { ErroresVerificacionService } from './errores-verificacion.service';

describe('ErroresVerificacionService', () => {
  let service: ErroresVerificacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresVerificacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
