import {VerificacionRegistro} from '../../interfaces/verificacion/verificacion-registro';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class VerificacionRegistroService {
  private baseURL;
  private CORE_URL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS + 'v1/verificacion-registro/';
    this.CORE_URL = environment.CORE_URL + '/verificacion-registro/';
  }

  // NEW
  post(idRegistro: number): Observable<VerificacionRegistro> {
    return this.http.post<VerificacionRegistro>(this.CORE_URL, {idRegistro});
  }
  getById(id: number): Observable<VerificacionRegistro> {
    return this.http.get<VerificacionRegistro>(this.CORE_URL + 'get-by-id/' + id);
  }
  documentoVerificado(idVerificacionRegistro: number, documentoVerificado: number, estado: number): Observable<string> {
    const headers = new HttpHeaders().append('idverificacionregistro', idVerificacionRegistro.toString()).
    append('iddocumentoverificado', documentoVerificado.toString()).
    append('estado', estado.toString());
    return this.http.put<string>(this.CORE_URL + 'documento/', null, {headers});
  }
  // FIN
  get(): Observable<VerificacionRegistro[]> {
    return this.http.get<VerificacionRegistro[]>(this.baseURL + sessionStorage.getItem('Empleado'));
  }

  put(id: number, verificacion: VerificacionRegistro): Observable<VerificacionRegistro> {
    return this.http.put<VerificacionRegistro>(this.baseURL + id, verificacion);
  }
  // Pagos

  getByIdPago(id: number): Observable<VerificacionRegistro> {
    return this.http.get<VerificacionRegistro>(this.baseURL + 'pago/get-by-id/' + id);
  }
}
