import { TestBed } from '@angular/core/testing';

import { DashBoardNewService } from './dash-board-new.service';

describe('DashBoardNewService', () => {
  let service: DashBoardNewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DashBoardNewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
