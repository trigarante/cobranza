import { Injectable } from '@angular/core';
import Menu from '../interfaces/menu/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  permisos = JSON.parse(window.localStorage.getItem('User'));
  menu: Menu[] = [
    {
      icon: 'arrow_right',
      name: 'Subsecuentes',
      permiso: true,
      alias: 'sub',
      children: [
        {
          icon: '',
          name: 'Solicitudes',
          rutas: 'subsecuentes/solicitudes/interno',
          permiso: this.permisos.SUB?.solicitudes,
        },
        {
          icon: '',
          name: 'Administración de Pólizas Internas',
          rutas: 'subsecuentes/administracion-polizas/internas',
          permiso: this.permisos.SUB?.adminPolizas
        },
        {
          icon: 'arrow_right',
          name: 'Administración de Pólizas Subsecuentes',
          rutas: 'subsecuentes/administracion-polizas/subsecuentes',
          permiso: this.permisos.SUB?.adminPolizas
        },
        {
          icon: 'arrow_right',
          name: 'Autorización de Errores En Póliza',
          permiso: true,
          grandChildren: [
            { rutas: 'subsecuentes/autorizacion-errores/1',
              icon: '',
              name: 'Pendientes',
              permiso: this.permisos.SUB?.autoErrPolizas
            },
            { rutas: 'subsecuentes/autorizacion-errores/2',
              icon: '',
              name: 'En Proceso',
              permiso: this.permisos.SUB?.autoErrPolizas
            },
            { rutas: 'subsecuentes/autorizacion-errores/3',
              icon: '',
              name: 'Corregidas',
              permiso: this.permisos.SUB?.autoErrPolizas
            },
          ],
        },
        {
          icon: 'arrow_right',
          permiso: true,
          name: 'Corrección de errores en póliza',
          grandChildren: [
            {
              icon: 'arrow_right',
              name: 'Autorización',
              permiso: true,
              gen4: [
                { rutas: 'subsecuentes/correccion-errores-poliza/documentos/autorizacion',
                  icon: '',
                  name: 'Documentos',
                  permiso: this.permisos.SUB?.corrErrDocsPolizas
                },
                { rutas: 'subsecuentes/correccion-errores-poliza/datos/autorizacion',
                  icon: '',
                  name: 'Datos',
                  permiso: this.permisos.SUB?.corrErrDatosPolizas
                },
              ],
            },
            {
              icon: 'arrow_right',
              name: 'Verificacion',
              permiso: true,
              gen4: [
                { rutas: 'subsecuentes/correccion-errores-poliza/documentos/verificacion',
                  icon: '',
                  name: 'Documentos',
                  permiso: this.permisos.SUB?.corrErrDocsPolizas
                },
                { rutas: 'subsecuentes/correccion-errores-poliza/datos/verificacion',
                  icon: '',
                  name: 'Datos',
                  permiso: this.permisos.SUB?.corrErrDatosPolizas
                },
              ],
            },
          ],
        }
      ],
    },
    {
      icon: 'arrow_right',
      name: 'Cobranza',
      permiso: this.permisos.COB,
      children: [
        {
          icon: 'arrow_right',
          name: 'Solicitudes',
          permiso: this.permisos.COB?.solicitudes,
          rutas: 'cobranza/solicitudes-interna'
        },
        {
          icon: 'arrow_right',
          name: 'Administracion de Pólizas',
          permiso: this.permisos.COB?.adminPolizas,
          rutas: 'cobranza/administracion-poliza'
        },
        {
          icon: 'arrow_right',
          name: 'Autorización de Errores',
          permiso: true,
          grandChildren: [
            { rutas: 'cobranza/autorizacion-errores/1',
              icon: '', name: 'Pendientes',
              permiso: this.permisos.COB?.autoErrores
            },
            { rutas: 'cobranza/autorizacion-errores/2',
              icon: '',
              name: 'En Proceso',
              permiso: this.permisos.COB?.autoErrores
            },
            { rutas: 'cobranza/autorizacion-errores/3',
              icon: '',
              name: 'Corregidas',
              permiso: this.permisos.COB?.autoErrores
            },
          ]
        },
        {
          icon: 'arrow_right',
          name: 'Corrección de errores',
          permiso: true,
          grandChildren: [
            { name: 'Autorización', icon: 'arrow_right', permiso: true, gen4: [
                { rutas: 'cobranza/correccion-errores/documentos/autorizacion',
                  icon: '',
                  name: 'Documentos',
                  permiso: this.permisos.COB?.corrErroresDocs
                },
                { rutas: 'cobranza/correccion-errores/datos/autorizacion',
                  icon: '',
                  name: 'Datos',
                  permiso: this.permisos.COB?.corrErroresDatos
                },
              ]
            },
            { name: 'Verificación', icon: 'arrow_right', permiso: true, gen4: [
                { rutas: 'cobranza/correccion-errores/documentos/verificacion',
                  icon: '',
                  name: 'Documentos',
                  permiso: this.permisos.COB?.corrErroresDocs
                },
                { rutas: 'cobranza/correccion-errores/datos/verificacion',
                  icon: '',
                  name: 'Datos',
                  permiso: this.permisos.COB?.corrErroresDatos
                },
              ]
            },
          ]
        },
        {
          icon: 'arrow_right',
          name: 'Solicitud de Pago',
          permiso: true,
          rutas: 'cobranza/solicitudes-pago'
        },
        {
          icon: 'arrow_right',
          name: 'solicitudes de pago IVR',
          permiso: true,
          rutas: 'cobranza/solicitudes-ivr'
        },
      ]
    },
    {
      icon: 'arrow_right',
      name: 'Emitido No Registrado',
      permiso: true,
      children: [
        {
          icon: 'arrow_right',
          name: 'Solicitudes',
          permiso: true,
          rutas: 'ENR/solicitudes-interna'
        },
        {
          icon: 'arrow_right',
          name: 'Administracion de Pólizas ENR',
          permiso: true,
          rutas: 'ENR/administracion-poliza',
        },
        {
          icon: 'arrow_right',
          name: 'Autorización de Errores',
          permiso: this.permisos.ENR?.autorizaciondeErrores,
          grandChildren: [
            { rutas: 'ENR/autorizacion-errores/1',
              icon: '',
              name: 'Pendientes',
              permiso: true,
            },
            { rutas: 'ENR/autorizacion-errores/2',
              icon: '',
              name: 'En Proceso',
              permiso: true
            },
            { rutas: 'ENR/autorizacion-errores/3',
              icon: '',
              name: 'Corregidas',
              permiso: true
            },
          ]
        },
        {
          icon: 'arrow_right',
          name: 'Corrección de errores',
          permiso: true,
          grandChildren: [
            { name: 'Autorización', icon: 'arrow_right', permiso: true, gen4: [
                { rutas: 'ENR/correccion-errores/documentos/autorizacion',
                  icon: '',
                  name: 'Documentos',
                  permiso: true,
                },
                { rutas: 'ENR/correccion-errores/datos/autorizacion',
                  icon: '',
                  name: 'Datos',
                  permiso: true,
                },
              ]
            },
            { name: 'Verificación', icon: 'arrow_right', permiso: true, gen4: [
                { rutas: 'ENR/correccion-errores/documentos/verificacion',
                  icon: '',
                  name: 'Documentos',
                  permiso: true,
                },
                { rutas: 'ENR/correccion-errores/datos/verificacion',
                  icon: '',
                  name: 'Datos',
                  permiso: true,
                },
              ]
            },
          ]
        },
        {
          icon: 'arrow_right',
          name: 'Historial Cancelaciones',
          permiso: this.permisos.ENR?.historialCan,
          rutas: 'ENR/historial-cancelaciones'
        }
      ]
    },
    {
      icon: '',
      name: 'Monitoreo de personal',
      rutas: 'telefonia/monitoreo',
      permiso: this.permisos?.COB?.monitoreo
    },
    {
      icon: '',
      name: 'Directorio de aseguradoras',
      rutas: 'telefonia/directorio-aseguradora',
      permiso: this.permisos.COB?.direcAseguradoras
    },
    {
      icon: '',
      name: 'Pólizas Totales',
      rutas: 'cobranza/administracion-poliza/totales',
      permiso: true
    },
    {
      icon: '',
      name: 'Verificaciones Pendientes',
      rutas: 'venta-nueva/pendientes-verificacion',
      permiso: true
    },
    {
      icon: '',
      name: 'Mi estado de cuenta',
      rutas: 'venta-nueva/mi-estado-cuenta',
      permiso: true
    },
  ];

  getMenu(): Menu[]{
    return this.menu;
  }

}
