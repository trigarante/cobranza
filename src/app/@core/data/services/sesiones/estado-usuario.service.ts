import { Injectable } from '@angular/core';
import {EstadoUsuario} from '../../interfaces/sesiones/estado-usuario.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EstadoUsuarioService  {
  private baseURL;
  private estadoUsuarioListener = new Subject();

  constructor(private http: HttpClient) {

    this.baseURL = environment.LOGIN_URL;
  }

  post(estadoUsuario): Observable<EstadoUsuario> {
    this.estadoUsuarioListener.next(estadoUsuario.idEstadoSesion);
    return this.http.post<EstadoUsuario>(`${this.baseURL}/estadoUsuario`, estadoUsuario);
  }

  get(idSesion): Observable<EstadoUsuario[]> {
    return this.http.get<EstadoUsuario[]>(`${this.baseURL}/estadoUsuario/${idSesion}`);
  }

  put(idEstadoUsuario): Observable<EstadoUsuario> {
    return this.http.put(`${this.baseURL}/estadoUsuario/fin-estado/${idEstadoUsuario}`, null);
  }
  getestadoUsuarioListener(): Observable<any> {
    return this.estadoUsuarioListener.asObservable();
  }
}
