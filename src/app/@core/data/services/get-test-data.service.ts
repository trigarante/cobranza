import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetTestDataService {

  constructor(private http: HttpClient) { }

  getTestData() {
    return this.http.get(`https://telefonia.mark-43.net/api/supervisores/pjsip-out-call/848/15/2/665`);
  }

  getLlamadasIn() {
    return this.http.get('https://telefonia.mark-43.net/api/supervisores/inbound-call/848/15/2/665');
  }
}
