import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SepomexService {
  private baseURLVn2;
  constructor(
    private http: HttpClient
  ) {
    this.baseURLVn2 = environment.CORE_URL + '/sepomex';
  }

  getColoniaByCp(cp): Observable<any[]> {
      const headers = new HttpHeaders().append('cp', cp.toString());
      return this.http.get<any[]>(this.baseURLVn2, {headers});
  }
}
