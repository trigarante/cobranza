
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Paises} from '../../interfaces/catalogos/paises';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaisesService  {
  private baseURL;
  private socketURL;
  private CORE = environment.CORE_URL + '/paises';
  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  // NEW
  get(): Observable<Paises[]> {
    return this.http.get<Paises[]>(this.CORE);
  }
  // FIN

  post(paises: Paises): Observable<Paises> {
    return this.http.post<Paises>(this.baseURL + 'v1/paises', paises);
  }

  put(idPaises, paises: Paises): Observable<Paises> {
    return this.http.put<Paises>(this.baseURL + 'v1/paises/' + idPaises + '/'
      + sessionStorage.getItem('Empleado'), paises);
  }

}
