import { Injectable } from '@angular/core';
import {
  tipificacionCobranza,
  tipificacionesCobranzaEtiqueta, tipificacionSubEtiqueta,
  tipificarPolizaCobranza
} from '../../interfaces/tipificaciones/tipificacionesCobranza';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipificacionesCobranzaService {

  url: string;
  url2: string;
  url3: string;
  url4: string;


  constructor(private http: HttpClient) {

    this.url = environment.CORE_URL + '/tipificacionCobranza';
    this.url2 = environment.CORE_URL + '/tipificacionCobranzaEtiqueta';
    this.url3 = environment.CORE_URL + '/tipificacionCobranzaSubEtiqueta';
    this.url4 = environment.CORE_URL + '/tipificarPolizaCobranza';

  }


  getAll(idFlujoTipificacion): Observable<tipificacionCobranza[]> {
    return this.http.get<tipificacionCobranza[]>(`${this.url}/${idFlujoTipificacion}`);
  }

  getEtiqueta(idEstadoSolicitud): Observable<any[]> {
    return this.http.get<tipificacionesCobranzaEtiqueta[]>(`${this.url2}/${idEstadoSolicitud}`);
  }

  getSubEtiqueta(idEtiquetaSolicitud): Observable<any[]>{
    return this.http.get<tipificacionSubEtiqueta[]>(`${this.url3}/${idEtiquetaSolicitud}`);
  }

  postTipificar(tipificacionCobranza: tipificarPolizaCobranza): Observable<any>{
    return this.http.post(`${this.url4}`, tipificacionCobranza);
  }
}

