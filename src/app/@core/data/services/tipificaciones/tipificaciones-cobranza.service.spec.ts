import { TestBed } from '@angular/core/testing';

import { TipificacionesCobranzaService } from './tipificaciones-cobranza.service';

describe('TipificacionesCobranzaService', () => {
  let service: TipificacionesCobranzaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipificacionesCobranzaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
