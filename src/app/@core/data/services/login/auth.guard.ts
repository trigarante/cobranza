import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate, CanLoad, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import {TokenStorageService} from './token-storage.service';
import { Route } from '@angular/compiler/src/core';
import {Usuario} from '../../../../models/usuario.model';

declare const gapi: any;

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanLoad {
  public usuario: Usuario;

    constructor(private auth: TokenStorageService,
                private myRoute: Router
    ) {
    }

    validarAuth(next?: ActivatedRouteSnapshot){
        const tokenStorage = window.localStorage.getItem('token');
        if (!tokenStorage || next.data?.permiso === false){
          this.myRoute.navigate(['/login']);
          return false;
        }

        if (!gapi.auth2){
          gapi.load('auth2', () => {
            gapi.auth2.init({ client_id: '1047898691737-jc2pdn07g0c5i0t13ijuk3igdl4qhrup.apps.googleusercontent.com'});
          });
        }
        return true;
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.validarAuth(next);
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        return this.validarAuth();
    }
}

/*
  try {
            try {
                if (next.data.permiso === false) {
                    this.myRoute.navigate(['modulos', this.log]);
                    return false;
                }
                return true;
            } catch (e) {
                this.log = '0';
                this.myRoute.navigate(['modulos', this.log]);
                return false;
            }
            const decodedToken = helper.decodeToken(window.localStorage.getItem('token'));
            const fechaToken = moment(decodedToken.exp * 1000).toDate();
            const fechaHoy = new Date();
            if (fechaToken < fechaHoy) {
                this.log = 'warn';
                this.myRoute.navigate(['login', this.log]);
                return false;
            }
            return true;
        } catch (e) {
            this.log = '0';
            this.myRoute.navigate(['login', this.log]);
            return false;
        }
*/
