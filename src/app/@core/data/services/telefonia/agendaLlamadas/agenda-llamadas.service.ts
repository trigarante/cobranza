import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from './../../../../../../environments/environment';
import {Observable} from 'rxjs';
const telefoniaEndpoints = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root',
})
export class AgendaLlamadasService {

  constructor(private http: HttpClient) {
  }

  getNumerosByIdSolicitud(idSolicitud): Observable<any[]> {
    return this.http.get<any[]>(`${telefoniaEndpoints}/agregar-numero/numeros-solicitud/${idSolicitud}`);
  }

}

