import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
const url = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class NumeroAseguradoraService {

  constructor(private http: HttpClient) { }

  getNumerosAsegura(){
    // return this.http.get("https://telefonia.mark-43.net/api/llamada-simultanea/call-centers/12")
    return this.http.get(`${url}/llamada-simultanea/call-centers/`);
  }

}
