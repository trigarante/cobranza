import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_TELEFONIA;
const hostName: string = window.location.hostname;

@Injectable({
  providedIn: 'root'
})
export class ActualServerTelefoniaService {

  constructor(private http: HttpClient) { }

  getCurrentServer() {
    return this.http.get<string>(`${base_url}/currentServer`, {
      headers: {
        'plataforma': hostName,
      },
    });
  }
}
