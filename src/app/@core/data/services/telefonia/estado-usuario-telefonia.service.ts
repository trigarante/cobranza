import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const baseUrl = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class EstadoUsuarioTelefoniaService {

  constructor(private http: HttpClient) { }
  activarUsuario(idUser) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-activo/${idUser}`, null);
  }

  initUsuario(idUser) {
    return this.http.post(`${baseUrl}/estado-u-telefonia/inicio-usuario/${idUser}`, null);
  }

  usuarioInactivo(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-inactivo/${idUsuario}`, null);
  }

  usuarioPausa(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-pausado/${idUsuario}`, null);
  }

  getEstadoUsuario(idUsuario) {
    return this.http.get(`${baseUrl}/estado-u-telefonia/estado-by-usuario/${idUsuario}`);
  }

  cambiarEstado(idEstadoSesion, idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/cambiar-estado/${idUsuario}/${idEstadoSesion}`, null);
  }

  inicioPausa(idUsuario, idMotivoPausa) {
    return this.http.post(`${baseUrl}/estado-u-telefonia/inicio-pausa/${idUsuario}/${idMotivoPausa}`, null);
  }

  finPausa(idPausaUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/fin-pausa/${idPausaUsuario}`, null);
  }

  enLlamada(idUsuario) {
    return this.http.put(`${baseUrl}/estado-u-telefonia/estado-en-llamada/${idUsuario}`, null);
  }
}
