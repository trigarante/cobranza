import { Injectable } from '@angular/core';
import {Grabaciones} from '../../interfaces/telefonia/grabaciones/grabaciones';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {LlamadaSalida} from '../../interfaces/llamadasSalida/llamadaSalida';

@Injectable({
  providedIn: 'root'
})
export class LlamadasSalidaService {

  private baseURL;

  private telefoniaNormal;

  constructor(private http: HttpClient) {
    // Servicios trigarante
    this.baseURL = environment.SERVICIOS_TELEFONIA_DATOS;
    // servicios telefonia pruebas
    // this.baseURL = environment.SERVICIOS_PJSIP_VN;
    this.telefoniaNormal = environment.SERVICIOS_TELEFONIA;
  }

  post(llamada): Observable<LlamadaSalida> {
    return this.http.post<LlamadaSalida>(`${this.baseURL}/llamada-salidas`, llamada);
  }

  setAsteriskid(idLlamada, numero): Observable<string> {
    return this.http.put<string>(`${this.baseURL}/llamada-salidas/idAsterisk/${numero}/${idLlamada}`, null);
  }

  postAgendarLlamada(llamadarLlamada): Observable<LlamadaSalida> {
    return this.http.post<LlamadaSalida>(`${this.baseURL}/callcenter/llamadasalida/agenda`, llamadarLlamada);
  }

  put(idEstadoLlamada: number, idLlamada: number): Observable<LlamadaSalida> {
    return this.http.put<LlamadaSalida>(`${this.baseURL}/llamada-salidas/${idLlamada}`, {
      idEstadoLlamada,
    });
  }

  putSubEtiqueta(idllamada: number, idSubetiqueta: number): Observable<LlamadaSalida> {
    return this.http.put<LlamadaSalida>(`${this.baseURL}/llamada-salidas/subetiqueta/${idllamada}`, {
      idSubEtiqueta: idSubetiqueta,
    });
  }

  getByIdEmpleado(idEmpleado): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>(`${this.baseURL}/llamada-salidas/llamada-sin-etiqueta/${idEmpleado}`);
  }

  getLlamadasPendientes(): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>
    (`${this.baseURL}/callcenter/llamadasalida/llamada-pendiente/${sessionStorage.getItem('Empleado')}`);
  }

  getLlamadasCallback(): Observable<LlamadaSalida[]> {
    const idEmpleado = sessionStorage.getItem('Empleado');
    return this.http.get<LlamadaSalida[]>(`${this.baseURL}/llamada-salidas/llamada-pendiente/${idEmpleado}`);
  }

  getCallbackRealizado(): Observable<LlamadaSalida[]> {
    const idEmpleado = sessionStorage.getItem('Empleado');
    return this.http.get<LlamadaSalida[]>(`${this.telefoniaNormal}/supervisores/callback-realizado/${idEmpleado}`);
  }

  getCallbackTotales(id, idPuesto, idTipo, idSubarea): Observable<any> {
    return this.http.get<any>(`${this.telefoniaNormal}/supervisores/callback-totales/${id}/${idPuesto}/${idTipo}/${idSubarea}`);
  }

  getCallbackByEmpleado(id): Observable<any> {
    return this.http.get<any>(`${this.telefoniaNormal}/supervisores/callback-empleados/${id}`);
  }

  startCallback(idLlamada: number, inicioLlamada): Observable<LlamadaSalida> {
    return this.http.put<LlamadaSalida>(`${this.baseURL}/callcenter/llamadasalida/start-callback/${idLlamada}`,
      {fechaInicio: inicioLlamada});
  }

  inicioCallback(idLlamada): Observable<LlamadaSalida> {
    return this.http.put<LlamadaSalida>(`${this.baseURL}/llamada-salidas/start-callback/${idLlamada}`, null);
  }

  finCallback(idLlamada, idEstadoLlamada): Observable<LlamadaSalida> {
    return this.http.put<LlamadaSalida>(`${this.baseURL}/llamada-salidas/end-callback/${idLlamada}`, {
      idEstadoLlamada,
    });
  }

  endCallback(idLlamada: number, idEstadoLlamada, finLlamada): Observable<LlamadaSalida> {
    return this.http.put<LlamadaSalida>(`${this.baseURL}/callcenter/llamadasalida/end-callback/${idLlamada}`, {
      idEstadoLlamada,
      fechaFinal: finLlamada,
    });
  }

  getSolicitudById(idSolicitud) {
    return this.http.get(`${this.baseURL}/solicitudes/get-solicitud/${idSolicitud}`);
  }

  getSolicitudByIdVnWsPremium(idSolicitud) {
    return this.http.get(`${this.baseURL}/callcenter/llamadasalida/cotizaciones-ws-premium/${idSolicitud}`);
  }

  getSolicitudByIdVnWs(idSolicitud) {
    return this.http.get(`${this.baseURL}/callcenter/llamadasalida/cotizaciones-ws/${idSolicitud}`);
  }

  getSolicitudByProspecto(idProspecto) {
    return this.http.get(`${this.baseURL}/callcenter/llamadasalida/datos/${idProspecto}`);
  }

  getllamadasByIdSolicitud(idsolicitud): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>(`${this.baseURL}/llamadaSalidasView/llamadasByIdSolicitud/${idsolicitud}`);
  }

  getllamadasByIdRegistro(idRegistro): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>(`${this.baseURL}/llamadaSalidasView/llamadasIdRegistro/${idRegistro}`);
  }

  getGrabacionOutbound(nombreArchivo: string) {
    return this.http.get(`${this.baseURL}/recordings`, {
      headers: {'nombreArchivo': nombreArchivo},
    });
  }

  getNombreArchivo(idAsterisk: string): Observable<Grabaciones[]> {
    return this.http.get<Grabaciones[]>(`${this.baseURL}/recordings/nombreArchivo/${idAsterisk}`);
  }

  getArchivoPjsip(idAsterisk: string): Observable<Grabaciones[]> {
    return this.http.get<Grabaciones[]>(`${this.baseURL}/recordings/archivo-pjsip/${idAsterisk}`);
  }

  getLlamadaSalidaSinTerminar(idEmpleado: string) {
    return this.http.get(`${this.baseURL}/callcenter/llamadasalida/llamada/sin-cerrar/${idEmpleado}`);
  }
  getSecondIdAsterisk(oldIdAsterisk: string, numero: string): Observable<string> {
    return this.http.get<string>(`${this.baseURL}/recordings/grabaciones/buzon/${oldIdAsterisk}/${numero}`);
  }

  getSecondIdPjsip(oldIdAsterisk: string, numero: string) {
    return this.http.get<string>(`${this.baseURL}/recordings/buzon-pjsip/${oldIdAsterisk}/${numero}`);
  }
  getLlamadasCallbackBySolicitud(idSolicitud, idEmpleadoViejo, idEmpleadoNuevo): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>
    (`${this.telefoniaNormal}/llamada-salidas/callback-solicitud/${idSolicitud}/${idEmpleadoViejo}/${idEmpleadoNuevo}`);
  }

  buscarSinTerminar(idAsterisk: string): Observable<number> {
    return this.http.get<number>(`${this.telefoniaNormal}/llamada-salidas/buscar-sin-terminar/${idAsterisk}`);
  }

  terminarSinAsterisk(idLlamada) {
    return this.http.put<number>(`${this.telefoniaNormal}/llamada-salidas/fin-sin-asterisk/${idLlamada}`, null);
  }

  sinTerminarCdr(idAsterisk: string): Observable<number> {
    return this.http.get<number>(`${this.baseURL}/llamada-salidas/sin-terminar-cdr/${idAsterisk}`);
  }

  getLlamadasPjsip(idSolicitud): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>(`${this.baseURL}/llamadaSalidasView/pjsip/llamadas-out/${idSolicitud}`);
  }

  getLlamadasRegistroPjsip(idRegistro): Observable<LlamadaSalida[]> {
    return this.http.get<LlamadaSalida[]>(`${this.baseURL}/llamadaSalidasView/pjsip/llamadasout-idRegistro/${idRegistro}`);
  }
}
