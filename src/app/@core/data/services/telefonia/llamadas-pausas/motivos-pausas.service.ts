import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {MotivosPausa} from '../../../interfaces/telefonia/llamadas-pausas/motivos-pausas';
const base_url = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class MotivosPausasService {

  constructor(private http: HttpClient) { }

  getMotivosPausas() {
    return this.http.get<MotivosPausa[]>(`${base_url}/estado-u-telefonia/motivos-pausa`);
  }
}
