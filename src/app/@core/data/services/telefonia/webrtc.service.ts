import { Injectable } from '@angular/core';
import SIPml from 'ecmascript-webrtc-sipml';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebrtcService {
  oSipStack; oSipSessionRegister; oSipSessionCall; oSipSessionTransferCall;
  oNotifICall;
  oConfigCall;
  ringtone; ringbacktone;
  pjsipEvents = new Subject<{
    evento: string,
    numero?: string,
  }>();
  public currentServer: string = '';
  private idUsuario: number;

  constructor() { }

  sipRegister = (idUser) => {
    this.idUsuario = idUser;
    try {
      Notification.requestPermission();
      // update debug level to be sure new values will be used if the user haven't updated the page
      SIPml.setDebugLevel((window.localStorage &&
        window.localStorage.getItem('org.doubango.expert.disable_debug') === 'true') ? 'error' : 'info');

      // create SIP stack
      const ext = localStorage.getItem('extension');
      this.oSipStack = new SIPml.Stack({
          realm: `${this.currentServer}`,
          impi: `${ext}`,
          impu: `sip:${ext}@${this.currentServer}`,
          password: 'triga.123',
          display_name: `${ext}`,
          websocket_proxy_url: `wss://${this.currentServer}:8089/ws`,
          outbound_proxy_url: '',
          ice_servers: '[{ url:\'stun:pruebas-stun.ahorraseguros.mx:3478\'}]',
          enable_rtcweb_breaker: '',
          events_listener: { events: '*', listener: this.onSipEventStack },
          enable_early_ims: '',
          enable_media_stream_cache: '',
          sip_headers: [
            { name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.2016.03.04' },
            { name: 'Organization', value: 'Doubango Telecom' },
          ],
        },
      );
      if (this.oSipStack.start() !== 0) {
        console.log('<b>Failed to start the SIP stack</b>');
      } else return;
    } catch (e) {
      console.log("<b>2:" + e + "</b>");
    }
  }

  sipUnRegister = () => {
    if (this.oSipStack) {
      this.oSipStack.stop(); // shutdown all sessions
    }
  }

  onSipEventStack = (e) => {
    console.log('==stack event = ' + e.type);
    switch (e.type) {
      case 'started':
      {
        // catch exception for IE (DOM not ready)
        try {
          // LogIn (REGISTER) as soon as the stack finish starting
          this.oSipSessionRegister = this.oSipStack.newSession('register', {
            expires: 200,
            events_listener: { events: '*', listener: this.onSipEventSession },
            sip_caps: [
              { name: '+g.oma.sip-im', value: null },
              { name: '+audio', value: null },
              { name: 'language', value: '\"en,fr\"' },
            ],
          });
          this.oSipSessionRegister.register();
        } catch (e) {
          console.log("<b>1:" + e + "</b>");
        }
        break;
      }
      case 'stopping': case 'stopped': case 'failed_to_start': case 'failed_to_stop':
      {
        const bFailure = (e.type === 'failed_to_start') || (e.type === 'failed_to_stop');
        this.oSipStack = null;
        this.oSipSessionRegister = null;
        this.oSipSessionCall = null;

        // uiOnConnectionEvent(false, false);

        // this.stopRingbackTone();
        this.stopRingTone();
        console.log(bFailure ? "<i>Disconnected: <b>" + e.description + "</b></i>" : "<i>Disconnected</i>");
        this.pjsipEvents.next({evento: 'disconnected'});

        break;
      }

      case 'i_new_call':
      {
        this.oSipSessionCall = e.newSession;
        // start listening for events
        this.oConfigCall = {
          events_listener: { events: '*', listener: this.onSipEventSession },
        };
        this.oSipSessionCall.setConfiguration(this.oConfigCall);
        this.startRingTone();

        const sRemoteNumber = (this.oSipSessionCall.getRemoteFriendlyName() || 'unknown');
        this.pjsipEvents.next({evento: 'i_new_call', numero: sRemoteNumber});
        console.log("<i>Incoming call from [<b>" + sRemoteNumber + "</b>]</i>");

        break;
      }

      case 'm_permission_requested':
      {
        break;
      }
      case 'm_permission_accepted':
      case 'm_permission_refused':
      {
        if (e.type === 'm_permission_refused') {
        }
        break;
      }

      case 'starting': default: break;
    }
  }

  onSipEventSession = (e) => {
    console.log('==session event = ' + e.type);

    switch (e.type) {
      case 'connecting': case 'connected':
      {
        const bConnected = (e.type === 'connected');
        if (e.type === 'connected') {
          this.pjsipEvents.next({evento: 'connected'});
          this.ringtone = document.getElementById("ringtone");
        }
        if (e.session === this.oSipSessionRegister) {
          console.log("<i>" + e.description + "</i>");
        } else if (e.session === this.oSipSessionCall) {

          if (bConnected) {
            // this.stopRingbackTone();
            this.stopRingTone();

            if (this.oNotifICall) {
              this.oNotifICall.cancel();
              this.oNotifICall = null;
            }
          }

          console.log("<i>" + e.description + "</i>");

          if (SIPml.isWebRtc4AllSupported()) { // IE don't provide stream callback
          }
        }
        break;
      } // 'connecting' | 'connected'
      case 'terminating': case 'terminated':
      {
        this.oSipSessionCall = null;
        this.oSipSessionRegister = null;
        if (e.description !== 'Disconnected') {
          this.pjsipEvents.next({evento: 'terminated'});
        }
        if (e.description === 'Unauthorized') {
          this.pjsipEvents.next({evento: 'disconnected'});
        }
        console.log("<i>" + e.description + "</i>");
        if (e.description.includes('Cancelled') || e.description.includes('Rejected')) {
          this.stopRingTone();
          this.pjsipEvents.next({evento: 'terminated'});
        }
        break;
      } // 'terminating' | 'terminated'

      case 'm_stream_video_local_added':
      {
        if (e.session === this.oSipSessionCall) {
        }
        break;
      }
      case 'm_stream_video_local_removed':
      {
        if (e.session === this.oSipSessionCall) {
        }
        break;
      }
      case 'm_stream_video_remote_added':
      {
        if (e.session === this.oSipSessionCall) {
        }
        break;
      }
      case 'm_stream_video_remote_removed':
      {
        if (e.session === this.oSipSessionCall) {
        }
        break;
      }

      case 'm_stream_audio_local_added':
      case 'm_stream_audio_local_removed':
      case 'm_stream_audio_remote_added':
      case 'm_stream_audio_remote_removed':
      {
        break;
      }

      case 'i_ect_new_call':
      {
        this.oSipSessionTransferCall = e.session;
        break;
      }

      case 'i_ao_request':
      {
        this.pjsipEvents.next({evento: 'i_ao_request'});
        if (e.session === this.oSipSessionCall) {
          const iSipResponseCode = e.getSipResponseCode();
          if (iSipResponseCode === 180 || iSipResponseCode === 183) {
            this.startRingbackTone();
            console.log('<i>Remote ringing...</i>');
          }
        }
        break;
      }

      case 'm_early_media':
      {
        if (e.session === this.oSipSessionCall) {
          this.stopRingbackTone();
          this.stopRingTone();
          console.log('<i>Early media started</i>');
        }
        break;
      }

      case 'm_local_hold_ok':
      {
        if (e.session === this.oSipSessionCall) {
          if (this.oSipSessionCall.bTransfering) {
            this.oSipSessionCall.bTransfering = false;
          }
          this.oSipSessionCall.bHeld = true;
        }
        break;
      }
      case 'm_local_hold_nok':
      {
        if (e.session === this.oSipSessionCall) {
          this.oSipSessionCall.bTransfering = false;
          console.log('<i>Failed to place remote party on hold</i>');
        }
        break;
      }
      case 'm_local_resume_ok':
      {
        if (e.session === this.oSipSessionCall) {
          this.oSipSessionCall.bTransfering = false;
          this.oSipSessionCall.bHeld = false;

          if (SIPml.isWebRtc4AllSupported()) {
          }
        }
        break;
      }
      case 'm_local_resume_nok':
      {
        if (e.session === this.oSipSessionCall) {
          this.oSipSessionCall.bTransfering = false;
          console.log('<i>Failed to unhold call</i>');
        }
        break;
      }
      case 'm_remote_hold':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('<i>Placed on hold by remote party</i>');
        }
        break;
      }
      case 'm_remote_resume':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('<i>Taken off hold by remote party</i>');
        }
        break;
      }
      case 'm_bfcp_info':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('BFCP Info: <i>' + e.description + '</i)>');
        }
        break;
      }

      case 'o_ect_trying':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('<i>Call transfer in progress...</i>')    ;                }
        break;
      }
      case 'o_ect_accepted':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('<i>Call transfer accepted</i>');
        }
        break;
      }
      case 'o_ect_completed':
      case 'i_ect_completed':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('<i>Call transfer completed</i>');
          if (this.oSipSessionTransferCall) {
            this.oSipSessionCall = this.oSipSessionTransferCall;
          }
          this.oSipSessionTransferCall = null;
        }
        break;
      }
      case 'o_ect_failed':
      case 'i_ect_failed':
      {
        if (e.session === this.oSipSessionCall) {
          console.log('<i>Call transfer failed</i>');
        }
        break;
      }
      case 'o_ect_notify':
      case 'i_ect_notify':
      {
        if (e.session === this.oSipSessionCall) {
          console.log("<i>Call Transfer: <b>" + e.getSipRespo + " " + e.description + "</b></i>");
          if (e.getSipResponseCode() >= 300) {
            if (this.oSipSessionCall.bHeld) {
              this.oSipSessionCall.resume();
            }
          }
        }
        break;
      }
      case 'i_ect_requested':
      {
        if (e.session === this.oSipSessionCall) {
          const s_message = "Do you accept call transfer to [" + e.getTransferDestinationFriendlyName() + "]?";//FIXME
          if (confirm(s_message)) {
            console.log("<i>Call transfer in progress...</i>");
            this.oSipSessionCall.acceptTransfer();
            break;
          }
          this.oSipSessionCall.rejectTransfer();
        }
        break;
      }
    }
  }

  sipCall = (s_type, phoneNumber: string) => {
    const audioRemote = document.getElementById('audio_remote');
    this.ringbacktone = document.getElementById('ringbacktone');
    this.oConfigCall = {
      audio_remote: audioRemote,
      video_local: null,
      video_remote: null,
      screencast_window_id: 0x00000000, // entire desktop
      bandwidth: { audio: undefined, video: undefined },
      video_size: { minWidth: undefined, minHeight: undefined, maxWidth: undefined, maxHeight: undefined },
      events_listener: { events: '*', listener: this.onSipEventSession },
      sip_caps: [
        { name: '+g.oma.sip-im' },
        { name: 'language', value: '\"en,fr\"' },
      ],
    };

    if (this.oSipStack && !this.oSipSessionCall) {
      // create call session
      this.oSipSessionCall = this.oSipStack.newSession(s_type, this.oConfigCall);
      // make call
      if (this.oSipSessionCall.call(`${phoneNumber}`) !== 0) {
        this.oSipSessionCall = null;
        console.log('Failed to make call');
        return;
      }
    } else if (this.oSipSessionCall) {
      console.log('<i>Connecting...</i>');
      this.oSipSessionCall.accept(this.oConfigCall);
    }
  }

  sipHangUp = () => {
    if (this.oSipSessionCall) {
      console.log('<i>Terminating the call...</i>');
      this.oSipSessionCall.hangup({ events_listener: { events: '*', listener: this.onSipEventSession } });
      // this.oSipSessionCall = null;
    }
  }

  sipToggleMute= () => {
    if (this.oSipSessionCall) {
      let i_ret;
      const bMute = !this.oSipSessionCall.bMute;
      i_ret = this.oSipSessionCall.mute('audio'/*could be 'video'*/, bMute);
      if (i_ret !== 0) {
        return;
      }
      this.oSipSessionCall.bMute = bMute;
    }
  }

  listenEvents() {
    return this.pjsipEvents.asObservable();
  }

  startRingbackTone = () => {
    try { this.ringbacktone.play(); } catch (e) { }
  }

  stopRingbackTone = () => {
    try { this.ringbacktone.pause(); } catch (e) { }
  }

  startRingTone = () => {

    try { this.ringtone.play(); } catch (e) { }
  }
  stopRingTone = () => {
    try { this.ringtone.pause(); } catch (e) { }
  }

  sipSendDTMF(value: string) {
    this.oSipSessionCall.dtmf(value);
  }

  sipTransfer = (s_destination) => {
    if (this.oSipSessionCall) {
      if (this.oSipSessionCall.transfer(s_destination) !== 0) {
        console.log('<i>Call transfer failed</i>');
        return;
      }
      console.log('<i>Transfering the call...</i>');
    }
  }
}
