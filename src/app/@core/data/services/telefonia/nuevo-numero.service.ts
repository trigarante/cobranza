import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_TELEFONIA;
@Injectable({
  providedIn: 'root'
})
export class NuevoNumeroService {

  constructor(private http: HttpClient) { }

  numeroSolicitud(idSolicitud, numero) {
    return this.http.post(`${base_url}/agregar-numero/solicitud/${idSolicitud}`, {
      numero
    });
  }

  numeroRegistro(idRegistro, numero) {
    return this.http.post(`${base_url}/agregar-numero/registro/${idRegistro}`, {
      numero
    });
  }

  getNumerosSolicitud(idSolicitud) {
    return this.http.get(`${base_url}/agregar-numero/numeros-solicitud/${idSolicitud}`);
  }

  getNumerosByIdRegistro(idRegistro) {
    return this.http.get(`${base_url}/agregar-numero/numeros-registro/${idRegistro}`);
  }
}
