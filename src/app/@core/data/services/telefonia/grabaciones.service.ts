import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const baseUrl = environment.SERVICIOS_TELEFONIA;

@Injectable({
  providedIn: 'root'
})
export class GrabacionesService {

  constructor(private http: HttpClient) { }

  getGrabacion(idAsterisk) {
    return this.http.get(`${baseUrl}/recordings/${idAsterisk}`);
  }
}
