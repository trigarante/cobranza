import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const url = environment.SERVICIOS_TELEFONIA;
const idEmpleado = sessionStorage.getItem('Empleado');


@Injectable({
  providedIn: 'root'
})
export class MonitoreoService {

  constructor(private http: HttpClient) { }

  getCurrentLlamadas() {
    return this.http.get(`${url}/monitoreo/${idEmpleado}`);
  }

  getUsersState() {
    return this.http.get(`${url}/monitoreo/edo-usr/${idEmpleado}`);
  }
}
