import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const url = `${environment.SERVICIOS_TELEFONIA}/tipificaciones`;
const idFLujoTipificacion = 1;

@Injectable({
  providedIn: 'root'
})
export class TipificarService {

  constructor(private http: HttpClient) { }
  getEstadoSolicitud() {
    return this.http.get(`${url}/edo-solicitud/${idFLujoTipificacion}`);
  }
  getEtiquetaSolicitud(idEstadoSolicitud) {
    return this.http.get(`${url}/etiquetas/${idEstadoSolicitud}`);
  }
  getSubetiquetaSolicitud(idEtiqueta) {
    return this.http.get(`${url}/sub-etiquetas/${idEtiqueta}`);
  }
}
