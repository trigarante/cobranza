import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
const base_url = environment.SERVICIOS_TELEFONIA_DATOS;
@Injectable({
  providedIn: 'root'
})
export class PeticioneAmiService {

  constructor(private http: HttpClient) { }

  public respSolicitud: EventEmitter<any> = new EventEmitter<any>();

  getSolicitud(extension: string, numero: string) {
    return this.http.get(`${base_url}/ami/asterisk-ami/${extension}/${numero}`);
  }
}
