import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';
import {environment} from '../../../../../../environments/environment';
import {EstadoUsuarioTelefoniaService} from '../estado-usuario-telefonia.service';
import {WebrtcService} from '../webrtc.service';

@Injectable({
  providedIn: 'root'
})
export class SocketsService {
  io = io(`${environment.SOCKETS_SERVICIOS_TELEFONIA}`, {
    autoConnect: true,
    auth: {
      usuario: Number(sessionStorage.getItem('Usuario')),
      empleado: Number(sessionStorage.getItem('Empleado')),
      extension: Number(localStorage.getItem('extension')),
    }
  });

  constructor(private telefoniaUsuariosService: EstadoUsuarioTelefoniaService,
              private webRTCService: WebrtcService,
              ) {
    this.estadoUsuario();
  }

  estadoUsuario() {
    this.io.on('connect', () => {
      console.log('Conectado');
    });

    this.io.on('disconnect', () => {
      this.webRTCService.sipUnRegister();
      this.telefoniaUsuariosService.usuarioInactivo(Number(sessionStorage.getItem('Usuario')));
      console.log('Desconectado');
    });
  }

  conectarExtension(idEstadoSesion, estado) {
    this.io.emit('estado-extension', {idEstadoSesion, estado});
  }

  tipificacion(idUsuario) {
    this.io.emit('tipificacion', {idUsuario});
  }

  delogueo(extension) {
    this.io.emit('deslogueo', {extension});
  }
}
