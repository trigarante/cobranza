import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
const base_url = environment.SERVICIOS_TELEFONIA_DATOS;
const idEmpleado = sessionStorage.getItem('Empleado');

@Injectable({
  providedIn: 'root'
})
export class LlamadasInService {

  constructor(private http: HttpClient) { }

  iniciarLlamadaIn(numero: string, idAsterisk, tipoLlamada, idSolicitud) {
    return this.http.post(`${base_url}/llamada-entrantes/inicio-llamada`, {numero, idAsterisk, idEmpleado, tipoLlamada, idSolicitud});
  }

  finLlamadaIn(idLlamada, idEstadoLlamada) {
    return this.http.put(`${base_url}/llamada-entrantes/fin-llamada/${idLlamada}/${idEstadoLlamada}`, null);
  }

  getLlamadas(fechaInicio, fechaFina) {
    return this.http.get(`${base_url}/llamada-entrantes/llamadas-recibidas/${idEmpleado}/${fechaInicio}/${fechaFina}`);
  }
}
