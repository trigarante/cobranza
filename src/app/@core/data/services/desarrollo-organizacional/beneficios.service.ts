import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BeneficiosService {

  url = environment.CORE_ESCUCHA + '/desarrollo-organizacional/beneficios';

  constructor(
    private http: HttpClient
  ) { }

  getBeneficioActual(): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/beneficioActivo`);
  }

}
