import { Injectable } from '@angular/core';
import {Socket} from 'ngx-socket-io';

@Injectable({
  providedIn: 'root',
})
export class SocketsWhatsappService {
  constructor(private socket: Socket) {
    this.abrirSocket();
    this.connectError();
  }

  getMessage() {
    return this.socket.fromEvent('mensaje-nuevo');
  }

  getMensaje() {
    return this.socket.fromEvent(`mensaje-nuevo${sessionStorage.getItem('idSolicitud')}`);
  }

  getMensajesSolicitudes() {
    const eventoTablaSolicitudes = `msg-solicitudes-${sessionStorage.getItem('Empleado')}`;
    return this.socket.fromEvent(eventoTablaSolicitudes);
  }

  getCantidadMensajesNoRespondidosSolicitudes() {
    return this.socket.fromEvent(`msg-noRespondido-${sessionStorage.getItem('Empleado')}`);
  }

  cerrarSocket() {
    this.socket.disconnect();
    console.log('Todos los sockets se cerraron');
  }

  cerrarSocketSolicitudes() {
    // console.log(this.socket.removeListener(`msg-solicitudes-${sessionStorage.getItem('Empleado')}`), '1');
    this.socket.removeListener(`msg-solicitudes-${sessionStorage.getItem('Empleado')}`, res => {console.log(res); });
  }

  cerrarSocketCampana() {
    // console.log(this.socket.removeListener(`msg-noRespondido-${sessionStorage.getItem('Empleado')}`), '2');
    this.socket.removeListener(`msg-noRespondido-${sessionStorage.getItem('Empleado')}`, res => {console.log(res); });
  }

  cerrarSocketGral() {
    // console.log(this.socket.removeListener(`mensaje-nuevo${sessionStorage.getItem('idSolicitud')}`), '3');
    this.socket.removeListener(`mensaje-nuevo${sessionStorage.getItem('idSolicitud')}`, res => {console.log(res); });
  }

  abrirSocket() {
    this.socket.connect();
    this.socket.on('connect', () => {
      console.log('socket abierto');
      const idEmpleado = sessionStorage.getItem('Empleado');
      if (idEmpleado) {
        console.log(`emitiendo header-icon-${idEmpleado}`);
        this.socket.emit(`headerRoom`, `header-icon-${idEmpleado}`);
      }
    });
  }

  connectError() {
    let counter = 0;
    this.socket.on('connect_error', () => {
      setTimeout(() => {
        this.socket.connect();
        console.log('error, intentando conectar');
        counter = counter + 1;
        if (counter === 3) {
          this.cerrarSocket();
          console.log('Se cerraron los sockets debido a que no hubo una conexión estable');
        }
      }, 3000);
    });
  }

  fromEventGeneral(tipo) {
    switch (tipo) {
      case 1:
        console.log('evento mensaje-nuevo');
        return this.socket.fromEvent(`mensaje-nuevo${sessionStorage.getItem('idSolicitud')}`);
        break;

      case 2:
        console.log('evento msg-solicitudes');
        const eventoTablaSolicitudes = `msg-solicitudes-${sessionStorage.getItem('Empleado')}`;
        return this.socket.fromEvent(eventoTablaSolicitudes);
        break;

      case 3:
        console.log('evento msg-noRespondido');
        return this.socket.fromEvent(`msg-noRespondido-${sessionStorage.getItem('Empleado')}`);
        break;
    }
  }
  // checkStatus() {
  //   this.ioSocket.on('connect', () => {
  //       // const test = new SocketsWhatsappService();
  //     const _this: any = this;
  //     _this.config.options.query = sessionStorage.getItem('idSolicitud');
  //     console.log('se conectó.....');
  //   });
  //
  //   this.ioSocket.on('disconnect', () => {
  //     const _this: any = this;
  //     _this.config.options.query = {};
  //     const algo: any = SocketsWhatsappService;
  //     algo.destruct;
  //     console.log('se desconectó.....');
  //   });
  // }
  //
  // /** Funcion que recibe el evento a donde se mandará el socket, el json a actualizar y el tipo de solicitud**/
  // enviarMensaje(json) {
  //   this.ioSocket.emit('mensaje', json);
  // }
  //
  // leeMensaje(evento) {
  //   return this.fromEvent(evento);
  // }
}
