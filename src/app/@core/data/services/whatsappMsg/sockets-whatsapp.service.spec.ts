import { TestBed } from '@angular/core/testing';

import { SocketsWhatsappService } from './sockets-whatsapp.service';

describe('SocketsWhatsappService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketsWhatsappService = TestBed.get(SocketsWhatsappService);
    expect(service).toBeTruthy();
  });
});
