export interface ImagenesGaceta {
  id: number;
  idGaceta: number;
  numero: number;
  archivo: string;
  link: string;
  activo: number;
}
