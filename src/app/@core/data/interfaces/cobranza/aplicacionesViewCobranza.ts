export interface AplicacionesViewCobranza {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  fechaRegistro: Date;
  archivo: string;
  cantidadPagos: number;
  tipoPago: string;
  productoSocio: string;
  datos: string;
  idEstadoPoliza: number;
  estado: string;
  idArea: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  correo: string;
  idSede: number;
  idGrupo: number;
  idPuesto: number;
  idEmpresa: number;
  idSubarea: number;
  subarea: string;
  idCliente: number;
  idSolictud: number;
  descripcion: string;
  idSubRamo: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  idRecibo: number;
  idEstadoRecibo: number;
  fechaPromesaPago: Date;
  archivoSubido: number;
  carpetaCliente: string;
  estadoRecibo: string;
  numeroSerie: string;
  nombreComercial: string;
  tipoSubramo: string;
  idSocio: number;
  idPago: number;
}
