export interface SesionUsuario {
  id?: number;
  idUsuario?: number;
  fechaInicio?: Date;
  fechaFin?: Date;
  idEstadoSesion: number;
}
