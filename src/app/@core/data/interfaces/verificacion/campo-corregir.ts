export interface CampoCorregir {
  id: number;
  idTabla: number;
  campo: string;
  descripcion: string;
  activo: boolean;
}
