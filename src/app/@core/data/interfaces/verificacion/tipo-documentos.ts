export interface TipoDocumentos {
  id: number;
  idTabla: number;
  nombre: string;
  descripcion: string;
  activo: boolean;
}
