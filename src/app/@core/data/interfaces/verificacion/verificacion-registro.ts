export interface VerificacionRegistro {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  idEmpleado: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  fechaCreacion: Date;
  fechaInicio: Date;
  fechaConclusion: Date;
  // Las variables que están abajo son exclusivas de la vista
  poliza?: string;
  idCliente?: number;
  nombreCliente?: string;
  apellidoPaternoCliente?: string;
  apellidoMaternoCliente?: string;
  nombre?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  nombreEmpleadoMesa?: string;
  apellidoPaternoEmpleadoMesa?: string;
  apellidoMaternoEmpleadoMesa?: string;
  carpetaCliente?: string;
  carpetaRegistro?: string;
  archivoPago?: string;
  archivoInspeccion?: string;
  estadoVerificacion?: string;
  idPago?: number;
  subarea: string;
  idProducto: number;
  primaNeta: Number;
  fechaInicioPoliza: Date;
  estadoRecibo: string;
  nombreComercial: string;
  cantidadPagos: string;
  idFlujoPoliza?: number;
  idPais: number;
  fechaPago: Date;
  correo: string;
  numeroSerie: string;
  estadoPoliza: string;
  telefonoMovil: string;
  estadoPago: string;
  idTipoBandejaRenovacion?: number;
  idEmpleadoAutorizacion?: number;
  idEmpleadoRegistro: number;
  horasRestantes?: string;
  fechaLiquidacion?: Date;
  idSolicitud?: number;
}
