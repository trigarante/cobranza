export interface Departamento {
  id: number;
  idArea: number;
  nombre: string;
  activo: number;
}
