export interface ClientePeru {
   id: number;
   idPais: number;
   nombre: string;
   paterno: string;
   materno: string;
   cp?: number;
   calle: string;
   numInt: string;
   numExt: string;
   idColoniaPeru?: number;
   capitales: string;
   genero: string;
   telefonoFijo: string;
   telefonoMovil: string;
   correo: string;
   fechaNacimiento: Date;
   dni: string;
   ruc?: string;
   fechaRegistro: Date;
   archivo: string;
   nombrePaises: string;
   razonSocial: number;
   archivoSubido: number;
}

