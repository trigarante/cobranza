export interface Domiciliacion {
  banco: string;
  primaNeta: string;
  estatusFinanzas: string;
  estatusPago: string;
}
