

export interface StepsCotizador {
  idSolicitud: number;
  idProductoCliente: number;
  idCliente: number;
  idRegistro: number;
  recibos: number;
  pagos: number;
}


