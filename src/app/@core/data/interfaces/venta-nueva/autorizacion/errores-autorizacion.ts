

export interface ErroresAutorizacion {
  id: number;
  idAutorizacionRegistro: number;
  idEmpleado: number;
  idEstadoCorreccion: number;
  idTipoDocumento: number;
  idEmpleadoCorreccion: number;
  correcciones: string;
}



