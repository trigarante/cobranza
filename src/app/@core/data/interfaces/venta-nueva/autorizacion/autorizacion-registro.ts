

export interface AutorizacionRegistro {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  // Las variables que están abajo son exclusivas de la vista
  idEmpleado?: number;
  poliza?: string;
  idCliente?: number;
  nombreCliente?: string;
  apellidoPaternoCliente?: string;
  apellidoMaternoCliente?: string;
  carpetaCliente?: string;
  archivoInspeccion?: string;
  carpetaRegistro?: string;
  archivoPago?: string;
  estadoVerificacion?: string;
  idPago?: number;
  idProducto: number;
  idFlujoPoliza?: number;
}

export interface AuthRegistro {
  id: number;
  idEstadoVerificacion: number;
  idEmpleado: number;
  idSubarea: number;
  idFlujoPoliza: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
}

