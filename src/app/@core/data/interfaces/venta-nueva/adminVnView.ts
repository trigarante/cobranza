import {Observable} from "rxjs";
import {Registro} from "./registro";



export interface AdminVnView {
  id: number;
  poliza: string;
  fechaInicio: string;
  primaNeta: number;
  fechaRegistro: string;
  archivo: string;
  cantidadPagos: number;
  archivoPago: string;
  datosPagos: string;
  nombreComercial: string;
  nombreEmpleado: string;
  numeroPagos: number;
  idEstadoRecibo: number;
  estadoRecibo: string;
  numeroSerie: string;
  idEstadoPoliza: number;
  estadoPoliza: string;
  tipoPago: string;
  estado: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  correo: string;
  subarea: string;
  idCliente: number;
  idRecibo: number;
  fechaPromesaPago: number;
  datos: string;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  archivoSubido: number;
  carpetaCliente: string;
  tipoSubramo: string;
  estadoPago: string;
  telefonoMovil: string;
  cierre: number;
}
export interface ModificarDatosPoliza {
  idRegistro: number;
  poliza: string;
  idSocio: number;
  idRamo: number;
  idSubRamo: number;
  idProductoSocio: number;
}

export abstract class AdminVnViewData {
  abstract get(): Observable<AdminVnView[]>;
  abstract getCanceladas(): Observable<AdminVnView[]>;
  abstract getAplicacionViewById(idAplicacionViwe): Observable<AdminVnView>;
  abstract getRegistroConValidacion(idFlujoPoliza: number): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionCobranza(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionSubsecuentes(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionGastosMedicos(): Observable<AdminVnView[]>;
  abstract crearAutorizacionnGastosMedicos(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionHogar(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionPyme(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionVida(): Observable<AdminVnView[]>;
  abstract getAplicacionFechaInicio(): Observable<AdminVnView[]>;
  abstract getAllRegistroByIdUsuario(): Observable<AdminVnView[]>;
  abstract getAllRegistroByIdUsuarioReno(): Observable<AdminVnView[]>;
  abstract getAllRegistroGM(idFlujoPoliza: number): Observable<AdminVnView[]>;
  abstract getAllRegistroByIdUsuarioAnIddFlujoPoliza(idFlujoPoliza: number): Observable<AdminVnView[]>;
  abstract getTablaByIdUsuario(tabla: string): Observable<AdminVnView[]>;
  abstract getCobranzaByIdUsuario(): Observable<AdminVnView[]>;
  abstract getAllTabla(tabla: string): Observable<AdminVnView[]>;
  abstract getAllCobranza(): Observable<AdminVnView[]>;
  abstract getReciboAPagar(idRegistro): Observable<number>;
  abstract getFinanzas (): Observable<AdminVnView[]>;
  abstract getEmisiones(): Observable<AdminVnView[]>;
  abstract getAllTablaSinAplicar(tabla: string): Observable<AdminVnView[]>;
  abstract getAllCobranzaSinAplicar(): Observable<AdminVnView[]>;
  abstract getEndosos(): Observable<AdminVnView[]>;
  abstract getCobranza(): Observable<AdminVnView[]>;
  abstract putSocketPorAplicar(idRegistro, registro): Observable <Registro>;
  abstract putModificarPoliza(idRegistro, regsitro): Observable<ModificarDatosPoliza>;
  abstract getPeru (): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionVn(): Observable<AdminVnView[]>;
  abstract getAllRegistroByIdUsuarioPeru(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionPeru(): Observable<AdminVnView[]>;
  abstract getRegistroConValidacionEcommerce(): Observable<AdminVnView[]>;
  abstract cierreFinanzas(fechaCierre): Observable<any>;
}

