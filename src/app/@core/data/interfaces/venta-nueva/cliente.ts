export interface Cliente {
  id: number;
  archivo: string;
  archivoSubido: number;
  calle: string;
  correo: string;
  cp: string;
  curp: string;
  dni: string;
  genero: string;
  idColonia: number;
  idColoniaPeru: number;
  idPais: number;
  materno: string;
  nombre: string;
  numExt: string;
  numInt: string;
  paterno: string;
  razonSocial: number;
  rfc: string;
  ruc: string;
  telefonoFijo: string;
  telefonoMovil: string;

  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
}
