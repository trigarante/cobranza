import {Observable} from 'rxjs';

export interface Periodicidad {
  id: number;
  nombre: string;
  cantidadPagos: number;
  activo: number;
}

export abstract class PeriodicidadData {
  abstract get(): Observable<Periodicidad[]>;
  abstract getById(idPeriodicidad: number): Observable<Periodicidad>;
}


