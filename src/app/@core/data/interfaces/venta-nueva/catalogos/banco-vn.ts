export interface BancoVn {
  id: string;
  nombre: string;
  activo: number;
  idCarrier: number;
}
export interface BancoExterno {
  id_banco: string;
  nombre: string;
  abreviacion: number;
}

export interface MsiView {
  id: number;
  idAseguradora: number;
  idBanco: number;
  idMsi: number;
  nombreAseguradora: string;
  nombreBanco: string;
  nombreMsi: number;
}


