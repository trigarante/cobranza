
export interface FormaPago {
  id: number;
  description: string;
  activo: number;
}
export interface FormaPagoOnline {
  id: number;
  descripcion: string;
  activo: number;
}
