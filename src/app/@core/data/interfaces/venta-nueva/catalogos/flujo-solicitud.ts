import {Observable} from 'rxjs';

export interface FlujoSolicitud {
  id: number;
  descripcion: string;
  activo: number;
}
export abstract class FlujoSolicitudData {
  abstract get(): Observable<FlujoSolicitud[]>;

  abstract post(flujo): Observable<FlujoSolicitud>;
  abstract getflujoById(idflujo): Observable<FlujoSolicitud>;
  abstract put(idflujo, flujo): Observable<FlujoSolicitud>;
}
