import {Observable} from 'rxjs';

export interface TarjetasBancoVn {
  idTarjetas: number;
  descripcion: string;
  idBanco: number;
  nombreBanco: string;
  idCarrier: number;
  carrier: string;
  tarjeta: string;
  activo: number;
}

export abstract class TarjetasBancoVnData {
  abstract get(): Observable<TarjetasBancoVn[]>;
  abstract getTarjetaById(id): Observable<TarjetasBancoVn>;
}
