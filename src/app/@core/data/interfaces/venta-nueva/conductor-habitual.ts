
export interface ConductorHabitual {
  id: number;
  idPais: number;
  idRegistro: number;
  nombre: string;
  paterno: string;
  materno: string;
  correo: string;
  cp: number;
  idColonia: string;
  calle: string;
  numInt: string;
  numExt: string;
  genero: string;
  telefonoFijo: string;
  telefonoMovil: string;
  fechaNacimiento: Date;
  fechaRegistro: Date;
}


