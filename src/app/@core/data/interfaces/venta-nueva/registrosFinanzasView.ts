import {Observable} from 'rxjs';
export interface RegistrosFinanzasView {
  id: number;
  poliza: string;
  fechaRegistro: Date;
  idEstadoPoliza: number;
  idFlujoPoliza: number;
  idCliente: number;
  nombre: string;
  paterno: string;
  materno: string;
  idProducto: number;
  estadoPoliza: string;
  flujoPoliza: string;
  idSubRamo: number;
}

export abstract class RegistrosFinanzasViewData {
  abstract get(): Observable<RegistrosFinanzasView[]>;
  abstract getRegistrosFinanzasViewById(idRegistrosFinanzasView): Observable<RegistrosFinanzasView>;
}
