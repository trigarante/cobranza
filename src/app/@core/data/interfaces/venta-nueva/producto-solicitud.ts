import { Observable } from 'rxjs';

export interface ProductoSolicitudvn {
  id: number;
  tipo: string;
  datos: string;
  nombre: string;
  correo: string;
  numero: string;
  genero: string;
  edad: string;
  cp: string;
  clave: string;
  marca: string;
  modelo: string;
  paquete: string;
  servicio: string;
  descuento: number;
  movimiento: string;
  aseguradora: string;
  idProspecto: number;
  descripcion: string;
  numeroMotor: string;
  numeroPlacas: string;
  fechaNaimiento: string;
  idTipoSubramo: number;
}
export interface ProductoSolicitudvnCorto {
  id: number;
  idProspecto: number;
  idTipoSubRamo: number;
  datos: string;
}

