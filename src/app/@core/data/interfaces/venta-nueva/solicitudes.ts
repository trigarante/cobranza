

export interface Solicitudes {
  id: number;
  idCotizacionAli: number;
  cotizo: string;
  idEmpleado: number;
  idEstadoSolicitud: number;
  idSubArea: number;
  idSubRamo: number;
  peticion: any;
  respuesta: any;
  estado: string;
  fechaSolicitud: string;
  comentarios: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  numeroProspecto: string;
  nombreProspecto: string;
  correoProspecto: string;
  idEtiquetaSolicitud: number;
  idSubetiquetaSolicitud: number;
  idFlujoSolicitud: number;
  etiquetaSolicitud: string;
  flujoSolicitud: string;
  idTipoContacto: number;
}

export interface SolicitudesView {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  idEstadoSolicitud: number;
  fechaSolicitud: Date;
  comentarios: string;
  idCotizacion: number;
  peticion: string;
  respuesta: string;
  fechaCotizacion: Date;
  fechaActualizacion: Date;
  estado: string;
  activo: number;
  idUsuario: number;
  idBanco: number;
  idCandidato: number;
  idPuesto: number;
  idTipoPuesto: number;
  puestoDetalle: string;
  idSubarea: number;
  subArea: string;
  idPrecandidato: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idProducto: number;
  idTipoContacto: number;
  idProspecto: number;
  idSubRamo: number;
  nombreProspecto: string;
  correoProspecto: string;
  numeroProspecto: string;
  idEtiquetaSolicitud: number;
  idFlujoSolicitud: number;
  etiquetaSolicitud: string;
  etiquetaSolicitudActivo: number;
  idLlamada: number;
}

export interface Solicitud {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  idEstadoSolicitud: number;
  idEtiquetaSolicitud: number;
  idSubetiquetaSolicitud: number;
  idFlujoSolicitud: number;
  comentarios: string;
  idFase: number;
  idSubarea: number;
}

export interface SolicitudesVN {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  fechaSolicitud: string;
  estado: string;
  idSubArea: number;
  subArea: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idProducto: number;
  idProspecto: number;
  nombreProspecto: string;
  numeroProspecto: string;
  etiquetaSolicitud: string;
  idTipoPeticion: number;
  idSolicitudes: number;
}

export interface SolicitudesVNInterno {
  id: number;
  idCotizacionAli: number;
  idEmpleado: number;
  fechaSolicitud: string;
  idDepartamentoRh: number;
  descripcionDepartemento: string;
  nombreaCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  idProducto: number;
  idProspecto: number;
  nombreEmpleado: string;
  apellidoPaternoEmpleado: string;
  apellidoMaternoEmpleado: string;
  numeroEmpleado: string;
  numero: string;
  estado: string;
}

export interface EtiquetaSolicitud {
  idEstadoSolicitud: number;
  idEtiquetaSolicitud: string;
  idSubetiquetaSolicitud: number;
  comentarios: string;
}


