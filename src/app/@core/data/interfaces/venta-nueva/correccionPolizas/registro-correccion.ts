import {Observable} from 'rxjs';

export interface RegistroCorreccion {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idEstadoPoliza: number;
  idTipoPoliza: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  idPeriodicidad: number;
  periodicidad: string;
  productoSocio: string;
  tipoRamo: string;
  tipoSubramo: string;
  idSocio: number;
  idRamo: number;
  idSubramo: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  fechaRegistro: Date;
  datos: any;
  archivo: string;
  idCliente?: number;
  cantidadPagos: number;
  archivoSubido: number;
  fechaFin: Date;
  idPais: number;
  oficina?: string;
}

export interface RegistroCorreccionSoloDatos {
  idSocio: number;
  idRamo: number;
  idSubRamo: number;
  idProductoSocio: number;
  fechaInicio: Date;
  idPeriodicidad: number;
  poliza: string;
  oficina?: string;
  primaNeta: number;
}

export abstract class RegistroCorreccionData {
  abstract get(): Observable<RegistroCorreccion[]>;
  abstract getRegistroById(idRegistro): Observable<RegistroCorreccion>;
  abstract getRegistroByIdADM(idRegistro): Observable<RegistroCorreccion>;
  abstract getRegistroFinanzasById(idRegistro): Observable<RegistroCorreccion>;
  abstract getNoSerieExistente(numSerie: string): Observable<string>;
  abstract post(registro): Observable<RegistroCorreccion>;
  abstract postRF(registro): Observable<RegistroCorreccion>;
  abstract postInOne(any): Observable<RegistroCorreccion>;
  abstract put(idRegistro, registro): Observable<RegistroCorreccion>;
  abstract putRF(idRegistro, registro): Observable<RegistroCorreccion>;
  abstract updateArchivo(idRegistro: number, idCarpeta: string): Observable<any>;
  abstract updateEstadoPoliza(idRegistro: number, estado: number): Observable<any>;
  abstract updateFlujoPoliza(idRegistro: number, idFlujoPoliza: number): Observable<any>;
  abstract updateFlujoAndEstadoPoliza(idRegistro: number, idFlujoPoliza: number, idEstadoPoliza): Observable<any>;
  abstract updateRegistroPolizaSoloDatos(datos: RegistroCorreccionSoloDatos, id: number): Observable<Boolean>;
  abstract modificarPolizaSocket(json): Observable<any>;
}
