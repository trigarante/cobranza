
export interface CotizacionesAli {
  id: number;
  idCotizacion: number;
  idCategoriaSocio: number;
  cotizo: number;
  peticion: string;
  respuesta: string;
  interesado: number;
  fechaCotizacion: string;
  prima: number;
  fechaActualizacion: string;
  idSubRamo: number;
}

