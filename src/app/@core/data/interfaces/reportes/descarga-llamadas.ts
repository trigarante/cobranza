export interface DescargaLlamadas {
  id: number;
  idSubarea: number;
  fechaRegistro: string;
  fechaInicio: string;
  horaInicio: string;
  area: string;
  subarea: string;
  ejecutivo: string;
  numero: string;
  duracion: string;
  idSolicitud: number;
  subetiqueta: string;
  tipo: string;
  estado: string;
}
