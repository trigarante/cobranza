export interface ProductividadEjecutivo {
  idEmpleado: string;
  tipoSubarea: string;
  nombreEjecutivo: string;
  antiguedad: string;
  contactos: number;
  polizasEmitidas: number;
  primaNetaEmitida: number;
  polizasCobradas: number;
  primaNetaCobrada: number;
  polizasAplicadas: number;
  primaNetaAplicada: number;
  conversion?: number;
}
