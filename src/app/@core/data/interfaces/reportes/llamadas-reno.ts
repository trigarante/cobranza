export interface LlamadasReno {
  idEmpleado: number;
  ejecutivo: string;
  acd: number;
  tiempoMedioAcd: string;
  out: number;
  tiempoMedioOut: string;
  tiempoPausa: string;
  tiempoProductivo: string;
  llamadasAbandonadas: string;
  horaLogueo: string;
  tiempoLogueo: string;
}
