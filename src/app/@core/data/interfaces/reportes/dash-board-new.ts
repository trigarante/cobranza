export interface DashBoardNew {
  idEmpleado: number;
  idSubarea: number;
  subarea: string;
  ejecutivo: string;
  leads: number;
  llamadas: number;
  contactoErroneo: number;
  contactados: number;
  noContactados: number;
  enMarcacion1a4: number;
  enMarcacion5: number;
  contactoEfectivo: number;
  horaLogueo: number;
  tiempoLogueo: number;
  pausas: number;
  tiempoPausaa: string;
  tiempoProductivo: number;
}
