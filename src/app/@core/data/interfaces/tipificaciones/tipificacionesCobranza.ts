export interface tipificacionCobranza{
  id: number,
  estado: string,
}

export interface tipificacionesCobranzaEtiqueta{
  id: number,
  idEstadoSolicitud: number,
  descripcion: string,
}

export interface tipificacionSubEtiqueta{
  id,
  idEtiquetaSolicitud,
  descripcion,
}

export interface tipificarPolizaCobranza{
  idRegistro,
  idEstatusPolizaCobranza,
  descripcion,
}
