import {Observable} from 'rxjs';
export interface SolicitudEndoso {
  id: number;
  idRegistro: number;
  idEstadoSolicitudEndoso: number;
  idEstadoEndoso: number;
  idEmpleado: number;
  idEmpleadoSolicitante: number;
  prima: number;
  idPenalizacionEndoso: number;
  idTipoSolicitudEndoso: number;
  fechaSolicitud: string;
  fechaResolucion: string;
  comentarios: string;
  activo: number;
  idMotivoEndoso: number;
  fechaAsignacion: Date;
  fechaTramiteAseguradora: Date;
  fechaTermino: Date;
  fechaResolucionAseguradora: Date;
  // Datos de la vista
  motivoEndosoDesc?: string;
  tipoEndosoDesc?: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombreSolicitante: string;
  apellidoPaternoSolicitante: string;
  apellidoMaternoSolicitante: string;
  nombreEmisor: string;
  apellidoPaternoEmisor: string;
  apellidoMaternoEmisor: string;
  estadoSolicitudDesc?: string;
  estadoEndoso: string;
  poliza?: string;
  primaNeta?: number;
  cantidadPagos?: number;
  tipoPago?: number;
  productoSocio?: string;
  estado?: string;
  idTipoEndoso: number;
  subareaRegistro: string;
  archivo: string;
  idProductoCliente: number;
  idSubRamo: number;
  idSolicitud: number;
  idCliente: number;
  alias: string;
}

export abstract class SolicitudEndosoData {
  abstract get(idEstadoSolicitudEndoso: number): Observable<SolicitudEndoso[]>;
  abstract post(solicitudEndoso: SolicitudEndoso): Observable<SolicitudEndoso>;
  abstract getSolicitudEndosoById(idSolicitudEndoso): Observable<SolicitudEndoso>;
  // abstract getSolicitudEndosoPendiente(): Observable<SolicitudEndosoPendiente[]>;
  abstract getParaEndoso(idRegistro): Observable<SolicitudEndoso[]>;
  abstract put(idSolicutudEndoso, solicitudEndoso): Observable<SolicitudEndoso>;
  abstract putfecha(idSolicutudEndoso, solicitudEndoso, tipoFecha): Observable<SolicitudEndoso>;
  abstract updateEmpleadoSolicitud(idSolicitud: number, idEmpleado: number): Observable<any>;
  abstract putEstadoEndoso (idSolicitudEndoso: number, estadoEndoso: number): Observable<SolicitudEndoso>;
  abstract bajaEndoso (idSolicitudEndoso: number): Observable<any>;
  abstract Completadas(): Observable<SolicitudEndoso[]>;
  abstract getByIdEmpleado(idEstadoSolicitudEndoso: number): Observable<SolicitudEndoso[]>;
}
