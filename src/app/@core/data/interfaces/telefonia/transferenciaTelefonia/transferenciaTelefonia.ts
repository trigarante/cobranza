export interface TransferenciaTelefonia {
  id: number;
  nombreCampania: string;
  numeroTelefono: string;
}
