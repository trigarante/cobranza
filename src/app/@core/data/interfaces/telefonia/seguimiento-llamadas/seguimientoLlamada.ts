export interface SeguimientoLlamada {
  id?: number;
  eventtype?: string;
  eventtime?: Date;
  userdeftype?: string;
  cid_name?: string;
  cid_num?: string;
  cid_ani?: string;
  cid_rdnis?: string;
  cid_dnid?: string;
  exten?: string;
  context?: string;
  channame?: string;
  appname?: string;
  appdata?: string;
  amaflags?: number;
  accountcode?: string;
  peeraccount?: string;
  uniqueid?: string;
  linkedid?: string;
  userfield?: string;
  peer?: string;
  tiempoEspera?: string;
  aplicacion?: string;
}
