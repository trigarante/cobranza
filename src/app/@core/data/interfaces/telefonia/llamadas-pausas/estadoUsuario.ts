export interface EstadoUsuario {
  id: number;
  idSesionUsuario: number;
  idEstadoSesion: number;
  fechaInicio: Date;
  idMotivosPausa: number;
  fechaFinal: Date;
}
