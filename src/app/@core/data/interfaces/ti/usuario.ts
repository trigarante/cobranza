export interface Usuario {
  id?: number;
  usuario?: string;
  password?: string;
  idCorreo?: string;
  idGrupo?: number;
  idTipo?: number;
  idSubarea?: number;
  fechaCreacion?: Date;
  datosUsuario?: string;
  estado: number;
  asignado?: number;
  imagenUrl?: string;
  nombre?: string;
  idEmpleado?: number;
  tipoUsuario?: string;
  idPermisosVisualizacion?: number;
  idTipoSubarea?: number;
  idPuesto?: number;
}
