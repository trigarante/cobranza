
export interface Drive {
    tipo: String;
    fecha: Date;
    numero: string;
    downloadLink: String;
}
