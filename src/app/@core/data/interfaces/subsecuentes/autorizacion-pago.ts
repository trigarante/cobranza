export interface AutorizacionPago {
    id: number;
    idPago: number;
    idEstadoVerificacion;
    idEmpleado: number;
    numero: number;
    verificadoPago: number;
    fechaCreacion: Date;
    fechaInicio: Date;
    fechaConclusion: Date;
  
    // Datos de la vista
    estadoVerificacion?: string;
    idProducto?: number;
    idRegistro?: number;
    numeroRecibo?: number;
    poliza?: string;
    idCliente?: number;
    nombreCliente?: string;
    apellidoPaternoCliente?: string;
    apellidoMaternoCliente?: string;
    carpetaCliente?: string;
    carpetaRegistro?: string;
    flujoPoliza?: string;
    idSubarea?: number;
    nombre?: string;
    apellidoPaterno?: string;
    apellidoMaterno?: string;
    archivoPago?: string;
    fechaPago: Date;
  
    // Datos extra de la vista de verificacion
    nombreComercial: string;
    estadoRecibo: string;
    estadoPago: string;
    primaNeta: number;
    inicioVigencia: Date;
    subarea: string;
    correo: string;
    numeroSerie: string;
    telefonoMovil: string;
    estadoPoliza: string;
  }