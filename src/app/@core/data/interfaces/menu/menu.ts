
export default interface Menu{
  name: string;
  icon?: string;
  alias?: string;
  children?: Menu[];
  rutas?: string;
  grandChildren?: Menu[];
  gen4?: Menu[];
  permiso: any;
}
