
export interface SubRamo {
  id: number;
  idRamo: number;
  idTipoSubRamo: number;
  prioridad: number;
  descripcion: string;
  activo: number;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  idSocio: number;
  tipoSubRamo: any;
  tipoRamo: any;
}

