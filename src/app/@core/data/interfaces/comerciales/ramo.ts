
export interface Ramo {
  id: number;
  idSocio: number;
  tipoRamo: string;
  idTipoRamo: number;
  descripcion: string;
  prioridad: number;
  prioridades: string;
  activo: number;
  alias: string;
  idEstado: number;
  idEstadoSocio: number;
  estado: string;
  nombreComercial: string;
}

