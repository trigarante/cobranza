
export interface ProductoSocios {
  idSubRamo: number;
  idCobertura: number;
  prioridad: number;
  nombre: string;
  activo: number;
  idTipoProducto: number;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  descripcion: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  tipoSubRamo: any;
  tipoRamo: any;
  tipoProducto: any;
  id: number;
}
export interface ProductoNameCorto {
  id: number;
  idSubRamo: number;
  idTipoProducto: number;
  nombre: string;
  prioridad: number;
  activo: number;
}

