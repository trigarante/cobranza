import {Observable} from 'rxjs';

export interface TipoCategoria {
  id: number;
  tipo: string;
  activo: number;
}

export abstract class TipoCategoriaData {
  abstract get(): Observable<TipoCategoria[]>;

  abstract post(tipoCategoria): Observable<TipoCategoria>;

  abstract getTipoCategoriaById(idTipoCategoria): Observable<TipoCategoria>;

  abstract put(idTipoCategoria, tipoCategoria): Observable<TipoCategoria>;
}
