
export interface SubCategoria {
  id: number;
  detalle: string;
  regla: string;
  idDivisas: number;
  activo: number;
  idCategoria: number;
}
