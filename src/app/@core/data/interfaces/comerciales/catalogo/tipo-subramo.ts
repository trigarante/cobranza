import {Observable} from 'rxjs';
export interface TipoSubramo {
  id: number;
  tipo: string;
  activo: number;
}

