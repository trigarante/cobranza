import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrigaranteEscuchaModalComponent } from './trigarante-escucha-modal.component';

describe('TrigaranteEscuchaModalComponent', () => {
  let component: TrigaranteEscuchaModalComponent;
  let fixture: ComponentFixture<TrigaranteEscuchaModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrigaranteEscuchaModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrigaranteEscuchaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
