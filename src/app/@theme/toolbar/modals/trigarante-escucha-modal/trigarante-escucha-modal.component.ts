import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {TrigaranteEscuchaService} from '../../../../@core/data/services/desarrollo-organizacional/trigarante-escucha.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'ngx-trigarante-escucha-modal',
  templateUrl: './trigarante-escucha-modal.component.html',
  styleUrls: ['./trigarante-escucha-modal.component.scss']
})
export class TrigaranteEscuchaModalComponent implements OnInit {
  trigaranteEscucha: FormGroup = new FormGroup({
    idTema: new FormControl('', Validators.required),
    idMotivoTrigaranteEscucha: new FormControl('', Validators.required),
    idEmpleado: new FormControl(sessionStorage.getItem('Empleado')),
    comentarios: new FormControl('', Validators.required),
    propuesta: new FormControl('', Validators.required),
  });
  temaTrigaranteEscucha: any[];
  motivoTrigaranteEscucha: any[];

  constructor(
    private trigaranteEscuchaService: TrigaranteEscuchaService,
    private notificacionesService: NotificacionesService,
    public  dialogRef: MatDialogRef<TrigaranteEscuchaModalComponent>
  ) {}

  ngOnInit(): void {
    this.getActivos();
  }

  getActivos() {
    this.trigaranteEscuchaService.getActivos(1).subscribe({
      next: data => {
        if (data.length === 0) {
          this.notificacionesService.advertencia('No hay Temas activos');
        }
        this.temaTrigaranteEscucha = data;
      }
    });
  }

  getByIdTema() {
    this.trigaranteEscucha.controls.idMotivoTrigaranteEscucha.setValue(null);
    this.trigaranteEscuchaService.getByIdTema(this.trigaranteEscucha.controls.idTema.value).subscribe({
      next: data => {
        if (data.length === 0) {
          this.notificacionesService.advertencia('No hay motivos activos para este tema');
        }
        this.motivoTrigaranteEscucha = data;
      }
    });
  }

  async registrar() {
    const respuesta = await this.notificacionesService.pregunta('¿Estás seguro de que la información es correcta?');
    if (respuesta.value) {
      this.notificacionesService.carga();
      this.trigaranteEscuchaService.post(this.trigaranteEscucha.getRawValue()).subscribe({
        next: data => this.notificacionesService.exito(`Se recibio la peticion con folio: ${data.folio} le daremos seguimiento tan pronto como podamos, este al pendiente de su correo `).then(() => {this.cerrar(); }),
        error: () => this.notificacionesService.error().then(() => this.cerrar())
      });
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

}
