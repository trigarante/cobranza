import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagenesEsquemasComponent } from './imagenes-esquemas.component';

describe('ImagenesEsquemasComponent', () => {
  let component: ImagenesEsquemasComponent;
  let fixture: ComponentFixture<ImagenesEsquemasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImagenesEsquemasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagenesEsquemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
