import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotivoPausaComponent } from './motivo-pausa.component';

describe('MotivoPausaComponent', () => {
  let component: MotivoPausaComponent;
  let fixture: ComponentFixture<MotivoPausaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotivoPausaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotivoPausaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
