import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {ImagenesSocios} from '../../../../@core/data/interfaces/venta-nueva/imagenes-socios';
import {ImagenesSociosService} from '../../../../@core/data/services/venta-nueva/imagenes-socios.service';
import {ImagenesEsquemasComponent} from '../imagenes-esquemas/imagenes-esquemas.component';
import {ImagenesEsquemasService} from '../../../../@core/data/services/rrhh/imagenes-esquemas.service';

@Component({
  selector: 'app-procedimientos-button-modal',
  templateUrl: './procedimientos-button-modal.component.html',
  styleUrls: ['./procedimientos-button-modal.component.scss']
})
export class ProcedimientosButtonModalComponent implements OnInit {
  socios: ImagenesSocios[];
  sociosGuardados: Array<{idSocio: number, socio: ImagenesSocios[]}> = [];
  imagenesDelSocio: ImagenesSocios[] = [];
  loadImage = false;
  contador = 0;

  constructor(public dialogRef: MatDialogRef<ImagenesEsquemasComponent>,
              private imagenesEsquemasService: ImagenesEsquemasService,
              private imagenesSociosService: ImagenesSociosService) { }

  ngOnInit() {
    this.getSocios();
    // this.getAreas();
  }
  getSocios() {
    this.imagenesSociosService.getAllImages().subscribe(data => {
      this.socios = data;
    });
  }
  getImageSocio(indexSocio): void {
    this.contador = 0;
    const idSocio = this.socios[indexSocio].idSocio;
    if (this.ifExistIdSocio(idSocio) === idSocio) {
      this.getArraySocio(idSocio);
    } else {
      this.getByIdSocio(idSocio);
    }
  }
  getByIdSocio(id: number) {
    this.loadImage = true;
    this.imagenesSociosService.getByIdImage(id).subscribe(socios => {
      this.sociosGuardados.push({idSocio: id, socio: socios});
    }, () => {}, () => {
      this.loadImage = false;
      this.getArraySocio(id);
    });
  }
  ifExistIdSocio(idSocio): number {
    try {
      return this.sociosGuardados.find(x => x.idSocio === idSocio).idSocio;
    } catch (e) {}
  }
  getArraySocio(idSocio): void {
    try {
      this.imagenesDelSocio = this.sociosGuardados.find(x => x.idSocio === idSocio).socio;
    } catch (e) {}
  }

  moverContador(incremento: boolean) {
    if (incremento) {
      this.contador++;
    } else {
      this.contador--;
    }
  }

  dismiss() {
    this.dialogRef.close();
  }
}
