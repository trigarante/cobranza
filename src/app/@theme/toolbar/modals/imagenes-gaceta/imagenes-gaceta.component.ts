import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ImagenesGacetaService} from '../../../../@core/data/services/rrhh/imagenes-gaceta.service';
import {VisualizacionesGacetaService} from '../../../../@core/data/services/rrhh/visualizaciones-gaceta.service';
import {NotificacionesService} from '../../../../@core/data/services/others/notificaciones.service';

@Component({
  selector: 'app-imagenes-gaceta',
  templateUrl: './imagenes-gaceta.component.html',
  styleUrls: ['./imagenes-gaceta.component.scss']
})
export class ImagenesGacetaComponent implements OnInit {
  imagenes: any;
  contador = 0;

  constructor(
    public dialogRef: MatDialogRef<ImagenesGacetaComponent>,
    public imagenesGacetaService: ImagenesGacetaService,
    public visualizacionesGacetaService: VisualizacionesGacetaService,
    @Inject(MAT_DIALOG_DATA) public visualizarGaceta: boolean,
    private notificaciones: NotificacionesService) { }

  ngOnInit() {
    this.getImagenes();
  }

  getImagenes() {
    this.imagenesGacetaService.getGacetaActual().subscribe(data => this.imagenes = data);
  }

  dismiss() {
    if (!this.visualizarGaceta) {
      this.dialogRef.close();
    } else {
      this.notificaciones.carga('Registrando que has visto la gaceta');

      this.visualizacionesGacetaService.post({
        idEmpleado: +sessionStorage.getItem('Empleado'),
        idGaceta: this.imagenes[0].idGaceta,
      }).subscribe({
        next: () => {
          this.notificaciones.exito('Se guardó la información correctamente')
          .then(() => {
            this.dialogRef.close();
          });
        },
        error: () => {
          this.notificaciones.error('Ocurrio un error, por favor vuelve a intentarlo');

        },
      });
    }
  }

  moverContador(incremento: boolean) {
    if (incremento) {
      this.contador++;
    } else {
      this.contador--;
    }
  }

  redireccionar() {
    if (this.imagenes[this.contador].link)
      window.open(this.imagenes[this.contador].link);
  }
}

