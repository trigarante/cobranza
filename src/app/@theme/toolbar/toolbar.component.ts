import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {MatDialog, } from '@angular/material/dialog';
import {MenuService} from '../../@core/data/services/menu.service';
import {AuthService} from '../../@core/AuthService';
import {TrigaranteEscuchaModalComponent} from './modals/trigarante-escucha-modal/trigarante-escucha-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {LlamadaEntranteComponent} from '../../modulos/telefonia/modals/llamada-entrante/llamada-entrante.component';
import {WebrtcService} from '../../@core/data/services/telefonia/webrtc.service';
import {Subscription, interval} from 'rxjs';
import {PeticioneAmiService} from '../../@core/data/services/telefonia/peticione-ami.service';
import {MotivoPausaComponent} from './modals/motivo-pausa/motivo-pausa.component';
import {ActualServerTelefoniaService} from '../../@core/data/services/telefonia/actual-server-telefonia.service';
import {ImagenesGacetaComponent} from './modals/imagenes-gaceta/imagenes-gaceta.component';
import {VisualizacionesGacetaService} from '../../@core/data/services/rrhh/visualizaciones-gaceta.service';
import {GacetasDelMesComponent} from './modals/gacetas-del-mes/gacetas-del-mes.component';
import {Usuario} from '../../models/usuario.model';
import {EstadoUsuarioService} from '../../@core/data/services/sesiones/estado-usuario.service';
import {SesionUsuarioService} from '../../@core/data/services/sesiones/sesion-usuario.service';
import {NotificacionesService} from '../../@core/data/services/others/notificaciones.service';
import {ProcedimientosButtonModalComponent} from './modals/procedimientos-button-modal/procedimientos-button-modal.component';
import {EstadoUsuarioTelefoniaService} from '../../@core/data/services/telefonia/estado-usuario-telefonia.service';
import {SnackPausaComponent} from '../../modulos/telefonia/modals/snack-pausa/snack-pausa.component';
import { MatSidenav } from '@angular/material/sidenav';
import { animate, style, transition, trigger, state } from '@angular/animations';
import {WhatsappMsgService} from '../../@core/data/services/whatsappMsg/whatsapp-msg.service';
import {ChatWhatsappComponent} from '../../modulos/cobranza/modals/chat-whatsapp/chat-whatsapp.component';
import {LlamadasOutService} from '../../@core/data/services/telefonia/llamadas-out.service';
import {LlamadaSalidaComponent} from '../../modulos/telefonia/modals/llamada-salida/llamada-salida.component';
import {SocketsService} from '../../@core/data/services/telefonia/sockets/sockets.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  animations: [
    trigger('openClose', [
      state('close', style({
        width: 0
      })),
      state('open', style({
        width: '*'
      })),
      transition('open <=> close', [
        animate('0.25s')
      ])
    ]),
    trigger('openCloseSub', [
      state('closeSub', style({
        width: 0
      })),
      state('openSub', style({
        width: '*',
      })),
      transition('openSub <=> closeSub', [
        animate('0.25s')
      ])
    ]),
    trigger('slideMenu', [
      transition(':enter', [
        style({opacity: 0}),
        animate('0.4s', style({opacity: 1}))
      ]),
    ]),
  ],
})
export class ToolbarComponent implements OnInit, OnDestroy {
  permisos = JSON.parse(window.localStorage.getItem('User'));
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('addStyle') addStyle: ElementRef;
  @ViewChild('mMenu') mMenu: ElementRef;
  efect: number;
  public menuOpen = false;
  public submenuOpen = false;
  public men: any[];
  public anio = new Date().getFullYear();
  public aparecer = true;
  private interval: any;
  public extension = localStorage.getItem('extension');
  public extStatus = '#F55C47';
  private pjsipEvents: Subscription;
  public conectado = false;
  public estados = [{id: 1, descripcion: 'EN LINEA'}, {id: 2, descripcion: 'EN PAUSA'}];
  public initialValue: number;
  public telefoniaServer: string;
  public visualizarGaceta: boolean;
  public usuario: Usuario =  new Usuario(localStorage.getItem('extension'), Number(sessionStorage.getItem('Usuario')), 1);
  subButtonActivatedID: number;
  buttonActivatedID: number;
  mandarPlantilla = false;
  color: string;
  mostrarMatMenu = false;
  cantidadNotificacion: number;
  cantidadNotificaciones: any;
  dataMensajesCanal: any[];
  cantidadMensajesPorCanal: any;
  public idPuesto = +sessionStorage.getItem('idPuesto');
  private chequeo = interval(60000);
  observableChequeo: Subscription;
  callbackring: any;
  public gen4Open = false;
  public gen5Open = false;
  public gen6Open = false;
  public gen7Open = false;
  public gen4Alias: string;
  public gen5Alias: string;
  public gen6Alias: string;
  public gen7Alias: string;
  public gen4ActivatedID: number;
  public gen5ActivatedID: number;
  public gen6ActivatedID: number;
  public gen7ActivatedID: number;
  public subMenuAlias: string;
  public menuAlias: string;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthService,
    private menu: MenuService,
    private snakBar: MatSnackBar,
    private webRTCService: WebrtcService,
    private amiService: PeticioneAmiService,
    private servidorTelefonia: ActualServerTelefoniaService,
    private estadoUsuarioService: EstadoUsuarioService,
    private sesionUsuarioService: SesionUsuarioService,
    private whatsMsgService: WhatsappMsgService,
    private render: Renderer2,
    private llamadaSalidaService: LlamadasOutService,
    private notificacionesService: NotificacionesService,
    private telefoniaUsuariosService: EstadoUsuarioTelefoniaService,
    private visualizacionesGacetaService: VisualizacionesGacetaService,
    private socketsService: SocketsService
  ) {
  }

  ngOnInit(): void {
    this.men = this.menu.getMenu();
    // declaración largo del arreglo de submodulos y contador de submodulos borrados
    let moduleLength = this.men.length,
      erasedModule = 0;
    // recorre el arreglo de modulos padre
    for (let i = 0; i < moduleLength; i++) {

      // si los objetos dentro del arreglo de submodulos tienen una propiedad llamada children
      if (this.men[i - erasedModule].children){
        // se inicializa el largo del arreglo children, y un contador de los children borrados
        let childrenLength = this.men[i - erasedModule].children.length,
          erasedChild = 0;

        // se recorre el arreglo children
        for (let y = 0; y < childrenLength; y++) {

          // se comprueba si los objetos dentro de children tienen una propiedad llamada grandChildren
          if (this.men[i - erasedModule].children[y - erasedChild].grandChildren){

            // se inicializa el largo del arreglo grandChildren, y su contador de borrados
            let granChildrenLength = this.men[i - erasedModule].children[y - erasedChild].grandChildren.length,
              erasedGrand = 0;

            // se recorre el arreglo grandChildren
            for (let z = 0; z < granChildrenLength; z++) {
              // se valida el permiso de los objetos en grandChildren, si se borra algun elemento del arreglo se incrementa erasedGrand
              if (!this.men[i - erasedModule].children[y - erasedChild].grandChildren[z - erasedGrand].permiso){
                this.men[i - erasedModule].children[y - erasedChild].grandChildren.splice(z - erasedGrand, 1);
                erasedGrand++;
              }

              // se valida que el arreglo grandChildren no este vacio, si lo esta se borra el children que lo contiene y se ingrementa erasedChild.
              if (!this.men[i - erasedModule].children[y - erasedChild].grandChildren.length){
                this.men[i - erasedModule].children.splice(y - erasedChild, 1);
                erasedChild++;
              }
            }
          }

          // se valida si el arrego children no este vacio, si lo esta se borra el objeto module que lo contiene y se incrementa erasedModule
          if (this.men[i - erasedModule].children.length && !this.men[i - erasedModule].children[y - erasedChild].permiso) {
            this.men[i - erasedModule].children.splice(y - erasedChild, 1);
            erasedChild++;
          }
          if (!this.men[i - erasedModule].children.length){
            this.men.splice(i - erasedModule, 1);
            erasedModule++;
          }
        }
      }
      // se valida los permisos de los objetos en el arreglo module
      if (!this.men[i - erasedModule].permiso) {
        this.men.splice(i - erasedModule, 1);
        erasedModule++;
      }
    }
    this.esucharTelefoniaEventos();
    this.getGaceta();
    // window.onbeforeunload = data => {
    //   this.abandonoPagina();
    // };
    // this.evitarRecargar();
    this.reloadPagina();
  }

  ngOnDestroy() {
    if (this.pjsipEvents) {
      this.pjsipEvents.unsubscribe();
    }
    if (this.observableChequeo) {
      this.observableChequeo.unsubscribe();
    }
  }
  abandonoPagina = async () => {
    this.telefoniaUsuariosService.usuarioInactivo(this.usuario.id).subscribe();
  }

  evitarRecargar() {
    /****** Estas lineas evitan que se recarge la pagina. Si estas desarrollando comentalas y al final descomentalas ***/
    window.addEventListener('keyup', disableF5);
    window.addEventListener('keydown', disableF5);
    window.addEventListener('beforeunload', disableRefersh);
    function disableF5(e) {
      if ((e.which || e.keyCode) === 116) { e.preventDefault(); }
    }
    function disableRefersh(e) {
      return e.returnValue = 'No recargues';
    }
    /*********************************************************************************************************************************/
  }

  reloadPagina() {
    setTimeout(() => {
      if (this.idPuesto === 8 && this.usuario.extension !== null) {
        this.conectarExtension();
      }
    }, 2000);
    // Si eres ejecutivo, tienes extension y no estas en llamada se recargara la pagina y se conectara la extensión automaticamente
    setInterval(() => {
      this.telefoniaUsuariosService.getEstadoUsuario(this.usuario.id).subscribe((data: number) => {
        if (this.idPuesto === 8 && this.usuario.extension !== null && data === 1) { location.reload(); }
      });
    }, (1000 * 60) * 15); // (1000 * 60) * 15
  }

  esucharTelefoniaEventos() {
    if (this.extension.length > 0 && this.extension !== 'null') {
      // this.whatsMetodos();
      this.servidorTelefonia.getCurrentServer().subscribe((resp) => {
        this.webRTCService.currentServer = resp;
        this.telefoniaServer = `https://${resp}:8089/httpstatus`;
      });
      this.evaluarEstadoU();
      if (this.idPuesto === 8) {this.llamadasCb(); }
      this.pjsipEvents = this.webRTCService.listenEvents().subscribe((resp) => {
        switch (resp.evento) {
          case 'connected':
          {
            this.conectado = true;
            this.extStatus = '#4AA96C';
            break;
          }
          case 'disconnected':
          {
            this.webRTCService.sipHangUp();
            this.socketsService.conectarExtension(4, 'desactivo');
            this.conectado = false;
            this.extStatus = '#F55C47';
            break;
          }
          case 'i_new_call':
            this.telefoniaUsuariosService.getEstadoUsuario(this.usuario.id).subscribe((data: number) => {
              this.telefoniaUsuariosService.enLlamada(this.usuario.id).subscribe();
              if (data  === 1) {
                this.amiService.getSolicitud(this.extension, resp.numero).subscribe((response) => {
                  this.amiService.respSolicitud.emit(response);
                });
                this.snakBar.openFromComponent(LlamadaEntranteComponent, {
                  data: {
                    numero: resp.numero
                  },
                });
              } else {
                this.webRTCService.stopRingTone();
                this.webRTCService.sipHangUp();
              }
            });
            break;
        }
      });
    }
  }
  llamadasCb() {
    this.observableChequeo = this.chequeo.subscribe(this.llamar);
  }

  llamar = () => {
    this.telefoniaUsuariosService.getEstadoUsuario(this.usuario.id).subscribe((data: number) => {
      if (data === 1 && this.conectado === true) {
        this.llamadaSalidaService.getLlamadasCb().subscribe((llamada: any) => {
          if (llamada.length > 0 ) {
            this.callbackring = document.getElementById('callbackring');
            this.callbackring.play();
            Swal.fire({
              title: 'Tienes una llamada callback',
              text: `Solicitud: ${llamada[0].idSolicitud}
              `,
              icon: 'info',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Atender llamada',
              allowEscapeKey: false,
              allowOutsideClick: false,
            }).then((result) => {
              if (result.value) {
                this.callbackring.pause();
                this.ejecutarLlamada(llamada);
              } else if ( result.dismiss === Swal.DismissReason.cancel) {
                this.callbackring.pause();
              }
            });
          }
        });
      } else {
        console.log('sin escuchar callbacks');
      }
    });
  }

  ejecutarLlamada(llamada) {
    this.snakBar.openFromComponent(LlamadaSalidaComponent, {
      data: {
        idSolicitud: llamada[0].idSolicitud,
        numero: llamada[0].numero,
        idLlamada: llamada[0].id,
      }
    });
  }

  whatsMetodos() {
    this.getDataCanal();
    // this.socket.emit(`headerRoom`, 4);
    // this.socket.on(`msg-noRespondido-${sessionStorage.getItem('Empleado')}`, (value: any) => {
    //   this.cantidadMensajesPorCanal = value.cantidadMensajesIndividuales;
    //   this.dataMensajesCanal = value.mensajes;
    //   this.cantidadNotificaciones = {cantidadNotificaciones: +value.cantidadNotificaciones, efect: 1};
    //   this.cantidadNotificacion = this.cantidadNotificaciones.cantidadNotificaciones;
    //   this.efect = this.cantidadNotificaciones.efect;
    //   setTimeout(() => {
    //     this.cantidadNotificaciones = {cantidadNotificaciones: +value.cantidadNotificaciones, efect: 0};
    //     this.efect = this.cantidadNotificaciones.efect;
    //   }, 3000);
    // });
  }

  evaluarEstadoU() {
    setTimeout(() => {
      this.telefoniaUsuariosService.getEstadoUsuario(this.usuario.id).subscribe((resp: number) => {
        this.usuario.estadoUsuario = resp;
        if (resp === 2) {
          this.snakBar.openFromComponent(SnackPausaComponent, {
            data: {
              idPausaUsuario: sessionStorage.getItem('pu'),
            }
          });
        }
      });
    }, 500);
  }
  async logOut() {
    if (this.menuOpen) {
      this.menuOpen = false;
    }
    const tkn = await Swal.fire({
      title: '¿Desea cerrar sesión?',
      text: '',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonColor: '#C33764',
      cancelButtonText: 'No',
      confirmButtonColor: '#1D2671',
      confirmButtonText: 'Si',
      allowOutsideClick: false,
      allowEscapeKey: false,
    });

    if (tkn.isConfirmed){
      this.socketsService.delogueo(localStorage.getItem('extension'));
      this.telefoniaUsuariosService.usuarioInactivo(this.usuario.id).subscribe();
      this.webRTCService.sipUnRegister();
      this.authService.logOut();
      // this.router.navigate(['login']);
    }
  }

  irInicio() {
    if (this.menuOpen) {
      this.menuOpen = false;
    }
    this.router.navigate(['/modulos/dashboard']);
  }

  trigaranteEscucha() {
    if (this.menuOpen) {
      this.menuOpen = false;
    }
    if (this.dialog.openDialogs.length === 0) {
      this.dialog.open(TrigaranteEscuchaModalComponent, {
        width: '500px',
      });
    }
  }

  conectarExtension() {
    try {
      this.telefoniaUsuariosService.getEstadoUsuario(this.usuario.id).subscribe((resp: number) => {
        if (resp === 4) {
          this.conexion();
        } else {
          let estado;
          switch (resp) {
            case 1: estado = 'Ya estas activo en otra pestaña.'; break; // ACTIVO
            case 2: estado = 'Estas en pausa en otra pestaña.'; break; // EN PAUSA
            case 3: estado = 'Estas en llamadaen otra pestaña por ahora no podras conectarte.'; break; // EN LLAMADA
            case 6: estado = 'Estas tipificando en otra pestaña. ¿Deseas conectarte aquí y cerrar la otra conexión?'; break; // TIPIFICANDO
          }
          this.notificacionesService.informacion(estado);
        }
      });
    } catch (e) {
      console.log('Algo tienen error');
    }
  }

  conexion() {
    this.socketsService.conectarExtension(1, 'activo');
    this.aparecer = false;
    this.usuario.estadoUsuario = 1;
    this.webRTCService.sipRegister(this.usuario.id);
    this.interval = setInterval(() => {
      this.aparecer = true;
      clearInterval(this.interval);
    }, 3000);
  }

  motivoPausa() {
    this.socketsService.conectarExtension(2, 'pausa');
    this.usuario.estadoUsuario = 2;
    this.dialog.open(MotivoPausaComponent, {
      data: this.usuario.id,
      width: '500px',
      disableClose: true,
    }).afterClosed().subscribe((resp) => {
      if (!resp.pauso) {
        this.usuario.estadoUsuario = 1;
      } else {
        this.snakBar.openFromComponent(SnackPausaComponent, {
          data: {
            idPausaUsuario: resp.idPausaUsuario,
            tiempo: resp.tiempo,
          }
        }).afterDismissed().subscribe(() => {
          this.usuario.estadoUsuario = 1;
        });
      }
    });
  }
  getGaceta() {
    this.visualizacionesGacetaService.getByIdEmpleado().subscribe({
      next: data => {
        if (!data) {
          this.visualizarGaceta = true;
          this.mostrarImagenes();
        }
      },
    });
  }


  mostrarImagenes() {
    this.dialog.open(ImagenesGacetaComponent, {
      disableClose: this.visualizarGaceta,
      data: this.visualizarGaceta,
    });
  }
  gacetasDelMes() {
    this.dialog.open(GacetasDelMesComponent, {
      panelClass: 'custom-dialog-Gaceta',
      height: '95%',
    });
  }

  procedimientosSteps(){
    this.dialog.open(ProcedimientosButtonModalComponent, {
      panelClass: 'custom-dialog-ProcedimientosModal'});
  }

  clickBtn() {
    this.mostrarMatMenu = true;
    this.getDataCanal();
    if (this.cantidadNotificacion !== 0) {
      this.render.setAttribute(this.addStyle.nativeElement, 'style', 'width: 280px !important;');
      this.render.setAttribute(this.mMenu.nativeElement, 'style', 'max-height: 235px !important; overflow-x: hidden;');
      // this.socket.emit('chatBtnHeader', 'chat');
    } else {
      // this.socket.emit('chatBtnHeader', 'sin-data');
      this.notificacionesService.advertencia('Sin mensajes pendientes');
    }
  }

  getDataCanal() {
    this.whatsMsgService.getMensajesCanalGeneral().subscribe({
      next: (value: any) => {
        this.cantidadMensajesPorCanal = value.cantidadMensajesIndividuales;
        this.dataMensajesCanal = value.mensajes;
        if (this.dataMensajesCanal.length === 0 && this.mostrarMatMenu) {
          document.getElementsByClassName('mat-menu-content')[0].setAttribute('hidden', 'true');
        } else {
          if (this.mostrarMatMenu) {
            document.getElementsByClassName('mat-menu-content')[0]
              .setAttribute('style', 'width: 280px; overflow: hidden;');
          }
        }
        this.cantidadNotificaciones = {cantidadNotificaciones: +value.cantidadNotificaciones, efect: 0};
        this.cantidadNotificacion = this.cantidadNotificaciones.cantidadNotificaciones;
        // this.efect = this.cantidadNotificaciones.efect;
      },
    });
  }

  /* Codigo para el funcionamiento del nuevo menu */
  //  función para cerrar el sidenav y todos los submenus abiertos
  closeSidenav(){
    this.sidenav.close();
    this.menuOpen = false;
    this.submenuOpen = false;
    this.gen4Open = false;
    this.gen5Open = false;
  }

  // función para abrir submenu hijo
  openMenu(index: number, alias: string): void{
    if (this.menuOpen === false){
      this.menuOpen = true;
    }
    else{
      if (index === this.buttonActivatedID){
        this.menuOpen = false;
      }
      else{
        this.menuOpen = true;
      }
      this.submenuOpen = false;
      this.gen4Open = false;
      this.gen5Open = false;
      this.gen6Open = false;
      this.gen7Open = false;
    }
    this.buttonActivatedID = index;
    this.menuAlias = alias;
  }

  // función para abrir submenu nieto
  openSubMenu(index: number, alias: string): void{
    if (this.submenuOpen === false){
      this.submenuOpen = true;
    }
    else{
      if (this.subButtonActivatedID === index){
        this.submenuOpen = false;
      }
      else{
        this.submenuOpen = true;
      }
      this.gen4Open = false;
      this.gen5Open = false;
      this.gen6Open = false;
      this.gen7Open = false;
    }
    this.subButtonActivatedID = index;
    this.subMenuAlias = alias;
  }
  openGen4(index: number, alias: string): void{
    if (this.gen4Open === false){
      this.gen4Open = true;
    }
    else{
      if (this.gen4ActivatedID === index){
        this.gen4Open = false;
      }
      else{
        this.gen4Open = true;
      }
      this.gen5Open = false;
      this.gen6Open = false;
      this.gen7Open = false;
    }
    this.gen4ActivatedID = index;
    this.gen4Alias = alias;
  }

  openGen5(index: number, alias: string): void{
    if (this.gen5Open === false){
      this.gen5Open = true;
    }
    else{
      if (this.gen5ActivatedID === index){
        this.gen5Open = false;
      }
      else{
        this.gen5Open = true;
      }
      this.gen6Open = false;
      this.gen7Open = false;
    }
    this.gen5ActivatedID = index;
    this.gen5Alias = alias;
  }
  openGen6(index: number, alias: string): void{
    if (this.gen6Open === false){
      this.gen6Open = true;
    }
    else{
      if (this.gen6ActivatedID === index){
        this.gen6Open = false;
      }
      else{
        this.gen6Open = true;
      }
      this.gen7Open = false;
    }
    this.gen6ActivatedID = index;
    this.gen6Alias = alias;
  }
  openGen7(index: number, alias: string): void{
    if (this.gen7Open === false){
      this.gen7Open = true;
    }
    else{
      if (this.gen7ActivatedID === index){
        this.gen7Open = false;
      }
      else{
        this.gen7Open = true;
      }
    }
    this.gen7ActivatedID = index;
    this.gen7Alias = alias;
  }

  aMensajesPendientes() {
    this.router.navigate(['/modulos/cobranza/whats-pendientes']);
  }

  iconoMatMenu(id) {
    // const data = this.solicitudesPorTelefono.filter(val => {
    //   return val.id === id;
    // });
    // this.socket.emit('chatBtnHeader', 'chat');
    this.notificacionesService.carga('Abriendo chat');
    this.whatsMsgService.getMensajesByIdCanal(id).subscribe({
      next: (value: any) => {
        const credentials = value.catalogoData;
        this.mandarPlantilla = true;
        this.modalChat(value.solicitudData, credentials, value);
      },
      error: err => {
        this.notificacionesService.error('Ocurrió  un error');
      },
    });
  }

  modalChat(data, credentials, dataCanal) {
    this.notificacionesService.exitoWhats();
    sessionStorage.setItem('idSolicitud', String(data.id));
    this.dialog.open(ChatWhatsappComponent, {
      width: '550px',
      height: '450px',
      data: {
        data,
        credentials,
        dataCanal
      }
      // panelClass: ['estilosModal', 'col-lg-8'],
      // data: {data: data, credentials: credentials},
    }).afterClosed().subscribe((value: any) => {
      // this.socket.emit(`solicitudesRoom`, 2);
      // // this.socket.emit(`tablaSolicitudesHeaderRoom`, 3);
      // if (data.notificacion !== false && data.notificacion === 1) {
      //   this.dataSource.data.map((dataMap, index) => {
      //     if (String(dataMap.id) === String(data.id)) {
      //       this.dataSource.data[index].notificacion = 0;
      //     }
      //   });
      // } else {
      //   if (!data.notificacion) {
      //     this.dataSource.data.map((dataMap, index) => {
      //       if (String(dataMap.id) === String(data.id)) {
      //         this.dataSource.data[index].notificacion = 0;
      //       }
      //     });
      //   }
      // }
    });
  }
}
