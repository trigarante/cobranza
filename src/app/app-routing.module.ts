import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Error404Component} from './modulos/error404/error404.component';
import {LoaderComponent} from './loader/loader.component';
import {LoginComponent} from './@theme/login/login.component';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {
        path: 'modulos',
        loadChildren: () => import('./modulos/modulos.module').then(m => m.ModulosModule),
    },
    {path: 'login', component: LoginComponent},
    {path: 'loader', component: LoaderComponent},
    {path: '**', component: Error404Component},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
